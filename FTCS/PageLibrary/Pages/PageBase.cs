﻿
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace PageLibrary.Pages
{
    public abstract class PageBase
    {
        public IWebDriver Driver { get; set; }
        public string BaseURL { get; set; }

        public static PageBase LoadHomePage(IWebDriver driver, object dEFAULT_HOMEPAGE)
        {
            throw new NotImplementedException();
        }

        public virtual string DefaultTitle { get { return ""; } }

        protected TPage GetInstance<TPage>(IWebDriver driver = null, string expectedTitle = "") where TPage : PageBase, new()
        {
            return GetInstance<TPage>(driver ?? Driver, BaseURL, expectedTitle);
        }

        protected static TPage GetInstance<TPage>(IWebDriver driver, string baseUrl, string expectedTitle = "") where TPage : PageBase, new()
        {
            TPage pageInstance = new TPage()
            {
                Driver = driver,
                BaseURL = baseUrl
            };
            PageFactory.InitElements(driver, pageInstance);

            if (string.IsNullOrWhiteSpace(expectedTitle)) expectedTitle = pageInstance.DefaultTitle;

            //wait up to 5s for an actual page title since Selenium no longer waits for page to load after 2.21
            new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(10))
                                            .Until((d) => {
                                                return d.FindElement(By.ClassName("body-content"));
                                            });

            AssertIsEqual(expectedTitle, driver.Title, "Page Title");

            return pageInstance;
        }

        /// <summary>
        /// Asserts that the current page is of the given type
        /// </summary>
        public void Is<TPage>() where TPage : PageBase, new()
        {
            if (!(this is TPage))
            {
                throw new AssertionException(string.Format("Page Type Mismatch: Current page is not a '{0}'", typeof(TPage).Name));
            }
        }

        /// <summary>
        /// Inline cast to the given page type
        /// </summary>
        public TPage As<TPage>() where TPage : PageBase, new()
        {
            return (TPage)this;
        }

        public void ClearAndType(IWebElement element, string value)
        {
            element.Clear();
            element.SendKeys(value);
        }

        public void WaitUpTo(int milliseconds, Func<bool> Condition, string description)
        {
            
            int timeElapsed = 0;
            while (!Condition() && timeElapsed < milliseconds)
            {
                Thread.Sleep(100);
                timeElapsed += 100;
            }

            if (timeElapsed >= milliseconds || !Condition())
            {
                throw new AssertionException("Timed out while waiting for: " + description);
            }
        }

       

        public static void AssertIsEqual(string expectedValue, string actualValue, string elementDescription)
        {
            if (expectedValue != actualValue)
            {
                throw new AssertionException(String.Format("AssertIsEqual Failed: '{0}' didn't match expectations. Expected [{1}], Actual [{2}]", elementDescription, expectedValue, actualValue));
            }
        }

        public static bool IsElementPresent(IWebElement element)
        {
            try
            {
                bool b = element.Displayed;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void AssertElementPresent(IWebElement element, string elementDescription)
        {
            if (!IsElementPresent(element))
                throw new AssertionException(String.Format("AssertElementPresent Failed: Could not find '{0}'", elementDescription));
        }

        public static bool IsElementPresent(ISearchContext context, By searchBy)
        {
            try
            {
                bool b = context.FindElement(searchBy).Displayed;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void AssertElementPresent(ISearchContext context, By searchBy, string elementDescription)
        {
            if (!IsElementPresent(context, searchBy))
                throw new AssertionException(String.Format("AssertElementPresent Failed: Could not find '{0}'", elementDescription));
        }

        public static void AssertElementsPresent(IWebElement[] elements, string elementDescription)
        {
            if (elements.Length == 0)
                throw new AssertionException(String.Format("AssertElementsPresent Failed: Could not find '{0}'", elementDescription));
        }

        public static void AssertElementText(IWebElement element, string expectedValue, string elementDescription)
        {
            AssertElementPresent(element, elementDescription);
            string actualtext = element.Text;
            if (actualtext != expectedValue)
            {
                throw new AssertionException(String.Format("AssertElementText Failed: Value for '{0}' did not match expectations. Expected: [{1}], Actual: [{2}]", elementDescription, expectedValue, actualtext));
            }
        }

        public static HomePage LoadHomePage(IWebDriver driver, string homepage)
        {
            driver.Navigate().GoToUrl(homepage);
            return GetInstance<HomePage>(driver, homepage, "Home");
        }

        //inspiration: https://watirmelon.com/2015/05/14/waiting-for-ajax-calls-in-webdriver-c/
        public bool WaitForAjax()
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            //We need to wait until all jQuery processes e.g. Ajax are inactive then proceed with tests. See Link above 
            return wait.Until(d =>  {return (bool)(d as IJavaScriptExecutor).ExecuteScript("return jQuery.active == 0"); });
        }

    }
}