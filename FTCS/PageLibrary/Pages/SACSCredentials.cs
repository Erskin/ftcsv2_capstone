﻿using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageLibrary.Pages
{
    public class SACSCredentials:PageBase
    {
        [FindsBy( How = How.CssSelector, Using ="h2")]
        public IWebElement SACSCredentialsGuidelines {
            get; set;}
        
    }
}
