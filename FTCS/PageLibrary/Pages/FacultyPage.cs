﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageLibrary.Pages
{
    public class FacultyPage:PageBase
    {
        [FindsBy(How = How.CssSelector, Using = "tbody tr td:last-of-type")]
        public IList<IWebElement> DepartmentColumnValues { get; set; }
    }
}
