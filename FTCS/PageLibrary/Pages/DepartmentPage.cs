﻿
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

namespace PageLibrary.Pages
{
    public class DepartmentPage : PageBase
    {
        [FindsBy(How = How.Id, Using = "departments")]
        public IWebElement DepartmentSection { get; set; }

        [FindsBy(How = How.CssSelector, Using = "div.loader")]
        public IWebElement LoadingAnimation { get; set; }

        [FindsBy(How = How.CssSelector, Using = "tbody tr:first-child td:first-child")]
        public IWebElement FirstDepartmentNameInList{get;set;}

        [FindsBy(How = How.CssSelector, Using = "td a[href=\"/Faculty/All/Accounting\"]")]
        public IWebElement AccountingDepartmentLink { get; set; }

        [FindsBy(How = How.Name, Using = "searchTerm")]
        public IWebElement SearchBox { get; set; }

        public FacultyPage ClickDepartmentAccounting(IWebDriver driver)
        {
            AccountingDepartmentLink.Click();
            return GetInstance<FacultyPage>(driver, "Faculty");

        }

        [FindsBy(How = How.Id, Using = "filter-search")]
        public IWebElement SearchButton { get; set; }
       
        public void ClickLetterFilter(IWebDriver driver, string p0)
        {
            var LetterWebElement = GetLetterFilter(driver, p0);
            LetterWebElement.Click();
        }

        private IWebElement GetLetterFilter(IWebDriver driver, string letter)
        {
            IWebElement element = null;
            var selector = string.Format("div#alpha-filter div input[value={0}]", letter);
            try
            {
               element = driver.FindElement(By.CssSelector(selector));
                return element;
            }
            catch (NoSuchElementException)
            {

                throw new AssertionException(string.Format("element cannot be found with css selector {0} on {1}",selector, GetType().ToString())) ;
            }
            
        }

        public void EnterTextInSearchBox(string searchTerm)
        {
            SearchBox.Clear();
            SearchBox.SendKeys(searchTerm);
        }
    }
}
