﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageLibrary.Pages
{
    public class HomePage : PageBase
    {
        [FindsBy(How = How.ClassName, Using = "body-content")]
        public IWebElement Navbar { get; set; }

        [FindsBy(How =How.CssSelector, Using = "a.ftcs-dropdown-a.search")]
        public IWebElement SearchFacultyDropdown { get; set; }
        [FindsBy(How = How.CssSelector, Using = "#ftcs-dropdown-options li a[href='/Departments']")]
        public IWebElement DepartmentLink { get; set; }
        [FindsBy(How = How.CssSelector, Using = "html body div div div.row a.ftcs-a.col-lg-2.ftcs-offset")]
        public IWebElement GuidelinesForGraduateFacultyLink { get; set; }



        public DepartmentPage NavigateToDepartmentList(IWebDriver driver)
        {
            SearchFacultyDropdown.Click();
            DepartmentLink.Click();
            return GetInstance<DepartmentPage>(driver,"/Departments", "Departments");
        }
    }
}
