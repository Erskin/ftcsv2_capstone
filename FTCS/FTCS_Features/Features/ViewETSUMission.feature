﻿Feature: ViewETSUMission
	In order to ETSU's mission statement
	As an FTCS general user
	I want to view ETSU's mission statement

@ViewETSUMission
Scenario:  View ETSU’s mission
	Given I am on the home page
	When I click the Mission Command
	Then the ETSU mission page is displayed
