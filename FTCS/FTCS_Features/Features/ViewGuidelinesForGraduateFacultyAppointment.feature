﻿Feature: ViewGuidelinesForGraduateFacultyAppointment
	In order to let users know the Guidelines for Graduate Faculty Appointment
	As an FTCS general user
	I want to view the Guidlines for Graduate Faculty Appointment

@ViewGuidelinesForGraduateFacultyAppointment
Scenario: View the Guidelines for Graduate Faculty Appointment
	Given I am on the home page
	When I click the “View Guidelines For Graduate Faculty Appointment” link
	Then the Guidelines for Graduate Faculty appoint are displayed
