﻿Feature: ViewCourseSectionQualificationPercentageReport
	In order to see if a course section has the qualifications needed
	As an FTCS general user
	I want to view the Course Section Qualification Percentage Report

@ViewCourseSectionQualificationPercentageReport
Scenario: I need to view the Course Section Qualification Percentage Report page
	Given I am on the FTCS home page looking for a report
	When I select the  Course Section Qualification Percentage Report dropdown option
	Then I can view the Course Section Qualifications Percentage Report page

@ViewCourseSectionQualificationPercentageReport
Scenario: I need to view the Graduate Course Section Qualification Percentage Report
	Given I am on the Course Section Qualification Percentage Report page
	When I click on the Graduate tab
	Then I can view the Department in the graduate table
	And I can see the Total Course Section in the second column in the graduate table
	#And I can see the Total Course Sections Percentage in the third column in the graduate table
	And I can see the Number Academically Qualified in the fourth column in the graduate table
	#And I can see the Number Academically Qualified Percentage in the fifth column in the graduate table
	And I can see the Number Professionally Qualified in the sixth column in the graduate table
	#And I can see the Number Professionally Qualified Percentage in the seventh column in the graduate table
	And I can see the Number Graduate Assistants in the eighth column in the graduate table
	#And I can see the Number Graduate Assistants Percentage in the ninth column in the graduate table
	And I can see the Missing Transcripts in the tenth column in the graduate table
	#And I can see the Missing Transcript Percentage in the eleventh column in the graduate table

@ViewCourseSectionQualificationPercentageReport
Scenario: I need to view the Undergraduate Course Section Qualification Percentage Report
	Given I am on the Course Section Qualification Percentage Report page
	Then I can view the Department in the undergrad table
	And I can see the Total Course Section in the second column in the undergraduate table
	#And I can see the Total Course Sections Percentage in the third column in the undergraduate table
	And I can see the Number Academically Qualified in the fourth column in the undergraduate table
	#And I can see the Number Academically Qualified Percentage in the fifth column in the undergraduate table
	And I can see the Number Professionally Qualified in the sixth column in the undergraduate table
	#And I can see the Number Professionally Qualified Percentage in the seventh column in the undergraduate table
	And I can see the Number Graduate Assistants in the eighth column in the undergraduate table
	#And I can see the Number Graduate Assistants Percentage in the nineth column in the undergraduate table
	And I can see the Missing Transcripts in the tenth column in the undergraduate table
	#And I can see the Missing Transcript Percentage in the eleventh column in the undergraduate table

@ViewCourseSectionQualificationPercentageReport
Scenario: I can filter the Course Section Qualification Percentage Report by academic start year
	Given I am on the Course Section Qualification Percentage Report page
	When I click the on a start year
	And I click the 2015 button
	Then I can view the report with the 2015 start year