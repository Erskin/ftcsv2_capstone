﻿Feature: View a list of courses taught by a faculty
	In order to learn about the courses taught by a faculty
	As a general user
	I want to be able to view all of the faculty member's courses taught


@CoursesTaughtByAFaculty
Scenario: Display current courses taught table
	Given I am on Gregory Love faculty profile page
	When I click on the option called Courses Taught
	Then I can see the Courses Taught Title is Courses Taught: Fall 2016 - Summer 2017
	Then The Course ID is CHEM1030
	And The Course Title is Intro Chemistry Surv
	And The Course Description is This course, designed for the non-science major,
	And The Course Semester is Spring 2017
	And The Course Certification is Professional

@CoursesTaughtByAFaculty
Scenario: Display a link to show all course taught by a faculty
	Given I am on Gregory Love faculty profile page
	When I click on the option called Courses Taught
	Then I can view a button that displays a list of All Courses Taught By Faculty

@CoursesTaughtByAFaculty
Scenario: Display a list of all courses taught by a faculty
	Given I am on Gregory Love faculty profile page
	When I click on the option called Courses Taught
	And I click on the All Courses Taught By Faculty button
	Then The Course Id CHEM1030 is in the displayed list
	And The Course Title Intro Chemistry Surv is in the displayed list
	And The Course Term Spring 2017 is in the displayed list
	And The Course Certification Professional is in the displayed list