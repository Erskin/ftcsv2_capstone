﻿Feature: ViewAllDepartments
	In order to select a department
	As an FTCS general user
	I want to view all departments 

Background:
		

@ViewAllDepartments
Scenario: view a list of departments that start with B
	Given I am viewing a list of all departments
	When I click the letter B
	Then the first department name on the list starts with a B

@ViewAllDepartments
Scenario: view a list of departments contains a searchTerm
	Given I am viewing a list of all departments
	When I have entered the text studies as the searchTerm in the search box
	Then the first department name on the list contains the text studies

@ViewAllDepartments
Scenario: view a list of departments containing a searchTerm and starts with C
	Given I am viewing a list of all departments
	When I click the letter C
	And I have waited for the page to load
	 And I have entered the text studies as the searchTerm in the search box
	Then The first department name should contain studies and should start with C

@ViewAllDepartments
Scenario: Sort departments in descending order
	Given I am viewing a list of all departments
	When I click the Department Header
	And I have waited for the page to load
	Then the first department listed is University Honors

@ViewAllDepartments
Scenario: Default Sort is on department name column
	Given I am viewing a list of all departments
	Then the first department listed is Academic Affairs


@ViewAllDepartments
Scenario: Sorting the department list on the number of faculty column in ascending order
	Given I am viewing the list of all departments
	When I click the number of faculty column name
	Then the department list is sorted in ascending order by the number of faculty

	 