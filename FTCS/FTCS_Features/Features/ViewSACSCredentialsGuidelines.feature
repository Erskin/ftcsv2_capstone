﻿Feature: ViewSACSCredentialsGuidelines
	In order to know the SACS Credentials Guidelines
	As an FTCS general user
	I want to view the SACS Credential Guidelines

@mytag
Scenario: View SACS Credentials Guidelines
	Given I am on the home page
	Then the SACS Credential Guidelines button links to a SACS PDF