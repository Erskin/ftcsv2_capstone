﻿Feature: ViewTeachingCredentailStandards
	In order to know if teaching credentail standards are being met 
	As an FTCS general user
	I want to view the Teaching Credential Standards 

@ViewTeachingCredentailStandards
Scenario: View ETSU’s Teaching Credential Standards
	Given I am on the home page
	And there is a link to displaying text that matches Teaching Credential Standards
	When I click on the Teaching Credential Standards link
	Then I will be presented with a  outlining the Credential Standards
