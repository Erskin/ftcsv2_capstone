﻿Feature: ViewHomePage
	In order to access FTCS 
	As an FTCS general user 
	I want to view FTCS's home page

@HomePage
Scenario: View the home page 
	Given I have a browser window open
	When I navigate to the FTCS web application
	Then Welcome is displayed 

@HomePage
Scenario: Link to the ETSU Mission page
	Given I am on the FTCS home page
	Then The ETSU mission link is displayed and has a target URL http://www.etsu.edu/president/mission.aspx

@HomePage
Scenario: Link to the Guidelines For Graduate Faculty page
	Given I am on the FTCS home page
	Then The Graduate Faculty Appointment Guidelines link is displayed and has a target URL http://localhost:8080/Documents/gradfacproc212.pdf

@HomePage
Scenario: Link to the Teaching Credential Standards
	Given I am on the FTCS home page
	Then The Teaching Credential Standards link is displayed and has a target URL http://localhost:8080/Documents/TeachingCredentialStandards.pdf

@HomePage
Scenario: Link to the SAC Guidelines
	Given I am on the FTCS home page
	Then The SACS Credentials Guidelines link is displayed with a target URL http://localhost:8080/Documents/SACS.pdf

@HomePage
Scenario: Link to the Chair Instructions
	Given I am on the FTCS home page 
	Then The Chair Instructions link is displayed as a Chair Instructions button
	
