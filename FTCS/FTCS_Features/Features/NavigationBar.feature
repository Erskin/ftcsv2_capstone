﻿Feature: NavigationBar
	In order to find credential information
	As a FTCS general user
	I want to navigate the FTCS site

@NavigationBar
Scenario: Go to Course Section Qualifications Percentage Report
	Given I can view the navigation bar
	When I hover on the Reports dropdown option
	And I select the Course Section Qualifications Percentage Report option
	Then I am on the Course Section Qualifications Percentage Report page

@NavigationBar
Scenario: Go to Faculty Credentials Report
	Given I can view the navigation bar
	When I hover on the Reports dropdown option
	And I select the Faculty Credentials Report option
	Then I am on the Faculty Credentials Report page

@NavigationBar
Scenario: Go to Faculty Roster Report
	Given I can view the navigation bar
	When I hover on the Reports dropdown option
	And I select the Faculty Roster Report option
	Then I am on the Faculty Roster Report page

@NavigationBar
Scenario: Go to the Graduate Assistant Report
	Given I can view the navigation bar
	When I hover on the Reports dropdown option
	And I select the Graduate Assistant Roster Report option
	Then I am on the Graduate Assistant Roster Report page

@NavigationBar
Scenario: Go to Home page
	Given I can view the navigation bar
	When I click on Home
	Then I am taken to the Welcome page

@NavigationBar
Scenario: Go to list of all departments from navigation bar
	Given I can view the navigation bar
	When I hover over the Search For Faculty dropdown
	And I select the By Department option
	Then I can view the List of Departments title

@NavigationBar
Scenario: Go to list of all faculty from navigation bar
	Given I can view the navigation bar
	When I hover over the Search For Faculty dropdown
	And I select the By Name option
	Then I can view the List of Faculty title

@NavigationBar
Scenario: Go to list of all courses from navigation bar
	Given I can view the navigation bar
	When I hover over the Search For Faculty dropdown
	And I select the By Course option
	Then I can view the List of Courses title
@NavigationBar
Scenario: Go to Contact Us page
	Given I can view the navigation bar
	When I select the ContactUs option
	Then I can see the Contact Us option on the navigation bar