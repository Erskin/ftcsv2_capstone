﻿Feature: ViewFacultyCredentialsReport
	In order see a faculty members credentials 
	As an FTCS general user
	I want to view the Faculty Credentials Report

Background: 
	Given I am viewing the Faculty Credentials Report

@ViewFacultyCredentialsReport
Scenario: a credential is unspecified 
	Then its background color is red 

@ViewFacultyCredentialsReport
Scenario: A user selects a different academic year to find how a teacher is qualified
	When I select Fall 2012 - Summer 2013 from the semester menu
	And I click the submit button
	Then I find out how Honeycutt, Natalie M (P) is qualified for ETSU-1350-016 Health Prof Exploration Sem


@ViewFacultyCredentialsReport
Scenario: A user selects a different department to find how a teacher is qualified  
	When I select Biological Sciences from the credentials department menu
	And I click the submit button
	Then I find out how Alsop, Fred J (F) is qualified for BIOL-5037-001 Coastal Biology Fld Trip

@ViewFacultyCredentialsReport
Scenario: A user uses report to find out if a teacher is qualified 
	Then I find out how Anderson, Joanna Marie (F) is qualified for ETSU-1020-006 Foundations of Student Success


@ViewFacultyCredentialsReport
Scenario: the convert to pdf button is click
	When I select the convert to pdf option
	Then a new page is opened with the report as a pdf


