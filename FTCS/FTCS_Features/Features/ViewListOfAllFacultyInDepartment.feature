﻿Feature: ViewListOfAllFacultyInDepartment
	In order to know the faculty of a selected department
	As an FTCS general user
	I want to view a list of all faculty in department


@ViewListOfAllFacultyInDepartment
Scenario: View the list of all faculty in department
   Given I am viewing the list of all departments
	When I click on the department name Accounting link
	Then I see a list of all faculty teaching in Accounting
	
