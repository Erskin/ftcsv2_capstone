﻿Feature: ViewGraduateAssistantReport
	In order to see active Graduate Assistants
	As an FTCS general user
	I want to view the Graduate Assistant roster Report

@ViewGraduateAssistantReport
Scenario: View the Graduate Assistant Roster Report
	Given I am on FTCS home page looking for a report 
	When I select the Graduate Assistant Roster Report dropdown button
	Then I am navigated toGraduate Assistant Roster Report page
	
@ViewGraduateAssistantReport
Scenario: View graduate report for the academic year 2016
	Given I am viewing the graduate assistant roster report
	When I select the year 2016
	Then I can see Risner, Maria (GA) as a faculty
		And Art & Design as corresponding department
		And Bachelor of Arts (Art) Morehead State University as academic degrees
		And ARTA 1204 Core: Color and Practice as one of the courses taught
		And None Required as professional qualifications

@ViewGraduateAssistantReport
Scenario: View the graduate assistant report for another academic year
	Given I am viewing the graduate assistant roster report
	When I select the year 2015
		And I click submit button
	Then I can see Brown, Jordan (GA) as a faculty
		And Computing as corresponding department
		And Bachelor of Science (Computer Science) Furman University as academic degrees
		And CSCI 1100 Using Information Tech as one of the courses taught
		And None Required as professional qualifications
	