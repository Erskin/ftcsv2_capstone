﻿Feature: View faculty profile
	In order to learn about the overall profile of a faculty
	As a general user
	I want to be able to view the profile information of the faculty

@facultyProfile
Scenario: View Academic Qualifications of a faculty
	Given I am on faculty profile page of Bridges, Laken Grace
	When I click on Academic Qualifications tab
	Then I can see Rank: as Non Standard Rank
		And I can see Date of Rank: as None
		And I can see Date Eligible for Next Rank: as None
		And I can see Graduate Faculty Status: as No status
		And I can see Employment Status: as Part Time
		And I can see Employment Category: as Adjunct Faculty 4-month
		And I can see Years at ETSU: as 0
		And I can see Tenure Status: as Ineligible
		And I can see Degree as Master of Fine Arts
		And I can see Initials as MFA
		And I can see Discipline as Visual Arts
		And I can see Institution as Clemson University
		And I can see Transcript On File as Yes

@facultyProfile
Scenario: View Professional qualifications of a faculty
     Given I am on faculty profile page of Love, Gregory Ray
	 When I click on Professional Qualifications tab
	 Then I can see Description of faculty's professional qualification under Narrative
		And I can see Related Work/Professional Experience header 
		And I can see Place as Abbott Laboratories
		And I can see position as Research Investigator
		And I can see Responsibilities as Carried out duties described in the Narrative statement.
		And Date Started as August 1996
		And Date Ended as June 2006
		 
@facultyProfile
Scenario: View Courses taught by a faculty
      Given I am on faculty profile page of Barrett, Martin L
	  When I click on Courses Taught tab
	  Then I can see Current Courses Taught 2016-2017 as header
		And I can see Course ID as CSCI3350 
		And I can see Title as Software Engineering II
		And I can see Description as Prerequisite: CSCI 3250. Software development as an engineering discipline with emphasis on detailed design, implementation, testing, maintenance, project management, verification and validation, configuration management, and software quality assurance. Communications (written and oral), legal, professional, ethical issues, participation on team projects, and use of automated tools are integral.
		And I can see Semester as Spring 2017
		And I can see Certification as Academic
		And I can see (View Course History) link
		And I can see View All Courses Taught By Faculty tab at the bottom of the page


@facultyProfile
Scenario: View Coordinator Information of a faculty
	  Given I am on faculty profile page of Pittarese, Anthony D. 
	  When I click on Coordainator Information
	  Then I can see Degree Name as Computer & Info Science-MS
	  Then I can see Level as Masters Degree Program
	  Then I can see Degree as MS
	  Then I can see How Qualified as Unspecified
	  Then I can see Date Started as Summer 2016
	  And I can see	Date Ended as NA