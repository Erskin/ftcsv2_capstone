﻿Feature: Academic qualifications for a faculty
	In order to learn the academic qualification of a faculty
	As a general user
	I want to be able to view the academic qualifications of a faculty

@FacultyProfile
Scenario: Display the academic qualifications
	Given I am on Gregory Love faculty profile page
	When I click the Academic Qualifications option
	Then I see the degree is a Doctor of Philosophy
	And the initials for the degree is PHD
	And the discipline is Chemistry
	And the institution is Iowa State University
	And the transcript on file is Yes

@FacultyProfile
Scenario: Display faculty informtaion
	Given I am on Gregory Love faculty profile page
	When I click the Academic Qualifications option
	Then I see the faculty name is Love, Gregory Ray
	And the faculty title is Lecturer
	And the faculty email is lovegr@etsu.edu

@FacultyProfile
Scenario: Display faculty employment information
	Given I am on Gregory Love faculty profile page
	When I click the Academic Qualifications option
	Then I see the faculty rank is Lecturer
	And the faculty date of rank is August 2013
	And the faculty date eligible for next rank is August 2018
	And the faculty graduate faculty status is No status
	And the faculty employment status is Full Time
	And the faculty employment category is Faculty Academic
	And the faculty years at ETSU is 0
	And the faculty tenure status is Ineligible