﻿Feature: ViewCoordinatorInformation
	In order to view information about a coordinator 
	As a general user
	I want to be able to view the coordinator information

@CoordinatorInformationFeature
Scenario: Display the Coordinator's Information
Given I am on the Barrett, Martin L. profile page
When I click on the option named Coordinator Information
Then I can see Applied Human Sciences-BS as the degree name
And Baccalaureate Degree Program as the level
And BS as the degree
And Academic as how qualified
And January 2017 as the date started
And January 2017 as the date ended