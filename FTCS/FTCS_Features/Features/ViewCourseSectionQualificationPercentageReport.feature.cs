﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.0.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace FTCS_Features.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("ViewCourseSectionQualificationPercentageReport")]
    public partial class ViewCourseSectionQualificationPercentageReportFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "ViewCourseSectionQualificationPercentageReport.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "ViewCourseSectionQualificationPercentageReport", "\tIn order to see if a course section has the qualifications needed\r\n\tAs an FTCS g" +
                    "eneral user\r\n\tI want to view the Course Section Qualification Percentage Report", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("I need to view the Course Section Qualification Percentage Report page")]
        [NUnit.Framework.CategoryAttribute("ViewCourseSectionQualificationPercentageReport")]
        public virtual void INeedToViewTheCourseSectionQualificationPercentageReportPage()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("I need to view the Course Section Qualification Percentage Report page", new string[] {
                        "ViewCourseSectionQualificationPercentageReport"});
#line 7
this.ScenarioSetup(scenarioInfo);
#line 8
 testRunner.Given("I am on the FTCS home page looking for a report", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 9
 testRunner.When("I select the  Course Section Qualification Percentage Report dropdown option", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 10
 testRunner.Then("I can view the Course Section Qualifications Percentage Report page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("I need to view the Graduate Course Section Qualification Percentage Report")]
        [NUnit.Framework.CategoryAttribute("ViewCourseSectionQualificationPercentageReport")]
        public virtual void INeedToViewTheGraduateCourseSectionQualificationPercentageReport()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("I need to view the Graduate Course Section Qualification Percentage Report", new string[] {
                        "ViewCourseSectionQualificationPercentageReport"});
#line 13
this.ScenarioSetup(scenarioInfo);
#line 14
 testRunner.Given("I am on the Course Section Qualification Percentage Report page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 15
 testRunner.When("I click on the Graduate tab", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 16
 testRunner.Then("I can view the Department in the graduate table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 17
 testRunner.And("I can see the Total Course Section in the second column in the graduate table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 19
 testRunner.And("I can see the Number Academically Qualified in the fourth column in the graduate " +
                    "table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 21
 testRunner.And("I can see the Number Professionally Qualified in the sixth column in the graduate" +
                    " table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 23
 testRunner.And("I can see the Number Graduate Assistants in the eighth column in the graduate tab" +
                    "le", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
 testRunner.And("I can see the Missing Transcripts in the tenth column in the graduate table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("I need to view the Undergraduate Course Section Qualification Percentage Report")]
        [NUnit.Framework.CategoryAttribute("ViewCourseSectionQualificationPercentageReport")]
        public virtual void INeedToViewTheUndergraduateCourseSectionQualificationPercentageReport()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("I need to view the Undergraduate Course Section Qualification Percentage Report", new string[] {
                        "ViewCourseSectionQualificationPercentageReport"});
#line 29
this.ScenarioSetup(scenarioInfo);
#line 30
 testRunner.Given("I am on the Course Section Qualification Percentage Report page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 31
 testRunner.Then("I can view the Department in the undergrad table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 32
 testRunner.And("I can see the Total Course Section in the second column in the undergraduate tabl" +
                    "e", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 34
 testRunner.And("I can see the Number Academically Qualified in the fourth column in the undergrad" +
                    "uate table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 36
 testRunner.And("I can see the Number Professionally Qualified in the sixth column in the undergra" +
                    "duate table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 38
 testRunner.And("I can see the Number Graduate Assistants in the eighth column in the undergraduat" +
                    "e table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 40
 testRunner.And("I can see the Missing Transcripts in the tenth column in the undergraduate table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("I can filter the Course Section Qualification Percentage Report by academic start" +
            " year")]
        [NUnit.Framework.CategoryAttribute("ViewCourseSectionQualificationPercentageReport")]
        public virtual void ICanFilterTheCourseSectionQualificationPercentageReportByAcademicStartYear()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("I can filter the Course Section Qualification Percentage Report by academic start" +
                    " year", new string[] {
                        "ViewCourseSectionQualificationPercentageReport"});
#line 44
this.ScenarioSetup(scenarioInfo);
#line 45
 testRunner.Given("I am on the Course Section Qualification Percentage Report page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 46
 testRunner.When("I click the on a start year", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 47
 testRunner.And("I click the 2015 button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 48
 testRunner.Then("I can view the report with the 2015 start year", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
