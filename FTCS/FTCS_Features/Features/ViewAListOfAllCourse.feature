﻿Feature: ViewAListOfAllCourse
	In order to select a course
	As an FTCS general user
	I want to view all courses 

Background:
	Given I am viewing a list of all courses

@ViewAListOfAllCourses_AcceptanceTests
Scenario: A user clicks the a header for sorting and navigates to a course
	When I click the Course Name header
	Then the course XML is found

@ViewAListOfAllCourses_AcceptanceTests
Scenario: A user selects a letter then navigates to a course
	When I click on the letter C
	Then the course Cancer Epidemiology is found 

@ViewAListOfAllCourses_AcceptanceTests
Scenario: A user types a search term into the search text box and navigates to a course 
	When I have entered the searchTerm intro in the search box
	Then the course Intro Chemistry Surv is found 




