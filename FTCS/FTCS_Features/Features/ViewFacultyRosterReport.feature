﻿Feature: ViewFacultyRosterReport
	In order to view what course a faculty member is teaching 
	As an FTCS general user
	I want to view the faculty roster report

@ViewFacultyRosterReport
Scenario: A user selects a different academic year to find the course taught by a teacher
    Given I am on Faculty Roster Report page
	When I select 2015 from the academic year menu
	And I click the submit button
	Then I find out Allen, Jacob (P) has taught the course ETSU 1020 Foundations of Student Success


@ViewFacultyRosterReport
Scenario: A user selects a different department to find the academic degrees of a teacher
    Given I am on Faculty Roster Report page
	When I select Curriculum & Instruction from the credentials department menu
	And I click the submit button
	Then I find out Baker, Tami (P) has a degree as Bachelor of Arts () Rowan University

@ViewFacultyRosterReport
Scenario: A user uses report to find out faculty type of teacher
    Given I am on Faculty Roster Report page
	When I select Part-time from faculty type menu
	And I click the submit button
	Then I find out Adler, Jennifer (P) has None Required as professional qualifications


@ViewFacultyRosterReport
Scenario: the convert to pdf button is click
    Given I am on Faculty Roster Report page
	When I select convert to pdf button
	Then it is directed to a new page having the pdf version of report
