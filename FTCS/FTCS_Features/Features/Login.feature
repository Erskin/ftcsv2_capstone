﻿Feature: Login
	In order to assign credentials to faculty
	As DCO, CCO, or administrator
	I want to be able to login to FTCS

@Login
Scenario: Login as a DCO
	Given I am on the Login page
	And I have entered dco as the username
	And I have entered password as the password
	When I press Login
	Then the DCO account is displayed at the top of the Home page

@Login
Scenario: Login as a CCO
	Given I am on the Login page
	And I have entered cco as the username
	And I have entered password as the password
	When I press Login
	Then the CCO account is displayed at the top of the Home page

@Login
Scenario: Try to Login with invalid credentials
	Given I am on the Login page
	And I have entered espy as the username
	And I have entered password as the password
	When I press Login
	Then an error message saying Username or Password not recognized. is shown

@Login
Scenario: Try to Login with empty fields
	Given I am on the Login page
	When I press Login
	Then two error messages should be displayed saying The Username field is required. and The Password field is required.


