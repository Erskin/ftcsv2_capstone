﻿Feature: ViewTheInstructionForChairs
	In order to inform chair members on how to use the site
	As an FTCS general user
	I want to view the PDF of instruction fot chair members

@ViewTheInstructionForChairs
Scenario:  View the Instruction for chairs
	Given I am on the home page
	When I click the Instruction for Chairs link
	Then the Instructions for chairs document is displayed
