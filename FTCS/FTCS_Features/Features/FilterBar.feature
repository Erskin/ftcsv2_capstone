﻿Feature: FilterBar
	In order refine department, faculty or course information I am viewing
	As a general user
	I want to be filter results based on the first letter and a search term

@FilterBarFeature
Scenario: Notify what letter index I am filtering on
	Given I am viewing a list of all departments
	When I click the letter C
	Then there is text in the filter bar indicating I am viewing results that start with the letter C
