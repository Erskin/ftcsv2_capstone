﻿Feature: ViewProfessionalQualifications
	In order to learn about the professional qualifications of a faculty
	As a general user
	I want to be able to view the professional qualifications of a faculty

@ProfessionalQualification
Scenario: I was to view the professional qualifications
	Given I am on Gregory Love faculty profile page
	When I click on the Professional Qualifications option
	Then The Narrative is Dr. Love
	And The Related Work Title is Related Work/Professional Experiences
	And The first related work experience place is Abbott Laboratories
	And The first related work experience position is Research Investigator
	And The first related work experience Responsibilities is Carried out duties described in the Narrative statement.
	And the first related work experience Date Started is August 1996
	And The first related work experience Date Ended is June 2006