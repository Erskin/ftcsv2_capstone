﻿Feature: ViewAListOfAllFaculty
	In order to view a faculty member's information 
	As an FTCS general user
	I want to view a list of all faculty


Background:

@ViewAListOfAllFaculty
Scenario: A user clicks the name header for sorting
	Given I am viewing the faculty search menu
	When I click the Name header
	Then the faculty member Zheng, Shimin is found

@ViewAListOfAllFaculty
Scenario: A user navigates to a faculty
	Given I am viewing the faculty search menu
	Then the faculty member Adjei-Baah, Dennis Kofi is found

@ViewAListOfAllFaculty
Scenario: A user selects a letter then navigates to a faculty
	Given I am viewing the faculty search menu
	When I select the letter D
	Then the faculty member Dalton, Bruce Owen is found

@ViewAListOfAllFaculty
Scenario: A user types a search term into the search text box and navigates to a faculty 
	Given I am viewing the faculty search menu
	When I have entered Jeff in the search box
	Then the faculty member Knisley, Jeff Randall is found

@ViewAListOfAllFaculty
Scenario: Default sort is on faculty name column
	Given I am viewing the faculty search menu
	Then the default name sort order is ascending

@ViewAListOfAllFaculty
Scenario: Sort faculty in descending order
	Given I am viewing the faculty search menu
	When I click the Name header
	And I have waited for the page
	Then the header shows the list is in descending order

@ViewAListOfAllFaculty
Scenario: Sorting on home department
	Given I am viewing the faculty search menu
	And I click the department header
	Then the header shows the department list is in descending order











