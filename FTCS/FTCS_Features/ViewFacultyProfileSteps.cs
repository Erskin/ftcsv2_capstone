﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features
{
    [Binding]
    public class ViewFacultyProfileSteps
    {
        private string urlDirector;

        [After]
        [Scope(Tag = "@FacultyProfilePage")]
        public void TearDown()
        {
            FacultyProfilePage.Quit();
        }
        [Given]
        public void GivenIAmOnFacultyProfilePageOfBridgesLakenGrace()
        {
            
            FacultyProfilePage.Initialize();
            FacultyProfilePage.GoToPage(urlDirector); 
        }
        
        [When]
        public void WhenIClickOn_ACADEMICQUALIFICATIONS_Tab(string AcademicQualificationsOption)
        {
            FacultyProfilePage.AcademicQualifications.ClickAcademicQualifications(AcademicQualificationsOption);
        }
        
        [Then]
        public void ThenICanSee_FACULTYRANK_As_RANK(string facultyRank)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.FacultyRank;
            Assert.That(actualText, Is.EqualTo(facultyRank));
        }
        
        [Then]
        public void ThenICanSeeDateOfRankAs_RANKDATE(string rankDate)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.FacultyDateOfRank;
            Assert.That(actualText, Is.EqualTo(rankDate));
        }
        
        [Then]
        public void ThenICanSee_DATEELIGIBLEFORNEXTRANK_As_DATE(string facultyEligibleRankDate)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.FacultyEligibleRankDate;
            Assert.That(actualText, Is.EqualTo(facultyEligibleRankDate));
        }
        
        [Then]
        public void ThenICanSee_STATUS_As_STATUS(string status)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.FacultyGraduateStatus;
            Assert.That(actualText, Is.EqualTo(status));
        }
        
        [Then]
        public void ThenICanSee_EMPLOYMENTSTATUS_As_STATUS(string employmentStatus)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.FacultyEmploymentStatus;
            Assert.That(actualText, Is.EqualTo(employmentStatus));
        }
        
        [Then]
        public void ThenICanSee_CATEGORY_As_CATEGORY(string category)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.FacultyEmploymentCategory;
            Assert.That(actualText, Is.EqualTo(category));
        }
        
        [Then]
        public void ThenICanSee_YEAR_As_YEAR(string year)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.Years;
            Assert.That(actualText, Is.EqualTo(year));
        }
        
        [Then]
        public void ThenICanSee_TENURESTATUS_As_TENURE(string tenure)
        {
            var actualText = FacultyProfilePage.AcademicQualifications.FacultyTenureStatus;
            Assert.That(actualText, Is.EqualTo(tenure));
        }
    }
}
