﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewProfessionalQualificationsSteps
    {
        [AfterScenario("ProfessionalQualification")]
        public void ScenarioTearDown()
        {
            FacultyProfilePage.FacultyProfileQuit();
        }
        [When]
        public void WhenIClickOnThe_PROFESSIONALQUALIFICATIONS_Option(string professionalQualifications)
        {
            FacultyProfilePage.ProfessionalQualifications.ClickProfessionalQualificationsOption(professionalQualifications);
        }

        [Then]
        public void ThenTheNarrativeIs_NARRATIVE(string narrative)
        {
            Assert.That(FacultyProfilePage.ProfessionalQualifications.Narrative, Contains.Substring(narrative));
        }
        
        [Then]
        public void ThenTheRelatedWorkTitleIs_RELATEDWORKEXPERIENCETITLE(string relatedWorkExperienceTitle)
        {
            Assert.That(FacultyProfilePage.ProfessionalQualifications.RelatedWorkExperienceTitle, Is.EqualTo(relatedWorkExperienceTitle));
        }
        
        [Then]
        public void ThenTheFirstRelatedWorkExperiencePlaceIs_RELATEDWORKEXPERIENCEPLACE(string relatedWorkExperiencePlace)
        {
            Assert.That(FacultyProfilePage.ProfessionalQualifications.RelatedWorkExperiencePlace, Is.EqualTo(relatedWorkExperiencePlace));
        }
        
        [Then]
        public void ThenTheFirstRelatedWorkExperiencePositionIs_RELATEDWORKEXPERIENCEPOSITION(string relatedWorkExperiencePosition)
        {
            Assert.That(FacultyProfilePage.ProfessionalQualifications.RelatedWorkExperiencePosition, Is.EqualTo(relatedWorkExperiencePosition));
        }
        
        [Then]
        public void ThenTheFirstRelatedWorkExperienceResponsibilitiesIs_RELATEDWORKEXPERIENCERESPONSIBILITIES(string relatedWorkExperienceResponsibilities)
        {
            Assert.That(FacultyProfilePage.ProfessionalQualifications.RelatedWorkExperienceResponsibilities, Is.EqualTo(relatedWorkExperienceResponsibilities));
        }
        
        [Then]
        public void ThenTheFirstRelatedWorkExperienceDateStartedIs_RELATEDWORKEXPERIENCESTARTDATE(string relatedWorkExperienceStartDate)
        {
            Assert.That(FacultyProfilePage.ProfessionalQualifications.RelatedWorkExperienceStartDate, Is.EqualTo(relatedWorkExperienceStartDate));
        }
        
        [Then]
        public void ThenTheFirstRelatedWorkExperienceDateEndedIs_RELATEDWORKEXPERIENCEENDDATE(string relatedWorkExperienceEndDate)
        {
            Assert.That(FacultyProfilePage.ProfessionalQualifications.RelatedWorkExperienceEndDate, Is.EqualTo(relatedWorkExperienceEndDate));
        }
    }
}
