﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewGraduateAssistantReportSteps
    {
        public object GraduateAssistantRosterReportTitle { get; private set; }

        [AfterScenario("ViewGraduateAssistantReport")]
        public void ScenarioTearDown()
        {
            ViewGradAssistantPage.Wait();
            ViewGradAssistantPage.Quit();
        }
        [Given]
        public void GivenIAmOnFTCS_HOME_pageLookingForAReport(string home)
        {
            ViewGradAssistantPage.Initialize();
            ViewGradAssistantPage.GoToPage(home);
        }
        [Given]
        public void GivenIAmViewingThe_GRADUATEASSISTANTROSTERREPORT(string report)
        {
            ViewGradAssistantPage.Initialize();
            ViewGradAssistantPage.GoToPage(report);
        }

        [When]
        public void WhenISelectTheGraduateAssistantRosterReportDropdownButton()
        {
            ViewGradAssistantPage.HoverOnTheReports();
            ViewGradAssistantPage.ClickGraduateAssistantRosterReportButton();
        }

        [When]
        public void WhenISelectTheYear_YEAR(string year)
        {
            ViewGradAssistantPage.SelectYear(year);
        }
        [When]
        public void WhenIClickSubmitButton()
        {
            ViewGradAssistantPage.clickSubmit();
        }


        [Then(@"I am navigated to(.*) page")]
        public void ThenIAmNavigatedTo_GRADUATEASSISTANTROSTERREPORT_Page(string graduateAssistantRosterReportTitle)
        {
            Assert.That(ViewGradAssistantPage.GraduateAssistantRosterReportTitle, Is.EqualTo(graduateAssistantRosterReportTitle));

        }

        [Then]
        public void ThenICanSee_FACULTY_AsAFaculty(string faculty)
        {
            var result = ViewGradAssistantPage.ViewFaculty(faculty);
            Assert.That(result, Is.EqualTo(faculty));
        }
        [Then]
        public void Then_DEPARTMENT_AsCorrespondingDepartment(string department)
        {
            var result = ViewGradAssistantPage.ViewDepartment(department);
            Assert.That(result, Is.EqualTo(department));
        }

        [Then]
        public void Then_DEGREE_AsAcademicDegrees(string degree)
        {
            var result = ViewGradAssistantPage.ViewDegree(degree);
            Assert.That(result, Is.EqualTo(degree));
        }

        [Then]
        public void Then_COURSE_AsOneOfTheCoursesTaught(string course)
        {
            var result = ViewGradAssistantPage.ViewCourse(course);
            Assert.That(result, Is.EqualTo(course));
        }

        [Then]
        public void Then_QUALIFICATION_AsProfessionalQualifications(string qualification)
        {
            var result = ViewGradAssistantPage.ViewQualification(qualification);
            Assert.That(result, Is.EqualTo(qualification));
        }


    }
}
