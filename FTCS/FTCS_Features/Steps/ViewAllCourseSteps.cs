﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewAListOfAllCourseSteps
    {
        [AfterScenario("ViewAListOfAllCourses_AcceptanceTests")]
        public void ScenarioTearDown()
        {
            ViewAllCoursePage.Wait();
            ViewAllCoursePage.Quit();
        }
        [Given]
        public void GivenIAmViewingAListOfAllCourses()
        {
            ViewAllCoursePage.Initialize();
            ViewAllCoursePage.GoToPage();
        }
        
        [When]
        public void WhenIClickOnTheLetter_LETTER(string letter)
        {
            ViewAllCoursePage.ClickLetterCourse(letter);
        }
        
        [When]
        public void WhenIHaveEnteredTheSearchTerm_SEARCH_InTheSearchBox(string search)
        {
            ViewAllCoursePage.EnterSearchBoxCourse(search);
            ViewAllCoursePage.ClickSearchButtonCourse();
        }
        
        [Then]
        public void ThenTheCourse_COURSE_IsFound(string course)
        {
            //ViewAllCoursePage.Wait();
            var courseName = ViewAllCoursePage.GetCourseByName(course);
            Assert.That(courseName, Does.Contain(course));
        }
        
        [When]
        public void WhenIClickTheCourseNameHeader()
        {
            ViewAllCoursePage.ClickCourseNameHeader();
        }
    }
}
