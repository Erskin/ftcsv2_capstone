﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]

    public class ViewAListOfAllFacultySteps
    {
        [After]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void TearDown()
        {
            ViewAllFacultyPage.Quit();
        }

        [Given]
        public void GivenIAmViewingTheFacultySearchMenu()
        {
            ViewAllFacultyPage.Initialize();
            ViewAllFacultyPage.GoToPage();
        }


        [When]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void WhenISelectTheLetter_LETTER(string letter)
        {
            ViewAllFacultyPage.ClickLetterFaculty(letter);
            ViewAllFacultyPage.WaitForPageToLoad();
        }

        [When]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void WhenIHaveEntered_SEARCH_InTheSearchBox(string search)
        {
            ViewAllFacultyPage.EnterSearchBoxFaculty(search);
            ViewAllFacultyPage.ClickSearchButtonFaculty();
            ViewAllFacultyPage.WaitForPageToLoad();
        }

        [When]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void WhenIHaveWaitedForThePage()
        {
            ViewAllFacultyPage.WaitForPageToLoad();
        }


        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenAListOfAllFacultyThatAreTeachingOrHaveBeenTeachingACourseAreShown()
        {
            ViewAllFacultyPage.WaitForPageToLoad();
            var facultyNames = ViewAllFacultyPage.GetFacultyNamesOnFacultyPage();
            Assert.That(facultyNames, Is.Not.EqualTo(null));
        }

        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenTheFirstFacultyNameOnTheListStartsWithA_LETTER(string letter)
        {
            ViewAllFacultyPage.WaitForPageToLoad();
            var facultyName = ViewAllFacultyPage.GetFirstFacultyName();
            Assert.That(facultyName, Does.StartWith(letter));
        }

        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenTheFirstNameOnTheListContainsTheName_SEARCH(string search)
        {
            ViewAllFacultyPage.WaitForPageToLoad();
            var facultyName = ViewAllFacultyPage.GetFirstFacultyName();
            Assert.That(facultyName.ToString().ToLower(), Does.Contain(search.ToLower()));
        }

        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenTheFirstNameShouldContain_SEARCH_AndShouldStartWith_LETTER(string search, string letter)
        {
            ViewAllFacultyPage.WaitForPageToLoad();
            var facultyName = ViewAllFacultyPage.GetFirstFacultyName();
            Assert.That(facultyName.ToString().ToLower(), Does.Contain(search.ToLower()));
            Assert.That(facultyName, Does.StartWith(letter));
        }

        [When]
        public void WhenIClickTheLinkToTheAllFacultyPage()
        {
            HomePage.Home.ClickLink.ClickSearchFacultyNavButton();
            HomePage.Home.ClickLink.ClinkLinkToAllFacultyPage();
        }

        [Then]
        public void ThenIAmOnTheAllFacultyPage()
        {
            ViewAllFacultyPage.WaitForPageToLoad();
            Assert.That(ViewAllFacultyPage.GetPageTitle, Is.EqualTo("Faculty"));
        }

        [When]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void WhenIClickTheNameHeader()
        {
            ViewAllFacultyPage.ClickTableHeader(1);
            ViewAllFacultyPage.WaitForPageToLoad();
        }

        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenTheHeaderShowsTheListIsInDescendingOrder()
        {
            Assert.That(ViewAllFacultyPage.ListOfFacultyName, Is.Ordered.Descending);
        }

        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenTheDefaultNameSortOrderIsAscending()
        {
            Assert.That(ViewAllFacultyPage.ListOfFacultyDepartmentName, Is.Ordered.Descending);
        }

        [Given]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void GivenIClickTheDepartmentHeader()
        {
            ViewAllFacultyPage.ClickTableHeader(2);
        }

        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenTheHeaderShowsTheDepartmentListIsInDescendingOrder()
        {
            Assert.That(ViewAllFacultyPage.ListOfFacultyName, Is.Ordered.Descending);
        }

        [Then]
        [Scope(Tag = "@ViewAListOfAllFaculty")]
        public void ThenTheFacultyMember_FACULTY_IsFound(string faculty)
        {
            ViewAllFacultyPage.WaitForPageToLoad();
            var facultyName = ViewAllFacultyPage.GetFacultyByName(faculty);
            Assert.That(facultyName, Does.Contain(faculty));
        }
       
    }
}
