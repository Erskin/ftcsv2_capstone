﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    [TestFixture]
    public class FilterBarSteps
    {
        [AfterScenario("FilterBarFeature")]
        public void FilterDispose()
        {
            ViewAllDepartmentPage.Quit();
        }
        [Given]
        public void GivenIAmViewingAListOfAllDepartments()
        {
            ViewAllDepartmentPage.Initialize();
            ViewAllDepartmentPage.GoToPage();
        }
        [When]
        public void WhenIClickTheLetter_LETTER(string letter)
        {
            ViewAllDepartmentPage.ClickLetter(letter);
        }
        [Then]
        public void ThenThereIsTextInTheFilterBarIndicatingIAmViewingResultsThatStartWithTheLetter_LETTER(string letter)
        {
            var startsWithMessage = ViewAllDepartmentPage.StartsWithMessage;
            Assert.That(startsWithMessage, Does.EndWith(letter));
        }
    }
}
