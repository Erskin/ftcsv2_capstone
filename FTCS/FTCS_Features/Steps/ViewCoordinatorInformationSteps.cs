﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewCoordinatorInformationSteps
    {
        [AfterScenario("CoordinatorInformationFeature")]
        public void ScenarioTearDown()
        {
            FacultyProfilePage.FacultyProfileQuit();
        }
        [Given]
        public void GivenIAmOnThe_BARRETT_ProfilePage(string barrett)
        {
            FacultyProfilePage.Initialize();
            FacultyProfilePage.GoToPage(barrett);
        }
        
        [When]
        public void WhenIClickOnTheOptionNamed_COORDINATORINFORMATION(string coordinatorInformation)
        {
            FacultyProfilePage.CoordinatorInformation.ClickCoordinatorInformationOption(coordinatorInformation);
        }
        
        [Then]
        public void ThenICanSee_DEGREENAME_AsTheDegreeName(string degreeName)
        {
            Assert.That(degreeName, Is.EqualTo(FacultyProfilePage.CoordinatorInformation.DegreeName));
        }
        
        [Then]
        public void Then_LEVEL_AsTheLevel(string level)
        {
            Assert.That(level, Is.EqualTo(FacultyProfilePage.CoordinatorInformation.Level));
        }
        
        [Then]
        public void Then_DEGREE_AsTheDegree(string degree)
        {
            Assert.That(degree, Is.EqualTo(FacultyProfilePage.CoordinatorInformation.Degree));
        }

        [Then]
        public void Then_ACADEMIC_AsHowQualified(string academic)
        {
            Assert.That(academic, Is.EqualTo(FacultyProfilePage.CoordinatorInformation.HowQualified));
        }        
        [Then]
        public void Then_STARTDATE_AsTheDateStarted(string startDate)
        {
            Assert.That(startDate, Is.EqualTo(FacultyProfilePage.CoordinatorInformation.StartDate));
        }
        
        [Then]
        public void Then_ENDDATE_AsTheDateEnded(string endDate)
        {
            Assert.That(endDate, Is.EqualTo(FacultyProfilePage.CoordinatorInformation.EndDate));
        }
    }
}
