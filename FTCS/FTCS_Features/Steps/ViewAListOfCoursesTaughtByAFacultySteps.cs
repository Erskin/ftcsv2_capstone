﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewAListOfCoursesTaughtByAFacultySteps
    {
        [AfterScenario("CoursesTaughtByAFaculty")]
        public void ScenarioTearDown()
        {
            FacultyProfilePage.FacultyProfileQuit();
        }
        [When(@"I click on the option called (.*)")]
        public void WhenIClickOnTheOptionCalled_COURSESTAUGHT(string coursesTaught)
        {
            FacultyProfilePage.CoursesTaught.ClickCoursesTaughtOption(coursesTaught);
        }
        [When(@"I click on the (.*) button")]
        public void WhenIClickOnThe_ALLCOURSESTAUGHT_button(string allCoursesTaught)
        {
            FacultyProfilePage.CoursesTaught.ClickAllCoursesTaughtButton(allCoursesTaught);
        }
        [Then]
        public void ThenICanSeeTheCoursesTaughtTitleIs_COURSESTAUGHTTABLETITLE(string coursesTaughtTableTitle)
        {
            Assert.That(coursesTaughtTableTitle, Is.EqualTo(FacultyProfilePage.CoursesTaught.CoursesTaughtTableTitle));
        }

        [Then]
        public void ThenTheCourseIDIs_COURSEID(string courseID)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.CourseID, Contains.Substring(courseID));
        }

        [Then]
        public void ThenTheCourseTitleIs_COURSETITLE(string courseTitle)
        {
            Assert.That(courseTitle, Is.EqualTo(FacultyProfilePage.CoursesTaught.CourseTitle));
        }

        [Then]
        public void ThenTheCourseDescriptionIs_COURSEDESCRIPTION(string courseDescription)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.CourseDescription, Contains.Substring(courseDescription));
        }

        [Then]
        public void ThenTheCourseSemesterIs_COURSESEMESTER(string courseSemester)
        {
            Assert.That(courseSemester, Is.EqualTo(FacultyProfilePage.CoursesTaught.CourseSemester));
        }

        [Then]
        public void ThenTheCourseCertificationIs_COURSECERTIFICATION(string courseCertification)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.CourseCertification, Is.EqualTo(courseCertification));
        }

        [Then]
        public void ThenICanViewAButtonThatDisplaysAListOf_ALLCOURSESTAUGHTBUTTONTEXT(string allCoursesTaughtButtonText)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.AllCoursesTaughtButtonText, Is.EqualTo(allCoursesTaughtButtonText));
        }

        [Then(@"The Course Id (.*) is in the displayed list")]
        public void ThenTheCourseId_ALLCOURSESTAUGHTCOURSETITLE_IsInTheDisplayedList(string allCoursesTaughtCourseID)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.AllCoursesTaughtCourseID, Is.EqualTo(allCoursesTaughtCourseID));
        }

        [Then]
        public void ThenTheCourseTitle_ALLCOURSESTAUGHTCOURSETITLE_IsInTheDisplayedList(string allCoursesTaughtCourseTitle)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.AllCoursesTaughtCourseTitle, Is.EqualTo(allCoursesTaughtCourseTitle));
        }

        [Then]
        public void ThenTheCourseTerm_ALLCOURSESTAUGHTCOURSETERM_IsInTheDisplayedList(string allCoursesTaughtCourseTerm)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.AllCoursesTaughtCourseTerm, Is.EqualTo(allCoursesTaughtCourseTerm));
        }

        [Then]
        public void ThenTheCourseCertification_ALLCOURSESTAUGHTCOURSECERTIFICATION_IsInTheDisplayedList(string allCoursesTaughtCourseCertification)
        {
            Assert.That(FacultyProfilePage.CoursesTaught.AllCoursesTaughtCourseCertification, Is.EqualTo(allCoursesTaughtCourseCertification));
        }
    }
}
