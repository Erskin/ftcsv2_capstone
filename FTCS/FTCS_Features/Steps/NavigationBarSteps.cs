﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class NavigationBarSteps
    {
        [AfterScenario("NavigationBar")]
        public void ScenarioTearDown()
        {
            HomePage.Quit();
        }

        [Given]
        public void GivenICanViewTheNavigationBar()
        {
            HomePage.Initialize();
            HomePage.GoToPage();
        }

        [TearDown]
        public void Dispose()
        {
            HomePage.Quit();
        }
        
        [When]
        public void WhenIHoverOnTheReportsDropdownOption()
        {
            HomePage.HoverOnReports();
        }

        [When]
        public void WhenISelectTheCourseSectionQualificationsPercentageReportOption()
        {
            HomePage.NavigationBar.ClickCourseSectionQualificationsPercentageReport();
        }

        [When]
        public void WhenISelectTheFacultyCredentialsReportOption()
        {
            HomePage.NavigationBar.ClickFacultyCredentialsReport();
        }

        [When]
        public void WhenISelectTheFacultyRosterReportOption()
        {
            HomePage.NavigationBar.ClickFacultyRosterReport();
        }

        [When]
        public void WhenISelectTheGraduateAssistantRosterReportOption()
        {
            HomePage.NavigationBar.ClickGraduateAssistantRosterReport();
        }

        [When]
        public void WhenIClickOnHome()
        {
            HomePage.NavigationBar.ClickHome();
        }
        
        [When]
        public void WhenIHoverOverTheSearchForFacultyDropdown()
        {
            HomePage.NavigationBar.HoverOnSearchForFaculty();
        }

        [When]
        public void WhenISelectTheContactUsOption()
        {
            HomePage.NavigationBar.ClickContactUs();
        }

        [When]
        public void WhenISelectTheByDepartmentOption()
        {
            HomePage.NavigationBar.ClickByDepartment();
        }

        [When]
        public void WhenISelectTheByNameOption()
        {
            HomePage.NavigationBar.ClickByName();
        }

        [When]
        public void WhenISelectTheByCourseOption()
        {
            HomePage.NavigationBar.ClickByCourse();
        }

        [Then(@"I am on the (.*) page")]
        public void ThenIAmOnThe_REPORT_page(string reportTitle)
        {
            Assert.That(HomePage.NavigationBar.ReportTitle(reportTitle), Is.EqualTo(reportTitle));
        }
       
        [Then]
        public void ThenIAmTakenToThe_HOME_Page(string home)
        {
            Assert.That(HomePage.NavigationBar.Home, Is.EqualTo(home));
        }

        [Then(@"I can view the (.*) title")]
        public void ThenICanViewThe_SEARCHTITLE_Title(string searchTitle)
        {
            Assert.That(HomePage.NavigationBar.SearchTitle(searchTitle), Is.EqualTo(searchTitle));
        }

        [Then]
        public void ThenICanSeeThe_CONTACTUS_OptionOnTheNavigationBar(string contactUs)
        {
            Assert.That(HomePage.NavigationBar.ContactUsText, Is.EqualTo(contactUs));
        }
    }
}
