﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewListOfAllFacultyInDepartmentSteps
    {

        [Given]
        public void GivenIAmViewingTheListOfAllDepartments()
        {
            ViewAllDepartmentPage.Initialize();
            ViewAllDepartmentPage.GoToPage();
        }
        
        [When]
        public void WhenIClickOnTheDepartmentNameAccountingLink()
        {
            ViewAllDepartmentPage.ClickAccountingLink();
        }
        
        [Then]
        public void ThenISeeAListOfAllFacultyTeachingInAccounting()
        {
            ViewAllDepartmentPage.WaitForPageLoad();
            var departmentNames = ViewAllFacultyPage.DepartmentColumnValues;
            foreach(var name in departmentNames)
            {
                Assert.That(name, Is.EqualTo("Accounting"));
            }
        }
    }
}
