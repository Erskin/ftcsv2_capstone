﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Interactions;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewHomePageSteps
    {
        [AfterScenario("HomePage")]
        public void ScenarioTearDown()
        {
            HomePage.Wait();
            HomePage.Quit();
        }
        [Given]
        public void GivenIHaveABrowserWindowOpen()
        {
            HomePage.Initialize();
        }
        
        [Given]
        public void GivenIAmOnTheFTCSHomePage()
        {
            HomePage.Initialize();
            HomePage.GoToPage();   
        }
        
        [When]
        public void WhenINavigateToTheFTCSWebApplication()
        {
            HomePage.GoToPage();
        }
        [Then]
        public void TEXT_IsDisplayed(string text)
        {
            var actualText = HomePage.Home.Text;
            Assert.That(actualText, Is.EqualTo(text));
        }

        [Then]
        public void ThenTheGraduateFacultyAppointmentGuidelinesLinkIsDisplayedAndHasATargetURL_TARGETURL(string targetURL)
        {
            Assert.That(targetURL, Is.EqualTo(HomePage.Home.GraduateFacultyAppointmentGuidelines));
        }

        [Then]
        public void ThenTheETSUMissionLinkIsDisplayedAndHasATargetURL_TARGETURL(string targetURL)
        {
            Assert.That(targetURL, Is.EqualTo(HomePage.Home.ETSUMission));
        }


        [Then]
        public void ThenTheTeachingCredentialStandardsLinkIsDisplayedAndHasATargetURL_TARGETURL(string targetURL)
        {
            Assert.That(targetURL, Is.EqualTo(HomePage.Home.TeachingCredentialStandard));
        }

        [Then]
        public void ThenTheSACSCredentialsGuidelinesLinkIsDisplayedWithATargetURL_TARGETURL(string targetURL)
        {
            Assert.That(targetURL, Is.EqualTo(HomePage.Home.SACSGuidelines));
        }



        [Then(@"The (.*) link is displayed as a Chair Instructions button")]
        public void ThenThe_CHAIRINSTRUCTIONS_LinkIsDisplayedAsAChairInstructionsButton(string chairInstructions)
        {
            var actualChairInstructions = HomePage.Home.ChairInstructions;
            Assert.That(actualChairInstructions, Is.EqualTo(chairInstructions));
        }
    }
}