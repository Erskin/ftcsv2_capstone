﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewFacultyCredentialsReportSteps
    {
        [AfterScenario("ViewFacultyCredentialsReport")]
        public void ScenarioTearDown()
        {
            ViewFacultyCredentialsReportPage.Wait();
            ViewFacultyCredentialsReportPage.Quit();
        }

        [Given]
        public void GivenIAmViewingTheFacultyCredentialsReport()
        {
            ViewFacultyCredentialsReportPage.Initialize();
            ViewFacultyCredentialsReportPage.GoToPage();
        }
        [Scope(Tag = "@ViewFacultyCredentialsReport")]
        [When]
        public void WhenISelect_DEPARTMENT_FromTheCredentialsDepartmentMenu(string department)
        {
            ViewFacultyCredentialsReportPage.SelectADepartment(department);
        }


        [When]
        public void WhenIClickTheSubmitButton()
        {
            ViewFacultyCredentialsReportPage.ClickSubmit();
        }
        
        [When]
        public void WhenISelect_SEMESTER_FromTheSemesterMenu(string semester)
        {
            ViewFacultyCredentialsReportPage.SelectASemester(semester);
        }
        
        [When]
        public void WhenISelectTheConvertToPdfOption()
        {
            ViewFacultyCredentialsReportPage.ClickPDFButton();
        }
        
        [Then]
        public void ThenIFindOutHow_TEACHER_IsQualifiedFor_COURSE(string teacher, string course)
        {
            var result = ViewFacultyCredentialsReportPage.FindTeacher(teacher, course);
            Assert.That(result, Is.EqualTo(course));
        }
        
        
        [Then]
        public void ThenANewPageIsOpenedWithTheReportAsAPdf()
        {
            var actualPDF = ViewFacultyCredentialsReportPage.FacultyCredentialsPDF;
            Assert.That(actualPDF, Is.EqualTo("Convert To PDF"));
        }
        
        [Then]
        public void ThenItsBackgroundColorIsRed()
        {
            var result = ViewFacultyCredentialsReportPage.GetBackgroundColor();
            Assert.That(result, Is.EqualTo("rgba(245, 85, 65, 1)"));
        }

    }
}
