﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewFacultyRosterReportSteps
    {
        [AfterScenario("ViewFacultyRosterReport")]
        public void ScenarioTearDown()
        {
            ViewFacultyRosterReportPage.Wait();
            ViewFacultyRosterReportPage.Quit();
        }
        [Scope(Tag = "@ViewFacultyRosterReport")]
        [Given]
        public void GivenIAmOnTheHomePage()
        {
            HomePage.Initialize();
            HomePage.GoToPage();
        }

        [Given]
        public void GivenIAmOnFacultyRosterReportPage()
        {
            ViewFacultyRosterReportPage.Initialize();
            ViewFacultyRosterReportPage.GoToPage();
        }

        [When]
        public void WhenIClickTheFacultyRosterReportLink()
        {
            ViewFacultyRosterReportPage.ClickFacultyRosterLink();
        }

        [When]
        public void WhenISelect_YEAR_FromTheAcademicYearMenu(string year)
        {
            ViewFacultyRosterReportPage.SelectAYear(year);
        }

        [When]
        public void WhenISelect_DEPARTMENT_FromTheCredentialsDepartmentMenu(string department)
        {
            ViewFacultyRosterReportPage.SelectADepartment(department);
        }


        [Scope(Tag = "@ViewFacultyRosterReport")]
        [When]
        public void WhenIClickTheSubmitButton()
        {
            ViewFacultyRosterReportPage.ClickSubmit();
        }

        [Then]
        public void ThenItNavigatesTo_FACULTYROSTER_Page(string facultyroster)
        {
            Assert.That(ViewFacultyRosterReportPage.FacultyRoster, Is.EqualTo(facultyroster));
        }

        [Then]
        public void ThenIFindOut_TEACHER_HasTaughtTheCourse_COURSE(string teacher, string course)
        {
            var result = ViewFacultyRosterReportPage.FindTeacherCourse(teacher, course);
            Assert.That(result, Is.EqualTo(course));
        }

        [Then]
        public void ThenIFindOut_TEACHER_HasADegreeAs_DEGREE(string teacher, string degree)
        {
            var result = ViewFacultyRosterReportPage.FindTeacherDegree(teacher, degree);
            Assert.That(result, Is.EqualTo(degree));
        }

        [When]
        public void WhenISelect_TYPE_FromFacultyTypeMenu(string type)
        {
            ViewFacultyRosterReportPage.SelectAType(type);
        }
        [Scope(Tag = "@ViewFacultyRosterReport")]
        [Then]
        public void ThenIFindOut_TEACHER_Has_QUALIFICATION_AsProfessionalQualifications(string teacher, string qualification)
        {
            var result = ViewFacultyRosterReportPage.FindTeacherQualification(teacher, qualification);
            Assert.That(result, Is.EqualTo(qualification));
        }
        [Scope(Tag = "@ViewFacultyRosterReport")]
        [When]
        public void WhenISelectConvertToPdfButton()
        {
            ViewFacultyRosterReportPage.ClickPDFButton();
        }
        [Then]
        public void ThenItIsDirectedToANewPageHavingThePdfVersionOfReport()
        {
            ScenarioContext.Current.Pending();
        }

    }
}
