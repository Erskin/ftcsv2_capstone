﻿using FTCSTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewAllDepartmentsSteps
    {
        [AfterScenario("ViewAllDepartments")]
        public void TearDown()
        {
            ViewAllDepartmentPage.Quit();
        }


        [Given]
        [Scope(Tag = "@ViewAllDepartments")]
        public void GivenIAmViewingAListOfAllDepartments()
        {
            ViewAllDepartmentPage.Initialize();
            ViewAllDepartmentPage.GoToPage();
        }


        [When]
        [Scope(Tag = "@ViewAllDepartments")]
        public void WhenIHaveWaitedForThePageToLoad()
        {
            ViewAllDepartmentPage.WaitForPageLoad();
        }


        [When]
        [Scope(Tag = "@ViewAllDepartments")]
        public void WhenIClickTheLetter_LETTER(string letter)
        {
            ViewAllDepartmentPage.ClickLetter(letter);
        }

        [When]
        [Scope(Tag = "@ViewAllDepartments")]
        public void WhenIHaveEnteredTheText_SEARCH_AsTheSearchTermInTheSearchBox(string search)
        {
            ViewAllDepartmentPage.EnterSearchText(search);
            ViewAllDepartmentPage.ClickSearchButton();
        }



        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheFirstDepartmentNameOnTheListStartsWithA_LETTER(string letter)
        {
            ViewAllDepartmentPage.WaitForPageLoad();
            var departmentName = ViewAllDepartmentPage.GetFirstDepartmentName();
            Assert.That(departmentName, Does.StartWith(letter));
        }

        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheFirstDepartmentNameOnTheListContainsTheText_SEARCH(string search)
        {
            ViewAllDepartmentPage.WaitForPageLoad();
            var departmentName = ViewAllDepartmentPage.GetFirstDepartmentName();
            Assert.That(departmentName.ToLower(), Does.Contain(search.ToLower()));
        }

        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheFirstDepartmentNameShouldContain_SEARCH_AndShouldStartWith_LETTER(string search, string letter)
        {
            ViewAllDepartmentPage.WaitForPageLoad();
            var departmentName = ViewAllDepartmentPage.GetFirstDepartmentName();
            Assert.That(departmentName.ToLower(), Does.Contain(search.ToLower()));
            Assert.That(departmentName, Does.StartWith(letter));
        }

        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheHeaderShowsItIsInDescendingOrder()
        {
            Assert.That(ViewAllDepartmentPage.IconForDepartmentHeaderDescending, Is.True);
        }

        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheDefaultSortOrderIsAscending()
        {
            Assert.That(ViewAllDepartmentPage.IconForDepartmentHeader, Is.True);
        }

        [Given]
        [Scope(Tag = "@ViewAllDepartments")]
        public void GivenTheDepartmentListIsInAscendingOrder()
        {
            Assert.That(ViewAllDepartmentPage.ListOfDepartmentName, Is.Ordered);
        }

        [When]
        [Scope(Tag = "@ViewAllDepartments")]
        public void WhenIClickTheDepartmentColumnName()
        {
            ScenarioContext.Current.Pending();
        }

        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheListIsSortedInDescendingOrder()
        {
            Assert.That(ViewAllDepartmentPage.ListOfDepartmentName, Is.Ordered.Descending);
        }

        [When]
        [Scope(Tag = "@ViewAllDepartments")]
        public void WhenIClickTheNumberOfFacultyColumnName()
        {
            ViewAllDepartmentPage.ClickTableHeader(2);
        }

        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheDepartmentListIsSortedInAscendingOrderByTheNumberOfFaculty()
        {
            Assert.That(ViewAllDepartmentPage.GetColumnData(2), Is.Ordered);
        }

        [When]
        [Scope(Tag = "@ViewAllDepartments")]
        public void WhenIClickTheDepartmentHeader()
        {
            ViewAllDepartmentPage.ClickDepartmentHeader();
        }

        [Then]
        [Scope(Tag = "@ViewAllDepartments")]
        public void ThenTheFirstDepartmentListedIs_DEPARTMENTNAME(string DepartmentName)
        {
            Assert.That(ViewAllDepartmentPage.GetFirstDepartmentName(), Is.EqualTo(DepartmentName));
        }

    }
}
