﻿using System;
using TechTalk.SpecFlow;
using FTCSTestFramework;
using NUnit.Framework;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewCourseSectionQualificationPercentageReportSteps
    {
        [AfterScenario("ViewCourseSectionQualificationPercentageReport")]
        public void ScenarioTearDown()
        {
            ViewCourseSectionQualificationPercentageReport.Wait();
            ViewCourseSectionQualificationPercentageReport.Quit();
        }
        [Given]
        public void GivenIAmOnTheFTCS_HOME_PageLookingForAReport(string home)
        {
            ViewCourseSectionQualificationPercentageReport.Initialize();
            ViewCourseSectionQualificationPercentageReport.GoToPage(home);
        }
        [Given]
        public void GivenIAmOnThe_COURSESECTIONQUALIFICATIONPERCENTAGEREPORT_Page(string report)
        {
            ViewCourseSectionQualificationPercentageReport.Initialize();
            ViewCourseSectionQualificationPercentageReport.GoToPage(report);
        }

        [When]
        public void WhenISelectTheCourseSectionQualificationPercentageReportDropdownOption()
        {
            ViewCourseSectionQualificationPercentageReport.HoverOnTheReports();
            ViewCourseSectionQualificationPercentageReport.ClickCourseSectionQualificationPercentageReportButton();
        }

        [When]
        public void WhenIClickOnTheGraduateTab()
        {
            ViewCourseSectionQualificationPercentageReport.ClickGraduateTab();
        }

        [When]
        public void WhenIChangeTheDropdownOptionTo_Year(string year)
        {
            ScenarioContext.Current.Pending();
        }

        [When]
        public void WhenIClickTheOnAStartYear()
        {
            ViewCourseSectionQualificationPercentageReport.ClickStartYearButton();
        }
        [When]
        public void WhenIClickThe2015Button()
        {
            ViewCourseSectionQualificationPercentageReport.Click2015Button();
        }
        [Then(@"I can view the (.*) page")]
        public void ThenICanViewThe_COURSESECTIONQUALIFICATIONPERCENTAGEREPORT_Page(string courseSectionQualificationReportTitle)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.CourseSectionQualificationPercentageReportTitle, Is.EqualTo(courseSectionQualificationReportTitle));
        }
        [Then]
        public void ThenICanViewThe_DEPARTMENT_InTheGraduateTable(string department)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateDepartment, Is.EqualTo(department));
        }
        [Then]
        public void ThenICanSeeThe_TOTALCOURSESECTIONS_InTheSecondColumnInTheGraduateTable(string totalCourseSections)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateTotalCourseSection, Is.EqualTo(totalCourseSections));
        }
        [Then]
        public void ThenICanSeeThe_TOTALCOURSESECTIONSPERCENTAGE_InTheThirdColumnInTheGraduateTable(string totalCourseSectionsPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateTotalCourseSectionsPercentage, Is.EqualTo(totalCourseSectionsPercentage));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERACADEMICALLYQUALIFIED_InTheFourthColumnInTheGraduateTable(string numberAcademicallyQualified)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateNumberAcademicallyQualified, Is.EqualTo(numberAcademicallyQualified));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERACADEMICALLYQUALIFIEDPERCENTAGE_InTheFifthColumnInTheGraduateTable(string numberAcademicallyQualifiedPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateNumberAcademicallyQualifiedPercentage, Is.EqualTo(numberAcademicallyQualifiedPercentage));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERPROFESSIONALLYQUALIFIED_InTheSixthColumnInTheGraduateTable(string numberProfessionallyQualified)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateNumberProfessionallyQualified, Is.EqualTo(numberProfessionallyQualified));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERPROFESSIONALLYQUALIFIEDPERCENTAGE_InTheSeventhColumnInTheGraduateTable(string numberProfessionallyQualifiedPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateNumberProfessionallyQualifiedPercentage, Is.EqualTo(numberProfessionallyQualifiedPercentage));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERGRADUATEASSISTANTS_InTheEighthColumnInTheGraduateTable(string numberGraduateAssistants)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateNumberGraduateAssistants, Is.EqualTo(numberGraduateAssistants));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERGRADUATEASSISTANTSPERCENTAGE_InTheNinthColumnInTheGraduateTable(string numberGraduateAssistantsPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateNumberGraduateAssistantsPercentage, Is.EqualTo(numberGraduateAssistantsPercentage));
        }
        [Then]
        public void ThenICanSeeThe_MISSINGTRANSCRIPTS_InTheTenthColumnInTheGraduateTable(string missingTranscripts)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateMissingTranscripts, Is.EqualTo(missingTranscripts));
        }
        [Then]
        public void ThenICanSeeThe_MISSINGTRANSCRIPTSPERCENTAGE_InTheEleventhColumnInTheGraduateTable(string missingTranscriptsPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.GraduateMissingTranscriptsPercentage, Is.EqualTo(missingTranscriptsPercentage));
        }
        [Then]
        public void ThenICanViewThe_DEPARTMENT_InTheUndergradTable(string department)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateDepartment, Is.EqualTo(department));
        }
        [Then]
        public void ThenICanSeeThe_TOTALCOURSESECTIONS_InTheSecondColumnInTheUndergraduateTable(string totalCourseSections)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateTotalCourseSection, Is.EqualTo(totalCourseSections));
        }
        [Then]
        public void ThenICanSeeThe_TOTALCOURSESECTIONPERCENTAGE_InTheThirdColumnInTheUndergraduateTable(string totalCourseSectionPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateTotalCourseSectionPercentage, Is.EqualTo(totalCourseSectionPercentage));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERACADEMICALLYQUALIFIED_InTheFourthColumnInTheUndergraduateTable(string numberAcademicallyQualified)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateNumberAcademicallyQualified, Is.EqualTo(numberAcademicallyQualified));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERACADEMICALLYQUALIFIEDPERCENTAGE_InTheFifthColumnInTheUndergraduateTable(string numberAcademicallyQualifiedPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateNumberAcademicallyQualifiedPercentage, Is.EqualTo(numberAcademicallyQualifiedPercentage));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERPROFESSIONALLYQUALIFIED_InTheSixthColumnInTheUndergraduateTable(string numberProfessionallyQualified)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateNumberProfessionallyQualified, Is.EqualTo(numberProfessionallyQualified));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERPROFESSIONALLYQUALIFIEDPERCENTAGE_InTheSeventhColumnInTheUndergraduateTable(string numberProfessionallyQualifiedPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateNumberProfessionallyQualifiedPercentage, Is.EqualTo(numberProfessionallyQualifiedPercentage));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERGRADUATEASSISTANTS_InTheEighthColumnInTheUndergraduateTable(string numberGraduateAssistants)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateNumberGraduateAssistants, Is.EqualTo(numberGraduateAssistants));
        }
        [Then]
        public void ThenICanSeeThe_NUMBERGRADUATEASSISTANTSPERCENTAGE_InTheNinethColumnInTheUndergraduateTable(string numberGraduateAssistantsPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateNumberGraduateAssistantsPercentage, Is.EqualTo(numberGraduateAssistantsPercentage));
        }
        [Then]
        public void ThenICanSeeThe_MISSINGTRANSCRIPTS_InTheTenthColumnInTheUndergraduateTable(string missingTranscripts)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateMissingTranscripts, Is.EqualTo(missingTranscripts));
        }
        [Then]
        public void ThenICanSeeThe_MISSINGTRANSCRIPTSPERCENTAGE_InTheEleventhColumnInTheUndergraduateTable(string missingTranscriptsPercentage)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.UndergraduateMissingTranscriptsPercentage, Is.EqualTo(missingTranscriptsPercentage));
        }
        [Then(@"I can view the report with the (.*) start year")]
        public void ThenICanViewTheReportWithThe_YEAR_StartYear(string startYear)
        {
            Assert.That(ViewCourseSectionQualificationPercentageReport.StartYear, Contains.Substring(startYear));
        }
    }
}