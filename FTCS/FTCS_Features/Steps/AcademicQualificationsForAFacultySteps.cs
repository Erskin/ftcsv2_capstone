﻿using System;
using TechTalk.SpecFlow;
using FTCSTestFramework;
using NUnit.Framework;

namespace FTCS_Features.Steps
{
    [Binding]
    [TestFixture]
    public class AcademicQualificationForAFacultySteps
    {
        [AfterScenario("FacultyProfile")]
        public void AcademicQualificationScenarioTearDown()
        {
            FacultyProfilePage.FacultyProfileQuit();
        }
        [Given]
        public void GivenIAmOn_LOVEGREGORY_FacultyProfilePage(string loveGregory)
        {
            FacultyProfilePage.Initialize();
            FacultyProfilePage.GoToPage(loveGregory);
        }

        [When]
        public void WhenIClickThe_ACADEMICQUALIFICATIONS_Option(string academicQualifications)
        {
            FacultyProfilePage.AcademicQualifications.ClickAcademicQualifications(academicQualifications);
        }

        [Then]
        public void ThenISeeTheDegreeIsA_DEGREE(string degree)
        {
            var actualDegree = FacultyProfilePage.AcademicQualifications.Degree;
            Assert.That(actualDegree, Is.EqualTo(degree));
        }

        [Then(@"the initials for the degree is (.*)")]
        public void ThenTheInitialsForTheDegreeIs_DEGREEINITIAL(string degreeInitial)
        {
            var actualDegreeInitial = FacultyProfilePage.AcademicQualifications.DegreeInitial;
            Assert.That(actualDegreeInitial, Is.EqualTo(degreeInitial));
        }

        [Then]
        public void ThenTheDisciplineIs_DISCIPLINE(string discipline)
        {
            var actualDiscipline = FacultyProfilePage.AcademicQualifications.Discipline;
            Assert.That(actualDiscipline, Is.EqualTo(discipline));
        }

        [Then]
        public void ThenTheInstitutionIs_INSTITUTION(string institution)
        {
            var actualInstitution = FacultyProfilePage.AcademicQualifications.Institution;
            Assert.That(actualInstitution, Is.EqualTo(institution));
        }

        [Then]
        public void ThenTheTranscriptOnFileIs_TRANSCRIPT(string transcript)
        {
            var actualTranscript = FacultyProfilePage.AcademicQualifications.Transcript;
            Assert.That(actualTranscript, Is.EqualTo(transcript));
        }

        [Then]
        public void ThenISeeTheFacultyNameIs_FACULTYNAME(string facultyName)
        {
            var actualFacultyName = FacultyProfilePage.AcademicQualifications.FacultyName;
            Assert.That(actualFacultyName, Is.EqualTo(facultyName));
        }

        [Then(@"the faculty title is (.*)")]
        public void ThenTheFacultyTitleIs_FACULTYTITLE(string facultyTitle)
        {
            var actualFacultyTitle = FacultyProfilePage.AcademicQualifications.FacultyTitle;
            Assert.That(actualFacultyTitle, Is.EqualTo(facultyTitle));
        }

        [Then(@"the faculty email is (.*)")]
        public void ThenTheFacultyEmailIs_EMAIL(string facultyEmail)
        {
            var actualEmail = FacultyProfilePage.AcademicQualifications.Email;
            Assert.That(actualEmail, Is.EqualTo(facultyEmail));
        }

        [Then]
        public void ThenISeeTheFacultyRankIs_FACULTYRANK(string facultyRank)
        {
            var actualFacultyRank = FacultyProfilePage.AcademicQualifications.FacultyRank;
            Assert.That(actualFacultyRank, Is.EqualTo(facultyRank));
        }

        [Then]
        public void ThenTheFacultyDateOfRankIs_FACULTYDATEOFRANK(string facultyDateOfRank)
        {
            var actualFacultyDateOfRank = FacultyProfilePage.AcademicQualifications.FacultyDateOfRank;
            Assert.That(actualFacultyDateOfRank, Is.EqualTo(facultyDateOfRank));
        }

        [Then]
        public void ThenTheFacultyDateEligibleForNextRankIs_FACULTYELIGIBLERANKDATE(string facultyEligibleRankDate)
        {
            var actualFacultyEligibleRankDate = FacultyProfilePage.AcademicQualifications.FacultyEligibleRankDate;
            Assert.That(actualFacultyEligibleRankDate, Is.EqualTo(facultyEligibleRankDate));
        }

        [Then]
        public void ThenTheFacultyGraduateFacultyStatusIs_FACULTYGRADUATESTATUS(string facultyGraduateStatus)
        {
            var actualFacultyGraduateStatus = FacultyProfilePage.AcademicQualifications.FacultyGraduateStatus;
            Assert.That(actualFacultyGraduateStatus, Is.EqualTo(facultyGraduateStatus));
        }

        [Then]
        public void ThenTheFacultyEmploymentStatusIs_FACULTYEMPLOYMENTSTATUS(string facultyEmploymentStatus)
        {
            var actualFacultyEmploymentStatus = FacultyProfilePage.AcademicQualifications.FacultyEmploymentStatus;
            Assert.That(actualFacultyEmploymentStatus, Is.EqualTo(facultyEmploymentStatus));
        }

        [Then]
        public void ThenTheFacultyEmploymentCategoryIs_FACULTYEMPLOYMENTCATEGORY(string facultyEmploymentCategory)
        {
            var actualFacultyEmploymentCategory = FacultyProfilePage.AcademicQualifications.FacultyEmploymentCategory;
            Assert.That(actualFacultyEmploymentCategory, Is.EqualTo(facultyEmploymentCategory));
        }

        [Then]
        public void ThenTheFacultyYearsAtETSUIs_YEARS(string years)
        {
            var actualYears = FacultyProfilePage.AcademicQualifications.Years;
            Assert.That(actualYears, Is.EqualTo(years));
        }

        [Then]
        public void ThenTheFacultyTenureStatusIs_FACULTYTENURESTATUS(string facultyTenureStatus)
        {
            var actualFacultyTenureStatus = FacultyProfilePage.AcademicQualifications.FacultyTenureStatus;
            Assert.That(actualFacultyTenureStatus, Is.EqualTo(facultyTenureStatus));
        }
    }
}
