﻿using System;
using FTCSTestFramework;
using TechTalk.SpecFlow;
using NUnit.Framework;

namespace FTCS_Features.Steps
{
    [Binding]
    public class LoginSteps
    {
        [AfterScenario("Login")]
        public void ScenarioTearDown()
        {
            LoginPage.Wait();
            LoginPage.Quit();
        }

        [Given]
        public void GivenIAmOnTheLoginPage()
        {
            LoginPage.Initialize();
            LoginPage.GoToPage();     
        }
        
        [Given]
        public void GivenIHaveEntered_USERNAME_AsTheUsername(string username)
        {
            LoginPage.EnterUsername(username);
        }
        
        [Given]
        public void GivenIHaveEntered_PASSWORD_AsThePassword(string password)
        {
            LoginPage.EnterPassword(password);
        }
        
        [When]
        public void WhenIPressLogin()
        {
            LoginPage.ClickLogin();
            LoginPage.Wait();
        }
        
        [Then]
        public void ThenThe_USER_AccountIsDisplayedAtTheTopOfTheHomePage(string user)
        {
            string name, userName;
            name = LoginPage.GetAccountName();
            userName = ("Hello, " + user);
            Assert.That(userName, Is.EqualTo(name));
        }
        
        [Then]
        public void ThenAnErrorMessageSaying_ERROR_IsShown(string error)
        {
            string error2;
            error2 = LoginPage.GetErrorMessage();
            Assert.That(error2, Is.EqualTo(error));
        }
        
        [Then]
        public void ThenTwoErrorMessagesShouldBeDisplayedSaying_ERROR1_And_ERROR2(string error1, string error2)
        {
            string userError, passwordError;
            userError = LoginPage.GetUserError();
            passwordError = LoginPage.GetPasswordError();
            Assert.That(userError, Is.EqualTo(error1));
            Assert.That(passwordError, Is.EqualTo(error2));
        }
    }
}
