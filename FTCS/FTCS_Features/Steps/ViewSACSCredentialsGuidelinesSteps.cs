﻿using FTCS_Features.FeatureTests;
using PageLibrary.Pages;
using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewSACSCredentialsGuidelinesSteps:BasicSteps
    {
        [Given(@"I am on the home page")]
        public void GivenIAmOnTheHomePage()
        {
            CurrentPage = PageBase.LoadHomePage(driver,"http://localhost:62491/");
        }
        
        [Given(@"there is a link to displaying text that matches SACS Credential Guidelines")]
        public void GivenThereIsALinkToDisplayingTextThatMatchesSACSCredentialGuidelines()
        {
           CurrentPage = CurrentPage.As<HomePage>.Navigateto;
        }
        
        [When(@"I click on the  SACS Credential Guidelines link")]
        public void WhenIClickOnTheSACSCredentialGuidelinesLink()
        {
          CurrentPage = CurrentPage.As<HomePage>.Navigateto ;
        }
        
        [Then(@"I will be presented with a  outlining the SACS Credential Guidelines")]
        public void ThenIWillBePresentedWithAOutliningTheSACSCredentialGuidelines()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
