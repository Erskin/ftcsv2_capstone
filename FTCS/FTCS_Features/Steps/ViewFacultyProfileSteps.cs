﻿using System;
using TechTalk.SpecFlow;

namespace FTCS_Features.Steps
{
    [Binding]
    public class ViewFacultyProfileSteps
    {
        [Given]
        public void GivenIAmOnFacultyProfilePageOfBridgesLakenGrace()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given]
        public void GivenIAmOnFacultyProfilePageOfLoveGregoryRay()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given]
        public void GivenIAmOnFacultyProfilePageOfBarrettMartinL()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given]
        public void GivenIAmOnFacultyProfilePageOfPittareseAnthonyD()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When]
        public void WhenIClickOnAcademicQualificationsTab()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When]
        public void WhenIClickOnProfessionalQualificationsTab()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When]
        public void WhenIClickOnCoursesTaughtTab()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When]
        public void WhenIClickOnCoordainatorInformation()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeRankAsNonStandardRank()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDateOfRankAsNone()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDateEligibleForNextRankAsNone()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeGraduateFacultyStatusAsNoStatus()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeEmploymentStatusAsPartTime()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeEmploymentCategoryAsAdjunctFaculty_P0_Month(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeYearsAtETSUAs_P0(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeTenureStatusAsIneligible()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDegreeAsMasterOfFineArts()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeInitialsAsMFA()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDisciplineAsVisualArts()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeInstitutionAsClemsonUniversity()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeTranscriptOnFileAsYes()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDescriptionOfFacultySProfessionalQualificationUnderNarrative()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeRelatedWorkProfessionalExperienceHeader()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeePlaceAsAbbottLaboratories()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeePositionAsResearchInvestigator()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeResponsibilitiesAsCarriedOutDutiesDescribedInTheNarrativeStatement()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenDateStartedAsAugust_P0(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenDateEndedAsJune_P0(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeCurrentCoursesTaught_P0_AsHeader(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeCourseIDAsCSCI_P0(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeTitleAsSoftwareEngineeringII()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDescriptionAsPrerequisiteCSCI_P0_SoftwareDevelopmentAsAnEngineeringDisciplineWithEmphasisOnDetailedDesignImplementationTestingMaintenanceProjectManagementVerificationAndValidationConfigurationManagementAndSoftwareQualityAssuranceCommunicationsWrittenAndOralLegalProfessionalEthicalIssuesParticipationOnTeamProjectsAndUseOfAutomatedToolsAreIntegral(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeSemesterAsSpring_P0(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeCertificationAsAcademic()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeViewCourseHistoryLink()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeViewAllCoursesTaughtByFacultyTabAtTheBottomOfThePage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDegreeNameAsComputerInfoScienceMS()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeLevelAsMastersDegreeProgram()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDegreeAsMS()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeHowQualifiedAsUnspecified()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDateStartedAsSummer_P0(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then]
        public void ThenICanSeeDateEndedAsNA()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
