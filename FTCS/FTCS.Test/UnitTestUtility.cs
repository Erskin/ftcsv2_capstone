﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace FTCS.Test
{
    public static class UnitTestUtility
    {
        public static void InitDBSet<T>(IQueryable<T> mockList, Mock<DbSet<T>> dbSet) where T : class
        {
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(mockList.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(mockList.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(mockList.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(mockList.GetEnumerator());
        }

        public static ClaimsIdentity GenerateClaimsIdentity(string userId, string role, string department, string college)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Role, role),
                new Claim(ClaimTypes.NameIdentifier, userId),
                new Claim("Department",department),
                new Claim("College", college)
            };
            return new ClaimsIdentity(claims);
        }
    }
}
