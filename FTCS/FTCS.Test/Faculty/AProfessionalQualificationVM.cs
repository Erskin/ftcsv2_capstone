﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FTCS.Models;
using FTCS.ViewModels.FacultyProfile;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace FTCS.Test.Faculty
{
    [TestFixture]
    class AProfessionalQualificationVM
    {
        [Test]
        public void ShouldAddAListOfRelatedWorkExperienceIfThereIsAtLeastOneRelatedWorkExperience()
        {
            ICollection<RELATED_WORK_EXPERIENCE> workExperiences = new List<RELATED_WORK_EXPERIENCE>()
            {
                new RELATED_WORK_EXPERIENCE()
                {
                    PLACE = "Place",
                    POSITION = "Position",
                    START_MONTH = 1,
                    END_MONTH = 1,
                    START_YEAR = 2017,
                    END_YEAR = 2017
                }
            };
            var sut = new ProfessionalQualificationsVM();
            sut.AddRelatedWorkExperience(workExperiences);
            Assert.That(sut.RelatedWorkExperience.First().Place, Is.EqualTo("Place"));
        }

        [Test]
        public void ShouldAddAListOfLicensesIfThereIsAtLeastOneLicense()
        {
            ICollection<LICENSE> licenses = new List<LICENSE>()
            {
                new LICENSE()
                {
                    AWARD_MONTH = 1,
                    AWARD_YEAR = 2017,
                    AWARDED_BY = "Tom",
                    EXPIRE_MONTH = 1,
                    EXPIRE_YEAR = 2017,
                    NAME = "Name"
                }
            };
            var sut = new ProfessionalQualificationsVM();
            sut.AddLicenses(licenses);
            Assert.That(sut.License.FirstOrDefault().Name, Is.EqualTo("Name"));
        }

        [Test]
        public void ShouldHaveAListOfRelatedWorkExperienceOfSize0IfNoneAreFound()
        {
            ICollection<RELATED_WORK_EXPERIENCE> workExperiences = new List<RELATED_WORK_EXPERIENCE>();
            var sut = new ProfessionalQualificationsVM();
            sut.AddRelatedWorkExperience(workExperiences);
            Assert.That(sut.RelatedWorkExperience.Count(), Is.EqualTo(0));
        }
    }
}
