﻿using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using FTCS.Controllers;
using FTCS.Services.Implementation;
using FTCS.ViewModels.Course;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Reports;

namespace FTCS.Test
{
    [Category("Unit")]
    [TestFixture]
    public class AFacultyController
    {
        private Mock<IFacultyService> _mockFacultyService;
        private Mock<IDepartmentService> _mockDepartmentService;
        private Mock<ICourseService> _mockCourseService;

        [SetUp]
        public void Setup()
        {
            _mockFacultyService = new Mock<IFacultyService>();
            _mockDepartmentService = new Mock<IDepartmentService>();
            _mockCourseService = new Mock<ICourseService>();
        }

        [Test]
        public void ShouldDisplayAListOfFacultyWhenIndexIsCalled()
        {
            _mockFacultyService.Setup(mock => mock.GetListOfFaculty("", "", "", "")).Returns(new List<FacultyVM>());
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object,null);

            sut.Index("", "", "", "", 1);

            _mockFacultyService.Verify(mock => mock.GetListOfFaculty("", "", "", ""), Times.Once);
        }

        [Test]
        public void ShouldSendInDefaultsIntoGetListOfFaculty()
        {
            _mockFacultyService.Setup(m => m.GetListOfFaculty(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>())).Returns(new List<FacultyVM>());
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object,null);
            sut.Index(null, null, null, null, null);
            _mockFacultyService.Verify(m => m.GetListOfFaculty("All", null, "NAME_asc", null), Times.Once);
        }


        [Test]
        public void ShouldDisplayAListOfFacultyInADepartmentWhenClicked()
        {
            ;
            var expectedDepartment = "";
            _mockFacultyService.Setup(mock =>
                    mock.GetListOfFaculty(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()))
                .Returns(new List<FacultyVM>())
                .Callback(
                    (string letter, string search, string sort, string department) => expectedDepartment = department);
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object,null);
            sut.Index("", "ing", "", "ACAF", 1);
            _mockFacultyService.Verify(mock =>
                        mock.GetListOfFaculty(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), "ACAF"),
                Times.Once);
            Assert.That(expectedDepartment, Is.EqualTo(expectedDepartment));
        }


        [Test]
        public void ShouldDisplayAcademicInfoWhenFacultyIsClicked()
        {
            var id = "E000123";
            var department = "ACAF";
            var routeData = new RouteData();
            routeData.Values.Add("action", "Academic");
            var mockControllerContext = new Mock<ControllerContext>();
            mockControllerContext.SetupGet(m => m.RouteData).Returns(routeData);
            _mockFacultyService.Setup(mock => mock.GetAcademicOfFaculty(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new AcademicQualificationsVM()
                {
                    Id = id,
                    Department = department
                });
            _mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>())).Returns(true);
            _mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>())).Returns(true);
            var facultyMember = new FacultyVM()
            {
                BANNER_ID = id,
                DepartmentCode = department
            };
            _mockFacultyService.Setup(m => m.GetFacultyById(id)).Returns(facultyMember);
            _mockFacultyService.Setup(m => m.GetDepartmentOfFacultyById(id)).Returns(facultyMember.DepartmentCode);
            _mockFacultyService.Setup(
                    m =>
                        m.GetListOfFaculty(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                            It.IsAny<string>()))
                .Returns(new List<FacultyVM>() {facultyMember});
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object,null) {ControllerContext = mockControllerContext.Object };
            sut.Academic(id, "ACAF");
            _mockFacultyService.Verify(mock => mock.GetAcademicOfFaculty(id, department), Times.Once);
        }

        [Test]
        public void
            ShouldCallGetListOfFacultySelectListWithTheDefaultSortOrderAndDepartmentCodeWhenIDAndDepartmentParamsAreNull
            ()
        {
            _mockFacultyService.Setup(m => m.GetListOfFacultySelectListItems(It.IsAny<string>(), It.IsAny<string>()));
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object,null);
            sut.FacultyProfileFilter(null, null);
            _mockFacultyService.Verify(m => m.GetListOfFacultySelectListItems("NAME_asc", "ACAF"), Times.Once);
        }

        [Test]
        public void ShouldCallGetListOfFacultySelectListWithParamsProvided()
        {
            _mockFacultyService.Setup(m => m.GetListOfFacultySelectListItems(It.IsAny<string>(), It.IsAny<string>()));
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object,null);
            sut.FacultyProfileFilter("E001", "CSCI");
            _mockFacultyService.Verify(m => m.GetListOfFacultySelectListItems("NAME_asc", "CSCI"), Times.Once);
        }

        [Test]
        public void ShouldCallGetPreviousAndNextFaculty()
        {
            _mockFacultyService.Setup(m => m.GetPreviousAndNextFaculty(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<FacultyVM>() {new FacultyVM(), new FacultyVM(), new FacultyVM()});
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object);
            sut.FacultyDepartmentNavigation("E001", "CSCI", "Academic");
            _mockFacultyService.Verify(m => m.GetPreviousAndNextFaculty("E001", "CSCI"), Times.Once);
        }


        [Test]
        public void ShouldRedirectToFirstFacultyInDepartmentIfIdIsValidButNotInDepartment()
        {
            var routeData = new RouteData();
            routeData.Values.Add("action", "Academic");
            var mockControllerContext = new Mock<ControllerContext>();
            mockControllerContext.Setup(m => m.RouteData).Returns(routeData);
            var mockFacultyService = new Mock<IFacultyService>();
            var mockCourseService = new Mock<ICourseService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            mockControllerContext.SetupGet(m => m.RouteData).Returns(routeData);
            mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>())).Returns(true);
            mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>())).Returns(true);
            mockFacultyService.Setup(m => m.FirstFacultyOnInvalidParameters(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new FacultyVM() {BANNER_ID = "E001",DepartmentCode = "CSCI"});
            var sut = new FacultyController(mockFacultyService.Object, mockDepartmentService.Object,
               mockCourseService.Object);
            sut.ControllerContext = mockControllerContext.Object;
            var result = sut.Academic("E002", "CSCI") as RedirectToRouteResult;
            Assert.That(result.RouteValues["id"] as string, Is.EqualTo("E001"));
        }

        [Test]
        public void ShouldReturnTheCoursesTaughtViewIfAYearParamDoesntExist()
        {
            var expectedAcademicYear = AcademicYear.GetCurrentAcademicYearString();
            var mockControllerContext = new Mock<ControllerContext>();
            var routeData = new RouteData();
            routeData.Values.Add("action", "CoursesTaught");
            mockControllerContext.Setup(m => m.RouteData).Returns(routeData);
            _mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>())).Returns(true);
            _mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>())).Returns(true);
            _mockCourseService.Setup(m => m.GetListOfCoursesByTeacher(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<CourseCredentialVM>() {new CourseCredentialVM()});
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object) {ControllerContext = mockControllerContext.Object};
            var result = sut.CoursesTaught("E001", "CSCI", null, 1) as PartialViewResult;
            Assert.That(result.ViewName, Is.EqualTo("_CoursesTaught"));
        }

        [TestCase(null, ExpectedResult = "_CoursesTaught")]
        [TestCase("Current", ExpectedResult = "_CoursesTaught")]
        [TestCase("All", ExpectedResult = "_AllCoursesTaught")]
        public string ShouldReturnCorrectCourseTaughtViewOnParam(string testValue)
        {
            var expectedAcademicYear = AcademicYear.GetCurrentAcademicYearString();
            var mockControllerContext = new Mock<ControllerContext>();
            var routeData = new RouteData();
            routeData.Values.Add("action", "CoursesTaught");
            mockControllerContext.Setup(m => m.RouteData).Returns(routeData);
            _mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>())).Returns(true);
            _mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>())).Returns(true);
            _mockCourseService.Setup(m => m.GetListOfCoursesByTeacher(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<CourseCredentialVM>() { new CourseCredentialVM() });
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object)
            { ControllerContext = mockControllerContext.Object };
            var result = sut.CoursesTaught("E001", "CSCI", testValue, 1) as PartialViewResult;
            return result.ViewName;
        }

        [Test]
        public void ShouldExpectGetListOfCoursesByTeacherToHaveBeenCalled()
        {
            _mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>())).Returns(true);
            _mockFacultyService.Setup(m => m.GetDepartmentOfFacultyById(It.IsAny<string>())).Returns("CSCI");
            _mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>())).Returns(true);
            _mockCourseService.Setup(m => m.GetListOfCoursesByTeacher(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<CourseCredentialVM>() {new CourseCredentialVM()});
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object);
            var result = (sut.CoursesTaught("E001", "CSCI", null, 1) as ViewResult).Model as FacultyDetailVM;
            _mockCourseService.Verify(m => m.GetListOfCoursesByTeacher("E001", "Current"));
        }

        [Test]
        public void ShouldCallGetListOfFacultyThatHaveTaughtInFacultyService()
        {
            _mockFacultyService.Setup(
                    m => m.GetListOfFacultyTeachingCourse(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<FacultyCourseVM> {new FacultyCourseVM()});
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object);
            sut.FacultyWhoHaveTaught("CSCI", "1100", "NAME_asc");
            _mockFacultyService.Verify(m => m.GetListOfFacultyTeachingCourse("CSCI", "1100", "NAME_asc"));
        }

        [Test]
        public void ShouldRedirectToAcademicIfFacultyHasNoNarrativeOrProfessionalWorkExperience()
        {
            var mockControllerContext = new Mock<ControllerContext>();
            var routeData = new RouteData();
            routeData.Values.Add("action", "Professional");
            mockControllerContext.Setup(m => m.RouteData).Returns(routeData);
            _mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>())).Returns(true);
            _mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>())).Returns(true);
            _mockFacultyService.Setup(m => m.GetProfessionalOfFaculty(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new ProfessionalQualificationsVM {RelatedWorkExperience = new List<RelatedWorkExperienceVM>()});
            _mockFacultyService.Setup(m => m.GetDepartmentOfFacultyById(It.IsAny<string>())).Returns("CSCI");
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object) {ControllerContext = mockControllerContext.Object};
            var result = (sut.Professional("E001", "CSCI")) as RedirectToRouteResult;
            Assert.That(result.RouteValues["action"], Is.EqualTo("Academic"));
        }


        [Test]
        public void ShouldSetTheCoursesTaughtVMToHaveTheCurrentAcademicYearWhenCurrent()
        {
            var expectedAcademicYear = AcademicYear.GetCurrentAcademicYearString();
            var mockControllerContext = new Mock<ControllerContext>();
            var routeData = new RouteData();
            routeData.Values.Add("action","CoursesTaught");
            mockControllerContext.Setup(m => m.RouteData).Returns(routeData);
            _mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>())).Returns(true);
            _mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>())).Returns(true);
            _mockCourseService.Setup(m => m.GetListOfCoursesByTeacher(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<CourseCredentialVM>() {new CourseCredentialVM()});
            var sut = new FacultyController(_mockFacultyService.Object, _mockDepartmentService.Object,
                _mockCourseService.Object);
            var result = (sut.CoursesTaught("E001", "CSCI", "Current", 1) as ViewResult).Model as FacultyDetailVM;
            Assert.That(result.CoursesTaught.AcademicYearTitle, Is.EqualTo(expectedAcademicYear));
        }

        [Test]
        public void ShouldRedirectOnFirstFacultyOnInvalidParams()
        {
            var id = "E001";
            var department = "CSCI";
            var mockControllerContext = new Mock<ControllerContext>();
            var routeData = new RouteData();
            routeData.Values.Add("action", "CoursesTaught");
            mockControllerContext.Setup(m => m.RouteData).Returns(routeData);
            var mockFacultyService = new Mock<IFacultyService>();
            var mockCourseService = new Mock<ICourseService>();
            var mockDepartmentService= new Mock<IDepartmentService>();
            mockFacultyService.Setup(m => m.IsFacultyIdValid(It.IsAny<string>()))
                .Returns(true);
            mockFacultyService.Setup(m => m.HasProfessionalQualifications(It.IsAny<string>()))
                .Returns(true);
            mockFacultyService.Setup(m => m.IsFacultyCoordinator(It.IsAny<string>()))
                .Returns(true);
            mockDepartmentService.Setup(m => m.IsDepartmentValid(It.IsAny<string>()))
                .Returns(true);
            mockFacultyService.Setup(m => m.GetAcademicOfFaculty(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new AcademicQualificationsVM() {Id = id, Department = department});
            var sut = new FacultyController(mockFacultyService.Object,
                mockDepartmentService.Object,mockCourseService.Object) {ControllerContext = mockControllerContext.Object};
            var result = sut.Academic(id, department);
            mockFacultyService.Verify(m => m.FirstFacultyOnInvalidParameters(id, department));
        }

       
    }
}
