﻿using System;
using FTCS.Models;
using FTCS.Services.Implementation;
using Moq;
using NUnit.Framework;
using FTCS.Services;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Web;
using FTCS.Services.Exceptions;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.FacultyProfile;

namespace FTCS.Test
{
    [Category("Unit")]
    public class AFacultyServices
    {
        Mock<DbSet<TEACHER>> _mockTeachers;
        private Mock<DbSet<TEACHER_TYPE>> _mockTeacherType;
        Mock<DbSet<DEPARTMENT>> _mockDepartments;
        private Mock<DbSet<TEACHER_COURSE_RECORD>> _mockTeacherCourseRecords;
        private Mock<FTCSEntities> _mockContext;
        private Semester currentSemester;
        private Mock<DbSet<COURSE>> _mockCourses;


        [SetUp]
        public void SetUp()
        {
            _mockDepartments = new Mock<DbSet<DEPARTMENT>>();
            _mockTeachers = new Mock<DbSet<TEACHER>>();
            _mockTeacherType = new Mock<DbSet<TEACHER_TYPE>>();
            _mockTeacherCourseRecords = new Mock<DbSet<TEACHER_COURSE_RECORD>>();
            _mockCourses = new Mock<DbSet<COURSE>>();
            currentSemester = AcademicYear.GetCurrentAcademicSemester();
            var mockDepartments = new List<DEPARTMENT>
            {
                new DEPARTMENT {CODE = "CHEM", NAME = "Chemistry", COLLEGE = "COLLEGE1"},
                new DEPARTMENT {CODE = "CSCI", NAME = "Computer Science", COLLEGE = "COLLEGE2"},
                new DEPARTMENT {CODE = "ARTA", NAME = "Art Studies", COLLEGE = "COLLEGE3"}
            }.AsQueryable();
            RANK_TYPE rankType1 = new RANK_TYPE() { ID = 0, RANK = "ASSOCIATE_PROFESSOR" };
            var facultyForTeacherA = new FACULTY()
            {
                RANK_TYPE = rankType1,
                CURRENT_RANK_MONTH = 8,
                CURRENT_RANK_YEAR = 2016,
                NEXT_RANK_MONTH = 1,
                NEXT_RANK_YEAR = 2016,
                GRADUATE_FACULTY_STATUS_TYPE = new GRADUATE_FACULTY_STATUS_TYPE() { CODE = "0", NAME = "Professor" },
                TENURE_STATUS = "Eligible"
            };
            ICollection<ACADEMIC_DEGREE> academicDegreeForTeacherA = new List<ACADEMIC_DEGREE>()
            {
                new ACADEMIC_DEGREE()
                {
                    LEVEL = 0,
                    NAME = "DegreeName",
                    INITIALS = "BS",
                    DISCIPLINE = "BS",
                    INSTITUTION = "College Name",
                    HAS_TRANSCRIPT = 1
                }
            };
            ICollection<GRADUATE_TRAINING> graduateTrainingForTeacherA = new List<GRADUATE_TRAINING>()
            {
                new GRADUATE_TRAINING() {DISCIPLINE = "BS", SEMESTER_HOURS = 12}
            };
            var courseForTeacherC = new COURSE()
            {
                DEPARTMENT1 = mockDepartments.First(d => d.CODE == "ARTA"),
                DEPARTMENT = "ARTA",
                RUBRIC = "ARTA",
                NUMBER = "101"
            };
            var courseForTeacherA = new COURSE()
            {
                DEPARTMENT1 = mockDepartments.First(d => d.CODE == "CHEM"),
                DEPARTMENT = "CHEM",
                RUBRIC = "CHEM",
                NUMBER = "101"
            };
            var courseForTeacherB = new COURSE()
            {
                DEPARTMENT1 = mockDepartments.First(d => d.CODE == "CSCI"),
                DEPARTMENT = "CSCI",
                RUBRIC = "CSCI",
                NUMBER = "101"
            };
            var teacherCCourseRecord = new TEACHER_COURSE_RECORD()
            {
                ACADEMIC_YEAR = currentSemester.Year,
                ACADEMIC_MONTH = currentSemester.Month,
                COURSE_RUBRIC = "ARTA",
                COURSE_NUMBER = "101"
            };
            var teacherBCourseRecord = new TEACHER_COURSE_RECORD()
            {
                ACADEMIC_YEAR = currentSemester.Year,
                ACADEMIC_MONTH = currentSemester.Month,
                COURSE_NUMBER = "101",
                COURSE_RUBRIC = "CSCI"
            };

            var teacherACourseRecord = new TEACHER_COURSE_RECORD()
            {
                ACADEMIC_YEAR = currentSemester.Year,
                ACADEMIC_MONTH = currentSemester.Month,
                COURSE_NUMBER = "101",
                COURSE_RUBRIC = "CHEM"
            };
            var teacherA = new TEACHER
            {
                HOME_DEPARTMENT = "CHEM",
                FIRST_NAME = "TeacherA",
                LAST_NAME = "ALast",
                BANNER_ID = "E000001",
                MIDDLE_INITIAL = "K",
                TITLE = "TEACHER",
                FACULTY = facultyForTeacherA,
                YEARS_TEACHING = 10,
                ACADEMIC_DEGREE = academicDegreeForTeacherA,
                GRADUATE_TRAINING = graduateTrainingForTeacherA,
                EMAIL_ADDRESS = "example.com",
                EMPLOYMENT_TYPE = 0,
                TEACHER_TYPE1 = new TEACHER_TYPE() { NAME = "Faculty" }
            };
            var teacherB = new TEACHER
            {
                HOME_DEPARTMENT = "CHEM",
                FIRST_NAME = "TeacherB",
                LAST_NAME = "LastB",
                BANNER_ID = "E000002",
                MIDDLE_INITIAL = "K",
                TITLE = "TEACHER",
                EMAIL_ADDRESS = "example.com",
                TEACHER_TYPE1 = new TEACHER_TYPE() { NAME = "Faculty" }
            };
            var teacherC = new TEACHER
            {
                HOME_DEPARTMENT = "CHEM",
                FIRST_NAME = "TeacherC",
                LAST_NAME = "LastC",
                BANNER_ID = "E000003",
                MIDDLE_INITIAL = "K",
                TITLE = "TEACHER",
                NARRATIVE = new NARRATIVE() { NARRATIVE1 = "Narrative1" },
                EMAIL_ADDRESS = "example.com",
                TEACHER_TYPE1 = new TEACHER_TYPE() { NAME = "Faculty" }
            };
            var mockTeachers = new List<TEACHER>
            {
                teacherA,
                teacherB,
                teacherC
            }.AsQueryable();

            var mockTeacherCourseRecords = new List<TEACHER_COURSE_RECORD>()
            {
                teacherCCourseRecord,
                teacherBCourseRecord,
                teacherACourseRecord,
                new TEACHER_COURSE_RECORD()
                {
                    COURSE_NUMBER = "102",
                    COURSE_RUBRIC = "CSCI",
                    ACADEMIC_YEAR = currentSemester.Year,
                    ACADEMIC_MONTH = currentSemester.Month,
                    TEACHER = new TEACHER()
                    {
                        BANNER_ID = "E001",
                        HOME_DEPARTMENT = "CHEM",
                        FIRST_NAME = "John",
                        LAST_NAME = "Smith",
                        MIDDLE_INITIAL = "K",
                        TITLE = "TEACHER",
                        EMAIL_ADDRESS = "example.com",
                        TEACHER_TYPE1 = new TEACHER_TYPE() {NAME = "Faculty"}
                    }
                },
                new TEACHER_COURSE_RECORD()
                {
                    COURSE_NUMBER = "103",
                    COURSE_RUBRIC = "CSCI",
                    ACADEMIC_YEAR = currentSemester.Year,
                    ACADEMIC_MONTH = currentSemester.Month,
                    TEACHER = new TEACHER()
                    {
                        BANNER_ID = "E002",
                        HOME_DEPARTMENT = "CHEM",
                        FIRST_NAME = "John1",
                        LAST_NAME = "Smith1",
                        MIDDLE_INITIAL = "K",
                        TITLE = "TEACHER",
                        EMAIL_ADDRESS = "example.com",
                        TEACHER_TYPE1 = new TEACHER_TYPE() {NAME = "Faculty"}
                    }
                }
            }.AsQueryable();

            var mockCourses = new List<COURSE>()
            {
                courseForTeacherA,
                courseForTeacherB,
                courseForTeacherC,
                new COURSE()
                {
                    DEPARTMENT1 = mockDepartments.First(d => d.CODE == "CSCI"),
                    DEPARTMENT = "CSCI",
                    RUBRIC = "CSCI",
                    NUMBER = "102"
                },
                new COURSE()
                {
                    DEPARTMENT1 = mockDepartments.First(d => d.CODE == "CSCI"),
                    DEPARTMENT = "CSCI",
                    RUBRIC = "CSCI",
                    NUMBER = "103"
                }
            }.AsQueryable();

            teacherACourseRecord.TEACHER = teacherA;
            teacherBCourseRecord.TEACHER = teacherB;
            teacherCCourseRecord.TEACHER = teacherC;


            _mockContext = new Mock<FTCSEntities>();
            UnitTestUtility.InitDBSet(mockTeachers, _mockTeachers);
            UnitTestUtility.InitDBSet(mockDepartments, _mockDepartments);
            UnitTestUtility.InitDBSet(mockTeacherCourseRecords, _mockTeacherCourseRecords);
            UnitTestUtility.InitDBSet(mockCourses, _mockCourses);
            _mockContext.Setup(m => m.TEACHERs).Returns(_mockTeachers.Object);
            _mockContext.Setup(m => m.DEPARTMENTs).Returns(_mockDepartments.Object);
            _mockContext.Setup(m => m.COURSEs).Returns(_mockCourses.Object);
            _mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_mockTeacherCourseRecords.Object);
        }


        [Test]
        public void ShouldReturnTheEntireListOfFacultyWhenALlAndNoSearchTermIsPassedIn()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFaculty("All", "", "", "");
            Assert.That(result.Count(), Is.EqualTo(5));
        }

        [Test]
        public void ShouldOnlyReturnFacultyThatWhoseLastNameStartWithA()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFaculty("A", "", "", "");
            Assert.That(result.Count(), Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().LAST_NAME, Is.EqualTo("ALast"));
        }

        [Test]
        public void ShouldOnlyReturnFacultyWhoseNameContainsTheSearchTerm()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFaculty("", "tB", "", "");
            Assert.That(result.Count(), Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().LAST_NAME, Is.EqualTo("LastB"));
        }

        [Test]
        public void ShouldOnlyReturnFacultyInAscendingOrderByLastName()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFaculty("All", "", "NAME_asc", "");
            Assert.That(result, Is.Ordered.By("LAST_NAME"));
        }

        [Test]
        public void ShouldOnlyReturnFacultyInDescendingOrderByLastName()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFaculty("All", "", "NAME_desc", "");

            Assert.That(result, Is.Ordered.By("LAST_NAME").Descending);
        }

        [Test]
        public void ShouldReturnAListOfAllFacultyMembersInADepartmentGreaterThan0()
        {
            //In test data, there are three teachers teaching in CSCI
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFaculty(null, "", "NAME_asc", "CSCI");
            //Assert!!
            Assert.That(result.Count(), Is.EqualTo(3));
        }

        [Test]
        public void ShouldReturnAListOfAllFacultyTypes()
        {
            var teacherType1 = new TEACHER_TYPE { ID = 1 };
            var teacherType2 = new TEACHER_TYPE { ID = 2 };
            var teacherType3 = new TEACHER_TYPE { ID = 3 };
            var teacherType4 = new TEACHER_TYPE { ID = 0 };
            var teacherTypes = new List<TEACHER_TYPE>()
            {
                teacherType1,
                teacherType2,
                teacherType3,
                teacherType4
            }.AsQueryable();

            var mockContext = new Mock<FTCSEntities>();
            UnitTestUtility.InitDBSet(teacherTypes, _mockTeacherType);
            mockContext.Setup(m => m.TEACHERs).Returns(_mockTeachers.Object);
            mockContext.Setup(m => m.TEACHER_TYPE).Returns(_mockTeacherType.Object);

            var sut = new FacultyService(mockContext.Object);

            var results = sut.GetFacultyTypes().Select(vm => vm.Id).ToArray();
            CollectionAssert.DoesNotContain(results, 3);
        }

        [Test]
        public void ShouldGetAcademicQualificationsOfAFacultyMember()
        {
            var sut = new FacultyService(_mockContext.Object);
            var id = "E000001";
            var departmentCode = "CSCI";
            var academicQualifications = sut.GetAcademicOfFaculty(id, departmentCode);
            Assert.That(academicQualifications, Is.TypeOf(typeof(AcademicQualificationsVM)));
            Assert.That(academicQualifications.EmploymentStatus, Is.Not.Null);
        }

        [Test]
        public void ShouldEnsureRankIsInTitleCase()
        {
            var sut = new FacultyService(_mockContext.Object);
            var id = "E000001";
            var departmentCode = "CSCI";
            var academicQualifications = sut.GetAcademicOfFaculty(id, departmentCode);
            Assert.That(academicQualifications.Rank, Does.Not.Contain("_"));
            Assert.That(academicQualifications.Rank, Does.StartWith("A"));
        }

        [Test]
        public void ShouldEnsureStatusIsFulltimeIfFacultyTypeIs0()
        {
            var sut = new FacultyService(_mockContext.Object);
            var id = "E000001";
            var departmentCode = "CSCI";
            var academicQualifications = sut.GetAcademicOfFaculty(id, departmentCode);
            Assert.That(academicQualifications.EmploymentStatus, Is.EqualTo("Full Time"));
        }

        [Test]
        public void ShouldGetAListOfFacultyTeachingACourse()
        {
            var teacher = new TEACHER()
            {
                LAST_NAME = "LastName",
                FIRST_NAME = "FirstName",
                MIDDLE_INITIAL = "",
                TEACHER_COURSE_RECORD = new List<TEACHER_COURSE_RECORD>()
                {
                    new TEACHER_COURSE_RECORD()
                    {
                        COURSE_NUMBER = "1000",
                        COURSE_RUBRIC = "CSCI",
                        COURSE = new COURSE()
                        {
                            DEPARTMENT1 = new DEPARTMENT()
                            {
                                NAME = "CSCI",
                                COLLEGE = "CollegeName",
                                COLLEGE1 = new COLLEGE()
                                {
                                    NAME = "CollegeName"
                                }
                            }
                        }
                    }
                }
            };
            var teachers = new List<TEACHER>()
            {
                teacher
            }.AsQueryable();
            UnitTestUtility.InitDBSet(teachers, _mockTeachers);
            _mockContext.Setup(m => m.TEACHERs).Returns(_mockTeachers.Object);
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFacultyTeachingCourse("CSCI", "1000", "NAME_asc");
            var expected = teacher.LAST_NAME + ", " + teacher.FIRST_NAME + " " + teacher.MIDDLE_INITIAL;
            Assert.That(result.First().NAME, Is.EqualTo(expected));
        }

        [Test]
        public void ShouldSetTheCurrentFacultyAsIdPassedIn()
        {
            /*
            Test Data:
            Setup method contains teacher LastB, Smith and Smith1, all teaching in the
            comp sci department. The faculty list based on department is in the order mentioned above.
            */

            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetPreviousAndNextFaculty("E000002", "CSCI");
            Assert.That(result.Count, Is.EqualTo(3));
            Assert.That(result.ElementAt(1).BANNER_ID, Is.EqualTo("E000002"));
            Assert.That(result.Last().BANNER_ID, Is.EqualTo("E001"));
        }

        [Test]
        public void ShouldHaveADepartmentCodeInTheProfessionalQualificationsObject()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetProfessionalOfFaculty("E000003", "CSCI");
            Assert.That(result.Department, Is.EqualTo("CSCI"));
        }

        [Test]
        public void ShouldHaveTheTeachersNameInTheProfessionalQualificationsObject()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetProfessionalOfFaculty("E000003", "CSCI");
            Assert.That(result.Name, Is.EqualTo("LastC, TeacherC K"));
        }

        [Test]
        public void ShouldHaveTheTeachersNarrativeInTheProfessionalQualificationsObject()
        {
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetProfessionalOfFaculty("E000003", "CSCI");
            Assert.That(result.Narrative, Is.EqualTo("Narrative1"));
        }

        [Test]
        public void ShouldReturnAListOfFacultTeachingACourseWithTheLastYearThatFacultyMemberTaughtTheCourse()
        {
            var department1 = new DEPARTMENT()
            {
                NAME = "CSCI",
                COLLEGE = "CollegeName",
                COLLEGE1 = new COLLEGE()
                {
                    NAME = "CollegeName"
                }
            };
            var course = new COURSE()
            {
                DEPARTMENT1 = department1
            };
            var teachers = new List<TEACHER>()
            {
                new TEACHER()
                {
                    LAST_NAME = "LastName",
                    FIRST_NAME = "FirstName",
                    TEACHER_COURSE_RECORD = new List<TEACHER_COURSE_RECORD>()
                    {
                        new TEACHER_COURSE_RECORD()
                        {
                            COURSE_NUMBER = "1000",
                            COURSE_RUBRIC = "CSCI",
                            COURSE = course,
                            ACADEMIC_MONTH = 1,
                            ACADEMIC_YEAR = 2018
                        },
                        new TEACHER_COURSE_RECORD()
                        {
                            COURSE_NUMBER = "1000",
                            COURSE_RUBRIC = "CSCI",
                            COURSE = course,
                            ACADEMIC_MONTH = 1,
                            ACADEMIC_YEAR = 2016
                        },
                        new TEACHER_COURSE_RECORD()
                        {
                            COURSE_NUMBER = "1000",
                            COURSE_RUBRIC = "CSCI",
                            COURSE = course,
                            ACADEMIC_MONTH = 1,
                            ACADEMIC_YEAR = 2015
                        }
                    }
                }
            }.AsQueryable();
            UnitTestUtility.InitDBSet(teachers, _mockTeachers);
            _mockContext.Setup(m => m.TEACHERs).Returns(_mockTeachers.Object);
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetListOfFacultyTeachingCourse("CSCI", "1000", "NAME_asc").First();
            Assert.That(result.LastSemesterTaught.ToString(), Is.EqualTo(new Semester(1, 2018).ToString()));
        }

        [Test]
        public void ShouldGetTheProfessionalQualificationsOfAFacultyMemberByIdAndDepartment()
        {
            var mockTeacherTable = new List<TEACHER>()
            {
                new TEACHER
                {
                    BANNER_ID = "E0123",
                    FIRST_NAME = "FirstName",
                    LAST_NAME = "LastName",
                    MIDDLE_INITIAL = "M",
                    NARRATIVE = new NARRATIVE {NARRATIVE1 = "Narrative"},
                    RELATED_WORK_EXPERIENCE = new List<RELATED_WORK_EXPERIENCE>()
                    {
                        new RELATED_WORK_EXPERIENCE()
                    }
                }
            }.AsQueryable();
            var mockTeacherDbSet = new Mock<DbSet<TEACHER>>();
            UnitTestUtility.InitDBSet(mockTeacherTable, mockTeacherDbSet);
            _mockContext.Setup(m => m.TEACHERs).Returns(mockTeacherDbSet.Object);
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetProfessionalOfFaculty("E0123", "CSCI");
            Assert.That(result.Id, Is.EqualTo("E0123"));
            Assert.That(result.RelatedWorkExperience, Is.Not.Null);
            Assert.That(result.Narrative, Is.EqualTo("Narrative"));
        }

        [Test]
        public void ShouldHaveAnEmptyNarrativeAndWorkExperienceIfTheTeacherHasNoProfessionalQualifications()
        {
            var mockTeacherTable = new List<TEACHER>()
            {
                new TEACHER
                {
                    BANNER_ID = "E0123",
                    FIRST_NAME = "FirstName",
                    LAST_NAME = "LastName",
                    MIDDLE_INITIAL = "M"
                }
            }.AsQueryable();
            var mockTeacherDbSet = new Mock<DbSet<TEACHER>>();
            UnitTestUtility.InitDBSet(mockTeacherTable, mockTeacherDbSet);
            _mockContext.Setup(m => m.TEACHERs).Returns(mockTeacherDbSet.Object);
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetProfessionalOfFaculty("E0123", "CSCI");
            Assert.That(result.Id, Is.EqualTo("E0123"));
            Assert.That(result.RelatedWorkExperience, Is.Empty);
            Assert.That(result.Narrative, Is.Null);
        }

        [Test]
        public void ShouldTheFirstFacultyOfTheCSCIDepartmentIfTheIdIsNull()
        {
            var department = "CSCI";
            var mockDepartmentService = new Mock<IDepartmentService>();
            mockDepartmentService.Setup(m => m.IsDepartmentValid(department)).Returns(false);
            var sut = new FacultyService(_mockContext.Object, mockDepartmentService.Object);
            var result = sut.FirstFacultyOnInvalidParameters(null, department);
            Assert.That(result?.BANNER_ID, Is.EqualTo("E000002"));

        }

        [Test]
        public void ShouldReturnADepartmentCodeIfAFacultyMemberIsPresent()
        {
            //faculty E001 teaches in the Computer Science Department
            var sut = new FacultyService(_mockContext.Object);
            var result = sut.GetDepartmentOfFacultyById("E001");
            Assert.That(result, Is.EqualTo("CSCI"));
        }

        [Test]
        public void ShouldReturnNullIfAFacultyMemberIsNotPresent()
        {
            var department = "CSCI";
            var mockDepartmentService = new Mock<IDepartmentService>();
            mockDepartmentService.Setup(m => m.IsDepartmentValid(department)).Returns(false);
            var sut = new FacultyService(_mockContext.Object, mockDepartmentService.Object);
            var result = sut.FirstFacultyOnInvalidParameters("E000002", department);
            Assert.That(result, Is.Null);
        }

        [Test]
        public void ShouldAddANewGraduateHour()
        {
            var mockGraduatehour = new Mock<DbSet<GRADUATE_TRAINING>>();
            var mockContext = new Mock<FTCSEntities>();
            var graduateHour = new List<GRADUATE_TRAINING>()
            {
                new GRADUATE_TRAINING()
                {
                    TEACHER_ID = "E0456",
                    DISCIPLINE = "Program2",
                    SEMESTER_HOURS = 200,
                    YEAR = 2010,
                    MONTH = 10
                }
            }.AsQueryable();

            UnitTestUtility.InitDBSet(graduateHour, mockGraduatehour);
            mockContext.Setup(m => m.GRADUATE_TRAINING).Returns(mockGraduatehour.Object);
            var mockFacultyService = new Mock<IFacultyService>();
            var sut = new FacultyService(mockContext.Object);
            var result = sut.AddGraduateHourOfFaculty("E01234", "program100", 1000);
            mockGraduatehour.Verify(m => m.Add(It.IsAny<GRADUATE_TRAINING>()), Times.Once);
        }

        [Test]
        public void ShouldRemoveGraduateHour()
        {
            var mockGraduatehour = new Mock<DbSet<GRADUATE_TRAINING>>();
            var mockContext = new Mock<FTCSEntities>();
            var graduateHour = new List<GRADUATE_TRAINING>()
            {
                new GRADUATE_TRAINING()
                {
                    TEACHER_ID = "E0456",
                    DISCIPLINE = "Program2",
                    SEMESTER_HOURS = 200,
                    YEAR = 2010,
                    MONTH = 10
                },
                new GRADUATE_TRAINING()
                {
                    TEACHER_ID = "E0001",
                    DISCIPLINE = "Program1",
                    SEMESTER_HOURS = 1200,
                    YEAR = 2017,
                    MONTH = 5
                }
            }.AsQueryable();

            UnitTestUtility.InitDBSet(graduateHour, mockGraduatehour);
            mockContext.Setup(m => m.GRADUATE_TRAINING).Returns(mockGraduatehour.Object);
            var mockFacultyService = new Mock<IFacultyService>();
            var sut = new FacultyService(mockContext.Object);
            sut.DeleteGraduateHourOfFaculty("E0456", "program2");
            mockGraduatehour.Verify(m => m.RemoveRange(It.IsAny<IEnumerable<GRADUATE_TRAINING>>()), Times.Once);             
            var updatedGraduateHour = sut.GetGraduateHour("E0456", "Program2");
            mockGraduatehour.Verify(m => m.Add(It.IsAny<GRADUATE_TRAINING>()), Times.Once);
        }

        [Test]
        public void ShouldUpdateGraduateHour()
        {
            var mockGraduatehour = new Mock<DbSet<GRADUATE_TRAINING>>();
            var mockContext = new Mock<FTCSEntities>();
            var graduateHour = new List<GRADUATE_TRAINING>()
            {
                new GRADUATE_TRAINING()
                {
                    TEACHER_ID = "E0456",
                    DISCIPLINE = "Program2",
                    SEMESTER_HOURS = 200,
                    YEAR = 2010,
                    MONTH = 10
                },
                new GRADUATE_TRAINING()
                {
                    TEACHER_ID = "E0001",
                    DISCIPLINE = "Program1",
                    SEMESTER_HOURS = 1200,
                    YEAR = 2017,
                    MONTH = 5
                }
            }.AsQueryable();

            UnitTestUtility.InitDBSet(graduateHour, mockGraduatehour);
            mockContext.Setup(m => m.GRADUATE_TRAINING).Returns(mockGraduatehour.Object);
            var mockFacultyService = new Mock<IFacultyService>();
            var sut = new FacultyService(mockContext.Object);
            sut.UpdateGraduteHourOfFaculty("E0456", "program2", 1001);
            mockGraduatehour.Verify(m => m.RemoveRange(It.IsAny<IEnumerable<GRADUATE_TRAINING>>()), Times.Once);
            var updatedGraduateHour = sut.GetGraduateHour("E0456", "Program2");
            mockGraduatehour.Verify(m => m.Add(It.IsAny<GRADUATE_TRAINING>()), Times.Once);
        }
    }
}
