﻿using FTCS.Models;
using FTCS.Services.Implementation;
using Moq;
using NUnit.Framework;
using FTCS.Services;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.FacultyProfile;

namespace FTCS.Test.Faculty
{
    [Category("Unit")]
    public class AProfessionalQualificationService
    {
        private Mock<FTCSEntities> _mockContext;

        [Test]
        public void ShouldAddNarrativeForNewID()
        {
            var mockNarrative = new Mock<DbSet<NARRATIVE>>();
            _mockContext = new Mock<FTCSEntities>();
            var narrative = new List<NARRATIVE>()
            {
                new NARRATIVE()
                {
                    TEACHER_ID = "E0001",
                    NARRATIVE1 = "this is test message1"
                },
                new NARRATIVE()
                {
                    TEACHER_ID = "E0002",
                    NARRATIVE1 = "this is test message2"
                }
            }.AsQueryable();

            UnitTestUtility.InitDBSet(narrative, mockNarrative);
            _mockContext.Setup(m => m.NARRATIVEs).Returns(mockNarrative.Object);
            mockNarrative.Setup(m => m.Add(It.IsAny<NARRATIVE>()));
            var mockProfessionalQualificationService = new Mock<IProfessionalQualificationService>();
            var sut = new ProfessionalQualificationService(_mockContext.Object);
            sut.AddNarrativeOfFaculty("E0003", "this is test message3");
            var updatedNarrative = sut.GetNarrative("E0003");
            mockNarrative.Verify(m => m.Add(It.IsAny<NARRATIVE>()), Times.Once);
        }

        [Test]
        public void ShouldAddNarrativeForExistID()
        {
            var mockNarrative = new Mock<DbSet<NARRATIVE>>();
            _mockContext = new Mock<FTCSEntities>();
            var narrative = new List<NARRATIVE>()
            {
                new NARRATIVE()
                {
                    TEACHER_ID = "E0001",
                    NARRATIVE1 = "this is test message1"
                },
                new NARRATIVE()
                {
                    TEACHER_ID = "E0002",
                    NARRATIVE1 = "this is test message2"
                }
            }.AsQueryable();

            UnitTestUtility.InitDBSet(narrative, mockNarrative);
            _mockContext.Setup(m => m.NARRATIVEs).Returns(mockNarrative.Object);
            mockNarrative.Setup(m => m.Add(It.IsAny<NARRATIVE>()));
            var mockProfessionalQualificationService = new Mock<IProfessionalQualificationService>();
            var sut = new ProfessionalQualificationService(_mockContext.Object);
            sut.AddNarrativeOfFaculty("E0001", "this is test message3");
            var updatedNarrative = sut.GetNarrative("E0003");
            mockNarrative.Verify(m => m.Add(It.IsAny<NARRATIVE>()), Times.Never);
        }
        [Test]
        public void ShouldAddRelatedWorkForNewID()
        {
            var mockRelatedWork = new Mock<DbSet<RELATED_WORK_EXPERIENCE>>();
            _mockContext = new Mock<FTCSEntities>();
            var relatedWork = new List<RELATED_WORK_EXPERIENCE>()
            {
                new RELATED_WORK_EXPERIENCE()
                {
                    TEACHER_ID = "E0001",
                    PLACE = "place1",
                    POSITION = "position1",
                    RESPONSIBILITIES = "responsibilities1",
                    START_MONTH = 1,
                    START_YEAR = 2000,
                    END_MONTH = 12,
                    END_YEAR = 2000
                },
                new RELATED_WORK_EXPERIENCE()
                {
                    TEACHER_ID = "E0002",
                    PLACE = "place2",
                    POSITION = "position2",
                    RESPONSIBILITIES = "responsibilities2",
                    START_MONTH = 2,
                    START_YEAR = 2002,
                    END_MONTH = 2,
                    END_YEAR = 2002
                }
            }.AsQueryable();

            UnitTestUtility.InitDBSet(relatedWork, mockRelatedWork);
            _mockContext.Setup(m => m.RELATED_WORK_EXPERIENCE).Returns(mockRelatedWork.Object);
            mockRelatedWork.Setup(m => m.Add(It.IsAny<RELATED_WORK_EXPERIENCE>()));
            var mockProfessionalQualificationService = new Mock<IProfessionalQualificationService>();
            var sut = new ProfessionalQualificationService(_mockContext.Object);
            sut.AddRelatedWorkExperiencesofFaculty("E0003", "place3", "position3", "responsibilities3", 3, 2003, 12, 2003);
            mockRelatedWork.Verify(m => m.Add(It.IsAny<RELATED_WORK_EXPERIENCE>()), Times.Once);
        }

    }
}

