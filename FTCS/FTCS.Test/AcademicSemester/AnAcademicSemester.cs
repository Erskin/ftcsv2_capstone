﻿using System;
using System.Collections.Generic;
using System.Linq;
using FTCS.Models;
using FTCS.Services;
using FTCS.Services.Implementation;
using NUnit.Framework;

namespace FTCS.Test
{
    [Category("Unit")]
    public class AnAcademicSemester
    {
        [Test]
        public void ShouldReturnAnAcademicYearForAYearProvided()
        {
            var year = AcademicYear.GetAcademicSemestersForYear(2016).FirstOrDefault();
            Assert.That(year.Month, Is.EqualTo(8));
            Assert.That(year.Year, Is.EqualTo(2016));
        }

        [Test]
        public void ShouldReturnTheProperStringRepresentationForASemester()
        {
            var sut = new Semester {Month = 8,Year = 2016};
            var result = sut.ToString();
            Assert.That(result, Is.EqualTo("Fall 2016"));
        }

        [Test]
        public void ShouldReturnAllSemestersForAGivenYear()
        {
            var sut = AcademicYear.GetAcademicSemestersForYear(2016);
            Assert.That(sut.First().ToString(), Is.EqualTo("Fall 2016"));
            Assert.That(sut.ElementAt(1).ToString(), Is.EqualTo("Spring 2017"));
            Assert.That(sut.Last().ToString(), Is.EqualTo("Summer 2017"));
        }

        [Test]
        public void ShouldGetTheCurrentSemesterOfTheYear()
        {
            var sut = AcademicYear.GetCurrentAcademicSemester();
            Assert.That(sut.Year, Is.EqualTo(DateTime.Today.Year));
        }

        [Test]
        public void ShouldSortAListOfSemestersInAscendingOrder()
        {
            var listOfSemesters = new List<Semester>()
            {
                new Semester(5, 2011),
                new Semester(1, 2011),
                new Semester(8, 2020),
            };
            listOfSemesters.Sort();
            CollectionAssert.IsOrdered(listOfSemesters);
            
        }

        [TestCase(1,2015,5,2015, ExpectedResult = -1)]
        [TestCase(1,2015,8,2016, ExpectedResult = -1)]
        [TestCase(1,2015,8,2016, ExpectedResult = -1)]
        [TestCase(5,2015,8,2016, ExpectedResult = -1)]
        [TestCase(5,2015,8,2015, ExpectedResult = -1)]
        [TestCase(8,2015,5,2015, ExpectedResult = 1)]
        public int SemesterCompareTests(int firstMonth, int firstYear, int secondMonth, int secondYear)
        {
            var firstSemester = new Semester(firstMonth, firstYear);
            var secondSemester = new Semester(secondMonth, secondYear);
            return firstSemester.CompareTo(secondSemester);
        }

        [Test]
        public void ShouldReturnAListOfSelectListItemsThatContainAcademicYearSelections()
        {
            var year = 2016;
            var result = AcademicYear.GetSemesterYearOptions(year, true);
            Assert.That(result.Count, Is.GreaterThan(0));
            result.ForEach(x =>
            {
                Assert.That(x.Text, Is.EqualTo(AcademicYear.GetAcademicYearStringForYear(Convert.ToInt32(year))));
                year--;
            });
        }

        [Test]
        public void ShouldDefaultTheYearToBeTheCurrentSemesterYearIfTheYearPassedInIsLessThan2011()
        {
            var year = 2010;
            var yearDecrement = AcademicYear.GetCurrentAcademicYear().First().Year;
            var result = AcademicYear.GetSemesterYearOptions(year, true);
            Assert.That(result.Count, Is.GreaterThan(0));
            result.ForEach(x =>
            {
                Assert.That(x.Text, Is.EqualTo(AcademicYear.GetAcademicYearStringForYear(Convert.ToInt32(yearDecrement))));
                yearDecrement--;
            });
        }
    }
}

