﻿using FTCS.Models;
using FTCS.Services;
using FTCS.ViewModels;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;

namespace FTCS.Test.Reports
{
    [Category("Unit")]
    public class AReportService
    {
        
      
        private Mock<DbSet<TEACHER_COURSE_RECORD>> _mockTeacherCourseRecord;
        private Mock<DbSet<DEPARTMENT>> _mockDepartmentSet;
        private Mock<DbSet<TEACHER_COURSE_QUALIFICATION>> _mockTeacherCourseQualificationSet;
        private Mock<DbSet<COURSE>> _mockCourseSetMock;
        private Mock<FTCSEntities> _mockContext;
        private List<Semester> _academicYear;
        private Mock<IDepartmentService> _mockDepartmentService;


        [SetUp]
        public void Setup()
        {
            _mockTeacherCourseRecord = new Mock<DbSet<TEACHER_COURSE_RECORD>>();
            _mockDepartmentSet = new Mock<DbSet<DEPARTMENT>>();
            _mockTeacherCourseQualificationSet = new Mock<DbSet<TEACHER_COURSE_QUALIFICATION>>();
            _mockCourseSetMock = new Mock<DbSet<COURSE>>();
            _mockContext = new Mock<FTCSEntities>();
            _mockDepartmentService = new Mock<IDepartmentService>();
            _academicYear = AcademicYear.GetAcademicSemestersForYear(2016).ToList();
            var mockDepartments = new List<DEPARTMENT>()
            {
                new DEPARTMENT()
                {
                    CODE= "CSCI",
                    NAME = "Computer Science"
                },
                new DEPARTMENT()
                {
                    CODE= "ACCT",
                    NAME = "Accounting"
                }

            };
            _mockDepartmentService.Setup(
                    m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<DepartmentVM>()
                {
                    new DepartmentVM()
                    {
                        DEPARTMENT_CODE = "CSCI"
                    },
                    new DepartmentVM()
                    {
                        DEPARTMENT_CODE = "ACCT"
                    }
                });
            var mockCourses = new List<COURSE>()
            {
                new COURSE
                {
                    RUBRIC = "CSCI",
                    NUMBER = "101",
                    DEPARTMENT = "CSCI",
                    NAME = "Course101",
                    DEPARTMENT1 = mockDepartments[0],
                    DESCRIPTION = "Description"
                },
                new COURSE
                {
                    RUBRIC = "CSCI",
                    NUMBER = "102",
                    DEPARTMENT = "CSCI",
                    NAME = "Course102",
                    DEPARTMENT1 = mockDepartments[0],
                    DESCRIPTION = "Description"
                },
                new COURSE
                {
                    RUBRIC = "CSCI",
                    NUMBER = "103",
                    DEPARTMENT = "CSCI",
                    NAME = "Course103",
                    DEPARTMENT1 = mockDepartments[0],
                    DESCRIPTION = "Description"
                },
                new COURSE
                {
                    RUBRIC = "ACCT",
                    NUMBER = "101",
                    DEPARTMENT = "ACCT",
                    NAME = "Course103",
                    DEPARTMENT1 = mockDepartments[1],
                    DESCRIPTION = "Description"
                }
            };
            var mockTeacherCourseRecords = new List<TEACHER_COURSE_RECORD>()
            {
                new TEACHER_COURSE_RECORD
                {
                    COURSE_RUBRIC = "CSCI",
                    COURSE_NUMBER = "101",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    TEACHER_ID = "1",
                    TEACHER = new TEACHER()
                    {
                        FIRST_NAME = "First",
                        LAST_NAME = "Last",
                        TEACHER_TYPE = 1,
                        TEACHER_TYPE1 = new TEACHER_TYPE()
                        {
                            NAME = "TeacherType"
                        }
                    },
                    COURSE = mockCourses.First(c => c.NUMBER == "101")
                },
                new TEACHER_COURSE_RECORD
                {
                    COURSE_RUBRIC = "CSCI",
                    COURSE_NUMBER = "102",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    TEACHER_ID = "2",
                    TEACHER = new TEACHER()
                    {
                        FIRST_NAME = "First",
                        LAST_NAME = "Last",
                        TEACHER_TYPE = 1,
                        TEACHER_TYPE1 = new TEACHER_TYPE()
                        {
                            NAME = "TeacherType"
                        }
                    },
                    COURSE = mockCourses.First(c => c.NUMBER == "102")
                },
                new TEACHER_COURSE_RECORD
                {
                    COURSE_RUBRIC = "CSCI",
                    COURSE_NUMBER = "103",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    TEACHER_ID = "3",
                    TEACHER = new TEACHER()
                    {
                        FIRST_NAME = "First",
                        LAST_NAME = "Last",
                        TEACHER_TYPE = 1,
                        TEACHER_TYPE1 = new TEACHER_TYPE()
                        {
                            NAME = "TeacherType"
                        }
                    },
                    COURSE = mockCourses.First(c => c.NUMBER == "103")
                },
                new TEACHER_COURSE_RECORD
                {
                    COURSE_RUBRIC = "ACCT",
                    COURSE_NUMBER = "101",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    TEACHER_ID = "4",
                    TEACHER = new TEACHER()
                    {
                        FIRST_NAME = "Needle",
                        LAST_NAME = "Haystack",
                        TEACHER_TYPE = 0,
                        TEACHER_TYPE1 = new TEACHER_TYPE()
                        {
                            NAME = "TeacherType"
                        }
                    },
                    COURSE = mockCourses.First(c => c.RUBRIC == "ACCT")
                },
                 new TEACHER_COURSE_RECORD
                {
                    COURSE_RUBRIC = "ACCT",
                    COURSE_NUMBER = "101",
                    ACADEMIC_MONTH = 8,
                    ACADEMIC_YEAR = 2015,
                    TEACHER_ID = "4",
                    TEACHER = new TEACHER()
                    {
                        FIRST_NAME = "Needle",
                        LAST_NAME = "Haystack",
                        TEACHER_TYPE = 0,
                        TEACHER_TYPE1 = new TEACHER_TYPE()
                        {
                            NAME = "TeacherType"
                        }
                    },
                    COURSE = mockCourses.First(c => c.RUBRIC == "ACCT")
                }
            };
            var mockTeacherCourseQualifications = new List<TEACHER_COURSE_QUALIFICATION>()
            {
                new TEACHER_COURSE_QUALIFICATION
                {
                    TEACHER_ID = "1",
                    COURSE_RUBRIC = "CSCI",
                    COURSE_NUMBER = "101",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    HOW_QUALIFIED = 1
                },
                new TEACHER_COURSE_QUALIFICATION
                {
                    TEACHER_ID = "2",
                    COURSE_RUBRIC = "CSCI",
                    COURSE_NUMBER = "102",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    HOW_QUALIFIED = 1
                },
                new TEACHER_COURSE_QUALIFICATION
                {
                    TEACHER_ID = "3",
                    COURSE_RUBRIC = "CSCI",
                    COURSE_NUMBER = "103",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    HOW_QUALIFIED = 1
                },
                new TEACHER_COURSE_QUALIFICATION
                {
                    TEACHER_ID = "4",
                    COURSE_RUBRIC = "ACCT",
                    COURSE_NUMBER = "101",
                    ACADEMIC_MONTH = _academicYear[0].Month,
                    ACADEMIC_YEAR = _academicYear[0].Year,
                    HOW_QUALIFIED = 2
                }
            };

            UnitTestUtility.InitDBSet(mockDepartments.AsQueryable(), _mockDepartmentSet);
            UnitTestUtility.InitDBSet(mockTeacherCourseRecords.AsQueryable(), _mockTeacherCourseRecord);
            UnitTestUtility.InitDBSet(mockTeacherCourseQualifications.AsQueryable(), _mockTeacherCourseQualificationSet);
            UnitTestUtility.InitDBSet(mockCourses.AsQueryable(), _mockCourseSetMock);
            _mockContext.Setup(m => m.DEPARTMENTs).Returns(_mockDepartmentSet.Object);
            _mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_mockTeacherCourseRecord.Object);
            _mockContext.Setup(m => m.TEACHER_COURSE_QUALIFICATION).Returns(_mockTeacherCourseQualificationSet.Object);
            _mockContext.Setup(m => m.COURSEs).Returns(_mockCourseSetMock.Object);
            
        }

        [TearDown]
        public void TearDown()
        {

        }

        
        public void ShouldReturnAllFacultyIfNoFilteringIsPresent()
        {
            var mockContext = new Mock<FTCSEntities>();
           
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_mockTeacherCourseRecord.Object);
            var sut = new ReportService(mockContext.Object);
            var result = sut.GetFacultyRoster(2016, null, "");
            CollectionAssert.IsNotEmpty(result);
        }

        
        public void ShouldReturnAllFacultyInAGivenYear()
        {
            var expected = new FacultyRosterVM {Name = "John" };
            var mockContext = new Mock<FTCSEntities>();
            
            var sut = new ReportService(mockContext.Object);
            var result = sut.GetFacultyRoster(2015, null, "All");
            Assert.That(result.First().Name, Is.EqualTo(expected.Name));
        }

        //TODO Build Unit test data for test
     
        public void ShouldReturnAllFacultyThatDoNotHaveANarrativeOrWorkExperience()
        {
            var mockDepartment = "CSCI";
            var mockContext = new Mock<FTCSEntities>();
            
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_mockTeacherCourseRecord.Object);
            var sut = new ReportService(mockContext.Object);
            var result = sut.GetIncompleteProfessionalQualification(mockDepartment);
            Assert.That(result.FirstOrDefault().Name, Is.EqualTo("Teacher1"));
        }

        [Test]
        public void ShouldReturnAllFacultyCredentialsForOneDepartment()
        {
            
            var sut = new ReportService(_mockContext.Object,_mockDepartmentService.Object);
           
            var result = sut.GetFacultyCredentialsReport("ACCT", _academicYear[0].Year);
            Assert.That(result.Count(), Is.GreaterThan(0));
            foreach (var facultyCredentialsReportVm in result)
            {
                Assert.That(facultyCredentialsReportVm.Course, Does.Contain("ACCT"));
            }
        }

        [Test]
        public void ShouldReturnACredentialThatIsUnspecifiedIfThereIsNoMatchingQualifications()
        {
            
            var sut = new ReportService(_mockContext.Object,_mockDepartmentService.Object);
            
            var result = sut.GetFacultyCredentialsReport("ACCT", _academicYear[0].Year);
            Assert.That(result.Count(), Is.GreaterThan(0));
            foreach (var facultyCredentialsReportVm in result)
            {
                Assert.That(facultyCredentialsReportVm.Course, Does.Contain("ACCT"));
                Assert.That(facultyCredentialsReportVm.Credential, Is.EqualTo("Unspecified"));
            }
        }

        [Test]
        public void ShouldReturnACredentialThatIsASpecificAcademicYear()
        {
            var sut = new ReportService(_mockContext.Object,_mockDepartmentService.Object);  
            var result = sut.GetFacultyCredentialsReport("ACCT", 2015);
            Assert.That(result.Count(), Is.GreaterThan(0));
            foreach (var facultyCredentialsReportVm in result)
            {
                Assert.That(facultyCredentialsReportVm.Course, Does.Contain("ACCT"));
                Assert.That(facultyCredentialsReportVm.Credential, Is.EqualTo("Unspecified"));
                Assert.That(facultyCredentialsReportVm.Semester, Is.EqualTo("Fall 2015"));
            }
        }

        [Test]
        public void ShouldReturnAllTeacherCourseRecordsWithTheirQualification()
        {
            var sut = new ReportService(_mockContext.Object,_mockDepartmentService.Object);
            var result = sut.GetTeacherCourseRecordsWithQualifications().First();
            Assert.That(result.TeacherId, Is.EqualTo("1"));
        }

        [Test]
        public void ShouldGetASummaryOfAllTypesOfCoursesQualifiedInTheDatabase()
        {
            
            var sut = new ReportService(_mockContext.Object,_mockDepartmentService.Object);
            var result = sut.GetFacultyCredentialsSummary();
            Assert.That(result.Totals.TotalAcademicNumber, Is.EqualTo(0));
            Assert.That(result.Totals.TotalProfessionallyQualified, Is.EqualTo(3));
            Assert.That(result.Totals.TotalUnspecified, Is.EqualTo(1));
        }

        [Test]
        public void ShouldReturnCredentialsWithCourseDecriptions()
        {
          var sut = new ReportService(_mockContext.Object, _mockDepartmentService.Object);
          var result = sut.GetTeacherCourseRecordsWithQualifications().ToList();
          result.ForEach(x =>
          {
              Assert.That(x.CourseDescription, Is.Not.Null);
          });
        }

        [Test]
        public void ShouldReturnACredentialWithCourseDescriptionsWhenFCRIsCalled()
        {
            var semester = AcademicYear.GetCurrentAcademicYear().First().Year;
            var sut = new ReportService(_mockContext.Object, _mockDepartmentService.Object);
            var result = sut.GetFacultyCredentialsReport("CSCI", semester);
            Assert.That(result.Count(), Is.GreaterThan(0));
            foreach (var facultyCredentialsReportVm in result)
            {
                Assert.That(facultyCredentialsReportVm.CourseDescription, Is.EqualTo("Description"));
            }
        }




    }
}
