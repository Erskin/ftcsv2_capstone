﻿using System;
using FTCS.Controllers;
using FTCS.Interfaces;
using FTCS.ViewModels;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using Rotativa;

namespace FTCS.Test.Reports
{

    [Category("Unit")]
    class AReportsController
    {
        [SetUp]
        public void Setup()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        [Test]
        public void ShouldReturnDataForTheGradAssistantRosterReport()
        {
            
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockFacultyService.Setup(m => m.GetFacultyTypes()).Returns(new List<FacultyTypeVM>());
            mockDepartmentService.Setup(m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new List<DepartmentVM>());
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(),It.IsAny<int?>(), It.IsAny<string>())).Returns(new List<FacultyRosterVM>());
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object, mockFacultyService.Object);
            sut.GradAssistantRoster(2016,false);
            mockReportService.Verify(m => m.GetFacultyRoster(2016, 0, ""), Times.Once);
        }
        [Test]
        public void ShouldReturnAllFacultyThatAreTeachingInTheAcademicAffairsDepartment()
        {
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockFacultyService.Setup(m => m.GetFacultyTypes()).Returns(new List<FacultyTypeVM>());
            mockDepartmentService.Setup(m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new List<DepartmentVM>());
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<string>())).Returns(new List<FacultyRosterVM>());
            var sut = new ReportsController(mockReportService.Object,mockDepartmentService.Object,mockFacultyService.Object);
            sut.FacultyRoster(2016, "ACAF", 4,false);
            mockReportService.Verify(m => m.GetFacultyRoster(2016,4,"ACAF"));
        }

        [Test]
        public void ShouldDefaultToTheFirstListOfDepartmentsCurrentYearAndAllFacultyForFacultyRoster()
        {
            var year = AcademicYear.GetCurrentAcademicYear().First().Year;
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockFacultyService.Setup(m => m.GetFacultyTypes()).Returns(new List<FacultyTypeVM>());
            mockDepartmentService.Setup(m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new List<DepartmentVM> {new DepartmentVM {DEPARTMENT_CODE = "ACAF"} });
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<string>())).Returns(new List<FacultyRosterVM>());
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object, mockFacultyService.Object);
            sut.FacultyRoster(null, null, null,false);
            mockReportService.Verify(m => m.GetFacultyRoster(year, 4, "ACAF"));
        }

        [Test]
        public void ShouldDefaultToTheCurrentYearIfTheControllerDoesNotReceiveAYear()
        {
            var listOfGradAssistants = new List<FacultyRosterVM>()
            {
                new FacultyRosterVM() {Department = "Computer Science"}
            };
            var currentYear = AcademicYear.GetCurrentAcademicYear().First();
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<string>())).Returns(listOfGradAssistants);
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object,
                mockFacultyService.Object);
            sut.GradAssistantRoster(null, false);
            mockReportService.Verify(m => m.GetFacultyRoster(currentYear.Year,0,""));
        }

        [Test]
        public void ShouldReturnThePDFViewIfActionHasTrueInPDFParam()
        {
            var listOfGradAssistants = new List<FacultyRosterVM>()
            {
                new FacultyRosterVM() {Department = "Computer Science"}
            };
            
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<string>())).Returns(listOfGradAssistants);
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object,
                mockFacultyService.Object);
            var result = sut.GradAssistantRoster(null, true);
            var pdfResult = result as ViewAsPdf;
            Assert.IsInstanceOf<ViewAsPdf>(result);
            Assert.That(pdfResult?.ViewName,Is.EqualTo("GradAssistantRosterPDF"));
        }

        [Test]
        public void ShouldReturnPDFViewForFacultyRoster()
        {
            var listOfFaculty = new List<FacultyRosterVM>()
            {
                new FacultyRosterVM() {Department = "Computer Science"}
            };

            var listOfDepartments = new List<DepartmentVM>()
            {
                new DepartmentVM() {DEPARTMENT_CODE = "CSCI", NAME = "Computer Science"}
            };
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<string>())).Returns(listOfFaculty);
            mockDepartmentService.Setup(
                    m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(listOfDepartments);
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object,
                mockFacultyService.Object);
            var result = sut.FacultyRoster(null,null,null, true);
            var pdfResult = result as ViewAsPdf;
            Assert.IsInstanceOf<ViewAsPdf>(result);
            Assert.That(pdfResult?.ViewName, Is.EqualTo("FacultyRosterPDF"));
        }

        [Test]
        public void ShouldCallGetFacultyCredentialsReport()
        {
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockDepartmentService.Setup(m => m.GetListOfDepartmentSelectListItems());
            mockDepartmentService.Setup(m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new List<DepartmentVM> { new DepartmentVM { DEPARTMENT_CODE = "ACAF" } });
            var sut = new ReportsController(mockReportService.Object,mockDepartmentService.Object,mockFacultyService.Object);
            sut.FacultyCredentialsReport(null,null,null);
            mockReportService.Verify(m => m.GetFacultyCredentialsReport("ACAF",2016),Times.AtLeastOnce);
            mockDepartmentService.Verify(m=> m.GetListOfDepartmentSelectListItems(), Times.Once);
        }

        [Test]
        public void ShouldReturnPDFViewForFacultyCredentialsReport()
        {
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockDepartmentService.Setup(m => m.GetListOfDepartmentSelectListItems());
            mockDepartmentService.Setup(m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new List<DepartmentVM> { new DepartmentVM { DEPARTMENT_CODE = "ACAF" } });
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<string>()));
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object,
                mockFacultyService.Object);
            var result = sut.FacultyCredentialsReport(null, null, true);
            var pdfResult = result as ViewAsPdf;
            Assert.IsInstanceOf<ViewAsPdf>(result);
            Assert.That(pdfResult?.ViewName, Is.EqualTo("FacultyCredentialsReportPDF"));
        }

        [Test]
        public void ShouldDefaultToTheFirstListOfDepartmentsCurrentYearAndAllFacultyForFacultyCredentialsReport()
        {
            var year = AcademicYear.GetCurrentAcademicYear().First().Year;
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            mockFacultyService.Setup(m => m.GetFacultyTypes()).Returns(new List<FacultyTypeVM>());
            mockDepartmentService.Setup(m => m.GetListOfDepartments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new List<DepartmentVM> { new DepartmentVM { DEPARTMENT_CODE = "ACAF" } });
            mockReportService.Setup(m => m.GetFacultyRoster(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<string>())).Returns(new List<FacultyRosterVM>());
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object, mockFacultyService.Object);
            sut.FacultyCredentialsReport(null, null, false);
            mockReportService.Verify(m => m.GetFacultyCredentialsReport("ACAF",year));
        }

        [Test]
        public void ShouldCallTheCredentialsSummaryMethodWhenCredentialsIsCalled()
        {
            var mockReportService = new Mock<IReportService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockFacultyService = new Mock<IFacultyService>();
            var sut = new ReportsController(mockReportService.Object, mockDepartmentService.Object, mockFacultyService.Object);
            sut.Credentials();
            mockReportService.Verify(m => m.GetFacultyCredentialsSummary(), Times.Once);


        }
    }
}
