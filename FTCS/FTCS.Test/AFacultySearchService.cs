﻿using FTCS.Models;
using FTCS.Services.Implementation;
using Moq;
using NUnit.Framework;
using FTCS.Services;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using FTCS.ViewModels;

namespace FTCS.Test
{
    [Category("Unit:AFacultySearchService")]
    [TestFixture]
    class AFacultySearchService
    {
        Mock<IGenericRepository<DEPARTMENT>> mockdepartmentRepository;
        Mock<IGenericRepository<TEACHER_COURSE_RECORD>> mocktcrRepository;
        Mock<IGenericRepository<COURSE>> mockCourseRepository;
        [SetUp]
        public void SetUp()
        {
            /*
            Instantiate new Mock objects over here so that you dont have to do it for
            each test.
            For each test, we will always get a fresh copy of the mocks because they are in
            the Setup method which is run before every test.
            */
            mockCourseRepository = new Mock<IGenericRepository<COURSE>>();
            mockdepartmentRepository = new Mock<IGenericRepository<DEPARTMENT>>();
            mocktcrRepository = new Mock<IGenericRepository<TEACHER_COURSE_RECORD>>();
        }

       
    }
}
