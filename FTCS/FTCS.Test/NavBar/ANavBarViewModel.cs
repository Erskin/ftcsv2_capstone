﻿using FTCS.ViewModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCS.Test.NavBar
{
    [TestFixture]
    [Category("Unit")]
    public class ANavBarViewModel
    {
        [Category("Unit:ANavBarViewModel")]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void ShouldBeAbleToSetTheSemesterAndYearForTheNavBarIfItIsSpring(int month)
        {
            var sut = new NavBarVM();
            sut.SetSemesterAndYear(month);
            Assert.That(sut.SemesterAndYear, Is.EqualTo("Spring " + DateTime.Now.Year));

        }
        [Category("Unit:ANavBarViewModel")]
        [TestCase(5)]
        [TestCase(6)]
        [TestCase(7)]
        public void ShouldBeAbleToSetTheSemesterAndYearForTheNavBarIfItIsSummer(int month)
        {
            var sut = new NavBarVM();
            sut.SetSemesterAndYear(month);
            Assert.That(sut.SemesterAndYear, Is.EqualTo("Summer " + DateTime.Now.Year));

        }
        [Category("Unit:ANavBarViewModel")]
        [TestCase(8)]
        [TestCase(9)]
        [TestCase(10)]
        [TestCase(11)]
        [TestCase(12)]
        public void ShouldBeAbleToSetTheSemesterAndYearForTheNavBarIfItIsFall(int month)
        {
            var sut = new NavBarVM();
            sut.SetSemesterAndYear(month);
            Assert.That(sut.SemesterAndYear, Is.EqualTo("Fall " + DateTime.Now.Year));

        }
    }
}
