﻿using NUnit.Framework;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FTCS.Views.Shared;
using System.Web.Mvc;

namespace FTCS.Test.NavBar
{
    [TestFixture]
    [Category("Unit")]
    public class ASharedController
    {
        [Category("Unit:SharedController")]
        [Test]
        public void ShouldBeAbleToRenderAPartialViewOfTheNavBarInTheLayout()
        {
            var sut = new SharedController();
            var result = sut.RenderNavBar() as PartialViewResult;
            Assert.AreEqual("_NavBar", result.ViewName);
        }
    }
}