﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using FTCS.Models;
using FTCS.Services;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.Task;
using Moq;
using NUnit.Framework;

namespace FTCS.Test.Home
{
    [TestFixture]
    class ACredentialManagerService
    {
        private Mock<DbSet<FACULTY_PROGRAM_COORDINATION>> _mockFacultyProgramCoordinations;
        private Mock<DbSet<PROGRAM_LEVEL_TYPE>> _mockProgramLevels;
        private Mock<DbSet<TEACHER_COURSE_QUALIFICATION>> _mockTeacherQualification;
        private IQueryable<PROGRAM> _mockProgramList;
        

        [SetUp]
        public void SetUp()
        {
            var bannerId = "E00123";
            var programLevels = new List<PROGRAM_LEVEL_TYPE>()
            {
                new PROGRAM_LEVEL_TYPE() {ID = 1, LEVEL = "BS Degree"}
            }.AsQueryable();
            _mockProgramList = new List<PROGRAM>()
            {
                new PROGRAM()
                {
                    MAJOR_CODE = "Program1",
                    FACULTY_PROGRAM_COORDINATION = new List<FACULTY_PROGRAM_COORDINATION>()
                    {
                        new FACULTY_PROGRAM_COORDINATION()
                        {
                            END_ACADEMIC_MONTH = 0,
                            END_ACADEMIC_YEAR = 0
                        },
                        new FACULTY_PROGRAM_COORDINATION()
                        {
                             END_ACADEMIC_MONTH = 8,
                            END_ACADEMIC_YEAR = 2016
                        }
                    },
                    CONCENTRATIONs = new List<CONCENTRATION>()
                    {
                        new CONCENTRATION()
                        {
                            CODE = "Concentration1",
                            PROGRAM = "Program1",
                            FACULTY_CONCENTRATION_COORDINATION = new List<FACULTY_CONCENTRATION_COORDINATION>()
                            {
                                new FACULTY_CONCENTRATION_COORDINATION()
                                {
                                    END_ACADEMIC_MONTH = 0,
                                    END_ACADEMIC_YEAR = 0
                                }
                            }
                        },
                        new CONCENTRATION()
                        {
                            CODE = "Concentration2",
                            PROGRAM = "Program1",
                            FACULTY_CONCENTRATION_COORDINATION = new List<FACULTY_CONCENTRATION_COORDINATION>()
                            {
                                new FACULTY_CONCENTRATION_COORDINATION()
                                {
                                    END_ACADEMIC_MONTH = 0,
                                    END_ACADEMIC_YEAR = 0
                                }
                            }
                        }
                    }
                }
            }.AsQueryable();

            var mockFacultyProgramCoordination = new List<FACULTY_PROGRAM_COORDINATION>()
            {
                new FACULTY_PROGRAM_COORDINATION()
                {
                    FACULTY_ID = bannerId,
                    PROGRAM_MAJOR_CODE = "CSCI-BS",
                    TEACHER = new TEACHER()
                    {
                        LAST_NAME = "LastName",
                        FIRST_NAME = "FirstName",
                        MIDDLE_INITIAL = "M"
                    },
                    PROGRAM = new PROGRAM()
                    {
                        MAJOR_CODE = "CSCI",
                        MAJOR_NAME = "Computing BS",
                        LEVEL = 1,
                        DEPARTMENT = "CSCI",
                        CONCENTRATIONs = new List<CONCENTRATION>()
                        {
                            new CONCENTRATION() {NAME = "Concentration1"},
                            new CONCENTRATION() {NAME = "Concentration2"}
                        }
                    },
                    END_ACADEMIC_MONTH = 0,
                    END_ACADEMIC_YEAR = 0


                }
            }.AsQueryable();

            var mockTeacherCourseQualification = new List<TEACHER_COURSE_QUALIFICATION>()
            {
                new TEACHER_COURSE_QUALIFICATION() {TEACHER_ID = "E001", COURSE_RUBRIC = "CSCI", COURSE_NUMBER = "100"}
            }.AsQueryable();
            _mockTeacherQualification = new Mock<DbSet<TEACHER_COURSE_QUALIFICATION>>();
            _mockFacultyProgramCoordinations = new Mock<DbSet<FACULTY_PROGRAM_COORDINATION>>();
            _mockProgramLevels = new Mock<DbSet<PROGRAM_LEVEL_TYPE>>();
            UnitTestUtility.InitDBSet(mockTeacherCourseQualification, _mockTeacherQualification);
            UnitTestUtility.InitDBSet(programLevels,_mockProgramLevels);
            UnitTestUtility.InitDBSet(mockFacultyProgramCoordination, _mockFacultyProgramCoordinations);
        }

        [Test]
        public void ShouldReturnProgramsForTheDepartmentPassedIn()
        {
            var bannerId = "E00123";
            var mockFacultyProgramCoordination = new FACULTY_PROGRAM_COORDINATION()
            {
                FACULTY_ID = bannerId,
                TEACHER = new TEACHER()
                {
                    LAST_NAME = "LastName",
                    FIRST_NAME = "FirstName",
                    MIDDLE_INITIAL = "M"
                },
                PROGRAM = new PROGRAM()
                {
                    MAJOR_CODE = "CSCI",
                    MAJOR_NAME = "Computing BS",
                    LEVEL = 1,
                    DEPARTMENT = "CSCI",
                    CONCENTRATIONs = new List<CONCENTRATION>()
                    {
                        new CONCENTRATION() {NAME = "Concentration1"},
                        new CONCENTRATION() {NAME = "Concentration2"}
                    }
                },
                END_ACADEMIC_MONTH = 0,
                END_ACADEMIC_YEAR = 0
                

            };
            
            UnitTestUtility.InitDBSet(
                new List<FACULTY_PROGRAM_COORDINATION>() {mockFacultyProgramCoordination}.AsQueryable(),
                _mockFacultyProgramCoordinations);
           
           
            var expectedProgramList = new List<ProgramsVM>()
            {
                new ProgramsVM()
                {
                    Program = "Computing BS",
                    Level = "BS Degree",
                    Coordinator = "LastName, FirstName M",
                    NumberOfConcentrations = 2
                }
            }.FirstOrDefault();
            var mockContext = new Mock<FTCSEntities>();
            mockContext.SetupGet(m => m.PROGRAM_LEVEL_TYPE).Returns(_mockProgramLevels.Object);
            mockContext.SetupGet(m => m.FACULTY_PROGRAM_COORDINATION).Returns(_mockFacultyProgramCoordinations.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            var result = sut.GetProgramsOfStudyForDepartment("CSCI").FirstOrDefault();
            Assert.That(result.Program, Is.EqualTo(expectedProgramList.Program));
            Assert.That(result.Coordinator, Is.EqualTo(expectedProgramList.Coordinator));
            Assert.That(result.NumberOfConcentrations, Is.EqualTo(2));
        }

        

        [Test]
        public void ShouldEndDateTheCurrentActiveConcentrationCoordinationsForAProgram()
        {
            var mockPrograms = new Mock<DbSet<PROGRAM>>();
            var mockContext = new Mock<FTCSEntities>();
            var mockFpc = new Mock<DbSet<FACULTY_PROGRAM_COORDINATION>>();
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();
            UnitTestUtility.InitDBSet(_mockProgramList, mockPrograms);
            mockFpc.Setup(m => m.RemoveRange(It.IsAny<IEnumerable<FACULTY_PROGRAM_COORDINATION>>()));
            mockContext.Setup(
                m => m.FACULTY_PROGRAM_COORDINATION).Returns(mockFpc.Object);
            mockContext.Setup(m => m.PROGRAMs).Returns(mockPrograms.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            sut.EndCurrentFacultyConcentrations("Program1");
            var program = _mockProgramList.First(p => p.MAJOR_CODE == "Program1");
            program.CONCENTRATIONs.ToList().ForEach(x => x.FACULTY_CONCENTRATION_COORDINATION.ToList().ForEach(y =>
            {
                Assert.That(y.END_ACADEMIC_YEAR, Is.EqualTo(currentSemester.Year));
                Assert.That(y.END_ACADEMIC_MONTH, Is.EqualTo(currentSemester.Month));
            }));
        }
        [Test]
        public void ShouldReturnAListOfSelectListItemsForPrograms()
        {
            var mockPrograms = new List<PROGRAM>()
            {
                new PROGRAM() {MAJOR_CODE = "Code1",MAJOR_NAME = "Program1",DEPARTMENT = "UND"}
            }.AsQueryable();
            var programDBset = new Mock<DbSet<PROGRAM>>();
            UnitTestUtility.InitDBSet(mockPrograms,programDBset);
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.PROGRAMs).Returns(programDBset.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            var result = sut.GetProgramOptions();
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(SelectListItem));
        }

        [Test]
        public void ShouldReturnAListOfSelectListItemsForLevels()
        {
            var mockContext = new Mock<FTCSEntities>();
            var mockProgramLevelTypes = new Mock<DbSet<PROGRAM_LEVEL_TYPE>>();
           var programLevels = new List<PROGRAM_LEVEL_TYPE>()
           {
               new PROGRAM_LEVEL_TYPE() {ID = 1, LEVEL = "ProgramLevel1"}
           };
            UnitTestUtility.InitDBSet(programLevels.AsQueryable(),mockProgramLevelTypes);
            mockContext.Setup(m => m.PROGRAM_LEVEL_TYPE).Returns(mockProgramLevelTypes.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            var result = sut.GetLevelOptions();
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(SelectListItem));
        }
        [Test]
        public void ShouldReturnAListOfSelectListItemsForCoordinators()
        {
            var department = "CSCI";
            
            var listOfFaculty = new List<FacultyVM>()
            {
                new FacultyVM() {BANNER_ID = "E00123", NAME = "FirstNameLastName"}
            };
            var mockContext = new Mock<FTCSEntities>();
            
            var facultyNameSortOrder = "NAME_asc";
            var mockFacultyService = new Mock<IFacultyService>();
            mockFacultyService.Setup(m => m.GetListOfFaculty(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listOfFaculty);
            var sut = new CredentialManagerService(mockContext.Object, mockFacultyService.Object, null);
            var result = sut.GetCoordinatorOptions(department, null);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(SelectListItem));
            mockFacultyService.Verify(m => m.GetListOfFaculty(string.Empty,string.Empty,facultyNameSortOrder,string.Empty), Times.Once);
        }

        [Test]
        public void ShouldReturnAListOfFacultyInACollegeWhenCollegeCodeIsPassedIn()
        {
            var facultyVm = new FacultyVM() {BANNER_ID = "E001",NAME = "Faculty2", College = "BT"};
            var listOfFaculty = new List<FacultyVM>()
            {
                new FacultyVM() {BANNER_ID = "E00123", NAME = "FirstNameLastName",College = "BIO"},
                facultyVm,
                new FacultyVM() {BANNER_ID = "E001",NAME = "Faculty3", College = "ACCT"},
            };
            var mockContext = new Mock<FTCSEntities>();

            var mockFacultyService = new Mock<IFacultyService>();
            mockFacultyService.Setup(m => m.GetListOfFaculty(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listOfFaculty);
            var sut = new CredentialManagerService(mockContext.Object, mockFacultyService.Object, null);
            var result = sut.GetCoordinatorOptions(null, "BT");
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(SelectListItem));
            //first item in the result is a null option
            Assert.That(result.LastOrDefault()?.Text, Is.EqualTo(facultyVm.NAME));
        }



        [Test]
        public void ShouldAddANewFacultyProgramCoordinationIfNoneExistsForTheProgramAndFacultyMember()
        {
            
            var mockContext = new Mock<FTCSEntities>();
            var mockPrograms = new Mock<DbSet<PROGRAM>>();
            var mockFacultyProgramQualifications = new Mock<DbSet<FACULTY_PROGRAM_QUALIFICATION>>();
            var fccList = new List<FACULTY_CONCENTRATION_COORDINATION>() {}.AsQueryable();
            var fpcList = new List<FACULTY_PROGRAM_COORDINATION>() {}.AsQueryable();
            var programQualificationsList = new List<FACULTY_PROGRAM_QUALIFICATION>() {}.AsQueryable();
            var mockFpq = new Mock<DbSet<FACULTY_PROGRAM_QUALIFICATION>>();
            

            //We then set up Program1 to have some concentrations

            mockFacultyProgramQualifications.Setup(m => m.RemoveRange(It.IsAny<ICollection<FACULTY_PROGRAM_QUALIFICATION>>()));
            UnitTestUtility.InitDBSet(_mockProgramList, mockPrograms);
            mockContext.Setup(m => m.PROGRAMs).Returns(mockPrograms.Object);
            var mockFpc = new Mock<DbSet<FACULTY_PROGRAM_COORDINATION>>();

            mockContext.Setup(m => m.FACULTY_PROGRAM_QUALIFICATION).Returns(mockFacultyProgramQualifications.Object);
            mockContext.Setup(m => m.FACULTY_PROGRAM_COORDINATION).Returns(mockFpc.Object);
            var mockFcc = new Mock<DbSet<FACULTY_CONCENTRATION_COORDINATION>>();
            UnitTestUtility.InitDBSet(fccList, mockFcc);
            UnitTestUtility.InitDBSet(programQualificationsList, mockFpq);
            UnitTestUtility.InitDBSet(fpcList, mockFpc);
            mockContext.Setup(m => m.FACULTY_CONCENTRATION_COORDINATION).Returns(mockFcc.Object);
            mockContext.Setup(m => m.FACULTY_PROGRAM_QUALIFICATION).Returns(mockFpq.Object);
            mockContext.Setup(m => m.FACULTY_PROGRAM_COORDINATION).Returns(mockFpc.Object);

            mockFpc.Setup(m => m.Add(It.IsAny<FACULTY_PROGRAM_COORDINATION>()));
            mockFcc.Setup(m => m.Add(It.IsAny<FACULTY_CONCENTRATION_COORDINATION>()));

           
           
            var mockFacultyService = new Mock<IFacultyService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var departmentCode = "DEPT";
            mockFacultyService.Setup(m => m.GetFacultyById("E0123"))
                .Returns(new FacultyVM() {BANNER_ID = "E0123", DepartmentCode = departmentCode});
            mockDepartmentService.Setup(m => m.GetDepartmentByDepartmentCode(departmentCode))
                .Returns(new DepartmentVM() { COLLEGE = "College", DEPARTMENT_CODE = departmentCode});

        var sut = new CredentialManagerService(mockContext.Object,mockFacultyService.Object, mockDepartmentService.Object);
            var result = sut.AddNewProgramOfStudy("Program1", "0","E0123", departmentCode);
            mockFpc.Verify(m => m.Add(It.IsAny<FACULTY_PROGRAM_COORDINATION>()), Times.Once);
            mockFcc.Verify(m => m.Add(It.IsAny<FACULTY_CONCENTRATION_COORDINATION>()), Times.AtLeastOnce);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }

        [Test]
        public void ShouldAddAProgramQualificationForANewlyAddedProgramOfStudy()
        {
            var levelId = "1";
            var department = "CSCI";
            var coordinatorId = "E001";
            var programCode = "Program1";
            var academicSemester = AcademicYear.GetCurrentAcademicSemester();
            var mockFacultyProgramQualifications = new Mock<DbSet<FACULTY_PROGRAM_QUALIFICATION>>();
            var mockFcc = new Mock<DbSet<FACULTY_CONCENTRATION_COORDINATION>>();
            var mockFpc = new Mock<DbSet<FACULTY_PROGRAM_COORDINATION>>();
            var mockFacultyService = new Mock<IFacultyService>();
            var mockDepartmentService = new Mock<IDepartmentService>();
            var mockPrograms = new Mock<DbSet<PROGRAM>>();
            var mockContext = new Mock<FTCSEntities>();
            var fccList = new List<FACULTY_CONCENTRATION_COORDINATION>() {}.AsQueryable();
            

            UnitTestUtility.InitDBSet(_mockProgramList, mockPrograms);
            UnitTestUtility.InitDBSet(fccList, mockFcc);
            mockFacultyService.Setup(
                m => m.GetListOfFaculty(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            mockDepartmentService.Setup(m => m.GetDepartmentByDepartmentCode(It.IsAny<string>()))
                .Returns(new DepartmentVM());
            mockFacultyProgramQualifications.Setup(m => m.Add(It.IsAny<FACULTY_PROGRAM_QUALIFICATION>()));
            mockContext.Setup(m => m.PROGRAMs).Returns(mockPrograms.Object);
            mockContext.Setup(m => m.FACULTY_CONCENTRATION_COORDINATION).Returns(mockFcc.Object);
            mockContext.Setup(m => m.FACULTY_PROGRAM_QUALIFICATION)
                .Returns(mockFacultyProgramQualifications.Object);
            
            mockContext.Setup(m => m.FACULTY_PROGRAM_COORDINATION)
                .Returns(mockFpc.Object);
           

            mockFcc.SetupAllProperties();
            mockFpc.SetupAllProperties();

            var sut = new CredentialManagerService(mockContext.Object,mockFacultyService.Object, mockDepartmentService.Object);
            sut.AddNewProgramOfStudy(programCode, levelId, coordinatorId, department);
            mockFacultyProgramQualifications.Verify(m =>m.Add(It.IsAny<FACULTY_PROGRAM_QUALIFICATION>()), Times.Once());
        }

        [Test]
        public void ShouldAddAProgramQualificationWithABannerIdAndAProgramsInformation()
        {
            var coordinatorId = "E001";
            var programCode = "Program1";
            var mockPrograms = new Mock<DbSet<PROGRAM>>();
            var academicSemester = AcademicYear.GetCurrentAcademicSemester();
            var mockContext = new Mock<FTCSEntities>();
            var mockFacultyProgramQualifications = new Mock<DbSet<FACULTY_PROGRAM_QUALIFICATION>>();

            var expectedFpq = new FACULTY_PROGRAM_QUALIFICATION()
            {
                FACULTY_ID = coordinatorId,
                ACADEMIC_MONTH = academicSemester.Month,
                ACADEMIC_YEAR = academicSemester.Year,
                PROGRAM_MAJOR_CODE = programCode,
                HOW_QUALIFIED = (int)QualificationType.Unspecified
            };
            mockFacultyProgramQualifications.Setup(m => m.Add(It.IsAny<FACULTY_PROGRAM_QUALIFICATION>()));
            UnitTestUtility.InitDBSet(_mockProgramList, mockPrograms);
            mockContext.Setup(m => m.PROGRAMs).Returns(mockPrograms.Object);
            mockContext.SetupGet(m => m.FACULTY_PROGRAM_QUALIFICATION)
               .Returns(mockFacultyProgramQualifications.Object);

            var sut = new CredentialManagerService(mockContext.Object);
            sut.AddProgramQualifications(coordinatorId, programCode);
            mockFacultyProgramQualifications.Verify(
                m =>
                    m.Add(
                        It.Is<FACULTY_PROGRAM_QUALIFICATION>(
                            qualification =>
                                qualification.FACULTY_ID == expectedFpq.FACULTY_ID &&
                                qualification.PROGRAM_MAJOR_CODE == expectedFpq.PROGRAM_MAJOR_CODE)), Times.Once);
        }

        [Test]
        public void ShouldeRemoveAllProgramQualificationsWhenAProgramCodeIsPassedIn()
        {
            
            var programCode = "Program1";
            var mockPrograms = new Mock<DbSet<PROGRAM>>();
            var academicSemester = AcademicYear.GetCurrentAcademicSemester();
            var mockContext = new Mock<FTCSEntities>();
            var mockFacultyProgramQualifications = new Mock<DbSet<FACULTY_PROGRAM_QUALIFICATION>>();
            mockFacultyProgramQualifications.Setup(m => m.RemoveRange(It.IsAny<ICollection<FACULTY_PROGRAM_QUALIFICATION>>()));
            UnitTestUtility.InitDBSet(_mockProgramList, mockPrograms);
            mockContext.Setup(m => m.PROGRAMs).Returns(mockPrograms.Object);
            mockContext.SetupGet(m => m.FACULTY_PROGRAM_QUALIFICATION)
               .Returns(mockFacultyProgramQualifications.Object);

            var sut = new CredentialManagerService(mockContext.Object);
            sut.RemoveProgramQualifications(programCode);
            mockFacultyProgramQualifications.Verify(
                m => m.RemoveRange(It.IsAny<ICollection<FACULTY_PROGRAM_QUALIFICATION>>()), Times.Once);
        }

        [Test]
        public void ShouldOnlyRemoveProgramCoordinationsThatHaveNoEndMonthOrYear()
        {
            var mockPrograms = new Mock<DbSet<PROGRAM>>();
            var mockContext = new Mock<FTCSEntities>();
            var mockFpc = new Mock<DbSet<FACULTY_PROGRAM_COORDINATION>>();
            UnitTestUtility.InitDBSet(_mockProgramList, mockPrograms);
            mockFpc.Setup(m => m.RemoveRange(It.IsAny<IEnumerable<FACULTY_PROGRAM_COORDINATION>>()));
            mockContext.Setup(
                m => m.FACULTY_PROGRAM_COORDINATION).Returns(mockFpc.Object);
            mockContext.Setup(m => m.PROGRAMs).Returns(mockPrograms.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            sut.RemoveProgramCoordinations("Program1");
            mockFpc.Verify(m => m.RemoveRange(It.Is<IEnumerable<FACULTY_PROGRAM_COORDINATION>>(x => x.All(fpc => fpc.END_ACADEMIC_YEAR == 0 && fpc.END_ACADEMIC_MONTH == 0))),Times.Once);

        }

        [Test]
        public void ShouldRemoveAProgramOfStudyAndItsConcentrationsWhenDeleteProgramOfStudyIsCalled()
        {
            var programCoordinationDbSet = new Mock<DbSet<FACULTY_PROGRAM_COORDINATION>>();
            var mockContext = new Mock<FTCSEntities>();
            var mockFacultyConcentrationCoordnations = new Mock<DbSet<FACULTY_CONCENTRATION_COORDINATION>>();
            var mockPrograms = new Mock<DbSet<PROGRAM>>();
            var program = new List<PROGRAM>()
            {
                new PROGRAM()
                {
                    MAJOR_CODE = "Program2",
                    MAJOR_NAME = "Programing2",
                    LEVEL = 1,
                    DEPARTMENT = "CSCI",
                    COLLEGE = "ETSU"
                },
                new PROGRAM()
                {
                    MAJOR_CODE = "Program1",
                    MAJOR_NAME = "Programing1",
                    LEVEL = 1,
                    DEPARTMENT = "CSCI",
                    COLLEGE = "ETSU"
                }
            }.AsQueryable();

            var facultyProgramCoordination = new List<FACULTY_PROGRAM_COORDINATION>()
            {
                new FACULTY_PROGRAM_COORDINATION()
                {
                    FACULTY_ID = "E0456",
                    PROGRAM_MAJOR_CODE = "Program2",
                    PROGRAM = new PROGRAM()
                    {
                        MAJOR_CODE = "Program2",
                        MAJOR_NAME = "Programing2",
                        LEVEL = 1,
                        DEPARTMENT = "CSCI",
                        COLLEGE = "ETSU"
                    }
                }
            }.AsQueryable();

            var facultyConcentrationCoordination = new List<FACULTY_CONCENTRATION_COORDINATION>()
            {
                new FACULTY_CONCENTRATION_COORDINATION()
                {
                    FACULTY_ID = "E0456",
                    PROGRAM_CODE = "Program2",
                    CONCENTRATION = new CONCENTRATION()
                    {
                        CODE = "Concentration3"
                    },
                },
                new FACULTY_CONCENTRATION_COORDINATION()
                {
                    FACULTY_ID = "E0456",
                    PROGRAM_CODE = "Program2",
                    CONCENTRATION = new CONCENTRATION()
                    {
                        CODE = "Concentration4"
                    },
                },
                new FACULTY_CONCENTRATION_COORDINATION()
                {
                    FACULTY_ID = "E0456",
                    PROGRAM_CODE = "Program1",
                    CONCENTRATION = new CONCENTRATION()
                    {
                        CODE = "Concentration3"
                    },

                },
                new FACULTY_CONCENTRATION_COORDINATION()
                {
                    FACULTY_ID = "E0456",
                    PROGRAM_CODE = "Program1",
                    CONCENTRATION = new CONCENTRATION()
                    {
                        CODE = "Concentration4"
                    },
                }
            }.AsQueryable();

            UnitTestUtility.InitDBSet(facultyProgramCoordination, programCoordinationDbSet);
            UnitTestUtility.InitDBSet(facultyConcentrationCoordination, mockFacultyConcentrationCoordnations);
            UnitTestUtility.InitDBSet(program, mockPrograms);

            mockContext.Setup(m => m.PROGRAMs).Returns(mockPrograms.Object);
            mockContext.Setup(m => m.FACULTY_PROGRAM_COORDINATION).Returns(programCoordinationDbSet.Object);
            mockContext.Setup(m => m.FACULTY_CONCENTRATION_COORDINATION).Returns(mockFacultyConcentrationCoordnations.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            sut.DeleteProgramOfStudy("Program2", "E0456");
            programCoordinationDbSet.Verify(m => m.RemoveRange(It.IsAny<IEnumerable<FACULTY_PROGRAM_COORDINATION>>()), Times.Once);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
            var updatedProgram = sut.GetProgramOfStudy("Program2", "E0456");
            Assert.IsNull(updatedProgram, "program hasn't been deleted");
        }

        [Test]
        public void ShouldGetASingleProgramBasedOnProgramCode()
        {
            var programCode = "CSCI-BS";
            var departmentCode = "CSCI";
            var mockContext = new Mock<FTCSEntities>();
            mockContext.SetupGet(m => m.PROGRAM_LEVEL_TYPE).Returns(_mockProgramLevels.Object);
            mockContext.SetupGet(m => m.FACULTY_PROGRAM_COORDINATION).Returns(_mockFacultyProgramCoordinations.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            var program = sut.GetProgramOfStudy(programCode, departmentCode);
            Assert.That(program.ProgramCode, Is.EqualTo(programCode));
        }

        [Test]
        public void ShouldGetTheCredentialingOptions()
        {
            var expectedList = new List<string>() {"Academic","Professional","Graduate Assistant","Unspecified"};
            var mockContext = new Mock<FTCSEntities>();
            var sut = new CredentialManagerService(mockContext.Object);
            var options = sut.GetCertificationOptions().Select(o => o.Text).ToList();
            foreach (var value in options)
            {
                Assert.That(expectedList, Does.Contain(value));
            }
        }

        [Test]
        public void ShouldAddACourseCertificationWhenOneAlreadyExists()
        {
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();
            var mockTcq = new Mock<DbSet<TEACHER_COURSE_QUALIFICATION>>();
            _mockTeacherQualification.Setup(m => m.Add(It.IsAny<TEACHER_COURSE_QUALIFICATION>()));
            var mockContext = new Mock<FTCSEntities>();
            mockContext.SetupGet(m => m.TEACHER_COURSE_QUALIFICATION).Returns(_mockTeacherQualification.Object);
            var sut = new CredentialManagerService(mockContext.Object);
            var result = sut.AddCourseCertification("E001", "CSCI", "100",1,1,0);
           mockContext.Verify(m =>m.SaveChanges(), Times.Once);
        }

       
    }
}
