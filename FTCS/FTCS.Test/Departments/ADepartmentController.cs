﻿using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.Views.Departments;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace FTCS.Test
{
    [Category("Unit")]
    [TestFixture]
    public class ADepartmentController
    {
        Mock<IDepartmentService> mockDepartmentService;
        [SetUp]
        public void Setup()
        {
            mockDepartmentService = new Mock<IDepartmentService>();
        }
        [Test]
        public void ShouldDisplayAListOfDepartmentsWhenIndexIsCalled()
        {
            var mockDepartmentService = new Mock<IDepartmentService>();
            mockDepartmentService.Setup(mock => mock.GetListOfDepartments("", "","")).Returns(new List<DepartmentVM>());
            var sut = new DepartmentController(mockDepartmentService.Object);

            sut.Index("","","", 1);

            mockDepartmentService.Verify(mock => mock.GetListOfDepartments("", "",""), Times.Once);
        }

        [Test]
        public void ShouldSendInDefaultsIntoGetListOfDepartments()
        {
            var mockDepartmentService = new Mock<IDepartmentService>();
            mockDepartmentService.Setup(mock => mock.GetListOfDepartments(
                    It.IsAny<string>(), 
                    It.IsAny<string>(), 
                    It.IsAny<string>())).Returns(new List<DepartmentVM>());
            var sut = new DepartmentController(mockDepartmentService.Object);
            sut.Index(null, null, null,null);
            mockDepartmentService.Verify(m => m.GetListOfDepartments("All", null, "NAME_asc"), Times.Once);
        }
    }
}
