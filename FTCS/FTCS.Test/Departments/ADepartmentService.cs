﻿using FTCS.Models;
using FTCS.Services.Implementation;
using Moq;
using NUnit.Framework;
using FTCS.Services;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System;
using FTCS.ViewModels;

namespace FTCS.Test
{
    [Category("Unit")]
    [TestFixture]
    class ADepartmentService
    {
        private Mock<DbSet<DEPARTMENT>> _departmentsMock;
        private Mock<DbSet<TEACHER_COURSE_RECORD>> _teacherCourseRecordMock;
        private Mock<DbSet<COURSE>> _coursesMock;


        [SetUp]
        public void SetUp()
        {
            /*
            Instantiate new Mock objects over here so that you dont have to do it for
            each test.
            For each test, we will always get a fresh copy of the mocks because they are in
            the Setup method which is run before every test.
            */


            /*
            This section arranges the test data we want our actual implementation to work on
            Caveat: They have to be Casted to IQueryables (the AsQueryable() ) for them to be useful
                    in the projections. 
            */
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();

            

            var teacherCourseRecord = new List<TEACHER_COURSE_RECORD>
            {
                new TEACHER_COURSE_RECORD
                {
                    TEACHER_ID = "E000001",
                    COURSE_NUMBER = "100",
                    COURSE_RUBRIC = "CSCI",
                    ACADEMIC_YEAR = currentSemester.Year,
                    ACADEMIC_MONTH = currentSemester.Month
                }
            }.AsQueryable();


            var departments = new List<DEPARTMENT>()
            {
                new DEPARTMENT() {CODE = "CSCI", NAME = "Computer Science",EXCLUDED = "N", COLLEGE = "BT"},
                new DEPARTMENT() {CODE = "ARTA"},
                new DEPARTMENT() { CODE = "CHEM", NAME = "Chemistry",EXCLUDED = "N", COLLEGE = "SCI"}
            }.AsQueryable();

            var courses = new List<COURSE>
            {
                new COURSE {NUMBER = "100", RUBRIC = "CSCI", DEPARTMENT = "CSCI", DEPARTMENT1 = departments.First(d => d.CODE == "CSCI")},
                new COURSE {NUMBER = "200", RUBRIC = "CHEM", DEPARTMENT = "CHEM", DEPARTMENT1 = departments.First(d => d.CODE == "CHEM")}
            }.AsQueryable();
            /*******************************************************************/
            _departmentsMock = new Mock<DbSet<DEPARTMENT>>();
            UnitTestUtility.InitDBSet(departments, _departmentsMock);

            _coursesMock = new Mock<DbSet<COURSE>>();
            UnitTestUtility.InitDBSet(courses, _coursesMock);

            _teacherCourseRecordMock = new Mock<DbSet<TEACHER_COURSE_RECORD>>();
            UnitTestUtility.InitDBSet(teacherCourseRecord, _teacherCourseRecordMock);
        }


        [Test]
        public void ShouldReturnAListOfDepartmentsWithFacultyMembersGreaterThan0()
        {
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.DEPARTMENTs).Returns(_departmentsMock.Object);
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_teacherCourseRecordMock.Object);
            mockContext.Setup(m => m.COURSEs).Returns(_coursesMock.Object);


            //Here we send in our mockRepositorys when creating our System Under Test
            var sut = new DepartmentService(mockContext.Object);

            //Assuming we know what we should get when in our test we do a project to get the object we are 
            //looking for in our test data.
            var expected = new DepartmentVM
            {
                NAME = "Computer Science",
                NUMBER_OF_TEACHERS = 2
            };

            //Act!!
            var result = sut.GetListOfDepartments("", "", "").Single();

            //Assert!!
            Assert.That(result.NAME, Is.EqualTo(expected.NAME));
        }

        [Test]
        public void ShouldReturnTheEntireListOfDepartmentsWhenAllAndNoSearchTermIsPassedIn()
        {
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.DEPARTMENTs).Returns(_departmentsMock.Object);
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_teacherCourseRecordMock.Object);
            mockContext.Setup(m => m.COURSEs).Returns(_coursesMock.Object);
            var sut = new DepartmentService(mockContext.Object);
            var result = sut.GetListOfDepartments("All", "", "");
            Assert.That(result.Count(), Is.EqualTo(1));
        }

        [Test]
        public void ShouldReturnOnlyDepartmentsThatStartWithC()
        {
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.DEPARTMENTs).Returns(_departmentsMock.Object);
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_teacherCourseRecordMock.Object);
            mockContext.Setup(m => m.COURSEs).Returns(_coursesMock.Object);
            var sut = new DepartmentService(mockContext.Object);
            var result = sut.GetListOfDepartments("C", "", "");
            Assert.That(result.Count(), Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().NAME, Is.EqualTo("Computer Science"));
        }


        [Test]
        public void ShouldReturnADepartmentThatStartsWithALetterAndASearchTerm()
        {
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.DEPARTMENTs).Returns(_departmentsMock.Object);
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_teacherCourseRecordMock.Object);
            mockContext.Setup(m => m.COURSEs).Returns(_coursesMock.Object);
            var sut = new DepartmentService(mockContext.Object);
            var result = sut.GetListOfDepartments("C", "Science", "");
            Assert.That(result.Count(), Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().NAME, Is.EqualTo("Computer Science"));
        }

        [Test]
        public void ShouldReturnAllDepartmentsInAscendingOrder()
        {
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.DEPARTMENTs).Returns(_departmentsMock.Object);
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_teacherCourseRecordMock.Object);
            mockContext.Setup(m => m.COURSEs).Returns(_coursesMock.Object);
            var sut = new DepartmentService(mockContext.Object);
            var result = sut.GetListOfDepartments("All", null, "NAME_asc");

            Assert.That(result, Is.Ordered.By("NAME"));
        }

        [Test]
        public void ShouldReturnAllDepartmentsInDescendingOrder()
        {
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.DEPARTMENTs).Returns(_departmentsMock.Object);
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_teacherCourseRecordMock.Object);
            mockContext.Setup(m => m.COURSEs).Returns(_coursesMock.Object);
            var sut = new DepartmentService(mockContext.Object);
            var result = sut.GetListOfDepartments("All", null, "NAME_desc");

            Assert.That(result, Is.Ordered.By("NAME").Descending);
        }

        [Test]
        public void ShouldReturnADepartmentGivenADepartmentCode()
        {
            var mockContext = new Mock<FTCSEntities>();
            mockContext.Setup(m => m.DEPARTMENTs).Returns(_departmentsMock.Object);
            mockContext.Setup(m => m.TEACHER_COURSE_RECORD).Returns(_teacherCourseRecordMock.Object);
            mockContext.Setup(m => m.COURSEs).Returns(_coursesMock.Object);
            var sut = new DepartmentService(mockContext.Object);
            var result = sut.GetDepartmentByDepartmentCode("CSCI");
            Assert.That(result.NAME, Is.EqualTo("Computer Science"));
        }
    }
}
