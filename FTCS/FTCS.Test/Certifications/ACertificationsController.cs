﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FTCS.Controllers;
using FTCS.Interfaces;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.Task;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FTCS.Test.Certifications
{
    [TestFixture]
    class ACertificationsController
    {
        private Mock<IPrincipal> _mockUser;
        private Mock<IReportService> _mockReportService;
        private Mock<ICredentialManagerService> _mockCredentialManager;
        private Mock<ControllerContext> _mockControllerCtx;

        [SetUp]
        public void Setup()
        {
            _mockUser = new Mock<IPrincipal>();
            _mockReportService = new Mock<IReportService>();
            _mockCredentialManager = new Mock<ICredentialManagerService>();
            _mockControllerCtx = new Mock<ControllerContext>();
        }
        

        [Test]
        public void ShouldGetTheFacultyCredentialsReportForTheDepartmentOfTheUser()
        {
            var department = "CSCI";
            var bannerId = "E00123";
            var currentSemester = AcademicYear.GetCurrentAcademicYear().First();
            _mockUser.SetupGet(m => m.Identity).Returns(UnitTestUtility.GenerateClaimsIdentity(bannerId, "Chair", department, string.Empty));
            _mockCredentialManager.Setup(m => m.GetCertificationOptions());
            _mockControllerCtx = new Mock<ControllerContext>();
            _mockControllerCtx.SetupGet(m => m.HttpContext.User).Returns(_mockUser.Object);
            var sut = new CertificationsController(_mockReportService.Object,_mockCredentialManager.Object) { ControllerContext = _mockControllerCtx.Object };
            var result = sut.Index();
            _mockCredentialManager.Verify(m => m.GetCertificationOptions(), Times.Once);
            _mockReportService.Verify(m => m.GetFacultyCredentialsReport(department, currentSemester.Year));
        }
    }
}
