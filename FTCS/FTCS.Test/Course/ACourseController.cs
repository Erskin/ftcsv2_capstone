﻿using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.Views.Courses;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using FTCS.ViewModels.Course;

namespace FTCS.Test.Course
{
    [TestFixture]
    [Category("Unit")]
    public class ACourseController
    {
        [Category("CourseController")]
        [Test]
        public void ShouldDisplayAListOfCoursesWhenIndexIsCalled()
        {
            var mockCourseService = new Mock<ICourseService>();
            mockCourseService.Setup(mock => mock.GetListOfCourses("", "", "")).Returns(new List<CourseVM>());
            var sut = new CourseController(mockCourseService.Object);
            sut.Index("All", "Name_asc", "", 1);

            mockCourseService.Verify(mock => mock.GetListOfCourses("All", "Name_asc", ""), Times.Once);

        }

    }
}
