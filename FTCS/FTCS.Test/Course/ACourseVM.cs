﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FTCS.Services.Implementation;
using FTCS.ViewModels;
using FTCS.ViewModels.Certifications;
using FTCS.ViewModels.Course;
using NUnit.Framework;

namespace FTCS.Test.Course
{
    [TestFixture]
    [Category("Unit")]
    public class ACourseVM
    {
        [Test]
        public void ShouldFormatCertificationToBeTitleCase()
        {
            var sut = new CourseCredentialVM();
            sut.Credential = new Qualification {Id = 0};
            Assert.That(sut.Credential.Description, Is.EqualTo("Academic"));
        }

        [Test]
        public void ShouldSetCertificationToUnspecifiedIfValueIsUnknown()
        {
            var sut = new CourseCredentialVM();
            sut.Credential = new Qualification {Id = 2};
            Assert.That(sut.Credential.Description, Is.EqualTo("Unspecified"));
        }
    }
}
