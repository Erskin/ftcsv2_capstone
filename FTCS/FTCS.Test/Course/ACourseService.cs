﻿using FTCS.Models;
using FTCS.Services.Implementation;
using Moq;
using NUnit.Framework;
using FTCS.Services;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Data.Entity;
using FTCS.ViewModels;
using FTCS.ViewModels.Course;


namespace FTCS.Test.Course
{
    [Category("CourseService")]
    [Category("Unit")]
    [TestFixture]
    public class ACourseService
    {
        private Mock<FTCSEntities> _mockContext;

        [SetUp]
        public void SetUp()
        {
            _mockContext = new Mock<FTCSEntities>();
            var mockCourses = new List<COURSE>
            {
                new COURSE {NUMBER = "1250", RUBRIC = "CSCI", DEPARTMENT = "CSCI", NAME = "Intro Computer Sci I"},
                new COURSE {NUMBER = "1260", RUBRIC = "CSCI", DEPARTMENT = "CSCI", NAME = "Intro Computer Sci II"},
                new COURSE {NUMBER = "1110", RUBRIC = "ARTA", DEPARTMENT = "ARTA", NAME = "Core Studio I"},
                new COURSE {NUMBER = "1140", RUBRIC = "ARTA", DEPARTMENT = "ARTA", NAME = "Core Studio II"},
                new COURSE {NUMBER = "1010", RUBRIC = "BIOL", DEPARTMENT = "BIOL", NAME = "Biol non-Major Lec I"},
                new COURSE {NUMBER = "1020", RUBRIC = "BIOL", DEPARTMENT = "BIOL", NAME = "Biol non-Major Lec II"},
                new COURSE {NUMBER = "3240", RUBRIC = "ADVR", DEPARTMENT = "COMM", NAME = "Advertising Princples"},
                new COURSE {NUMBER = "2100", RUBRIC = "DANC", DEPARTMENT = "COMM", NAME = "Beginnning Aerial Skills"},
                new COURSE {NUMBER = "2016", RUBRIC = "NURS", DEPARTMENT = "NURS", NAME = "Pathophysiology for Nursing"},
                new COURSE {NUMBER = "2020", RUBRIC = "NURS", DEPARTMENT = "NURS", NAME = "Intro to Nursing Profession"},
            }.AsQueryable();

            var mockDepartmentRecords = new List<DEPARTMENT>()
            {
                new DEPARTMENT() {CODE = "CSCI", NAME = "Computer & Information Sciences"},
                new DEPARTMENT() {CODE = "ARTA", NAME = "Art & Design"},
                new DEPARTMENT() {CODE = "BIOL", NAME = "Biology"},
                new DEPARTMENT() {CODE = "COMM", NAME = "Communication"},
                new DEPARTMENT() {CODE = "NURS", NAME = "Nursing - Undergraduate"}
            }.AsQueryable();
            /*
           This section setups all the Get() methods in each mocked repository to return
           our test data. The It.IsAny(...) thingy basically says when our the Get method is
           called during testing, we don't care what gets passed into the method and then we finally
           return the test data we created.
           */
            var mockCoursesSet = new Mock<DbSet<COURSE>>();
            var mockDepartmentSet = new Mock<DbSet<DEPARTMENT>>();
            UnitTestUtility.InitDBSet(mockCourses, mockCoursesSet);
            UnitTestUtility.InitDBSet(mockDepartmentRecords, mockDepartmentSet);
            _mockContext.Setup(m => m.COURSEs).Returns(mockCoursesSet.Object);
            _mockContext.Setup(m => m.DEPARTMENTs).Returns(mockDepartmentSet.Object);
            /*******************************************************************/
        }

        [Test]
        public void ShouldReturnAListOfCoursesWhereThereIsACourseNumberNameAndDepartment()
        {
            //Here we send in our mockRepositorys when creating our System Under Test

            var sut = new CourseService(_mockContext.Object);
            //Assuming we know what we should get when in our test we do a project to get the object we are 
            //looking for in our test data.
            var expected = new List<CourseVM>
            {
                new CourseVM
                {
                    CourseCode = "ADVR" + " " + "3240",
                    Department = "Communication",
                    Name = "Advertising Princples"
                },
                new CourseVM {CourseCode = "ARTA" + " " + "1110", Department = "Art & Design", Name = "Core Studio I"},
                new CourseVM {CourseCode = "ARTA" + " " + "1140", Department = "Art & Design", Name = "Core Studio II"},
                new CourseVM {CourseCode = "BIOL" + " " + "1010", Department = "Biology", Name = "Biol non-Major Lec I"},
                new CourseVM
                {
                    CourseCode = "BIOL" + " " + "1020",
                    Department = "Biology",
                    Name = "Biol non-Major Lec II"
                },
                new CourseVM
                {
                    CourseCode = "CSCI" + " " + "1250",
                    Department = "Computer & Information Sciences",
                    Name = "Intro Computer Sci I"
                },
                new CourseVM
                {
                    CourseCode = "CSCI" + " " + "1260",
                    Department = "Computer & Information Sciences",
                    Name = "Intro Computer Sci II"
                },
                new CourseVM
                {
                    CourseCode = "DANC" + " " + "2100",
                    Department = "Communication",
                    Name = "Beginnning Aerial Skills"
                },
                new CourseVM
                {
                    CourseCode = "NURS" + " " + "2016",
                    Department = "Nursing - Undergraduate",
                    Name = "Pathophysiology for Nursing"
                },
                new CourseVM
                {
                    CourseCode = "NURS" + " " + "2020",
                    Department = "Nursing - Undergraduate",
                    Name = "Intro to Nursing Profession"
                }
            };

            //Act!!
            var result = sut.GetListOfCourses("", "Name_asc", "");

            CollectionAssert.IsNotEmpty(result);
        }

        [Test]
        public void ShouldBeAbleToReturnAListOfCourseBasedOnAFilter()
        {
            //Here we send in our mockRepositorys when creating our System Under Test
            var sut = new CourseService(_mockContext.Object);
            //Assuming we know what we should get when in our test we do a project to get the object we are 
            //looking for in our test data.
            var expected = new List<CourseVM>
            {
                new CourseVM
                {
                    CourseCode = "CSCI" + " " + "1250",
                    Department = "Computer & Information Sciences",
                    Name = "Intro Computer Sci I"
                },
                new CourseVM
                {
                    CourseCode = "CSCI" + " " + "1260",
                    Department = "Computer & Information Sciences",
                    Name = "Intro Computer Sci II"
                },
                new CourseVM
                {
                    CourseCode = "NURS" + " " + "2020",
                    Department = "Nursing - Undergraduate",
                    Name = "Intro to Nursing Profession"
                }
            };

            //Act!!
            var result = sut.GetListOfCourses("I", "", "");

            //Assert!!
            foreach(var item in result)
            {
                Assert.That(item.Name[0], Is.EqualTo('I'));
            }
        }

        [Test]
        public void ShouldBeAbleToReturnAListOfCourseBasedOnASearchString()
        {
            //Here we send in our mockRepositorys when creating our System Under Test
            var sut = new CourseService(_mockContext.Object);
            var searchTerm = "Intro";
            //Assuming we know what we should get when in our test we do a project to get the object we are 
            //looking for in our test data.
            var expected = new List<CourseVM>
            {
                new CourseVM
                {
                    CourseCode = "CSCI" + " " + "1250",
                    Department = "Computer & Information Sciences",
                    Name = "Intro Computer Sci I"
                },
                new CourseVM
                {
                    CourseCode = "CSCI" + " " + "1260",
                    Department = "Computer & Information Sciences",
                    Name = "Intro Computer Sci II"
                },
                new CourseVM
                {
                    CourseCode = "NURS" + " " + "2020",
                    Department = "Nursing - Undergraduate",
                    Name = "Intro to Nursing Profession"
                }
            };

            //Act!!
            var result = sut.GetListOfCourses("", "", searchTerm);

            //Assert!!
            foreach (var item in result)
            {
                Assert.That(item.Name, Does.Contain(searchTerm));
            }
        }

        [Test]
        public void ShouldBeAbleToReturnAListOfCourseBasedOnAFirstLetterAndSearchString()
        {
            //Here we send in our mockRepositorys when creating our System Under Test
            var sut = new CourseService(_mockContext.Object);
            //Assuming we know what we should get when in our test we do a project to get the object we are 
            //looking for in our test data.
            var courseName = "Core Studio I";
            var expected = new List<CourseVM>
            {
                new CourseVM {CourseCode = "ARTA" + " " + "1110", Department = "Art & Design", Name = courseName}
            };

            //Act!!
            var filter = "C";
            var result = sut.GetListOfCourses(filter, courseName, "");

            //Assert!!

            foreach (var item in result)
            {
                Assert.That(item.Name, Does.Contain(courseName));
                Assert.That(item.Name[0].ToString(), Is.EqualTo(filter));
            }
        }
    }
}
