﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FTCS.Controllers;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.Task;
using Moq;
using NUnit.Framework;

namespace FTCS.Test.ProgramOfStudy
{
    class AProgramOfStudyController
    {
        private Mock<IPrincipal> _mockUser;
        private Mock<IDepartmentService> _mockDepartmentService;

        [SetUp]
        public void SetUp()
        {
            _mockUser = new Mock<IPrincipal>();
            _mockDepartmentService = new Mock<IDepartmentService>();
        }

        [Test]
        public void ShouldCallGetProgramOfStudyForDepartmentWhenChairDashboardIsCalled()
        {
            var department = "CSCI";
            var bannerId = "E00123";
            _mockUser.SetupGet(m => m.Identity).Returns(UnitTestUtility.GenerateClaimsIdentity(bannerId, "Chair", department, string.Empty));
            var mockDashboardService = new Mock<ICredentialManagerService>();
            
            mockDashboardService.Setup(m => m.GetProgramsOfStudyForDepartment(department)).Returns(new List<ProgramsVM>());
            var mockControllerContext = new Mock<ControllerContext>();
            mockControllerContext.SetupGet(m => m.HttpContext.User).Returns(_mockUser.Object);
            var sut = new ProgramOfStudyController(mockDashboardService.Object,_mockDepartmentService.Object) { ControllerContext = mockControllerContext.Object };
            var result = sut.Index();
            mockDashboardService.Verify(m => m.GetProgramsOfStudyForDepartment(department), Times.Once);
        }

        [Test]
        public void ShouldCallGetProgramsOfStudyForCollegeWithTheCredentialOfficersCollege()
        {
            var bannerId = "E00123";
            var college = "BT";
            _mockUser.SetupGet(m => m.Identity).Returns(UnitTestUtility.GenerateClaimsIdentity(bannerId, "Chair", string.Empty, college));
            var mockCredentialManagerService = new Mock<ICredentialManagerService>();
            mockCredentialManagerService.Setup(m => m.GetProgramsOfStudyForCollege(college)).Returns(new List<ProgramsVM>());
            var mockControllerContext = new Mock<ControllerContext>();
            mockControllerContext.SetupGet(m => m.HttpContext.User).Returns(_mockUser.Object);
            var sut = new ProgramOfStudyController(mockCredentialManagerService.Object, _mockDepartmentService.Object) { ControllerContext = mockControllerContext.Object };
            var result = sut.Index();
            mockCredentialManagerService.Verify(m => m.GetProgramsOfStudyForCollege(college), Times.Once);
        }

        [Test]
        public void ShouldGetCoordinatorDropDownOptionsWhenShowChangeProgramCoordinatorIsCalled()
        {
            var bannerId = "E00123";
            var college = "BT";
            var departmentCode = "CSCI";
            var programCode = "CSCI-BS";
            _mockUser.SetupGet(m => m.Identity).Returns(UnitTestUtility.GenerateClaimsIdentity(bannerId, "Chair", string.Empty, college));
            var mockCredentialManagerService = new Mock<ICredentialManagerService>();
            mockCredentialManagerService.Setup(m => m.GetCoordinatorOptions(string.Empty, college));
            var mockControllerContext = new Mock<ControllerContext>();
            mockControllerContext.SetupGet(m => m.HttpContext.User).Returns(_mockUser.Object);
            var sut = new ProgramOfStudyController(mockCredentialManagerService.Object, _mockDepartmentService.Object) { ControllerContext = mockControllerContext.Object };
            sut.ShowChangeProgramCoordinator(programCode, departmentCode);
            mockCredentialManagerService.Verify(m => m.GetCoordinatorOptions(string.Empty, college), Times.Once);
        }

        [Test]
        public void ShouldGetAProgramOfStudyWhenShowChangeProgramCoordinatorIsCalled()
        {
            var bannerId = "E00123";
            var college = "BT";
            var departmentCode = "CSCI";
            var programCode = "CSCI-BS";
            _mockUser.SetupGet(m => m.Identity).Returns(UnitTestUtility.GenerateClaimsIdentity(bannerId, "Chair", string.Empty, college));
            var mockCredentialManagerService = new Mock<ICredentialManagerService>();
            mockCredentialManagerService.Setup(m => m.GetProgramOfStudy(programCode, departmentCode));
            var mockControllerContext = new Mock<ControllerContext>();
            mockControllerContext.SetupGet(m => m.HttpContext.User).Returns(_mockUser.Object);
            var sut = new ProgramOfStudyController(mockCredentialManagerService.Object, _mockDepartmentService.Object) { ControllerContext = mockControllerContext.Object };
            sut.ShowChangeProgramCoordinator(programCode, departmentCode);
            mockCredentialManagerService.Verify(m => m.GetProgramOfStudy(programCode, departmentCode), Times.Once);
        }

        
    }
}

