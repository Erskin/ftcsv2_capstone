﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using FTCS.Controllers;
using FTCS.Interfaces;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.Task;
using Moq;
using NUnit.Framework;

namespace FTCS.Test.Task
{
    [TestFixture]
    public class ATaskController
    {
        private Mock<IPrincipal> _mockUser;

        [SetUp]
        public void SetUp()
        {
           _mockUser = new Mock<IPrincipal>(); 
        }

       

        

        

        private void mockHomeControllerDependencies(Mock<ICredentialManagerService> mockCredentialManagerService,
            Mock<IDepartmentService> mockDepartmentService,
            Mock<IReportService> mockReportService,
            string role,
            string department,
            string college)
        {
            mockCredentialManagerService.Setup(m => m.GetProgramsOfStudyForDepartment(It.IsAny<string>()))
                .Returns(new List<ProgramsVM>());
            mockCredentialManagerService.Setup(m => m.GetProgramsOfStudyForCollege(It.IsAny<string>()))
                .Returns(new List<ProgramsVM>());
            mockDepartmentService.Setup(m => m.GetDepartmentByDepartmentCode(It.IsAny<string>()))
                .Returns(new DepartmentVM() { DEPARTMENT_CODE = "CSCI", NAME = "Computer Science" });
            mockReportService.Setup(m => m.GetIncompleteProfessionalQualification(It.IsAny<string>()))
                .Returns(new List<FacultyRosterVM>());
            mockReportService.Setup(m => m.GetFacultyCredentialsReport(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(new List<FacultyCredentialsReportVM>() { });
            _mockUser.SetupGet(m => m.Identity)
                .Returns(UnitTestUtility.GenerateClaimsIdentity("E00123", role, department, college));
        }


    }
}
