﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
    public static partial class LoginChrome
    {
        private static IWebDriver _page = null;

        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }

        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }

        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }

        public static void EnterUsername(string username)
        {
            _page.FindElement(By.CssSelector("#UserName")).SendKeys(username);
        }

        public static void EnterPassword(string password)
        {
            _page.FindElement(By.CssSelector("#Password")).SendKeys(password);
        }

        public static void ClickLogin()
        {
            _page.FindElement(By.CssSelector("body > div.body-content > div > div:nth-child(2) > form > div:nth-child(4) > div > input")).Click();
        }

        public static string GetAccountName()
        {
            return _page.FindElement(By.CssSelector("#site-header > div.row > div.col-lg-3.col-md-3.col-sm-3.col-xs-3.col-right > div:nth-child(1) > div > button")).Text;
        }

        public static string GetErrorMessage()
        {
            return _page.FindElement(By.CssSelector("body > div.body-content > div > div:nth-child(2) > form > div.row.padding-top-2.row-centered > div > div > div > ul > li")).Text;
        }

        public static string GetUserError()
        {
            return _page.FindElement(By.CssSelector("body > div.body-content > div > div:nth-child(2) > form > div.row.padding-top-2.row-centered > div > div > div > ul > li:nth-child(1)")).Text;
        }

        public static string GetPasswordError()
        {
            return _page.FindElement(By.CssSelector("body > div.body-content > div > div:nth-child(2) > form > div.row.padding-top-2.row-centered > div > div > div > ul > li:nth-child(2)")).Text;
        }
    }
}
