﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
    public  static partial class Chrome
    {
        private static IWebDriver _page = null;
       
       
        public static string Text
        {
            get
            {
                return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(1) > h1")).Text;
            }
        }

        public static string GetAccountName()
        {
            throw new NotImplementedException();
        }

        public static string ETSUMission
        {
            get
            {
                return _page.FindElement(By.CssSelector("#etsu-mission-link")).GetAttribute("href");
            }
        }
        public static void HoverOnReports()
        {
            var hoverAction = new Actions(_page);
            var navBarReportsLink = _page.FindElement(By.Id("reports-link"));
            hoverAction.MoveToElement(navBarReportsLink).Perform();
        }       
         
        

        
        
        public static string TeachingCredentialStandard
        {
            get
            {
                return _page.FindElement(By.CssSelector("#teaching-credential-standards-link")).GetAttribute("href");
            }
        }
        public static string GraduateFacultyAppointmentGuidelines
        {
            get
            {
                return _page.FindElement(By.CssSelector("#graduate-faculty-appointment-guidelines-link")).GetAttribute("href");
            }
        }
        public static string SACSGuidelines
        {
            get
            {
                return _page.FindElement(By.CssSelector("#sacs-guidelines-link")).GetAttribute("href");
            }
        }
        
        public static IReadOnlyCollection<string> BrowserTabs()
        {
            return _page.WindowHandles;
        }
        public static string ChairInstructions
        {
            get
            {
                return _page.FindElement(By.CssSelector("#chair-instructions-link")).Text;
            }
        }
        public static void SwitchToLastTab()
        {
            _page.SwitchTo().Window(_page.WindowHandles.Last());
        }
        public static string pageTitle
        {
            get
            {
                return _page.Title;
            }
        }
        public static string GetFilterStartsWithMessage()
        {
            return _page.FindElement(By.Id("starts-with-message")).Text;
        }
        public static IEnumerable<string> GetDepartmentNamesOnFacultyPage()
        {
            var elements = _page.FindElements(By.CssSelector("tbody tr td:last-of-type"));
            var listOfDepartments = elements.Select(el => el.Text);
            return listOfDepartments;
        }
        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }
        
        
        public static IEnumerable<string> ListOfFacultyName { get { return _page.FindElements(By.CssSelector("#content-table > thead > tr > th:nth-child(1) > a")).Select(e => e.Text); } }
       
        public static IEnumerable<int> ColumnData(int columnNumber)
        {
            return _page.FindElements(By.CssSelector(string.Format("tr > td:nth-child({0})", columnNumber))).Select(e => Convert.ToInt32(e.Text)).ToArray();
        }  
        
        public static string GetFirstCourseCode()
        {
            return _page.FindElements(By.CssSelector("#ftcs-table > article > table > tbody > tr:nth-child(1) > td:nth-child(1)")).First().Text;
        }
        public static void HoverOnSearchBy()
        {
            Actions hoverAction = new Actions(_page);
            var searchBy = _page.FindElement(By.Id("search-faculty-link"));
            hoverAction.MoveToElement(searchBy).Perform();
        }
        public static void ClickCourseHeader()
        {
            _page.FindElement(By.CssSelector("#content-table > thead > tr > th:nth-child(1) > a")).Click();
        }
       
        public static void ClickAccountingLink()
        {
            _page.FindElement(By.CssSelector(@"td a[href='/Faculty/All/Accounting']")).Click();
        }
        public static void ClickReportsNavButton()
        {
            _page.FindElement(By.CssSelector("#ftcs-nav-bar-button.reports")).Click();
        }
        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        
        
        
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }
        public static void ClickLinkToAllDepartmentPage()
        {
            _page.FindElement(By.CssSelector("#search-by-department-link")).Click();
        }
        public static void ClickLinkToAllCoursePage()
        {
            _page.FindElement(By.CssSelector("#ftcs-dropdown-options > li:nth-child(3) > a")).Click();
        }

        public static void ClickSearchFacultyNavButton()
        {
            _page.FindElement(By.CssSelector(@"#menu")).Click();
        }
        public static void ClickLinkToAllFacultyPage()
        {
            _page.FindElement(By.CssSelector("#ftcs-dropdown-options > li:nth-child(2) > a")).Click();
        }
        public static void ClickLinkToCourseSectionQualificationPercentageReport()
        {
            _page.FindElement(By.CssSelector("#ftcs-dropdown-options > li:nth-child(1) > a")).Click();
        }
        public static void ClickLinkToFacultyCredentialReport()
        {
            _page.FindElement(By.CssSelector("#ftcs-dropdown-options > li:nth-child(2) > a")).Click();
        }
        public static void ClickLinkToFacultyRosterReport()
        {
            _page.FindElement(By.CssSelector("#ftcs-dropdown-options > li:nth-child(3) > a")).Click();
        }
        public static void ClickLinkToGraduateAssistantRosterReport()
        {
            _page.FindElement(By.CssSelector("#graduate-assistant-roster-link")).Click();
        }
        public static void ClickETSUMissionButton()
        {
            _page.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(4) > div > a:nth-child(1)")).Click();
        }
        public static void ClickTeachingCredentialStandardsButton()
        {
            _page.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(4) > div > a:nth-child(2)")).Click();
        }
        public static void ClickSACSGuidelinesButton()
        {
            _page.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(4) > div > a:nth-child(3)")).Click();
        }
        public static void ClickChairInstructionsButton()
        {
            _page.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(4) > div > a:nth-child(5)")).Click();
        }
        public static void ClickGraduateFacultyAppointmentGuidelinesButton()
        {
            _page.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(4) > div > a:nth-child(6)")).Click();
        }
        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
        public static IEnumerable<string> GetFacultyNamesOnFacultyPage()
        {
            var elements = _page.FindElements(By.XPath("tbody tr td:first-of-type"));
            var listOfFaculty = elements.Select(el => el.Text);
            return listOfFaculty;
        }
        public static object GetFirstFacultyName()
        {
            return _page.FindElements(By.CssSelector("#ftcs-table > table > tbody > tr:nth-child(1) > td:nth-child(1)")).First().Text;
        }
        public class NavigationBar
        {
            public static void ClickHome()
            {
                _page.FindElement(By.CssSelector("#home-link")).Click();
            }

            public static void HoverOnSearchForFaculty()
            {
                var hoverAction = new Actions(_page);
                var navBarFacultySearchLink = _page.FindElement(By.Id("search-faculty-link"));
                hoverAction.MoveToElement(navBarFacultySearchLink).Perform();
            }
            public static void ClickCourseSectionQualificationsPercentageReport()
            {
                _page.FindElement(By.CssSelector("#course-section-qualifications-link")).Click();
            }
            public static void ClickFacultyCredentialsReport()
            {
                _page.FindElement(By.CssSelector("#faculty-credentials-report-link")).Click();
            }
            public static void ClickFacultyRosterReport()
            {
                _page.FindElement(By.CssSelector("#faculty-roster-link")).Click();
            }
            public static void ClickGraduateAssistantRosterReport()
            {
                _page.FindElement(By.CssSelector("#graduate-assistant-roster-link")).Click();
            }
            public static void ClickContactUs()
            {
                _page.FindElement(By.CssSelector("#contact-us-link")).Click();
            }
            public static void ClickByDepartment()
            {
                _page.FindElement(By.CssSelector("#search-by-department-link")).Click();
            }
            public static void ClickByName()
            {
                _page.FindElement(By.CssSelector("#search-by-faculty-link")).Click();
            }
            public static void ClickByCourse()
            {
                _page.FindElement(By.CssSelector("#search-by-course-link")).Click();
            }
            public static string CourseSectionQualificationsPercentageReportTitle
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(1) > div.col-lg-10.col-md-10.col-sm-10.col-xs-10.padding-top-4 > h1")).Text; }
            }
            public static string FacultyCredentialsReportTitle
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.content-title > div.col-lg-6.col-md-8.col-sm-10.col-xs-10.padding-top-4 > h1")).Text; }
            }
            public static string FacultyRosterReportTitle
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(1) > div.col-lg-6.col-md-6.col-sm-10.col-xs-10.padding-top-4 > h1")).Text; }
            }
            public static string GraduateAssistantRosterReportTitle
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.content-title > div.col-lg-10.col-md-10.col-sm-10.col-xs-10.padding-top-4 > h1")).Text; }
            }
            public static string Home
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(1) > h1")).Text; }
            }
            public static string ContactUsText
            {
                get { return _page.FindElement(By.CssSelector("")).Text; }
            }
            public static string ByDepartmentTitle
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.content-title.padding-top-4 > h1")).Text; }
            }
            public static string ByCourseTitle
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.content-title.padding-top-4 > h1")).Text; }
            }
            public static string ByNameTitle
            {
                get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.content-title.padding-top-4 > h1")).Text; }
            }
        }
    }
}
