﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
    public static partial class ViewAllFacultyChrome
    {
        private static IWebDriver _page = null;

        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }
        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }

        public static void ClickFaculty(string faculty)
        {
            _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(1) > td:nth-child(1) > a")).Click();
        }

        public static IWebElement ClickColumnHeader(int v)
        {
            return _page.FindElement(By.CssSelector(string.Format("thead tr th:nth-child({0}) a", v)));
        }
        public static string GetFacultyByName(string faculty)
        {
            var i = 1;
            var name = "";
            while (faculty != name)
            {
                name = _page.FindElements(By.XPath("/html/body/div[3]/div/div/div[4]/table/tbody/tr["+ i +"]/td[1]")).First().Text;
                i++;
            }
            return name;
        }
        public static object GetFirstFacultyName()
        {
            return _page.FindElements(By.CssSelector("#ftcs-table > table > tbody > tr:nth-child(1) > td:nth-child(1)")).First().Text;
        }
        public static void ClickLetter(string letter)
        {
            char[] letters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            int i = 0;
            while (letter != letters[i].ToString())
            {
                i++;
            }

            var LetterButton = _page.FindElement(By.CssSelector("#filter-letters > div > a:nth-child(" + (i + 2) + ")"));
            LetterButton.Click();
        }
        public static string FilterSearchBox
        {
            set
            {
                var searchbox = _page.FindElement(By.CssSelector("#search-text-box"));
                searchbox.Clear();
                searchbox.SendKeys(value);
            }
        }
        public static void ClickFilterSearchButton()
        {
            _page.FindElement(By.CssSelector("#search-button")).Click();
        }
        public static IEnumerable<string> GetDepartmentNamesOnFacultyPage()
        {
            var elements = _page.FindElements(By.CssSelector("tbody tr td:last-of-type"));
            var listOfDepartments = elements.Select(el => el.Text);
            return listOfDepartments;
        }
        public static string pageTitle
        {
            get
            {
                return _page.Title;
            }
        }
        public static IEnumerable<string> ListOfFacultyDepartmentName
        {
            get
            {
                return _page.FindElements(By.CssSelector("#content-table > thead > tr > th:nth-child(2) > a")).Select(e => e.Text);
            }
        }
        public static IEnumerable<string> GetFacultyNamesOnFacultyPage()
        {
            var elements = _page.FindElements(By.XPath("tbody tr td:first-of-type"));
            var listOfFaculty = elements.Select(el => el.Text);
            return listOfFaculty;
        }
        public static IEnumerable<string> ListOfFacultyName
        {
            get
            {
                return _page.FindElements(By.CssSelector("#content-table > thead > tr > th:nth-child(1) > a")).Select(e => e.Text);
            }
        }
    }
}
