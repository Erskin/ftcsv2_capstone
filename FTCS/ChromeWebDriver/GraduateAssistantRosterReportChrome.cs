﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
    public static partial class GraduateAssistantRosterReportChrome
    {
        private static IWebDriver _page = null;
        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }
        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }
        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
       

        public static void ClickGraduateAssistantRosterReport()
        {
            _page.FindElement(By.CssSelector("#graduate-assistant-roster-link")).Click();
        }

        public static void HoverOnTheReports()
        {
            Actions hoverAction = new Actions(_page);
            var navBarReportsLink = _page.FindElement(By.Id("reports-link"));
            hoverAction.MoveToElement(navBarReportsLink).Perform();
        }

        public static void ClickYearMenu()
        {
            _page.FindElement(By.CssSelector("#Year")).Click();
        }
        public static object GetYearSelectOption
        {
            get
            {
                var selectElement = _page.FindElement(By.Id("Year"));
                var yearSelect = new SelectElement(selectElement);
                return yearSelect.SelectedOption.Text;
            }
        }

        public static void clickSubmit()
        {
            _page.FindElement(By.CssSelector("#report-filter-submit")).Click();
        }

        public static string GraduateAssistantRosterReportTitle
        {
            get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.content-title > div.col-lg-10.col-md-10.col-sm-10.col-xs-10.padding-top-4 > h1")).Text; }
        }

        public static string ViewFaculty(string faculty)
        {
            int i = 0;
            string f = "";
            while (f != faculty)
            {
                i++;
                f = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child("+ i + ") > td:nth-child(2)")).Text;
                                                     
            }
            return f;

        }

        public static string ViewDepartment(string department)
        {
            int i = 0;
            string d = "";
            while (d!= department)
            {
                i++;
                d = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child("+i+") > td:nth-child(1)")).Text;

            }
            return d;
        }

        public static string ViewCourse(string course)
        {
            int i = 0;
            string c = "";
            while (c != course)
            {
                i++;
                c = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child("+ i + ") > td:nth-child(4) > ul:nth-child(1) > li > ul > li")).Text;

            }
            return c;
        }

        public static string ViewQualification(string qualification)
        {
            int i = 0;
            string q = "";
            while (q != qualification)
            {
                i++;
                q = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(" + i + ") > td:nth-child(5) > ul > li")).Text;
            }
                                                     
            return q;
        }

        public static string ViewDegree(string degree)
        {
            int i = 0;
            string d = "";
            while (d != degree)
            {
                i++;
                d = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(" +i+ ") > td:nth-child(3) > ul > li")).Text;

            }
            return d;
        }



        public static void SelectYearInSelectOption(string year)
        {
            var selectElement = _page.FindElement(By.Id("Years"));
            var yearSelect = new SelectElement(selectElement);
            yearSelect.SelectByText(year);
        }
        public static void SelectYear(string year)
        {
            string s = "";
            int i = 0;
            while (s != year)
            {
                i++;
                s = _page.FindElement(By.CssSelector("#Year > option:nth-child(" + i + ")")).Text;
            }
            _page.FindElement(By.CssSelector("#Year > option:nth-child(" + i + ")")).Click();
        }
    }
}
