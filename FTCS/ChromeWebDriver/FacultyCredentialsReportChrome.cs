﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
   public static partial class FacultyCredentialsReportChrome
    {
        private static IWebDriver _page = null;

        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }
        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }
        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
        public static string FindTeacher(string teacher, string course)
        {
            int i = 0;
            string t = "";
            string c = "";
            while (t != teacher || c != course)
            {
                i++;
                t = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)")).Text;
                c = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(" + i + ") > td:nth-child(3)")).Text;
            }
            return c;

        }
        public static void SelectSemester(string semester)
        {
            string s = "";
            int i = 0;
            while (s != semester)
            {
                i++; 
                s = _page.FindElement(By.CssSelector("#year > option:nth-child(" + i + ")")).Text;
            }
            _page.FindElement(By.CssSelector("#year > option:nth-child(" + i + ")")).Click();
        }
        public static void SelectDepartment(string department)
        {
            string s = "";
            int i = 0;
            while (s != department)
            {
                i++;
                s = _page.FindElement(By.CssSelector("#department > option:nth-child("+ i +")")).Text;
            }
            _page.FindElement(By.CssSelector("#department > option:nth-child(" + i + ")")).Click();
        }
        public static string GetBackgroundColor()
        {
            int i = 0;
            string s = "";
            while (s != "Unspecified")
            {
                i++;
                s = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)")).Text;

            }
            return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4)")).GetCssValue("background-color");
        }
        public static void ClickDepartMenu()
        {
            _page.FindElement(By.CssSelector("#department")).Click();
        }

        public static void ClickSubmitButton()
        {
            _page.FindElement(By.CssSelector("#submit-button")).Click();
        }
        public static void ClickSemester()
        {
            _page.FindElement(By.CssSelector("#year")).Click();
        }
       
        public static void ClickPDFButton()
        {
            _page.FindElement(By.CssSelector("#pdf-button")).Click();
        }
        public static string FacultyCredentialsPDF
        {
            get
            {
                return _page.FindElement(By.CssSelector("#pdf-button")).GetAttribute("href");
            }
        }
    }
}
