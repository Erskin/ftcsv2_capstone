﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
    public static partial class FacultyRosterReportChrome
    {
        private static IWebDriver _page = null;

        public static void ClickFacultyRosterLink()
        {
            _page.FindElement(By.CssSelector("#faculty-roster-link")).Click();
        }

        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }
        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }
        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
        public static void ClickFacultyTypeMenu()
        {
            _page.FindElement(By.CssSelector("#FacultyType")).Click();
        }
        public static void ClickDepartmentMenu()
        {
            _page.FindElement(By.CssSelector("#Department")).Click();
        }

        public static object FindTeacherQualification(string teacher, string qualification)
        {
            int i = 0;
            string t = "";
            string q = "";
            while (t != teacher || q != qualification)
            {
                i++;
                t = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.padding-top-2 > table > tbody > tr:nth-child("+ i +") > td:nth-child(1)")).Text;
                q = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.padding-top-2 > table > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > ul > li")).Text;
            }
            return q;
        }
        public static void ClickPDFButton()
        {
            _page.FindElement(By.CssSelector("#pdf-button")).Click();
        }

        public static void ClickYearMenu()
        {
            _page.FindElement(By.CssSelector("#Year")).Click();
        }
        public static void ClickSubmitButton()
        {
            _page.FindElement(By.CssSelector("#report-filter-submit")).Click();
        }
        public static void SelectDepartment(string department)
        {
            string s = "";
            int i = 0;
            while (s != department)
            {
                i++;
                s = _page.FindElement(By.CssSelector("#Department > option:nth-child(" + i +")")).Text;
            }
            _page.FindElement(By.CssSelector("#Department > option:nth-child(" + i +")")).Click();
        }

        public static string FindTeacherCourse(string teacher, string course)
        {
            int i = 0;
            string t = "";
            string c = "";
            while (t != teacher || c != course)
            {
                i++;
                t = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.padding-top-2 > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)")).Text;
                c = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.padding-top-2 > table > tbody > tr:nth-child("+ i + ") > td:nth-child(3) > ul:nth-child(1) > li > ul > li:nth-child(1)")).Text;
            }
            return c;
        }
        public static string FindTeacherDegree(string teacher, string degree)
        {
            int i = 0;
            string t = "";
            string d = "";
            while (t != teacher && d !=degree )
            {
                i++;
                t = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.padding-top-2 > table > tbody > tr:nth-child("+ i + ") > td:nth-child(1)")).Text;
                d = _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.padding-top-2 > table > tbody > tr:nth-child("+ i +") > td:nth-child(2) > ul > li:nth-child(1)")).Text;
            }
            return d;

        }
        public static void SelectYear(string year)
        {
            string s = "";
            int i = 0;
            while (s != year)
            {
                i++;
                s = _page.FindElement(By.CssSelector("#Year > option:nth-child(" + i +")")).Text;
            }
            _page.FindElement(By.CssSelector("#Year > option:nth-child(" + i + ")")).Click();
        }
        public static void SelectFacultyType(string type)
        {
            string s = "";
            int i = 0;
            while (s != type)
            {
                i++; 
                s = _page.FindElement(By.CssSelector("#FacultyType > option:nth-child(" + i + ")")).Text;
            }
            _page.FindElement(By.CssSelector("#FacultyType > option:nth-child(" + i + ")")).Click();
        }
    }
}
