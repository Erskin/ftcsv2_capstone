﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
    public static class FacultyProfileChrome
    { 
        private static IWebDriver _page = null;
        public static void FPQuit()
        {
            _page.Dispose();
            _page.Quit();
        }

        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }

        public static class ProfessionalQualificationsChrome
        {
            public static void ClickProfessionalQualifications(string professionalQualificationsOption)
            {
                var ProfessionalQualificationsOption = _page.FindElement(By.CssSelector("#profile-qualifications-button"));
                ProfessionalQualificationsOption.Click();
            }
            public static string Narrative { get { return _page.FindElement(By.CssSelector("#narrative-table > tbody > tr > td")).Text; } }
            public static string RelatedWorkExperienceEndDate { get; set; }
            public static string RelatedWorkExperiencePlace { get { return _page.FindElement(By.CssSelector("#related-work-table > tbody > tr:nth-child(1) > td:nth-child(1)")).Text; } }
            public static string RelatedWorkExperiencePosition { get { return _page.FindElement(By.CssSelector("#related-work-table > tbody > tr:nth-child(1) > td:nth-child(2)")).Text; } }
            public static string RelatedWorkExperienceResponsibilities { get { return _page.FindElement(By.CssSelector("#related-work-table > tbody > tr:nth-child(1) > td:nth-child(3)")).Text; } }
            public static string RelatedWorkExperienceStartDate { get; set; }
            public static string RelatedWorkExperienceTitle { get { return _page.FindElement(By.CssSelector("#related-work-table-title > h2")).Text; } }
        }

        public static string FacultyName(string facultyName)
        {
            return _page.FindElement(By.CssSelector("#faculty-name")).Text;

        }

        public static class CoursesTaughtChrome
        {
            public static string AllCoursesTaughtButtonText { get { return _page.FindElement(By.CssSelector("#all-courses-button")).Text; } }
            public static string AllCoursesTaughtCourseCertification { get { return _page.FindElement(By.CssSelector("#courses-taught-table > tbody > tr:nth-child(1) > td:nth-child(4)")).Text; } }
            public static string AllCoursesTaughtCourseID { get { return _page.FindElement(By.CssSelector("#courses-taught-table > tbody > tr:nth-child(1) > td:nth-child(1)")).Text; } }
            public static string AllCoursesTaughtCourseTerm { get { return _page.FindElement(By.CssSelector("#courses-taught-table > tbody > tr:nth-child(1) > td:nth-child(3)")).Text; } }
            public static string AllCoursesTaughtCourseTitle { get { return _page.FindElement(By.CssSelector("#courses-taught-table > tbody > tr:nth-child(1) > td:nth-child(2)")).Text; } }
            public static string CourseCertification { get { return _page.FindElement(By.CssSelector("#course-taught-table > tbody > tr:nth-child(1) > td:nth-child(5)")).Text; } }
            public static string CourseDescription { get { return _page.FindElement(By.CssSelector("#course-taught-table > tbody > tr:nth-child(1) > td:nth-child(3)")).Text; } }
            public static string CourseID { get { return _page.FindElement(By.CssSelector("#course-taught-table > tbody > tr:nth-child(1) > td:nth-child(1)")).Text; } }
            public static string CourseSemester { get { return _page.FindElement(By.CssSelector("#course-taught-table > tbody > tr:nth-child(1) > td:nth-child(4)")).Text; } }
            public static string CoursesTaughtTableTitle { get { return _page.FindElement(By.CssSelector("#faculty-profile-panel > div > div:nth-child(1) > h2")).Text; } }
            public static string CourseTitle { get { return _page.FindElement(By.CssSelector("#course-taught-table > tbody > tr:nth-child(1) > td:nth-child(2)")).Text; } }
            public static void ClickCoursesTaughtOption(string coursesTaughtOption)
            {
                var CoursesTaughtOption = _page.FindElement(By.CssSelector("#courses-taught-button"));
                CoursesTaughtOption.Click();
            }
            public static void ClickAllCoursesTaughtButton(string allCoursesTaughtButton)
            {
                var AllCoursesTaughtButton = _page.FindElement(By.CssSelector("#all-courses-button"));
                AllCoursesTaughtButton.Click();
            }
        }
        public static class AcademicQualification
        {
            public static void ClickAcademicQualificationOption(string academicQualifications)
            {
                var academicQualificationsButton = _page.FindElement(By.CssSelector("#academic-qualifications-button"));
                academicQualificationsButton.Click();
            }
            public static string Degree { get { return _page.FindElement(By.CssSelector("#academic-degree-table > tbody > tr:nth-child(1) > td:nth-child(1)")).Text; } }
            public static string DegreeInitial { get { return _page.FindElement(By.CssSelector("#academic-degree-table > tbody > tr:nth-child(1) > td:nth-child(2)")).Text; } }
            public static string Discipline { get { return _page.FindElement(By.CssSelector("#academic-degree-table > tbody > tr:nth-child(1) > td:nth-child(3)")).Text; } }
            public static string Email { get { return _page.FindElement(By.CssSelector("#faculty-email > a")).Text; } }
            public static string FacultyDateOfRank { get { return _page.FindElement(By.CssSelector("#date-of-rank")).Text; } }
            public static string FacultyEligibleRankDate { get { return _page.FindElement(By.CssSelector("#next-rank-date")).Text; } }
            public static string FacultyEmploymentCategory { get { return _page.FindElement(By.CssSelector("#employement-category")).Text; } }
            public static string FacultyEmploymentStatus { get { return _page.FindElement(By.CssSelector("#employment-status")).Text; } }
            public static string FacultyGraduateStatus { get { return _page.FindElement(By.CssSelector("#faculty-status")).Text; } }
            public static string FacultyName { get { return _page.FindElement(By.CssSelector("#faculty-name")).Text; } }
            public static string FacultyRank { get { return _page.FindElement(By.CssSelector("#faculty-rank")).Text; } }
            public static string FacultyTenureStatus { get { return _page.FindElement(By.CssSelector("#tenure-status")).Text; } }
            public static string FacultyTitle { get { return _page.FindElement(By.CssSelector("#faculty-title > h3")).Text; } }
            public static string Institution { get { return _page.FindElement(By.CssSelector("#academic-degree-table > tbody > tr:nth-child(1) > td:nth-child(4)")).Text; } }
            public static string Transcript { get { return _page.FindElement(By.CssSelector("#academic-degree-table > tbody > tr:nth-child(1) > td:nth-child(5)")).Text; } }
            public static string Years { get { return _page.FindElement(By.CssSelector("#years-at-etsu")).Text; } }
        }
        public static class CoordinatorInformationChrome
        {
            public static string DegreeName { get { return _page.FindElement(By.CssSelector("#coordination-information > tbody > tr:nth-child(1) > td:nth-child(1)")).Text; } }
            public static string Level { get { return _page.FindElement(By.CssSelector("#coordination-information > tbody > tr:nth-child(1) > td:nth-child(2)")).Text; } }
            public static string Degree { get { return _page.FindElement(By.CssSelector("#coordination-information > tbody > tr:nth-child(1) > td:nth-child(3)")).Text; } }
            public static string HowQualified { get { return _page.FindElement(By.CssSelector("#coordination-information > tbody > tr:nth-child(1) > td:nth-child(4)")).Text; } }
            public static string StartDate { get { return _page.FindElement(By.CssSelector("#coordination-information > tbody > tr:nth-child(1) > td:nth-child(5)")).Text; } }
            public static string EndDate { get { return _page.FindElement(By.CssSelector("#coordination-information > tbody > tr:nth-child(1) > td:nth-child(6)")).Text; } }

            public static void ClickCoordinatorInformationOption(string coordinatorInformation)
            {
                var CoordinatorInformationOption = _page.FindElement(By.CssSelector("#coordinator-information-button"));
                CoordinatorInformationOption.Click();
            }
        }
        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }

        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }


    }
}
