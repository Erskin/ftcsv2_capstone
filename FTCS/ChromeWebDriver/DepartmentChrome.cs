﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ChromeWebDriver
{
    public static partial class DepartmentChrome
    {
        private static IWebDriver _page = null;

        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }

        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }

        public static IEnumerable<string> ListOfDepartmentName { get { return _page.FindElements(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody")).Select(e => e.Text); } }

        public static bool GetDefaultDepartmentHeaderDescendingIconIsDisplayed()
        {
            return _page.FindElement(By.CssSelector("th:first-child span.glyphicon-sort-by-attributes-alt")).Displayed;
        }

        public static bool GetDefaultDepartmentHeaderIconIsDisplayed()
        {
            return _page.FindElement(By.CssSelector("th:first-child span.glyphicon-sort-by-attributes")).Displayed;
        }

        public static string GetFirstDepartmentName()
        {
            return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > tbody > tr:nth-child(1) > td:nth-child(1) > a")).Text;
        }

        public static void ClickDepartmentHeader()
        {
            _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > thead > tr > th:nth-child(1) > a")).Click();
        }

        public static IEnumerable<int> ColumnData(int columnNumber)
        {
            return _page.FindElements(By.CssSelector(string.Format("tr > td:nth-child({0})", columnNumber))).Select(e => Convert.ToInt32(e.Text)).ToArray();
        }

        public static void ClickLetter(string letter)
        {
            char[] letters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            int i = 0;
            while (letter != letters[i].ToString())
            {
                i++;
            }

            var LetterButton = _page.FindElement(By.CssSelector("#filter-letters > div > a:nth-child(" + (i + 2) + ")"));
            LetterButton.Click();
        }
        public static string FilterSearchBox
        {
            set
            {
                var searchbox = _page.FindElement(By.CssSelector("#search-text-box"));
                searchbox.Clear();
                searchbox.SendKeys(value);
            }
        }
        public static void ClickFilterSearchButton()
        {
            _page.FindElement(By.CssSelector("#search-button")).Click();
        }

        public static IWebElement ClickColumnHeader(int v)
        {
            return _page.FindElement(By.CssSelector(string.Format("thead tr th:nth-child({0}) a", v)));
        }

        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
    }
}
