﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Linq;
using System.Threading;

namespace ChromeWebDriver
{
    public static partial class ViewAllCourseChrome
    {
        private static IWebDriver _page = null;

        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }

        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }
        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
        public static string GetCourseByName(string course)
        {
            var i = 1;
            var name = "";
            while (course != name)
            {
                name = _page.FindElements(By.XPath("/html/body/div[3]/div/div/div[4]/table/tbody/tr[" + i +"]/td[2]/a")).First().Text;
                i++;
            }
            return name;
        }
        public static void ClickLetterCourse(string letter)
        {
            char[] letters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            int i = 0;
            while (letter != letters[i].ToString())
            {
                i++;
            }

            var LetterButton = _page.FindElement(By.CssSelector("#filter-letters > div > a:nth-child(" + (i + 2) + ")"));
            LetterButton.Click();
        }
        public static string ClickEnterSearchBox
        {
            set
            {
                var searchbox = _page.FindElement(By.CssSelector("#search-text-box"));
                searchbox.Clear();
                searchbox.SendKeys(value);
            }
        }
        public static void ClickCourseSearchButton()
        {
            _page.FindElement(By.CssSelector("#search-button")).Click();
        }
        public static void ClickCourseNameHeader()
        {
            _page.FindElement(By.CssSelector("body > div.body-content > div > div > div:nth-child(4) > table > thead > tr > th:nth-child(2) > a")).Click();
        }
    }
}
