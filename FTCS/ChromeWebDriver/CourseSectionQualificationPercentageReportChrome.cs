﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChromeWebDriver
{
    public static partial class CourseSectionQualificationPercentageReportChrome
    {
        private static IWebDriver _page = null;
        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }
        public static void Create()
        {
            _page = new ChromeDriver(@"C:\");
            _page.Manage().Window.Maximize();
        }
        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }
        public static void WaitForPageToLoad()
        {
            Thread.Sleep(5000);
        }
        public static void HoverOnTheReports()
        {
            Actions hoverAction = new Actions(_page);
            var navBarReportsLink = _page.FindElement(By.Id("reports-link"));
            hoverAction.MoveToElement(navBarReportsLink).Perform();
        }
        public static void ClickCourseSectionQualificationPercentageReportButton()
        {
            _page.FindElement(By.CssSelector("#course-section-qualifications-link")).Click();
        }
        public static void ClickGraduateTab()
        {
            _page.FindElement(By.CssSelector("#graduate-button")).Click();
        }
        public static void ClickStartYearButton()
        {
            _page.FindElement(By.CssSelector("#Years")).Click();
        }
        public static void Click2015Button()
        {
            _page.FindElement(By.CssSelector("#Years > option:nth-child(2)")).Click();
        }
        public static void HoverOverAcademicYearDropdown()
        {
            Actions hoverYearAction = new Actions(_page);
            var navBarReportsLink = _page.FindElement(By.Id("#Years"));
            hoverYearAction.MoveToElement(navBarReportsLink).Perform();
        }
        public static string CourseSectionQualificationPercentageReportTitle
        {
            get { return _page.FindElement(By.CssSelector("body > div.body-content > div > div > div.row.content-title > div.col-sm-10.col-md-10.col-lg-10.padding-top-4 > h1")).Text; }
        }
        public static string GraduateDepartment
        {
            get { return _page.FindElement(By.CssSelector("#graduate-table > thead > tr > th:nth-child(1)")).Text; }
        }
        public static string GraduateTotalCourseSection
        {
            get { return _page.FindElement(By.CssSelector("#graduate-table > thead > tr > th:nth-child(2)")).Text; }
        }
        public static string GraduateTotalCourseSectionsPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string GraduateNumberAcademiccallyQualified
        {
            get { return _page.FindElement(By.CssSelector("#graduate-table > thead > tr > th:nth-child(3)")).Text; }
        }
        public static string GraduateNumberAcademicallyQualifiedPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string GraduateNumberProfessionallyQualified
        {
            get { return _page.FindElement(By.CssSelector("#graduate-table > thead > tr > th:nth-child(4)")).Text; }
        }
        public static string GraduateNumberProfessionallyQualifiedPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string GraduateNumberGraduateAssistants
        {
            get { return _page.FindElement(By.CssSelector("#graduate-table > thead > tr > th:nth-child(5)")).Text; }
        }
        public static string GraduateNumberGraduateAssistantsPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string GraduateMissingTranscripts
        {
            get { return _page.FindElement(By.CssSelector("#graduate-table > thead > tr > th:nth-child(6)")).Text; }
        }
        public static string GraduateMissingTranscriptsPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string UndergraduateDepartment
        {
            get { return _page.FindElement(By.CssSelector("#undergraduate-table > thead > tr > th:nth-child(1)")).Text; }
        }
        public static string UndergraduateTotalCourseSection
        {
            get { return _page.FindElement(By.CssSelector("#undergraduate-table > thead > tr > th:nth-child(2)")).Text; }
        }
        public static string UndergraduateTotalCourseSectionPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string UndergraduateNumberAcademicallyQualified
        {
            get { return _page.FindElement(By.CssSelector("#undergraduate-table > thead > tr > th:nth-child(3)")).Text; }
        }
        public static string UndergraduateNumberAcademicallyQualifiedPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string UndergraduateNumberProfessionallyQualified
        {
            get { return _page.FindElement(By.CssSelector("#undergraduate-table > thead > tr > th:nth-child(4)")).Text; }
        }
        public static string UndergraduateNumberProfessionallyQualifiedPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string UndergraduateNumberGraduateAssistants
        {
            get { return _page.FindElement(By.CssSelector("#undergraduate-table > thead > tr > th:nth-child(5)")).Text; }
        }
        public static string UndergraduateNumberGraduateAssistantsPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string UndergraduateMissingTranscripts
        {
            get { return _page.FindElement(By.CssSelector("#undergraduate-table > thead > tr > th:nth-child(6)")).Text; }
        }
        public static string UndergraduateMissingTranscriptsPercentage
        {
            get { return _page.FindElement(By.CssSelector("")).Text; }
        }
        public static string StartYear
        {
            get { return _page.FindElement(By.CssSelector("#Years")).Text; }
        }
    }
}
