﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels
{
    public class DepartmentFacultyVM
    {
        [Display(Name = "Faculties in Department")]
        public string DEPARTMENTNAME { get; set; }

        public static explicit operator DepartmentFacultyVM(DEPARTMENT department)
        {
            var vm = new DepartmentVM
            {
                NAME = department.NAME
            };

            return vm;
        }
    }
}