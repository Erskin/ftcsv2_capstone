﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels
{
    public class ConcentrationVM
    {
        public string Program { get; set; }

        public string Coordinator { get; set; }
        [Required(ErrorMessage = "A valid coordinator must be selected")]
        public string CoordinatorId { get; set; }
        public string ProgramCode { get; set; }
        public string ConcentrationCode { get; set; }
    }
}