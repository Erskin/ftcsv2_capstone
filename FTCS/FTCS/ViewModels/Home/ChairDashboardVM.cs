﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FTCS.ViewModels.Task;

namespace FTCS.ViewModels.Home
{
    public class ChairDashboardVM
    {
        public IEnumerable<ProgramsVM> Programs { get; set; }
        public List<SelectListItem> ProgramOptions { get; set; }
        public List<SelectListItem> LevelOptions { get; set; }
        public List<SelectListItem> CoordinatorOptions { get; set; }
    }
}