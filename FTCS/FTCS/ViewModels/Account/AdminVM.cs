﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.ViewModels.Account
{
    public class AdminVM
    {
        [Display(Name = "Department")]
        public List<SelectListItem> Departments { get; set; }

        public string Role { get; set; }
    }
}