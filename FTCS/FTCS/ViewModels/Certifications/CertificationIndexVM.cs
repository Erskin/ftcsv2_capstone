﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.ViewModels.Certifications
{
    public class CertificationIndexVM
    {
        public IEnumerable<FacultyCredentialsReportVM> IncompleteCertifications { get; set; }
        public List<SelectListItem> CertificationOptions { get; set; }
    }
}