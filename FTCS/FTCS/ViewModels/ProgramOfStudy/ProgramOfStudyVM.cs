﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.ViewModels
{
    public class ProgramOfStudyVM
    {
        public string Program { get; set; }

        public string Level { get; set; }

        public string Coordinator { get; set; }

        public List<ConcentrationVM> Concentration { get; set; }

    }
}