﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.ViewModels
{
    public class AddProgramOfStudyVM
    {
        public List<SelectListItem> SelectProgram { get; set; }

        public List<SelectListItem> SelectLevel { get; set; }

        public List<SelectListItem> SelectCoordinator { get; set; }
        public List<SelectListItem> SelectDepartment { get; set; }
    }
}