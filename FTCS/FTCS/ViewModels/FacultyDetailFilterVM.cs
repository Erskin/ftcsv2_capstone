﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.ViewModels
{
    public class FacultyDetailFilterVM
    {
        [Display(Name = "Departments")]
        public IEnumerable<SelectListItem> Departments { get; set; }
        [Display(Name = "Faculty")]
        public  IEnumerable<SelectListItem> FacultyName { get; set; }

        public string CurrentView { get; set; }
    }
}