﻿namespace FTCS.ViewModels
{
    public class FacultyTypeVM
    {
        public string TypeName { get; set; }
        public int Id { get; set; }

        public FacultyTypeVM(int id, string name)
        {
            switch (id)
            {
                case 0:
                    TypeName = "Graduate Assistant";
                    Id = id;
                    break;
                case 1:
                    TypeName = "Part-time";
                    Id = id;
                    break;
                case 2:
                    TypeName = "Full-time";
                    Id = id;
                    break;
                case 3:
                    TypeName = "Other";
                    Id = id;
                    break;
                default:
                    TypeName = name;
                    Id = id;
                    break;
            }
        }
    }
}