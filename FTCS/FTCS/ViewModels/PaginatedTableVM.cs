﻿using PagedList;

namespace FTCS.ViewModels
{
    public class PaginatedTableVM<T> where T : class
    {
        public IPagedList<T> TableLineItemsVMs { get; set; }
        public FilterBarVM FilterBarVM { get; set; }
    }
}