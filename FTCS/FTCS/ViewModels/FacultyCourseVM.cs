﻿using System.ComponentModel;
using FTCS.Services;

namespace FTCS.ViewModels
{
    public class FacultyCourseVM : FacultyVM
    {
        [DisplayName("Last Semester Taught")]
        public Semester LastSemesterTaught { get; set; }
    }
}