﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FTCS.Models;


namespace FTCS.ViewModels
{
    public class ProfessionalQualificationVM
    {
        public String Narrative { get; set; }
        [Display(Name = "Related Work Experience")]
        public IEnumerable<RELATED_WORK_EXPERIENCE> RelatedWorkExperience { get; set; }
        [Display(Name = "Licenses")]
        public IEnumerable<LICENSE> License { get; set; }
        [Display(Name = "Certificates")]
        public IEnumerable<CERTIFICATE> Certificate { get; set; }
        [Display(Name = "Certifications")]
        public IEnumerable<CERTIFICATION> Certification { get; set; }
        [Display(Name = "Documented Excellence In Teaching")]
        public String DocumentedExcellenceInTeaching { get; set; }
        [Display(Name = "Presented Papers")]
        public IEnumerable<PRESENTED_PAPER> PresentedPaper { get; set; }
        [Display(Name = "Publication Citation")]
        public String PublicationCitation { get; set; }
        [Display(Name = "Other Achievements")]
        public String OtherAchievements { get; set; }
        [Display(Name = "Awards")]
        public IEnumerable<AWARD> Award { get; set; }
        [Display(Name = "Other Demonstrated Competencies")]
        public String OtherDemonstratedCompetencies { get; set; }
    }
}