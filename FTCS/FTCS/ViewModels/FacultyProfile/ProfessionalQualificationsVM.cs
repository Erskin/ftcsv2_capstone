﻿using FTCS.Models;
using FTCS.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FTCS.Services.Implementation;

namespace FTCS.ViewModels.FacultyProfile
{
    public class ProfessionalQualificationsVM:QualificationsBase
    {
        public ProfessionalQualificationsVM(
            string id,
            string department, 
            string firstName, 
            string lastName, 
            string narrative,  
            string documentedExcellenceInTeaching, 
            string publicationCitation, 
            string otherAchievements
            )
        {
            Name = lastName + ", " + firstName;
            Id = id;
            Department = department;
            Narrative = narrative;
            DocumentedExcellenceInTeaching = documentedExcellenceInTeaching;
            PublicationCitation = publicationCitation;
            OtherAchievements = otherAchievements;
            OtherDemonstratedCompetencies = OtherDemonstratedCompetencies;
        }

        public ProfessionalQualificationsVM() { }
        [Display(Name = "Narrative")]
        public String Narrative { get; set; }
        [Display(Name = "Documented Excellence In Teaching")]
        public String DocumentedExcellenceInTeaching { get; set; }
        [Display(Name = "Publication Citation")]
        public String PublicationCitation { get; set; }
        [Display(Name = "Other Achievements")]
        public String OtherAchievements { get; set; }
        [Display(Name = "Other Demonstrated Competencies")]
        public String OtherDemonstratedCompetencies { get; set; }
        [Display(Name = "Related Work Experience")]
        public IEnumerable<RelatedWorkExperienceVM> RelatedWorkExperience { get; set; }
        [Display(Name = "Licenses")]
        public IEnumerable<LicenseVM> License { get; set; }
        [Display(Name = "Certificates")]
        public IEnumerable<CertificatesVM> Certificate { get; set; }
        [Display(Name = "Certifications")]
        public IEnumerable<CertificationVM> Certification { get; set; }
        [Display(Name = "Presented Papers")]
        public IEnumerable<PresentedPaperVM> PresentedPaper { get; set; }
        [Display(Name = "Awards")]
        public IEnumerable<AwardVM> Award { get; set; }

        public void AddRelatedWorkExperience(ICollection<RELATED_WORK_EXPERIENCE> teacherRelatedWorkExperience)
        {
            if (teacherRelatedWorkExperience != null)
            {
                RelatedWorkExperience = teacherRelatedWorkExperience.Select(rwe => new RelatedWorkExperienceVM
                {
                    Place = rwe.PLACE,
                    Position = rwe.POSITION,
                    Responsibilities = rwe.RESPONSIBILITIES,
                    StartMonth = AcademicYear.ConvertMonthNumberToName(rwe.START_MONTH),
                    StartYear = rwe.START_YEAR,
                    EndMonth = AcademicYear.ConvertMonthNumberToName(rwe.END_MONTH),
                    EndYear = rwe.END_YEAR
                });
            }
            else
            {
                RelatedWorkExperience = new List<RelatedWorkExperienceVM>(0);
            }
        }

        public void AddLicenses(ICollection<LICENSE> licences)
        {
            if (licences != null)
            {

                License = licences.Select(l => new LicenseVM
                {
                    Name = l.NAME,
                    AwardedBy = l.AWARDED_BY,
                    AwardMonth = AcademicYear.ConvertMonthNumberToName(l.AWARD_MONTH),
                    AwardYear = l.AWARD_YEAR,
                    ExpireMonth = AcademicYear.ConvertMonthNumberToName(l.EXPIRE_MONTH),
                    ExpireYear = l.EXPIRE_YEAR
                });
            }
            else
            {
                License = new List<LicenseVM>();
            }
        }

        public void AddAwards(ICollection<AWARD> teacherAwards)
        {
            if (teacherAwards != null)
            {
                Award = teacherAwards.Select(a => new AwardVM
                {
                    Name = a.NAME,
                    AwardedBy = a.AWARDED_BY,
                    AwardMonth = AcademicYear.ConvertMonthNumberToName(a.AWARD_MONTH),
                    AwardYear = a.AWARD_YEAR
                });
            }
            else
            {
                Award = new List<AwardVM>();
            }
        }

        public void AddCertifications(ICollection<CERTIFICATION> certifications)
        {
            if (certifications != null)
            {

                Certification = certifications.Select(cert => new CertificationVM
                {
                    Name = cert.NAME,
                    Organization = cert.ORGANIZATION,
                    AwardMonth = AcademicYear.ConvertMonthNumberToName(cert.AWARD_MONTH),
                    AwardYear = cert.AWARD_YEAR,
                    ExpireMonth = AcademicYear.ConvertMonthNumberToName(cert.EXPIRE_MONTH),
                    ExpireYear = cert.EXPIRE_YEAR
                });
            }
            else
            {
                Certification = new List<CertificationVM>();
            }
        }

        public void AddPresentedPapers(ICollection<PRESENTED_PAPER> papers)
        {
            if (papers != null)
            {
                PresentedPaper = papers.Select(pp => new PresentedPaperVM
                {
                    Title = pp.TITLE,
                    Conference = pp.CONFERENCE,
                    Location = pp.LOCATION,
                    PresentedMonth = AcademicYear.ConvertMonthNumberToName(pp.PRESENTED_MONTH),
                    PresentedYear = pp.PRESENTED_YEAR
                });
            }
            else { PresentedPaper = new List<PresentedPaperVM>();}
        }

        public void AddCertificates(ICollection<CERTIFICATE> teacherCertificates)
        {
            if (teacherCertificates != null)
            {
                Certificate = teacherCertificates.Select(tc => new CertificatesVM()
                {
                    Name = tc.NAME,
                    Discipline = tc.DISCIPLINE,
                    ReceivedMonth = AcademicYear.ConvertMonthNumberToName(tc.RECEIVED_MONTH),
                    ReceivedYear = tc.RECEIVED_YEAR,
                   
                });
            }
            else { Certificate = new List<CertificatesVM>(); }
        }
    }

    
}