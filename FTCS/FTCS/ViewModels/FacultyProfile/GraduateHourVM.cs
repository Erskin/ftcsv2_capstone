﻿namespace FTCS.ViewModels.FacultyProfile
{
    public class GraduateHourVM
    {
        public string TeacherId { get; set; }
        public string GraduateDispline { get; set; }
        public int? GraduateHour { get; set; }
    }
}