﻿using System.Collections.Generic;

namespace FTCS.ViewModels.FacultyProfile
{
    public class ExperienceSummaryVM
    {
        public string Narrative { get; set; }
        public IEnumerable<string> WorkExperience { get; set; }
    }
}