﻿using System;

namespace FTCS.ViewModels.FacultyProfile
{
    public class PresentedPaperVM
    {

        public String Title { get; set; }
        public String Conference { get; set; }
        public String Location { get; set; }
        public string PresentedMonth { get; set; }
        public int PresentedYear { get; set; }
        public String Date => $"{PresentedMonth} {PresentedYear}";
    }
}