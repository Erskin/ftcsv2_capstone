﻿namespace FTCS.ViewModels.FacultyProfile
{
    public class CertificatesVM
    {
        public string Name { get; set; }
        public string Discipline { get; set; }
        public string ReceivedMonth { get; set; }
        public int ReceivedYear { get; set; }
    }
}