﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FTCS.ViewModels.FacultyProfile;
using System.Web.Mvc;

namespace FTCS.ViewModels.FacultyProfile
{
    public class GraduateSemesterCreditHoursVM
    {
       
        public List<SelectListItem> FacultyOptions { get; set; }
        public AcademicQualificationsVM GraduateCreditHours { get; set; }
    }
}