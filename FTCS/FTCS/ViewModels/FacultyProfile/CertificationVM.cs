﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FTCS.ViewModels.FacultyProfile
{
    public class CertificationVM
    {
        public String Name { get; set; }
        public String Organization { get; set; }
        public string AwardMonth { get; set; }
        public int AwardYear { get; set; }
        public string ExpireMonth { get; set; }
        public int ExpireYear { get; set; }

        [Display(Name = "Date Awarded")]
        public String DateAwarded => $"{AwardMonth} {AwardYear}";

        [Display(Name = "Date Expires")]
        public String DateExpires => $"{ExpireMonth} {ExpireYear}";
    }
}