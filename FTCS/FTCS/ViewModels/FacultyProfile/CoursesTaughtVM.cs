﻿using System.Collections.Generic;
using FTCS.Services.Implementation;
using FTCS.ViewModels.Course;

namespace FTCS.ViewModels.FacultyProfile
{
    public class CoursesTaughtVM
    {
        public string AcademicYearTitle;
        private const string Current = "Current";
        private const string All = "All";
        public CoursesTaughtVM(string year, string id, int? page,IEnumerable<CourseVM> listOfCourses, string department)
        {
            ID = id;
            CoursesTaught = listOfCourses;
            Page = page;
            if (year == Current)
            {
                NextYearState = All;
                
            }
            else if (year == All)
            {
                NextYearState = Current;
            }
            else
            {
                NextYearState = Current;
            }
            CurrentYearState = year;
            AcademicYearTitle = AcademicYear.GetCurrentAcademicYearString();
            Department = department;
        }
        public string NextYearState { get; set; }
        public string CurrentYearState { get; set; }
        public string ID { get; set; }
        public int? Page { get; set; }
        public string Department { get; set; }
        public IEnumerable<CourseVM> CoursesTaught { get; set; }
    }
}