﻿using System.ComponentModel.DataAnnotations;

namespace FTCS.ViewModels.FacultyProfile
{
    public class RelatedWorkExperienceVM
    {
        public string Place { get; set; }
        public string Position { get; set; }
        public string Responsibilities { get; set; }
        public string StartMonth { get; set; }
        public int StartYear { get; set; }
        public string EndMonth { get; set; }
        public int EndYear { get; set; }
        [Display(Name = "Date Started")]
        public string DateStarted => $"{StartMonth} {StartYear}";

        [Display(Name = "Date Ended")]
        public string DateEnded => $"{EndMonth} {EndYear}";
    }
}