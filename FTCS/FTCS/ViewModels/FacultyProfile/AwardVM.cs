﻿using System.ComponentModel.DataAnnotations;

namespace FTCS.ViewModels.FacultyProfile
{
    public class AwardVM
    {
        public string Name { get; set; }

        [Display(Name = "Awarded By")]
        public string AwardedBy { get; set; }

        public string AwardMonth { get; set; }
        public int AwardYear { get; set; }
        public string Date => $"{AwardMonth} {AwardYear}";
    }
}
