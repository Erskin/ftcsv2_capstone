﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FTCS.Models;
using FTCS.Services.Implementation;
using FTCS.ViewModels.Reports;
using static System.Globalization.CultureInfo;

namespace FTCS.ViewModels.FacultyProfile
{
    public class AcademicQualificationsVM:QualificationsBase
    {
        public AcademicQualificationsVM(string id, string department, FACULTY faculty,  int employmentStatus,
                         string employmentCategory,  int workyear, IEnumerable<DegreeRecordVM> degreeRecord, IEnumerable<GraduateHourVM> graduateHour)
        {
            if (faculty != null)
            {
                Rank = faculty.RANK_TYPE.RANK == null
                    ? "Non-standard Rank"
                    : CurrentCulture.TextInfo.ToTitleCase(faculty.RANK_TYPE.RANK.ToLower()).Replace("_", " ");
                if (faculty.CURRENT_RANK_MONTH != 1 && faculty.CURRENT_RANK_YEAR != 1)
                {
                    DateofRank = CurrentCulture.DateTimeFormat.GetMonthName(faculty.CURRENT_RANK_MONTH) + " " +
                                 faculty.CURRENT_RANK_YEAR;
                }
                else
                {
                    DateofRank = "None";
                }
                if (faculty.NEXT_RANK_MONTH != 1 && faculty.NEXT_RANK_YEAR != 1)
                {
                    NextRank = CurrentCulture.DateTimeFormat.GetMonthName(faculty.CURRENT_RANK_MONTH) + " " +
                               faculty.NEXT_RANK_YEAR;
                }
                else
                {
                    NextRank = "None";
                }
                FacultyStatus = faculty.GRADUATE_FACULTY_STATUS_TYPE.NAME;
                Tenure = faculty.TENURE_STATUS;
            }
            else
            {
                Rank = "Non-standard Rank";
                DateofRank = "None";
                NextRank = "None";
                FacultyStatus = "No status";
                Tenure = "Does not apply";
            }
            
            Id = id;
            Department = department;
            EmploymentStatus = employmentStatus == 0 ? "Full Time" : "Part Time";
            EmploymentCategory = employmentCategory;
            WorkYear = workyear;
            Degrees = degreeRecord;
            GraduateHours = graduateHour;
        }
        public AcademicQualificationsVM() { }
        public int CurrentRankMonth { get; set; }
        public int CurrentRankYear { get; set; }
        public int? NextRankMonth { get; set; }
        public int? NextRankYear { get; set; }
        [Display(Name = "Rank")]
        public string Rank { get; set; }
        [Display(Name = "Date of Rank")]
        public string DateofRank { get; set; }
        [Display(Name = "Date Eligible for Next Rank")]
        public string NextRank { get; set; }
        [Display(Name = "Employment Status")]
        public string EmploymentStatus { get; set; }
        [Display(Name = "Graduate Faculty Status")]
        public string FacultyStatus { get; set; }
        [Display(Name = "Employment Category")]
        public string EmploymentCategory { get; set; }
        [Display(Name = "Tenure Status")]
        public string Tenure { get; set; }
        [Display(Name = "Years at ETSU")]
        public string Year { get; set; }
        public string Month { get; set; }
        public string NextYear { get; set; }
        public string NextMonth { get; set; }
        public int? WorkYear { get; set; }
        [Display(Name = "DegreeRecords")]
        public IEnumerable<DegreeRecordVM> Degrees { get; set; }
        public IEnumerable<GraduateHourVM> GraduateHours { get; set; }
        public int EmploymentStatusValue { get; set; }
    }
}