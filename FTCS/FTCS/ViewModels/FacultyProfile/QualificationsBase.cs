﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels.FacultyProfile
{
    public abstract class QualificationsBase
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        
    }
}