﻿namespace FTCS.ViewModels.FacultyProfile
{
    public class DegreeRecordVM
    {
        public int Level { get; set; }
        public string Name { get; set; }
        public string Initial { get; set; }
        public string Discipline { get; set; }
        public string Institution { get; set; }
        public string Transcript { get; set; }
    }
}