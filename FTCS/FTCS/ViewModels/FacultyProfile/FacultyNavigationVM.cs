﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels.FacultyProfile
{
    public class FacultyNavigationVM
    {
        public FacultyVM CurrentFaculty { get; set; }
        public FacultyVM PreviousFaculty { get; set; }
        public FacultyVM NextFaculty { get; set; }
        public string CurrentAction { get; set; }
        public bool HasProfessionalQualifications { get; set; }
        public bool IsCoordinator { get; set; }
        public string Id { get; set; }
        public string Department { get; set; }
        public string CurrentView { get; internal set; }
    }
}