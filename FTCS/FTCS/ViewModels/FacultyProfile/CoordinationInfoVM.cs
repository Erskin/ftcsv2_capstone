﻿using System;
using System.Globalization;

namespace FTCS.ViewModels.FacultyProfile
{
    public class CoordinationInfoVM
    {
        public CoordinationInfoVM(string howQualified, int monthStarted, int yearStarted, int? monthEnded, int? yearEnded, string level, string degree, string degreeName)
        {
            HowQualified = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(howQualified == "UNKNOWN" ? "Unspecified" : howQualified.ToLower());
            DateStarted = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthStarted) + " " + yearStarted;            
            MonthEnded = monthEnded == 0 ? " " : CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Int32.Parse(monthEnded.ToString()));            
            YearEnded = yearEnded == 0 ? "N/A" : yearEnded.ToString();
            DateEnded = $"{MonthEnded} {YearEnded}";
            Level = level;
            DegreeName = degreeName;
            Degree = degree;    
        }
        public string DegreeName { get; set; }
        public string Level { get; set; }
        public string Degree { get; set; }
        public string HowQualified { get; set; }        
        public string DateStarted { get; set; }
        public string DateEnded { get; set; }
        public string MonthEnded { get; set; }
        public string YearEnded { get; set; }
    }
}