﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FTCS.Models;
using FTCS.ViewModels.Reports;
using NUnit.Framework.Constraints;
using FTCS.ViewModels.FacultyProfile;
using static System.Globalization.CultureInfo;

namespace FTCS.ViewModels
{
    public class FacultyVM
    {           
        public string BANNER_ID { get; set; }
        [Display(Name = "First Name")]
        public string FIRST_NAME { get; set; }
        [Display(Name = "Last Name")]
        public string LAST_NAME { get; set; }
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Name")]
        public string NAME { get; set; }
        [Display(Name = "Department")]
        public string HOME_DEPARTMENT { get; set; }   
        public string DepartmentCode { get; set; }
        [Display(Name = "College")]
        public string College { get; set; }
        [Display(Name = "NextYearState of Rank")]
        public string RankYear { get; set; }
        [Display(Name = "Month of Rank")]
        public string RankMonth { get; set; }
        [Display(Name = "NextYearState Eligible for Next Rank")]
        public string NextRankYear { get; set; }
        public string DateofRank { get; set; }
        public string NextRank { get; set; }
        [Display(Name = "Month Eligible for Next Rank")]
        public string NexRanktMonth { get; set; }                     
        public string EmploymentStatus { get; internal set; }
        public string FacultyStatus { get; internal set; }
        public string EmploymentCategory { get; internal set; }
        public string Rank { get; internal set; }
        public string Tenure { get; set; }
        public int WorkYear { get; internal set; }
        public IEnumerable<DegreeRecordVM> DegreeRecord { get; set; }
        public IEnumerable<GraduateHourVM> GraduateHour { get; set; }
        public ProfessionalQualificationsVM ProfessionalQualifications { get; set; }
       
    }
}