﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels
{
    public class DepartmentCredentialsOfficerVM
    {
        public int ProgramsOfStudy { get; set; }
        public int IncompleteFacultyCertification { get; set; }
        public int NeededProfessionalQualification { get; set; }

    }
}