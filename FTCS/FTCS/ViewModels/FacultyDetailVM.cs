﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FTCS.Controllers;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Reports;

namespace FTCS.ViewModels
{
    public class FacultyDetailVM
    {
        public string Id { get; set; }
        public string Department { get; set; }
        public AcademicQualificationsVM AcademicQualifications { get; set; }
        public ProfessionalQualificationsVM ProfessionalQualifications { get; set; }
        public CoursesTaughtVM CoursesTaught { get; set; }
        public IEnumerable<CoordinationInfoVM> CoordinationInfo { get; set; }
        public bool IsCoordinator { get; set; }
        public CoursesTaughtVM CourseHistory { get; set; }
        public bool HasProfessionalQualifications { get; set; }
        public string CurrentView { get; internal set; }
    }
}