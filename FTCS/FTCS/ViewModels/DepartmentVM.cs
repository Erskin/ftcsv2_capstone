﻿using FTCS.Models;
using System.ComponentModel.DataAnnotations;

namespace FTCS.ViewModels
{
    public class DepartmentVM
    {
        [Display(Name = "Department Code")]
        public string DEPARTMENT_CODE { get; set; }

        [Display(Name = "Department")]
        public string NAME { get; set; }

        [Display(Name = "Number of Courses Taught By Faculty")]
        public int NUMBER_OF_TEACHERS { get; set; }

        public string COLLEGE { get; set; }

        public static explicit operator DepartmentVM(DEPARTMENT department)
        {
            var vm = new DepartmentVM
            {
                DEPARTMENT_CODE = department.CODE,
                NAME = department.NAME,
                NUMBER_OF_TEACHERS = department.TEACHERs.Count
            };

            return vm;
        }

        public override string ToString()
        {
            return string.Format("CODE={0} | NAME={1} | NUM_OF_TEACHERS={2}", this.DEPARTMENT_CODE, this.NAME, this.NUMBER_OF_TEACHERS);
        }
    }


}