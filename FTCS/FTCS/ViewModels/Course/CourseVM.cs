﻿using System.ComponentModel.DataAnnotations;
using FTCS.Services;

namespace FTCS.ViewModels.Course
{
    public class CourseVM
    {
        [Display(Name = "Title")]
        public string Name { get; set; }

        public string Department { get; set; }

        [Display(Name = "Course Code")]
        public string CourseCode { get; set; }
        public string Rubric { get; set; }
        public string Number { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Term")]
        public int Month { get; set; }

        public int AcademicYear { get; set; }
        public Semester SemesterTaught { get; set; }
    }
}