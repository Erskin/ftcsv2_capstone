﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FTCS.Services.Implementation;

namespace FTCS.ViewModels.Course
{
    public class CourseCredentialVM:CourseVM
    {
        public string TeacherId { get; set; }
        public string FacultyName { get; set; }
        public Qualification Credential { get; set; }
       
       
       
      
    }
}