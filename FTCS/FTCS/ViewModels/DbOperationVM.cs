﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels
{
    public class DbOperationVM
    {
        
        public DbOperationVM()
        {
            
        }
        public DbOperationVM(bool isSuccess)
        {
            if (isSuccess)
            {
                IsSuccess = true;
                Response = "Success";
            }
            else
            {
                IsSuccess = false;
                Response = "Failure";
            }
            
        }

        public bool IsSuccess { get; set; }
        public string Response { get; set; }
    }
}