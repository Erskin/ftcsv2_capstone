﻿using System;

namespace FTCS.ViewModels
{
    public class NavBarVM
    {
        public string SemesterAndYear { get; set; }

        public void SetSemesterAndYear(int month)
        { 
            var str = "";
            if (month < 5)
            {
                str = "Spring";
            }
            else if (month < 8)
            {
                str = "Summer";
            }
            else
            {
                str = "Fall";
            }
            this.SemesterAndYear = (str + " " + DateTime.Today.Year);
        }
    }
}