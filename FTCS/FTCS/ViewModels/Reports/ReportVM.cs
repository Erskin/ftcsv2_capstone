﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels
{
    public class ReportVM<T> where T :class 
    {
        public IEnumerable<T> ReportLines { get; set; }
        public ReportFilterBarVM ReportFilterBarVM { get; set; }
    }
}