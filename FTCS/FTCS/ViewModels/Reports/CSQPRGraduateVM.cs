﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FTCS.Models;
using FTCS.Services;

namespace FTCS.ViewModels.Reports
{
    public class CSQPRGraduateVM
    {
        public string Code { get; set; }
        public string DepartmentName { get; set; }
        public int AllSection { get; set; }
        public int TotalCourse { get; set; }
        public int GraduateCourse { get; set; }
        public int GraduateCoursePercent { get; set; }
        public int AcademicCourse { get; set; }
        public int AcademicCoursePercent { get; set; }
        public int ProfessionalCourse { get; set; }
        public int ProfessionalCoursePercent { get; set; }
        public int GaCourse { get; set; }
        public int GaCourseCoursePercent { get; set; }
        public int TranscriptMissed { get; set; }
        public IEnumerable<String> TranscriptMissingList { get; set; }
        public int TotalCoursePercent { get; internal set; }
    }
}