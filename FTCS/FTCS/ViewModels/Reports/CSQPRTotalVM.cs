﻿using NUnit.Framework.Constraints;

namespace FTCS.ViewModels.Reports
{
    public class CSQPRTotalVM
    {
        public int TotalUnspecified;
        public int TotalAcademicNumber { get; internal set; }
        public int TotalAcademicPercent { get; internal set; }
        public int TotalCourseNumber { get; internal set; }
        public int TotalCoursePercent { get; internal set; }
        public int TotalProfessionallyQualified { get; set; }
        public int TotalProfessionallyQualifiedPercent { get; set; }
        public int TotalGraduateAssistants { get; set; }
        public int TotalGraduateAssistantsPercent { get; set; }
        public int TotalMissingTranscripts { get; set; }
    }
}