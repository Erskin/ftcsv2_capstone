﻿using FTCS.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FTCS.Services;
using FTCS.ViewModels.FacultyProfile;

namespace FTCS.ViewModels
{
    public class FacultyRosterVM
    {
        [Display(Name="Full Name")]
        public string Name { get; set; }
        public TeacherType TeacherType;
        [Display(Name = "Academic Degrees")]
        public string AcademicDegree { get; set; }
        public string Department { get; set; }
        [DisplayFormat(NullDisplayText = "None")]
        public string Narrative { get; set; }
        [Display(Name="Academic Degrees")]
        public IEnumerable<string> AcademicDegrees { get; set;}
        [Display(Name="Courses Taught")]
        public IOrderedEnumerable<CourseRecordsBySemesterVM> CourseRecords { get; set; }
        [Display(Name="Professional Qualifications")]
        public ExperienceSummaryVM SummaryVm { get; set; }
        
    }
}