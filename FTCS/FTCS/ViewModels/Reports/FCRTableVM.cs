﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FTCS.ViewModels
{
    public class FCRTableVM<T> where T : class
    {
        public List<T> TableItemVM { get; set; }

        public List<SelectListItem> Department { get; set; }

        public List<SelectListItem> Semester { get; set; }

        public ReportFilterBarVM ReportFilterBarVM { get; set; }

    }
}