﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FTCS.ViewModels.Reports
{
    public class LicenseVM
    {
        public String Name { get; set; }
        [Display(Name = "Awarded By")]
        public String AwardedBy { get; set; }
        public string AwardMonth { get; set; }
        public int AwardYear { get; set; }
        public string ExpireMonth { get; set; }
        public int ExpireYear { get; set; }

        [Display(Name = "Date Awarded")]
        public String DateAwarded => $"{AwardMonth} {AwardYear}";

        [Display(Name = "Date Expires")]
        public String DateExpires => $"{ExpireMonth} {ExpireYear}";
    }
}