﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels.Reports
{
    public class CSQPRReportVM
    {
        public IEnumerable<CSQPRGraduateVM> CSQPRGraduate { get; set; }
        public CSQPRTotalVM CSQPRTotal { get; set; }
    }
}