﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FTCS.Controllers;

namespace FTCS.ViewModels.Reports
{
    public class CourseSectionQualificationsPercentageReportVM
    {
        public List<SelectListItem> Years { get; set; }
        public IEnumerable<CSQPRGraduateVM> GraduateReport { get; set; }

        public IEnumerable<CSQPRUndergraduateVM> UndergraduateReport { get; set; }
        public string AcademicYearForReport { get; set; }
        public CSQPRTotalVM UndergraduateTotal { get; internal set; }
        public CSQPRTotalVM GraduateTotal { get; internal set; }
        public CSQPRTotalVM CSQPRTotal { get; internal set; }
    }
}