﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web.Mvc;

namespace FTCS.ViewModels
{
    public class ReportFilterBarVM
    {
        private const int MaxYears = 5;
        public int? FacultyType { get; set; }
        
        [Display(Name = "Departments")]
        public IEnumerable<SelectListItem> Departments { get; set; }
        [Display(Name = "Academic Year")]
        public IEnumerable<SelectListItem> Years { get; set; }
        [Display(Name = "Faculty Type")]
        public IEnumerable<SelectListItem> Faculty { get; set; }

        public ReportFilterBarVM()
        {
            var currentDate = DateTime.Today.Year;
            Years = GenerateSelectYears(currentDate);
        }
        public ReportFilterBarVM(IEnumerable<DepartmentVM> departmentList, IEnumerable<FacultyTypeVM> facultyTypeVMs)
        {
            var currentDate = DateTime.Today.Year;
            Years = GenerateSelectYears(currentDate);
            if (facultyTypeVMs != null)
            {
                Faculty = facultyTypeVMs.Select(ft => new SelectListItem {Text = ft.TypeName, Value = ft.Id.ToString()}).OrderBy(ft=> ft.Text);
            }
            else
            {
                Faculty = new List<SelectListItem>();
            }
            if (departmentList != null)
            {
                Departments =
                    departmentList.Select(dl => new SelectListItem {Text = dl.NAME, Value = dl.DEPARTMENT_CODE});
            }
            else
            {
                Departments = new List<SelectListItem>();
            }
        }

        public static List<SelectListItem> GenerateSelectYears(int currentDate)
        {
            return new List<SelectListItem>()
            {
                
                new SelectListItem {Text = currentDate.ToString(), Value = currentDate.ToString()},
                new SelectListItem {Text = (currentDate - 1).ToString(), Value = (currentDate - 1).ToString() },
                new SelectListItem {Text = (currentDate - 2).ToString(), Value = (currentDate - 2).ToString() }
            };
        }    
    }
}