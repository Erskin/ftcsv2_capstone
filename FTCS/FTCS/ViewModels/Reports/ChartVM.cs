﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels.Reports
{
    public class ChartVM
    {
        public object UnspecifiedDepartments { get; set; }
        public CSQPRTotalVM Totals { get; set; }
    }
}