﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels.Reports
{
    public class GraduateCourseByDepartmentVM
    {
        public string Code { get; set; }
        public string DepartmentName { get; set; }
        public int AllCourse { get; set; }
        public int TotalCourse { get; set; }
        public int GraduateCourse { get; set; }
        public int AcademicCourse { get; set; }
        public int ProfessionalCourse { get; set; }
        public int GaCourse { get; set; }
        public double GraduateCoursePercent { get; set; }
        public double AcademicCoursePercent { get; set; }
        public double ProfessionalCoursePercent { get; set; }
        public double GaCourseCoursePercent { get; set; }
        public int TranscriptMissed { get; set; }
        public IEnumerable<TranscriptMissListVM> TranscriptMissList { get; set; }
    }
}