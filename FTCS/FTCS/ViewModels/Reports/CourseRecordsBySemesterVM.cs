﻿using FTCS.Models;
using System.Collections;
using System.Collections.Generic;
using FTCS.Services;
using FTCS.Services.Implementation;

namespace FTCS.ViewModels
{
    public class CourseRecordsBySemesterVM
    {
        public Semester Name { get; set; }
        public IEnumerable<string> Records { get; set; }
    }
}