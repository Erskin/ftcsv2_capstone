﻿using System.ComponentModel.DataAnnotations;
using FTCS.Services.Implementation;

namespace FTCS.ViewModels
{
    public class FacultyCredentialsReportVM
    {
        public string TeacherId { get; set; }
        public string FacultyName { get; set; }

        public string Semester { get; set; }

        public string Course { get; set; }

        public Qualification Credential { get; set; }
        public string CourseDescription { get; set; }
        public string SelectedCredentialId { get; set; }
        public string CourseRubric { get; set; }
        public string CourseNumber { get; set; }
        public string CourseCode { get; internal set; }
    }
}