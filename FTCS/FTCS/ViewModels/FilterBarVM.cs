﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.ViewModels
{
    public class FilterBarVM
    {
        public string Letter { get; set; }
        public string Search { get; set; }
        public string Department { get; set; }
        public string Sort { get; set; }
        public string Controller { get; set; }
        public int Page { get; set; }
        public string Id { get; set; }
    }
}