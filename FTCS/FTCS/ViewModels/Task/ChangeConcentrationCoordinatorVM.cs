﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.ViewModels.Task
{
    public class ChangeConcentrationCoordinatorVM
    {
        public List<SelectListItem> CoordinatorOptions { get; set; }
        public ConcentrationVM Concentration { get; set; }
    }
}