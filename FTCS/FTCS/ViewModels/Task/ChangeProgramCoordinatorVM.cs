﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.ViewModels.Task
{
    public class ChangeProgramCoordinatorVM
    {
        public List<SelectListItem> CoordinatorOptions { get; set; }
        public ProgramsVM Program { get; set; }
        public string CoordinatorId { get; set; }
    }
}