﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FTCS.ViewModels.Task
{
    public class ProgramsVM
    {
        public string Program { get; set; }
        public string Level { get; set; }
        public string Coordinator { get; set; }
        public int NumberOfConcentrations { get; set; }
        [Required(ErrorMessage = "A valid coordinator must be selected")]
        public string CoordinatorId { get; set; }
        public string ProgramCode { get; set; }
        public string DepartmentCode { get; set; }
    }
}