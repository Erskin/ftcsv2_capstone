﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace FTCS.ViewModels.Task
{
    public class TaskVM
    {
        public string Department { get; set; }
        public List<SelectListItem> SelectDepartment { get; set; }
        public int IncompleteCertification  { get; set; }
        public int ProfessionalQualification { get; set; }
    }
}