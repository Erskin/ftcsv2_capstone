﻿using System;

namespace FTCS.App_Code
{
    public static class CSSHelper
    {
        public static string showIf(Func<bool> condition)
        {
            if (condition())
            {
                return "show";
            }
            else { return ""; }
        }

        public static string hideIf(Func<bool> condition)
        {
            if (condition())
            {
                return "hide";
            }
            else { return ""; }
        }
        public static string notVisibleIf(bool condition)
        {
            if (condition)
            {
                return "display: none";
            }
            else { return ""; }
        }
        public static string VisibleIf(bool condition)
        {
            if (condition)
            {
                return "display: block;";
            }
            else { return ""; }
        }
        public static string hideIf(bool condition)
        {
            if (condition)
            {
                return "hide";
            }
            else { return ""; }
        }

        public static string activeIf(Func<bool> condition)
        {
            if (condition())
            {
                return "active";
            }
            else return "";
        }
        public static string activeIf(bool condition)
        {
            if (condition)
            {
                return "active";
            }
            else return "";
        }
        public static string showIf(bool condition)
        {
            if (condition)
            {
                return "show";
            }
            else { return ""; }
        }
        
        public static string backgroundRedIf(bool condition)
        {
            if (condition)
            {
                return "fcr-table-unspecified";
            }
            else return "";
        }
        public static string selectedIf(bool condition)
        {
            if (condition)
            {
                return "selected='selected'";
            }
            else return "";
        }
    }
}