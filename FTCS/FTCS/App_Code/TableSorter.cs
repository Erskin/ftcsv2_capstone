﻿using System.Web;
using System.Web.Mvc;

namespace FTCS.App_Code
{
    public static class TableSorter
    {
        public static MvcHtmlString DisplayCurrentSort(string currentSortValue, string columnName)
        {
            if (currentSortValue != null)
            {
                if (currentSortValue.Equals(string.Format("{0}_asc", columnName)))
                {
                    return MvcHtmlString.Create("<span class=\"glyphicon glyphicon-sort-by-attributes\"></span>");
                }
                else if (currentSortValue.Equals(string.Format("{0}_desc", columnName)))
                {
                    return MvcHtmlString.Create("<span class=\"glyphicon glyphicon-sort-by-attributes-alt\"></span>");
                }
                else { return MvcHtmlString.Create(""); }
            }
            else { return MvcHtmlString.Create(""); }
        }

        public static MvcHtmlString SetNextSort(string actionName,object parameters,  string currentSort, string columnName)
        {
           
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var sort = "";
            if (currentSort.Equals($"{columnName}_asc"))
            {
                sort = $"{columnName}_desc";
            }
            else if (currentSort.Equals($"{columnName}_desc"))
            {
                sort = $"{columnName}_asc";
            }
            else
            {
                sort = $"{columnName}_asc";
            }
            string url = $"{urlHelper.Action(actionName, parameters)}&sort={sort}";
            return MvcHtmlString.Create(url);
        }
    }
}