﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using FTCS.Models;
using FTCS.Services;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Task;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PagedList;

namespace FTCS.Controllers
{
    public class FacultyController : Controller
    {
        private readonly IFacultyService _facultyService;
        private readonly IDepartmentService _departmentService;
        private readonly ICourseService _courseService;
        private readonly IProfessionalQualificationService _professionalQualificationService;
        private readonly ICredentialManagerService _credentialManagerService;
        private SignInManager<FTCSUser, string> _loginManager;
        private const string DefaultFacultySortOrder = "NAME_asc";
        private const string DefaultDepartmentCode = "ACAF";
        private const int _CoursesTaughtPageNumber = 10;

        public enum ViewType
        {
            Academic,
            Professional,
            CurrentCourses,
            AllCourses,
            Coordinator
        }
        public SignInManager<FTCSUser, string> LoginManager
        {
            get { return _loginManager ?? HttpContext.GetOwinContext().Get<LoginManager>(); }
            private set { _loginManager = value; }
        }
        public FacultyController()
        {
            var context = new FTCSEntities();
            _facultyService = new FacultyService(context);
            _departmentService = new DepartmentService(context);
            _courseService = new CourseService(context);
            _credentialManagerService = new CredentialManagerService(context);

        }
        public FacultyController(IProfessionalQualificationService professionalQualificationService)
        {
            _professionalQualificationService = professionalQualificationService;
            var context = new FTCSEntities();
            _facultyService = new FacultyService(context);
            _departmentService = new DepartmentService(context);
            _courseService = new CourseService(context);

        }

        public FacultyController(IFacultyService facultyService, IDepartmentService departmentService,
            ICourseService courseService)
        {
            _facultyService = facultyService;
            _departmentService = departmentService;
            _courseService = courseService;
        }
        public FacultyController(IFacultyService facultyService, IDepartmentService departmentService, ICourseService courseService, IProfessionalQualificationService professionalQualificationService)
        {
            _facultyService = facultyService;
            _departmentService = departmentService;
            _courseService = courseService;
            _professionalQualificationService = professionalQualificationService;
        }

        // GET: Faculty
        public ActionResult Index(string letter, string search, string sort, string department, int? page)
        {
            ViewBag.Title = "Faculty";
            var pageSize = 10;
            var pageNumber = page ?? 1;
            var defaultSortOrder = sort == null ? "NAME_asc" : sort;
            var defaultLetter = letter == null ? "All" : letter;
            var listOfFaculty = _facultyService.GetListOfFaculty(defaultLetter, search, defaultSortOrder, department);
            var tableResult = new PaginatedTableVM<FacultyVM>
            {
                TableLineItemsVMs = listOfFaculty.ToPagedList(pageNumber, pageSize),
                FilterBarVM = new FilterBarVM { Letter = defaultLetter, Search = search, Sort = defaultSortOrder, Department = department }
            };
            return View(tableResult);
        }

       
        public ActionResult FacultyProfile(string department, string id, string currentView)
        {
            CheckFacultyProfileParameters(id, department);
            var redirect = RedirectToFirstFacultyMemberInDepartmentOnEmptyId(id, department);
            if (redirect != null)
            {
                return redirect;
            }

            if (currentView == ViewType.Professional.ToString() && !_facultyService.HasProfessionalQualifications(id))
            {
                currentView = null;
            }
            if (currentView == ViewType.Coordinator.ToString() && !_facultyService.IsFacultyCoordinator(id))
            {
                currentView = null;
            }
            var model =new FacultyDetailVM()
            {
                Id = id,
                Department = department,
                CurrentView = currentView == null ? ViewType.Academic.ToString(): currentView
            };
            return View(model);
        }

        public PartialViewResult FacultyProfileFilter(string id, string department)
        {
            var departmentCode = department ?? DefaultDepartmentCode;
            var departmentSelectItems = _departmentService.GetListOfDepartmentSelectListItems();
            var facultySelectList = _facultyService.GetListOfFacultySelectListItems(DefaultFacultySortOrder, departmentCode);
            var model = new FacultyDetailFilterVM()
            {
                Departments = departmentSelectItems,
                FacultyName = facultySelectList,
                CurrentView = "Academic"
            };
            return PartialView("_FacultyProfileFilter", model);
        }

        //TODO: Absorb this functionality into the Faculty Profile Action
        public PartialViewResult FacultyDepartmentNavigation(string id, string department, string currentView)
        {
            var previousNextFaculty = _facultyService.GetPreviousAndNextFaculty(id, department);
            var model = new FacultyNavigationVM()
            {
                PreviousFaculty = previousNextFaculty[0],
                CurrentFaculty = previousNextFaculty[1],
                NextFaculty = previousNextFaculty[2],
                CurrentView = currentView,
                HasProfessionalQualifications = _facultyService.HasProfessionalQualifications(id),
                IsCoordinator = _facultyService.IsFacultyCoordinator(id),
                Id = id,
                Department = department
            };
            return PartialView("_FacultyNavigation", model);
        }

        public ActionResult Academic(string id, string department)
        {
            ViewData["Editable"] = IsEditable(department);
            var academicQualifications = _facultyService.GetAcademicOfFaculty(id, department);
            return PartialView("_AcademicQualification", academicQualifications);
        }

        public ActionResult Professional(string id, string department)
        { 
            var professionalQualifications = _facultyService.GetProfessionalOfFaculty(id, department);
            ViewBag.Title = $"Professional Qualification for {id}";
            return PartialView("_ProfessionalQualification", professionalQualifications);
        }

        public ActionResult CoursesTaught(string id, string department, string year, int? page)
        {
            var allCourses = "All";
            var currentCourses = "Current";
            ViewData["Editable"] = IsEditable(department);
            ViewData["Year"] = year;
            ViewData["NextYearState"] = year == "Current" ? allCourses : currentCourses;
            ViewData["TeacherId"] = id;
            ViewData["DepartmentCode"] = department;
            ViewData["CredentialOptions"] = _credentialManagerService.GetCertificationOptions();
            var listOfCourses = _courseService.GetListOfCoursesByTeacher(id, year);
            return PartialView("_CoursesTaught", listOfCourses);
        }

        private bool IsEditable(string department)
        {
            var userClaims = LoginManager.UserManager.GetClaims(User.Identity.GetUserId());
            var claims = userClaims.Where(c => c.Type == FTCSUserManager.DepartmentClaim).Select(c => c.Value);
            if (claims.Contains(department))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public PartialViewResult CoursesHistory(string id, string courseRubric, string courseNumber, string department, string year, int? page)
        {
            var listOfCourses = _courseService.GetCourseHistory(id, courseRubric, courseNumber, "All");
            return PartialView("_CourseHistory", listOfCourses);
        }

        public ActionResult Coordinator(string id, string department)
        {
            var coordinationInformation = _facultyService.GetCoordinationOfFaculty(id);
            return PartialView("_CoordinationInformation", coordinationInformation);
            
        }

        public ActionResult FacultyWhoHaveTaught(string rubric, string number, string sort)
        {
            var defaultSortOrder = sort ?? "NAME_asc";
            var listOfTeachers = _facultyService.GetListOfFacultyTeachingCourse(rubric, number, defaultSortOrder);
            var courseName = _courseService.GetCourseName(rubric, number);
            ViewBag.Heading = "Faculty Who Have Taught";
            ViewBag.CourseName = $"{courseName} ({rubric} {number})";
            ViewBag.Rubric = rubric;
            ViewBag.Number = number;
            var filterVM = new FilterBarVM() { Sort = defaultSortOrder };
            return View(new PaginatedTableVM<FacultyCourseVM>()
            {
                TableLineItemsVMs = listOfTeachers.ToPagedList(1, listOfTeachers.Count()),
                FilterBarVM = filterVM
            });
        }
        private void CheckFacultyProfileParameters(string id, string department)
        {
            if (id != null && !_facultyService.IsFacultyIdValid(id))
            {
                throw new HttpException(402, "The specified ENumber does not exist");
            }
            if (!_departmentService.IsDepartmentValid(department))
            {
                throw new HttpException(402, "The specified department code does not exist");
            }
        }
       
        private ActionResult RedirectToFirstFacultyMemberInDepartmentOnEmptyId(string id, string department)
        {
            var facultyToRedirectTo = _facultyService.FirstFacultyOnInvalidParameters(id, department);
            if (facultyToRedirectTo == null) return null;
            var redirect = RedirectToAction("FacultyProfile",
                new {id = facultyToRedirectTo.BANNER_ID, department = facultyToRedirectTo.DepartmentCode});
            return redirect;
        }

        public ActionResult RemoveGraduateHour(string teacherId, string displine)
        {
            _facultyService.DeleteGraduateHourOfFaculty(teacherId, displine);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddGraduateHour(string teacherId, string displine, int hours)
        {
            var response = _facultyService.AddGraduateHourOfFaculty(teacherId, displine, hours);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult UpdateGraduateHour(string teacherId, string displine, int hours)
        {
            _facultyService.UpdateGraduteHourOfFaculty(teacherId, displine, hours);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddCertifications(string teacherId, string name, string orginaztion, int awardMonth, int awardyear, int expireMonth, int expireYear)
        {
            var response = _professionalQualificationService.AddCertificationsofFaculty(teacherId, name, orginaztion, awardMonth, awardyear, expireMonth, expireYear);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddLicense(string teacherId, string name, string awardBy, int awardMonth, int awardyear, int expireMonth, int expireYear)
        {
            var response = _professionalQualificationService.AddLicenseofFaculty(teacherId, name, awardBy, awardMonth, awardyear, expireMonth, expireYear);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddPresentdPapers(string teacherId, string title, string conference, string location, int presentedMonth, int presentedYear)
        {
            var response = _professionalQualificationService.AddPresentedPapersofFaculty(teacherId, title, conference, location, presentedMonth, presentedYear);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddPublicationCitations(string teacherId, string publicationCitation)
        {
            var response = _professionalQualificationService.AddPublicationCitationsofFaculty(teacherId, publicationCitation);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddRelatedWorkExperiences(string teacherId, string place, string position, string responsibilities, int startMonth, int startYear, int endMonth, int endYear)
        {
            var response = _professionalQualificationService.AddRelatedWorkExperiencesofFaculty(teacherId, place, position, responsibilities, startMonth, startYear, endMonth, endYear);
            return RedirectToAction("Index", "Task");
        }
       
        public ActionResult AddDocumentedExcellenceinTeaching(string teacherId, string documentedExcellenceInTeaching)
        {
            var response = _professionalQualificationService.AddDocumentedExcellenceinTeachingOfFaculty(teacherId, documentedExcellenceInTeaching);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddInterdisciplinaryTraining(string teacherId, string interdisciplinaryTraining)
        {
            var response = _professionalQualificationService.AddInterdisciplinaryTrainingOfFaculty(teacherId, interdisciplinaryTraining);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddNarrative(string teacherId, string narrative)
        {
            var response = _professionalQualificationService.AddNarrativeOfFaculty(teacherId, narrative);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddOtherAchievement(string teacherId, string OtherAchievement)
        {
            var response = _professionalQualificationService.AddOtherAchievementOfFaculty(teacherId, OtherAchievement);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult AddOtherDemonstratedCompotencies(string teacherId, string otherDemonstratedCompotency)
        {
            var response = _professionalQualificationService.AddOtherDemonstratedCompetenciesOfFaculty(teacherId, otherDemonstratedCompotency);
            return RedirectToAction("Index", "Task");
        }

        public ActionResult ShowFacultyView(string id, string department, string currentView)
        {
            ActionResult returnedView = null;
            if (currentView == ViewType.Academic.ToString())
            {
                returnedView = Academic(id, department);
            }else if (currentView == ViewType.Professional.ToString())
            {
                returnedView = Professional(id, department);
            }else if (currentView == ViewType.CurrentCourses.ToString())
            {
                returnedView = CoursesTaught(id, department, "Current", 1);
            }else if (currentView == ViewType.AllCourses.ToString())
            {
                returnedView = CoursesTaught(id, department, "All", 1);
            }
            else if (currentView == ViewType.Coordinator.ToString())
            {
                returnedView = Coordinator(id, department);
            }
            else
            {
                returnedView = Academic(id, department);
            }
            return returnedView;
        }

        protected override void Dispose(bool disposing)
        {
            _facultyService.Dispose();
            base.Dispose(disposing);
        }

    }
}