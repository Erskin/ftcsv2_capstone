﻿using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;
using FTCS.Models;
using FTCS.ViewModels.Course;

namespace FTCS.Views.Courses
{
    public class CourseController : Controller
    {
        private const string DefaultSortOrder = "Name_asc";
        private const string DefaultFilterValue = "All";
        public const int DefaultPageNumber = 1;
        public const int DefaultPageSize = 5;
        private ICourseService _courseService;
        // Constructors
        public CourseController()
        {
            var context = new FTCSEntities();
            _courseService = new CourseService(context);
        }
        public CourseController(ICourseService courseService)
        {
            _courseService = courseService;
        }
        // GET: Course
        public ActionResult Index(string letter, string sort, string search, int? page)
        {
            ViewBag.Title = "Course";
            var pageSize = 10;
            var pageNumber = page ?? 1;
            var sortOrder = sort != null ? VerifySortOrderValue(sort) : DefaultSortOrder ;
            var filterValue = letter != null ? VerifyFilterValue(letter) : DefaultFilterValue;
            var listOfCourses = _courseService.GetListOfCourses(filterValue, sortOrder, search).Select(lot => lot);
            var tableResult = new PaginatedTableVM<CourseVM>
            {
                TableLineItemsVMs = listOfCourses.ToPagedList(pageNumber, pageSize),
                FilterBarVM = new FilterBarVM { Letter = filterValue, Search = search, Sort = sortOrder }
            };
            return View(tableResult);
        }
        
        //Dispose
        protected override void Dispose(bool disposing)
        {
            _courseService.Dispose();
            base.Dispose(disposing);
        }
        private static string VerifySortOrderValue(string sort)
        {
            if (sort != "Name_asc" && sort != "Name_desc"
                && sort != "Department_asc" && sort != "Department_desc"
                && sort != "CourseCode_asc" && sort != "CourseCode_desc")
            {
                return DefaultSortOrder;
            }

            return sort;
        }
        private static string VerifyFilterValue(string filter)
        {
            
            filter.ToUpper();
            if(filter == "ALL" || filter.Length > 1 )
            {
                return DefaultFilterValue;
            }
            else if(Char.IsLetter(filter[0]))
            {
                return filter;
            }
            else
            {
                return DefaultFilterValue;
            }
        }

    }
}