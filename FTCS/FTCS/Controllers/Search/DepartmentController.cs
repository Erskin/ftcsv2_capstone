﻿using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using PagedList;
using System;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Web.Mvc;

namespace FTCS.Views.Departments
{

    public class DepartmentController : Controller
    {
        private IDepartmentService _departmentService;

        public DepartmentController()
        {
            _departmentService = new DepartmentService();
        }
        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }
        // GET: Departments
        
        public ActionResult Index(string letter, string search, string sort, int? page)
        {
            ViewBag.Title = "Departments";
            var pageSize = 10;
            var pageNumber = page ?? 1;
            var defaultSortOrder = sort ?? "NAME_asc";
            var defaultLetter = letter ?? "All";
            var listOfDepartments = _departmentService.GetListOfDepartments(defaultLetter, search, defaultSortOrder);
            var tableResult = new PaginatedTableVM<DepartmentVM>
            {
                TableLineItemsVMs = listOfDepartments.ToPagedList(pageNumber, pageSize),
                FilterBarVM = new FilterBarVM { Letter = defaultLetter, Search = search, Sort = defaultSortOrder }
            };
            return View(tableResult);
        }

        

        protected override void Dispose(bool disposing)
        {
            _departmentService.Dispose();
            base.Dispose(disposing);
        }
    }
}