﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using FTCS.Services;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.Task;
using Microsoft.AspNet.Identity;

namespace FTCS.Controllers
{
    public class ProgramOfStudyController : Controller
    {
        private readonly ICredentialManagerService _credentialManagerService;
        private readonly IDepartmentService _departmentService;

        public ProgramOfStudyController()
        {
            _credentialManagerService = new CredentialManagerService();
            _departmentService = new DepartmentService();
        }

        public ProgramOfStudyController(ICredentialManagerService credentialManagerService, IDepartmentService departmentService)
        {
            _credentialManagerService = credentialManagerService;
            _departmentService = departmentService;

        }

        public ActionResult Index()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<ProgramsVM> listOfPrograms;
            if (!string.IsNullOrEmpty(identity.FindFirstValue(FTCSUserManager.CollegeClaim)))
            {
                var college = identity.FindFirstValue(FTCSUserManager.CollegeClaim);
                listOfPrograms = _credentialManagerService.GetProgramsOfStudyForCollege(college);
            }
            else if (!string.IsNullOrEmpty(identity.FindFirstValue(FTCSUserManager.DepartmentClaim)))
            {
                var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
                listOfPrograms = _credentialManagerService.GetProgramsOfStudyForDepartment(department);
            }
            else
            {
                throw new HttpException(402, "Credential Officer does not have valid Credentials");
            }

            return PartialView("_ProgramsOfStudy", listOfPrograms);
        }

        public ActionResult AddProgramOfStudy()
        {
            List<SelectListItem> listOfDepartments = new List<SelectListItem>();
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var college = identity.FindFirstValue(FTCSUserManager.CollegeClaim);
            if (college != null && identity.FindFirstValue(ClaimTypes.Role) == RoleType.Dean.ToString())
            {
                listOfDepartments.AddRange(_departmentService.GetListOfDepartmentSelectListItems());
            }

            var model = new AddProgramOfStudyVM()
            {
                SelectProgram = _credentialManagerService.GetProgramOptions(),
                SelectLevel = _credentialManagerService.GetLevelOptions(),
                SelectCoordinator = _credentialManagerService.GetCoordinatorOptions(department, college),
                SelectDepartment = listOfDepartments
            };

            return PartialView("_AddForm", model);
        }
        public ActionResult AddProgramOfStudyButton()
        {
            return PartialView("_AddButton");
        }

        [HttpPost]
        public ActionResult AddProgramofStudy(string programCode, string levelId, string coordinatorId, string departmentCode)
        {
            if (departmentCode == null)
            {
                departmentCode = ((ClaimsIdentity) User.Identity).FindFirstValue(FTCSUserManager.DepartmentClaim);
            }
            var response = _credentialManagerService.AddNewProgramOfStudy(programCode, levelId, coordinatorId, departmentCode);
            return RedirectToAction("Index", "Task");

        }

        [RoleAuthorize(Roles = "Chair")]
        public ActionResult RemoveProgramOfStudy(string coordinatorid, string programcode)
        {
            _credentialManagerService.DeleteProgramOfStudy(programcode, coordinatorid);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [RoleAuthorize(Roles = "Chair")]
        public ActionResult ChangeProgramCoordinator(ProgramsVM vmInfo)
        {
            if (ModelState.IsValid)
            {
                _credentialManagerService.ChangeCoordinatorForProgramOfStudy(vmInfo.ProgramCode,
                vmInfo.CoordinatorId);
                var model = _credentialManagerService.GetProgramOfStudy(vmInfo.ProgramCode, vmInfo.DepartmentCode);
                return PartialView("_AccordionButton", model);
            }
            var errors = string.Join(",", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
            Response.StatusCode = 402;
            return Content(errors);


        }

        public ActionResult ShowProgramOfStudyContent(string programCode, string departmentCode)
        {
            var model = _credentialManagerService.GetProgramOfStudy(programCode, departmentCode);
            return PartialView("_AccordionButton", model);
        }

        public ActionResult ShowChangeProgramCoordinator(string programCode, string departmentCode)
        {
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var college = identity.FindFirstValue(FTCSUserManager.CollegeClaim);
            ViewData["CoordinatorOptions"] = _credentialManagerService.GetCoordinatorOptions(department, college);
            var model = _credentialManagerService.GetProgramOfStudy(programCode, departmentCode);
            return PartialView("_EditCoordinator", model);
        }

        public ActionResult ShowProgramOfStudyConcentration(string programCode)
        {
            var concentrations = _credentialManagerService.GetProgramConcentrations(programCode);
            return PartialView("_ConcentrationTable", concentrations);
        }

        public ActionResult ShowNormalProgramOfStudyConcentration(string programCode, string concentrationCode)
        {
            var model = _credentialManagerService.GetProgramConcentration(programCode, concentrationCode);
            return PartialView("_Concentration", model);
        }

        public ActionResult ShowChangeProgramOfStudyConcentration(string programCode, string concentrationCode)
        {
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var college = identity.FindFirstValue(FTCSUserManager.CollegeClaim);
            ViewData["CoordinatorOptions"] = _credentialManagerService.GetCoordinatorOptions(department, college);
            var model = _credentialManagerService.GetProgramConcentration(programCode, concentrationCode);
            return PartialView("_EditConcentration", model);
        }
        [HttpPost]
        public ActionResult ChangeConcentrationCoordinator(ConcentrationVM vmInfo)
        {
            if (ModelState.IsValid)
            {
                _credentialManagerService.ChangeCoordinatorForConcentration(vmInfo.ConcentrationCode,
                vmInfo.ProgramCode, vmInfo.CoordinatorId);
                var model = _credentialManagerService.GetProgramConcentration(vmInfo.ProgramCode, vmInfo.ConcentrationCode);
                return PartialView("_Concentration", model);
            }
            var errors = string.Join(",", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
            Response.StatusCode = 402;
            return Content(errors);

        }
    }
}