﻿using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.Task;
using Microsoft.AspNet.Identity;
using FTCS.Services;
using FTCS.Interfaces;
using System.Collections.Generic;
using FTCS.ViewModels;

namespace FTCS.Controllers
{
    [RoleAuthorize(Roles = "Chair,Dean")]
    public class TaskController : Controller
    {
        private readonly ICredentialManagerService _credentialManagerService;
        private readonly IReportService _reportService;
        private readonly IDepartmentService _departmentService;
        public TaskController()
        {
            _credentialManagerService = new CredentialManagerService();
            _reportService = new ReportService();
            _departmentService = new DepartmentService();

        }

        public TaskController(ICredentialManagerService credentialManagerService)
        {
            _credentialManagerService = credentialManagerService;
        }

        public TaskController(IReportService reportService)
        {
            _reportService = reportService;
        }

        public TaskController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        public TaskController(ICredentialManagerService credentialManagerService, IDepartmentService departmentService, IReportService reportService)
        {
            _departmentService = departmentService;
            _reportService = reportService;
            _credentialManagerService = credentialManagerService;
        }
        
        public ActionResult Index()
        {
            var academicYear = AcademicYear.GetCurrentAcademicYear().First().Year;
            int facultyCredentialsReport = 0;
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var college = identity.FindFirstValue(FTCSUserManager.CollegeClaim);
            if (!string.IsNullOrEmpty(college))
            {
                facultyCredentialsReport =
                    _reportService.GetFacultyCredentialReportForCollege(college, academicYear)
                        .Count(f => f.Credential.Description == "Unspecified");

            }
            else if (!string.IsNullOrEmpty(department))
            {
                var departmentName = _departmentService.GetDepartmentByDepartmentCode(department);
                facultyCredentialsReport =
                    _reportService.GetFacultyCredentialsReport(departmentName.NAME, 2016)
                        .Count(f => f.Credential.Description == "Unspecified");
            }
            var model = new TaskVM()
            {
                //Still need list of departments for a college
                Department = department,
                IncompleteCertification = facultyCredentialsReport,
                ProfessionalQualification = _reportService.GetIncompleteProfessionalQualification(department).Count()

            };
            return View("Index", model);
        }
    }
}