﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using FTCS.Interfaces;
using FTCS.Models;
using FTCS.Services;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.Certifications;
using FTCS.ViewModels.Course;
using Microsoft.AspNet.Identity;

namespace FTCS.Controllers
{
    public class CertificationsController : Controller
    {
        private readonly IReportService _reportService;
        private readonly ICredentialManagerService _credentialManagerService;
        public CertificationsController()
        {
            var context = new FTCSEntities();
           _reportService = new ReportService(context);
           _credentialManagerService = new CredentialManagerService(context);
        }
        public CertificationsController(IReportService reportService, ICredentialManagerService credentialManagerService)
        {
            _reportService = reportService;
            _credentialManagerService = credentialManagerService;
        }
       
        // GET: Certifications
        public PartialViewResult Index()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var currentYearFirstSemester = AcademicYear.GetCurrentAcademicYear().First();
            var model = _reportService.GetUnspecifiedFacultyCredentials(department, currentYearFirstSemester.Year);
            return PartialView("Index",model);
        }

        [HttpPost]
        [RoleAuthorize(Roles = "Chair")]
        public ActionResult Update(CourseCredentialVM vm)
        {
            var operationResult =_credentialManagerService.AddCourseCertification(vm.TeacherId, vm.Rubric, vm.Number,
                vm.SemesterTaught.Month, vm.SemesterTaught.Year, vm.Credential.Id ?? (int)Qualification.QualificationType.Unspecified);
            if (operationResult.IsSuccess)
            {
                return Content(operationResult.Response);
            }
            Response.StatusCode = 402;
            return Content(operationResult.Response);
        }
    }
}