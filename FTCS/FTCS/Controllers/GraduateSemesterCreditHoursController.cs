﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using FTCS.Services;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Task;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace FTCS.Controllers
{
    public class GraduateSemesterCreditHoursController : Controller
    {
        private IFacultyService _facultyService;
        private SignInManager<FTCSUser, string> _loginManager;
        public SignInManager<FTCSUser, string> LoginManager
        {
            get { return _loginManager ?? HttpContext.GetOwinContext().Get<LoginManager>(); }
            private set { _loginManager = value; }
        }

        public GraduateSemesterCreditHoursController()
        {
            _facultyService = new FacultyService();
        }
        public GraduateSemesterCreditHoursController(IFacultyService facultyService)
        {
            _facultyService = facultyService;
        }

        public ActionResult Index()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var model = new GraduateSemesterCreditHoursVM()
            {
                GraduateCreditHours = new AcademicQualificationsVM()
                {
                    GraduateHours = new List<GraduateHourVM>()
                },
                FacultyOptions = _facultyService.GetListOfFacultySelectListItems(FacultyService.DefaultSortOrder, department)
            };
            return PartialView("_GraduateSemesterCreditHours", model);
        }

        // GET: GraduateSemesterCreditHours
        public ActionResult ViewGraduateSemesterCreditHours()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var model = new GraduateSemesterCreditHoursVM()
            {
                FacultyOptions = _facultyService.GetListOfFacultySelectListItems(FacultyService.DefaultSortOrder, department)
            };
            return PartialView("_GraduateSemesterCreditHours", model);
        }

        public ActionResult AddCreditHours(string TeacherId)
        {
            var model = new GraduateHourVM
            {
                GraduateDispline = null,
                GraduateHour = null,
                TeacherId = TeacherId
            };
            
            return PartialView("_AddCreditHours", model);
        }

        public ActionResult AddCreditHourButton(GraduateSemesterCreditHoursVM graduateCreditHour)
        {
            return PartialView("_AddCreditHoursButton", graduateCreditHour);
        }

        public ActionResult EditCreditHours(string GraduateDispline, int? GraduateHour, string TeacherId)
        {
           var model = _facultyService.GetSingleGraduateHour(TeacherId, GraduateDispline);
           return PartialView("_EditGraduateHour", model);
        }

        public ActionResult ChangeCreditHours(string GraduateDispline, int GraduateHour, string TeacherId)
        {
            _facultyService.UpdateGraduteHourOfFaculty(TeacherId, GraduateDispline, GraduateHour);
            var model = new GraduateHourVM
            {
                GraduateDispline = GraduateDispline,
                GraduateHour = GraduateHour,
                TeacherId = TeacherId
            };

            return PartialView("_GraduateHour", model);
        }

        [HttpPost]
        public ActionResult ChangeCreditHourInformation(string GraduateDispline, int GraduateHour, string TeacherId)
        {
            var model = new GraduateHourVM
            {
                GraduateDispline = GraduateDispline,
                GraduateHour = GraduateHour,
                TeacherId = TeacherId,
            };
            int graduatehour = (int)model.GraduateHour;
            _facultyService.UpdateGraduteHourOfFaculty(model.TeacherId, model.GraduateDispline, graduatehour);

            return RedirectToAction("GraduateSemesterCreditHoursTable",new {Id = TeacherId });
        }

        [HttpPost]
        public ActionResult AddNewCreditHourInformation(string GraduateDispline, int GraduateHour, string TeacherId)
        {
            _facultyService.UpdateGraduteHourOfFaculty(TeacherId, GraduateDispline, GraduateHour);

            return RedirectToAction("GraduateSemesterCreditHoursTable", new { id = TeacherId });
        }

        public ActionResult CancelAddCreditHour(string TeacherId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var academicQualifications = _facultyService.GetAcademicOfFaculty(TeacherId);
            var model = new GraduateSemesterCreditHoursVM
            {
                FacultyOptions = _facultyService.GetListOfFacultySelectListItems(FacultyService.DefaultSortOrder, department),
                GraduateCreditHours = academicQualifications
            };
            return PartialView("_AddCreditHoursButton", model);
        }

        public ActionResult DeleteCreditHour(string TeacherId, string GraduateDispline)
        {
            var deleteCreditHours = _facultyService.DeleteGraduateHourOfFaculty(TeacherId, GraduateDispline);

            return RedirectToAction("GraduateSemesterCreditHoursTable", new { id = TeacherId});
        }

        public ActionResult GraduateSemesterCreditHoursTable(string id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            var department = identity.FindFirstValue(FTCSUserManager.DepartmentClaim);
            var academicQualifications = _facultyService.GetAcademicOfFaculty(id);
            var userClaims = LoginManager.UserManager.GetClaims(User.Identity.GetUserId());
            var claims = userClaims.Where(c => c.Type == FTCSUserManager.DepartmentClaim).Select(c => c.Value);
            if (claims.Contains(department))
            {
                ViewData["Editable"] = true;
            }
            else
            {
                ViewData["Editable"] = false;
            }
            var model = new GraduateSemesterCreditHoursVM
            {
                FacultyOptions = _facultyService.GetListOfFacultySelectListItems(FacultyService.DefaultSortOrder, department),
                GraduateCreditHours = academicQualifications
            };
            return PartialView("_GraduateSemesterCreditHoursTable", model);
        }
    }
}