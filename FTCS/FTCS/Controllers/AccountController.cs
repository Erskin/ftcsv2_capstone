﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FTCS.Services;
using FTCS.Services.Exceptions;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.Account;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;


namespace FTCS.Controllers
{
    public class AccountController : AsyncController
    {
        private const string DefaultAdminRole = "Chair";
        private SignInManager<FTCSUser, string> _loginManager;
        private IDepartmentService _departmentService;
        private IDepartmentService @object;

        public SignInManager<FTCSUser, string> LoginManager
        {
            get { return _loginManager ?? HttpContext.GetOwinContext().Get<LoginManager>(); }
            private set { _loginManager = value; }
        }

        public AccountController()
        {
            _departmentService = new DepartmentService();
        }
        public AccountController(SignInManager<FTCSUser, string> loginManager, IDepartmentService departmentService)
        {
            _loginManager = loginManager;
            _departmentService = departmentService;
        }

        public AccountController(IDepartmentService @object)
        {
            this.@object = @object;
        }


        // GET: Account
        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(UserVM user, string redirectUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Please remove when code is released 
                    if (user.UserName == "dco" || user.UserName == "cco" || user.UserName == "admin" && user.Password == "password")
                    {
                        var ftcsUser = new FTCSUser();
                        if (user.UserName == "dco")
                        {
                            ftcsUser = new FTCSUser()
                            {
                                Id = "DCO",
                                UserName = "DCO",
                                Roles = new List<string> { "Chair" },
                                Email = "email@etsu.edu",
                                Department = "CSCI"
                            };
                        }
                        if (user.UserName == "cco")
                        {
                            ftcsUser = new FTCSUser()
                            {
                                Id = "CCO",
                                UserName = "CCO",
                                Roles = new List<string> { "Dean" },
                                Email = "email@etsu.edu",
                                College = "BT"
                            };
                        }
                        if (user.UserName == "admin")
                        {
                            ftcsUser = new FTCSUser()
                            {
                                Id = "Admin",
                                UserName = "Admin",
                                Roles = new List<string> { "Proxy" },
                                Email = "email@etsu.edu"
                            };
                        }
                        var identity = await LoginManager.UserManager.CreateIdentityAsync(ftcsUser, DefaultAuthenticationTypes.ApplicationCookie);
                        LoginManager.AuthenticationManager.SignIn(new AuthenticationProperties(), identity);
                        if (ftcsUser.Roles.Contains("Proxy"))
                        {
                            return RedirectToAction("Admin");
                        }
                        return RedirectToAction("Index", "Home");
                    }
                    //Until here


                    var loginStatus = ((LoginManager)LoginManager).AuthenticateOnLDAP(user.UserName, user.Password);
                    if (loginStatus != null)
                    {
                        //gets the full user identity
                        var ftcsUser = ((LoginManager)LoginManager).GetFTCSUserFromLDAP(loginStatus);
                        var roles = await LoginManager.UserManager.GetRolesAsync(ftcsUser.Id);
                        ftcsUser.Roles = roles;
                        var identity = await LoginManager.UserManager.CreateIdentityAsync(ftcsUser, DefaultAuthenticationTypes.ApplicationCookie);
                        LoginManager.AuthenticationManager.SignIn(new AuthenticationProperties(), identity);
                        if (roles.Contains("Proxy"))
                        {
                            return RedirectToAction("Admin");
                        }
                        return RedirectToAction("Index", "Home");

                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Invalid Username or Password");

                    }
                }
                catch (InvalidLoginException ex)
                {

                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                catch (UnavailableRoleException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
                catch (InvalidOperationException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }


            }
            return View(user);
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult Logout()
        {
            LoginManager.AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Admin()
        {
            var model = new AdminVM()
            {
                Departments = _departmentService.GetListOfDepartmentSelectListItems()
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Admin(AdminVM adminOptions, string department)
        {
            var identity = new ClaimsIdentity(User.Identity);
            if (!string.IsNullOrEmpty(adminOptions.Role))
            {
                identity.RemoveClaim(identity.FindFirst(ClaimTypes.Role));
                identity.TryRemoveClaim(identity.FindFirst(FTCSUserManager.DepartmentClaim));
                identity.AddClaim(new Claim(ClaimTypes.Role, adminOptions.Role));
                identity.AddClaim(new Claim("Department", department));
            }
            LoginManager.AuthenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(identity, new AuthenticationProperties());
            return RedirectToAction("Index", "Home");
        }
    }


}