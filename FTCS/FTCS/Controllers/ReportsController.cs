﻿using FTCS.Interfaces;
using FTCS.Services;
using FTCS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FTCS.Models;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using Rotativa;
using FTCS.ViewModels.Reports;

namespace FTCS.Controllers
{
    public class ReportsController : Controller
    {
        
        private readonly IReportService _reportService;
        private readonly IDepartmentService _departmentService;
        private readonly IFacultyService _facultyService;
        private const string DefaultDepartmentSortOrder = "NAME_asc";
        private const int GradAssistantFacultyType = 0;
        private const int FullTimeFacultyType = 4;
        private const int CacheDuration = 60 * 60 * 2; //cache duration every two hours


        public ReportsController()
        {
            var context = new FTCSEntities();
            _departmentService = new DepartmentService(context);
            _reportService = new ReportService(context, _departmentService);
            _facultyService = new FacultyService(context);
        }
        public ReportsController(IReportService reportService, IDepartmentService departmentService, IFacultyService facultyService)
        {
            _reportService = reportService;
            _departmentService = departmentService;
            _facultyService = facultyService;
        }
        // GET: Reports

        public ActionResult GradAssistantRoster(int? year, bool? pdf)
        {
            var isPdf = pdf ?? false;
            var defaultYear = AcademicYear.GetCurrentAcademicYear().First().Year;
            var convertedYear = year ?? defaultYear;
            ViewBag.Year = convertedYear;
            var listOfGradAssistants = _reportService.GetFacultyRoster(convertedYear, GradAssistantFacultyType, "").OrderBy(log => log.Department);
            if (isPdf)
            {
                return new ViewAsPdf("GradAssistantRosterPDF",listOfGradAssistants);
            }
            var model = new ReportVM<FacultyRosterVM>
            {
                ReportLines = listOfGradAssistants,
                ReportFilterBarVM = new ReportFilterBarVM {}
                   
            };
            return View(model);
        }
        public ActionResult FacultyRoster(int? year, string department, int? facultyType, bool? pdf)
        {
            var isPdf = pdf ?? false;
            var defaultYear = AcademicYear.GetCurrentAcademicYear().First().Year;
            var convertedYear = year ?? defaultYear;
            facultyType = facultyType ?? FullTimeFacultyType;
            var listOfDepartments = _departmentService.GetListOfDepartments(string.Empty, string.Empty, DefaultDepartmentSortOrder);
            department = string.IsNullOrEmpty(department) ? listOfDepartments.FirstOrDefault()?.DEPARTMENT_CODE : department;
            var listOfFacultyTypes = _facultyService.GetFacultyTypes();
            var listOfGradAssistants = _reportService.GetFacultyRoster(convertedYear, facultyType, department);
            ViewBag.Year = convertedYear;
            var facultyTypeVMs = listOfFacultyTypes as IList<FacultyTypeVM> ?? listOfFacultyTypes.ToList();
            ViewBag.Faculty = facultyTypeVMs.FirstOrDefault(f => f.Id == facultyType.Value)?.TypeName;
            ViewBag.Department = listOfDepartments.FirstOrDefault(d => d.DEPARTMENT_CODE == department)?.NAME;
            if (isPdf)
            {
                return new ViewAsPdf("FacultyRosterPDF", listOfGradAssistants);
            }
            var model = new ReportVM<FacultyRosterVM>
            {
                ReportLines = listOfGradAssistants,
                ReportFilterBarVM = new ReportFilterBarVM(listOfDepartments, facultyTypeVMs)
            };
            return View(model);
        }

        public ActionResult FacultyCredentialsReport(string department, int? year, bool? pdf)
        {
            var defaultYear = AcademicYear.GetCurrentAcademicYear().First().Year;
            var convertedYear = year ?? defaultYear;
            ViewBag.Year = convertedYear;
            var academicYearString = AcademicYear.GetAcademicYearStringForYear(convertedYear);
            var listOfDepartments = _departmentService.GetListOfDepartments(string.Empty, string.Empty, DefaultDepartmentSortOrder);
            department = string.IsNullOrEmpty(department) ? listOfDepartments.FirstOrDefault()?.DEPARTMENT_CODE : department;
            ViewBag.Title = $"Faculty Credentials Report: {department} {academicYearString}";
            ViewBag.Department = department;
            ViewBag.SemesterYear = academicYearString;
            var isPdf = pdf ?? false;
            var facultyCredentialsReport = _reportService.GetFacultyCredentialsReport(department,convertedYear);
            var semesterMenu = AcademicYear.GetSemesterYearOptions(defaultYear, true);
            var departmentMenu = _departmentService.GetListOfDepartmentSelectListItems();
            if (isPdf)
            {
                return new ViewAsPdf("FacultyCredentialsReportPDF", facultyCredentialsReport);
            }
            var model = new FCRTableVM<FacultyCredentialsReportVM>
            {
                TableItemVM = facultyCredentialsReport.ToList(),
                Department = departmentMenu,
                Semester = semesterMenu,
                ReportFilterBarVM = new ReportFilterBarVM { }
            };
            return View(model);
        }

        public ActionResult CourseSectionQualificationPercentageReport(int? years, bool? pdf)
        {
            var isPdf = pdf ?? false;
            var academicYear = AcademicYear.GetCurrentAcademicYear();
            var defaultYear = academicYear.First().Year;
            var convertedYear = years ?? defaultYear;
            var ReportInfo = _reportService.GetCourseSectionQualificationPercentageReport(convertedYear);

            
            var report = new CourseSectionQualificationsPercentageReportVM()
            {
                AcademicYearForReport = AcademicYear.GetAcademicYearStringForYear(convertedYear),
                UndergraduateReport = ReportInfo.UndergraduateReport,
                GraduateReport = ReportInfo.GraduateReport,
                UndergraduateTotal = ReportInfo.UndergraduateTotal,
                GraduateTotal = ReportInfo.GraduateTotal,
                CSQPRTotal = ReportInfo.CSQPRTotal,
                Years = ReportFilterBarVM.GenerateSelectYears(academicYear.ElementAt(0).Year)
            };
            if (isPdf)
            {
                return new ViewAsPdf("CourseSectionQualificationPDF", report);
            }

            return View("CourseSectionQualificationsPercentageReport", report);
        }

        [OutputCache(Duration = CacheDuration)]
        public JsonResult Credentials()
        {
            var result = _reportService.GetFacultyCredentialsSummary();
            return Json(result,JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            _facultyService.Dispose();
            _departmentService.Dispose();
            _reportService.Dispose();
            base.Dispose(disposing);
        }
    }
}