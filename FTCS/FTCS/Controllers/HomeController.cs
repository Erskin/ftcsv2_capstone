﻿using FTCS.Services;
using FTCS.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using FTCS.Interfaces;
using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.Home;
using FTCS.ViewModels.Task;
using Microsoft.AspNet.Identity;

namespace FTCS.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

        

        public ActionResult SACSCredentialGuidelines()
        {
            return View();
        }

        public ActionResult TeachingCredentialStandards()
        {
            return View();
        }
    }
}
