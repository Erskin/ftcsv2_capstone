﻿using FTCS.Services.Implementation;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FTCS.Views.Faculty
{
    public class ProfessionalQualificationController : Controller
    {
        private IProfessionalQualificationService _professionalQualificationService;


        public ProfessionalQualificationController()
        {
            _professionalQualificationService = new ProfessionalQualificationServices();

        }
        public ProfessionalQualificationController(IProfessionalQualificationService professionalQualificationService)
        {
            _professionalQualificationService = professionalQualificationService;
        }

        // GET: Faculty

        public ActionResult FacultyProfessionalQualification(string id, int? page)
        {
            ViewBag.Title = "Faculty Professional Qualifications";
            var pageSize = 10;
            var pageNumber = page ?? 1;
            var listOfFacultyProfessionalQualification = _professionalQualificationService.GetListOfProfessionalQualifications(id).Select(lof => lof);
            var tableResult = new TableVM<ProfessionalQualificationVM>
            {
                TableLineItemsVMs = listOfFacultyProfessionalQualification.ToPagedList(pageNumber, pageSize),
                FilterBarVM = new FilterBarVM {Id = id }
            };
            return View(tableResult);
        }


        protected override void Dispose(bool disposing)
        {
            _professionalQualificationService.Dispose();
            base.Dispose(disposing);
        }
    }
}