﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.Controllers
{
    public class AccessController : Controller
    {
        // GET: Access
        [AllowAnonymous]
        public ActionResult AllUsers()
        {
            return View();
        }

        [RoleAuthorize(Roles = "Dean")]
        public ActionResult DeanOnly()
        {
            return View();
        }

        [RoleAuthorize(Roles = "Chair")]
        public ActionResult ChairOnly()
        {
            return View();
        }
    }
}