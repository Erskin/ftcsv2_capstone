﻿using FTCS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FTCS.Views.Shared
{
    public class SharedController : Controller
    {
        // GET: Shared
        public ActionResult RenderNavBar()
        {
            return PartialView("_NavBar");
        }
        public ActionResult RenderSemesterYear()
        {
            NavBarVM viewModel = new NavBarVM();
            var month = DateTime.Now.Month;
            viewModel.SetSemesterAndYear(month);
            return PartialView("_SemesterAndYear", viewModel);
        }
    }
}