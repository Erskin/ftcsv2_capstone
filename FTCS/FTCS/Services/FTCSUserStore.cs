﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.DirectoryServices;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using FTCS.Models;
using System.DirectoryServices.AccountManagement;
using System.Security.Claims;
using FTCS.Services.Exceptions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using WebGrease.Css.Extensions;

namespace FTCS.Services
{
    //http://stackoverflow.com/questions/14408079/principalcontext-for-query-in-active-directory
    public class FTCSUserStore:IUserStore<FTCSUser>, IUserRoleStore<FTCSUser, string>,IUserClaimStore<FTCSUser,string>
    {
        private readonly FTCSEntities _context;
       
        
        private readonly IQueryable<FTCSUSER> _ftcsUser;
        private readonly IQueryable<TEACHER> _teachers;
        private readonly IQueryable<COLLEGE> _colleges;
        private readonly IQueryable<DEPARTMENT> _departments;
        
        public void Dispose()
        {
            //_adContext.Dispose();
            _context.Dispose();
        }

        public FTCSUserStore()
        {
            _context = new FTCSEntities();
            _ftcsUser = _context.FTCSUSERs;
            _teachers = _context.TEACHERs;
            _departments = _context.DEPARTMENTs;
            _colleges = _context.COLLEGEs;

        }

        public Task CreateAsync(FTCSUser user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(FTCSUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(FTCSUser user)
        {
            throw new NotImplementedException();
        }
        
        public Task<FTCSUser> FindByIdAsync(string userId)
        {
            if (userId == null) return Task.Factory.StartNew(() => new FTCSUser());
            var ftcsUser = _ftcsUser.FirstAsync(f => f.USERNAME == userId);
            var orgUnitCode = ftcsUser.Result.ORG_UNIT_CODE;
            var department = _departments.FirstOrDefault(d => d.CODE == orgUnitCode)?.CODE;
            var college = _colleges.FirstOrDefault(c => c.CODE == orgUnitCode)?.CODE;
            if (department != null)
            {
                return Task.Factory.StartNew(()=> new FTCSUser()
                {
                    Id = ftcsUser.Result.USERNAME,
                    Roles = new List<string>() { ftcsUser.Result.ROLE},
                    Department = department
                });
            }
            if (college != null)
            {
                return Task.Factory.StartNew(() => new FTCSUser()
                {
                    Id = ftcsUser.Result.USERNAME,
                    Roles = new List<string>() { ftcsUser.Result.ROLE },
                    College = college
                });
            }
            throw new UnavailableRoleException("You are not authorized to access the FTCS");

        }

        public  Task<FTCSUser> FindByNameAsync(string userName)
        {
            var user = _ftcsUser.Where(fu => fu.USERNAME == userName).Select(fu => new FTCSUser
            {
                Id = fu.BANNER_ID,
                UserName = fu.BANNER_ID
            });
            return user.FirstAsync();
        }


        public Task AddToRoleAsync(FTCSUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(FTCSUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(FTCSUser user)
        {
            if (user.Id == null)
            {
                IList<string> claims = new List<string>();
                return Task.Factory.StartNew(() => claims);
            }
            var roleObject =_ftcsUser.Where(u => u.USERNAME == user.Id).Select(u => new {u.ROLE, u.ORG_UNIT_CODE}).FirstOrDefault();
            if (roleObject?.ORG_UNIT_CODE == null && roleObject?.ROLE == null)
            {
                throw new UnavailableRoleException("You are not authorized to access the FTCS");
            }
            IList<string> roleList = new List<string>() {roleObject.ROLE, roleObject.ORG_UNIT_CODE};
            return Task.Factory.StartNew(() => roleList);
        }

       

        public Task<bool> IsInRoleAsync(FTCSUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<Claim>> GetClaimsAsync(FTCSUser user)
        {
           
            IList<Claim> claims = new List<Claim>();
            if (user.Id == null) return Task.Factory.StartNew(() => claims);
            claims.Add(new Claim(FTCSUserManager.DepartmentClaim,user.Department));
            return Task.Factory.StartNew(()=> claims);
        }

        public Task AddClaimAsync(FTCSUser user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimAsync(FTCSUser user, Claim claim)
        {
            throw new NotImplementedException();
        }
    }
}