﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using FTCS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.DirectoryServices;
using System.Security.Claims;
using FTCS.Services.Exceptions;
using NUnit.Framework;

namespace FTCS.Services
{
    public class LoginManager : SignInManager<FTCSUser, string>
    {

        private const string _FacStaffOu = "OU=FacStaff";



        public LoginManager(UserManager<FTCSUser, string> userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {

        }

        public static LoginManager Create(IdentityFactoryOptions<LoginManager> options, IOwinContext context)
        {


            return new LoginManager(context.GetUserManager<FTCSUserManager>(), context.Authentication);
        }





        public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent,
            bool shouldLockOut)
        {
           DirectoryEntry ldapUser = null;
            FTCSUser user = null;

            ldapUser = AuthenticateOnLDAP(userName, password);
            user = GetFTCSUserFromLDAP(ldapUser);


            if (user.Member.Any(m => m.Contains(_FacStaffOu)))
            {
                try
                {
                    user.Roles = await UserManager.GetRolesAsync(user.Id);
                    
                }
                catch (InvalidOperationException)
                {
                    throw new UnavailableRoleException("You are not authorized to access the FTCS.");
                }
                return SignInStatus.Success;
            }
            else
            {
                throw new UnavailableRoleException("You are not authorized to access FTCS.");
            }
        }


        public DirectoryEntry AuthenticateOnLDAP(string username, string password)
        {
            var ldapUrl = ConfigurationManager.AppSettings["LDAPDomain"];
            var entry = new DirectoryEntry(ldapUrl, username, password, AuthenticationTypes.Secure);
            try
            {
                var obj = entry.NativeObject;
                return entry;
            }
            catch (Exception)
            {
                throw new InvalidLoginException("Username or Password not recognized.");
            }

        }

        public FTCSUser GetFTCSUserFromLDAP(DirectoryEntry entry)
        {
            var user = new FTCSUser();
            DirectorySearcher directorySearcher = new DirectorySearcher(entry)
            {
                Filter = $"(SAMAccountname={entry.Username})",
                SearchScope = SearchScope.Subtree
            };
            SearchResult results;
            try
            {
                results = directorySearcher.FindOne();
                if (results == null)
                {
                    throw new InvalidLoginException("Username or Password not recognized.");
                }
            }
            catch (DirectoryServicesCOMException ex)
            {
                throw new InvalidLoginException($"An error occured while trying to find {entry.Name}" + ex.Message);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("An unexpected error has occured" + ex.Message);
            }


            var memberOf = results.Properties["memberOf"];
            var eNumber = results.Properties["samaccountname"];
            var memberList = new List<string>();
            foreach (var item in memberOf)
            {
                memberList.Add(item.ToString());
            }
            
            user.Member = memberList;
            user.Id = eNumber[0].ToString();
            var identity = UserManager.FindById(user.Id);
            user.UserName = entry.Username;
            user.Email = $"{entry.Username}@etsu.edu";
            user.Department = identity.Department;
            user.College = identity.College;
            
            return user;
        }
    }




}