﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.Services
{
    public class Semester : IComparable<Semester>
    {
        public enum SemesterMonth
        {
            Fall = 8,
            Spring = 1,
            Summer = 5
        }
        public Semester()
        {
        }

        public Semester(int academicMonth, int academicYear)
        {
            Month = academicMonth;
            Year = academicYear;
        }

        private SemesterMonth _month;
        private int _year;

        public int Month
        {
            get { return (int)_month; }
            set
            {
                if (value >= 8 && value <= 12)
                {
                    _month = SemesterMonth.Fall;
                }
                else if (value >= 1 && value < 5)
                {
                    _month = SemesterMonth.Spring;
                }
                else
                {
                    _month = SemesterMonth.Summer;
                }
            }
        }

        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public int CompareTo(Semester other)
        {
            var value = 99;
             if (Year > other.Year)
            {
                value = 1;
            }
            else if (Year < other.Year)
            {
                value = -1;
            } 
            else if (Year == other.Year)
            {
                if (Month == 8 && other.Month < Month)
                {
                    value = 1;
                }
                else if (Month == 1 && other.Month > Month)
                {
                    value = -1;
                }
                else if (Month == 5 && other.Month == 1)
                {
                    value = 1;
                }
                else if (Month == 5 && other.Month == 8)
                {
                    value = -1;
                }
            }
            else if (Year == other.Year && Month == other.Month)
            {
                value = 0;
            }
            
            
            return value;
        }

        public override string ToString()
        {
            var semesterName = Enum.GetName(typeof(SemesterMonth), Month);
            return $"{semesterName} {Year}";
        }
    }
}