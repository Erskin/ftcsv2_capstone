﻿using FTCS.Models;
using FTCS.Services.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using FTCS.ViewModels;

namespace FTCS.Services.Implementation
{
    public class CurrentActiveDepartment
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int FacultyTeaching { get; set; }
    }
    public class DepartmentService : IDepartmentService
    {
        private const string AscendingPostfix = "_asc";
        private const string DescendingPostfix = "_desc";
        private const string DepartmentCol = "NAME";
        private const string NumberOfTeachersCol = "NUMBER_OF_TEACHERS";
        private readonly IQueryable<DEPARTMENT> _department;
        private readonly IQueryable<TEACHER_COURSE_RECORD> _teacherCourseRecords;
        private readonly IQueryable<COURSE> _courses;
        private readonly FTCSEntities _context;
        private readonly string[] _codeExclusions = new string[5] { "BIMS", "IMED", "RPCT", "RURL", "BMED" };
        private bool _disposed = false;


        public DepartmentService(FTCSEntities db
                                    
                                    )
        {
            _context = db;
            _teacherCourseRecords = _context.TEACHER_COURSE_RECORD;
            _department = _context.DEPARTMENTs;
            _courses = _context.COURSEs;
        }

        public DepartmentService()
        {
            _context = new FTCSEntities();

            _teacherCourseRecords = _context.Set<TEACHER_COURSE_RECORD>();
            _department = _context.Set<DEPARTMENT>();
            _courses = _context.Set<COURSE>();
        }

        public List<DepartmentVM> GetListOfDepartments(string firstLetter, string searchTerm, string sort)
        {
            var currentSemester = AcademicSemester.GetCurrentAcademicSemester();
            var results = _teacherCourseRecords
                .Join(_courses, tcr => new {Rubric = tcr.COURSE_RUBRIC, Number = tcr.COURSE_NUMBER},
                    c => new {Rubric = c.RUBRIC, Number = c.NUMBER},
                    (tcr, c) => new
                    {
                        Teaching = tcr,
                        Course = c
                    })
                .Join(_department, tcrC => new {DepartmentCode = tcrC.Course.DEPARTMENT},
                    d => new {DepartmentCode = d.CODE}, (tcrC, d) => new
                    {
                        tcrC.Teaching.TEACHER_ID,
                        d.NAME,
                        d.CODE,
                        d.EXCLUDED,
                        tcrC.Teaching.ACADEMIC_YEAR,
                        tcrC.Teaching.ACADEMIC_MONTH
                    })
                .Where(tcrd =>
                    tcrd.ACADEMIC_MONTH == currentSemester.Month &&
                    tcrd.ACADEMIC_YEAR == currentSemester.Year &&
                    (tcrd.EXCLUDED == null || tcrd.EXCLUDED != "Y") &&
                    !_codeExclusions.Contains(tcrd.CODE))
                .GroupBy(tcrd => tcrd.NAME).Select(grp => new DepartmentVM
                {
                    NAME = grp.Key,
                    DEPARTMENT_CODE = grp.FirstOrDefault().CODE,
                    NUMBER_OF_TEACHERS = grp.Distinct().Count()
                });
                            
            
                        //Filtering
                        if (firstLetter != "All" && !string.IsNullOrEmpty(firstLetter))
                        {
                            results = results.Where(lod=> lod.NAME.StartsWith(firstLetter));
                        }
            
                        if (!string.IsNullOrEmpty(searchTerm))
                        {
                            results = results.Where(d => d.NAME.Contains(searchTerm));
                        }
            
                        results = sortColumnOrder(sort, results);
            
                        return results.ToList();

        }

        private static IQueryable<DepartmentVM> sortColumnOrder(string sort, IQueryable<DepartmentVM> listOfDepartmentsQuery)
        {
            if (sort.Contains(AscendingPostfix))
            {
                var sortColumn = sort.Replace(AscendingPostfix, "");
                switch (sortColumn)
                {

                    case DepartmentCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderBy(d => d.NAME);
                        break;
                    case NumberOfTeachersCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderBy(d => d.NUMBER_OF_TEACHERS);
                        break;
                    default:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderBy(d => d.NAME);
                        break;
                }
            }
            else if (sort.Contains(DescendingPostfix))
            {
                var sortColumn = sort.Replace(DescendingPostfix, "");
                switch (sortColumn)
                {
                    case DepartmentCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderByDescending(d => d.NAME);
                        break;
                    case NumberOfTeachersCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderByDescending(d => d.NUMBER_OF_TEACHERS);
                        break;
                    default:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderByDescending(d => d.NAME);
                        break;
                }
            }

            return listOfDepartmentsQuery;
        }

       

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}