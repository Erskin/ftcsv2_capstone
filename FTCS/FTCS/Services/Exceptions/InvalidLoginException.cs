﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.Services.Exceptions
{
    public class InvalidLoginException : Exception
    {
        public InvalidLoginException(string message) : base(message)
        {
        }
    }
}