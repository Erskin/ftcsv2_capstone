﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.Services.Exceptions
{
    public class InvalidDepartmentException:Exception
    {
        public InvalidDepartmentException(string message) : base(message)
        {
        }
    }
}