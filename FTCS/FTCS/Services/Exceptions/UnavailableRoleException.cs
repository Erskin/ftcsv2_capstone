﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.Services.Exceptions
{
    public class UnavailableRoleException : Exception
    {
        public UnavailableRoleException(string message) : base(message)
        {
        }
    }
}