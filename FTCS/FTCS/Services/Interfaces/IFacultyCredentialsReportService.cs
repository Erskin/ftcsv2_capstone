﻿using System;
using System.Collections.Generic;
using FTCS.Models;
using FTCS.ViewModels;

namespace FTCS.Services.Interfaces
{
    public interface IFacultyCredentialsReportService : IDisposable
    {
        IEnumerable<FacultyCredentialsReportVM > GetFacultyCredentialsReport(string department, string semesterYear);
        List<string> GetSemestersForMenu();
        List<string> GetDepartmentsForMenu();
    }
}