﻿using FTCS.Models;
using FTCS.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FTCS.Services.Interfaces
{
    public interface IDepartmentService : IDisposable
    {

        List<DepartmentVM> GetListOfDepartments(string firstLetter, string searchTerm, string sort);
        bool IsDepartmentValid(string departmentCode);
        List<SelectListItem> GetListOfDepartmentSelectListItems();
        DepartmentVM GetDepartmentByDepartmentCode(string departmentCode);
        
    }
}