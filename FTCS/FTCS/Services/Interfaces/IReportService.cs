﻿using FTCS.Models;
using FTCS.ViewModels;
using FTCS.ViewModels.Reports;
using System;
using System.Collections.Generic;
using FTCS.ViewModels.Course;

namespace FTCS.Interfaces
{
    public interface IReportService: IDisposable
    {
        IEnumerable<FacultyRosterVM> GetFacultyRoster(int year, int? facultyType, string departments);
        IEnumerable<DepartmentVM> GetDepartmentList();
        IEnumerable<FacultyCredentialsReportVM> GetFacultyCredentialsReport(string department, int year);
        IEnumerable<FacultyCredentialsReportVM> GetFacultyCredentialReportForCollege(string college, int year);
        IEnumerable<CourseCredentialVM> GetUnspecifiedFacultyCredentials(string department, int year);
        List<string> GetDepartmentsForMenu();
        CourseSectionQualificationsPercentageReportVM GetCourseSectionQualificationPercentageReport(int year);
        List<string> GetSemestersForMenu();
        List<FacultyRosterVM> GetIncompleteProfessionalQualification(string departmentCode);
        ChartVM GetFacultyCredentialsSummary();
        
    }
}