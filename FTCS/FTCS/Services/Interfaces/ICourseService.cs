﻿using System;
using System.Collections.Generic;
using System.Linq;
using FTCS.ViewModels.Course;

namespace FTCS.Services.Interfaces
{
    public interface ICourseService : IDisposable
    {
        IEnumerable<CourseVM> GetListOfCourses(string letter, string search, string sort);
        
        string GetCourseName(string rubric, string number);
        List<CourseCredentialVM> GetListOfCoursesByTeacher(string id, string year);
        List<CourseCredentialVM> GetCourseHistory(string id, string rubric, string number, string year);
        IQueryable<CourseCredentialVM> GetAllCourseCredentials();
        string GetSemester(int month);
    }
}
