﻿using FTCS.Models;
using FTCS.ViewModels;
using System;
using System.Collections.Generic;

namespace FTCS.Services.Interfaces
{
    public interface IDepartmentService:IDisposable
    {

        IEnumerable<DepartmentVM> GetListOfDepartments(string firstLetter, string searchTerm);
    }
}