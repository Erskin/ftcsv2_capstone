﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FTCS.ViewModels;
using FTCS.ViewModels.Task;

namespace FTCS.Services.Interfaces
{
    public interface ICredentialManagerService : IDisposable
    {
        IEnumerable<ProgramsVM> GetProgramsOfStudyForDepartment(string department);
        IEnumerable<ProgramsVM> GetProgramsOfStudyForCollege(string college);
        List<SelectListItem> GetProgramOptions();
        List<SelectListItem> GetLevelOptions();
        List<SelectListItem> GetCertificationOptions();
        List<SelectListItem> GetCoordinatorOptions(string department, string college);
        DbOperationVM AddNewProgramOfStudy(string program, string level, string coordinator, string department);
        DbOperationVM DeleteProgramOfStudy(string programCode, string coordinatorId);
        DbOperationVM ChangeCoordinatorForProgramOfStudy(string programCode, string newCoordinatorId);
        DbOperationVM ChangeCoordinatorForConcentration(string concentrationCode, string programCode, string newCoordinatorId);
        DbOperationVM AddCourseCertification(string teacherId, string courseRubric, string courseNumber, int academicMonth, int academicYear, int qualification);
        IEnumerable<ConcentrationVM> GetProgramConcentrations(string programCode);
        ConcentrationVM GetProgramConcentration(string programCode, string concentrationCode);
        ProgramsVM GetProgramOfStudy(string programCode, string departmentCode);
        void AddProgramQualifications(string bannerId, string programCode);
        void RemoveProgramQualifications(string programCode);
        void RemoveProgramCoordinations(string programCode);

        void AddFacultyConcentrationCoordinations(string programCode, string coordinatorId,
            Semester currentSemester);

        void RemoveConcentrationCoordinations(string programCode);
        void EndCurrentFacultyConcentrations(string programCode);


    }
}
