﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FTCS.ViewModels;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Reports;

namespace FTCS.Services.Interfaces
{
    public interface IProfessionalQualificationService : IDisposable
    {        
        DbOperationVM AddCertificationsofFaculty(string teacherId, string name, string organization, int awardMonth, int awardyear, int expireMonth, int expireYear);
        DbOperationVM AddLicenseofFaculty(string teacherId, string name, string awardBy, int awardMonth, int awardyear, int expireMonth, int expireYear);
        DbOperationVM AddPresentedPapersofFaculty(string teacherId, string title, string conference, string location, int presentedMonth, int presentedYear);
        DbOperationVM AddPublicationCitationsofFaculty(string teacherId, string publicationCitation);
        DbOperationVM AddRelatedWorkExperiencesofFaculty(string teacherId, string place, string position, string responsibilities, int startMonth, int startYear, int endMonth, int endYear);
        DbOperationVM AddDocumentedExcellenceinTeachingOfFaculty(string teacherId, string documentedExcellenceInTeaching);
        DbOperationVM AddInterdisciplinaryTrainingOfFaculty(string teacherId, string interdisciplinaryTraining);
        DbOperationVM AddNarrativeOfFaculty(string teacherId, string narrative);
        DbOperationVM AddOtherAchievementOfFaculty(string teacherId, string OtherAchievement);
        DbOperationVM AddOtherDemonstratedCompetenciesOfFaculty(string teacherId, string otherDemonstratedCompetency);
    }
}
