﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FTCS.ViewModels;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Reports;

namespace FTCS.Services.Interfaces
{

    public interface IFacultyService : IDisposable
    {
        IEnumerable<FacultyVM> GetListOfFaculty(string letter, string search, string sort, string department);
        AcademicQualificationsVM GetAcademicOfFaculty(string id, string department);
        AcademicQualificationsVM GetAcademicOfFaculty(string id);
        ProfessionalQualificationsVM GetProfessionalOfFaculty(string id, string department);
        IEnumerable<FacultyCourseVM> GetListOfFacultyTeachingCourse(string rubric, string number, string sort);
        IEnumerable<GraduateHourVM> GetFacultyGraduateHours(string id);
        IEnumerable<FacultyTypeVM> GetFacultyTypes();
        List<SelectListItem> GetListOfFacultySelectListItems(string sortOrder, string departmentCode);
        IEnumerable<CoordinationInfoVM> GetCoordinationOfFaculty(string id);
        List<FacultyVM> GetPreviousAndNextFaculty(string id, string department);
        bool IsFacultyCoordinator(string id);
        bool HasProfessionalQualifications(string id);
        bool IsFacultyIdValid(string id);
        int GetIndexOfFacultyInList(string id, IEnumerable<FacultyVM> list);
        FacultyVM GetFacultyById(string id);
        string GetDepartmentOfFacultyById(string id);
        FacultyVM FirstFacultyOnInvalidParameters(string id, string department);
        DbOperationVM AddGraduateHourOfFaculty(string teacherId, string displine, int hours);
        DbOperationVM DeleteGraduateHourOfFaculty(string teacherId, string displine);
        DbOperationVM UpdateGraduteHourOfFaculty(string teacherId, string displine, int hours);
        IEnumerable<GraduateHourVM> GetGraduateHour(string teacherId, string displine);
        GraduateHourVM GetSingleGraduateHour(string id, string discipline);
        IEnumerable<GraduateHourVM> GetGraduateHourList(string id, string discipline, int hours);

    }
}
