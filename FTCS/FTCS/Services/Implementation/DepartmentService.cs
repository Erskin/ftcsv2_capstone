﻿using FTCS.Models;
using FTCS.Services.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using FTCS.ViewModels;

namespace FTCS.Services.Implementation
{
    public class DepartmentService : IDepartmentService
    {
        private const string AscendingPostfix = "_asc";
        private const string DescendingPostfix = "_desc";
        private const string DepartmentCol = "NAME";
        private const string NumberOfTeachersCol = "NUMBER_OF_TEACHERS";
        private readonly IQueryable<DEPARTMENT> _department;
        private readonly IQueryable<TEACHER_COURSE_RECORD> _teacherCourseRecords;
        private readonly IQueryable<COURSE> _courses;
        private readonly FTCSEntities _context;
        public static string[] CodeExclusions = new string[] { "MD","ME" };
        private bool _disposed = false;


        public DepartmentService(FTCSEntities db
                                    
                                    )
        {
            _context = db;
            _teacherCourseRecords = _context.TEACHER_COURSE_RECORD;
            _department = _context.DEPARTMENTs;
            _courses = _context.COURSEs;
        }

        public DepartmentService()
        {
            _context = new FTCSEntities();

            _teacherCourseRecords = _context.Set<TEACHER_COURSE_RECORD>();
            _department = _context.Set<DEPARTMENT>();
            _courses = _context.Set<COURSE>();
        }

        /// <summary>
        /// Returns all departments that have at least 1 course being taught
        /// Excludes all departments from the medical college
        /// </summary>
        /// <param name="firstLetter"></param>
        /// <param name="searchTerm"></param>
        /// <param name="sort"></param>
        /// <returns>A list of departmentVMs</returns>
        public List<DepartmentVM> GetListOfDepartments(string firstLetter, string searchTerm, string sort)
        {
            //doing this because we can't do an 'IN' Clause with no primitive types. i.e. '5-2017' represents summer 2017
            var currentAcademicYear = AcademicYear.GetCurrentAcademicYear().Select(ay => ay.Month + "-" + ay.Year);
            var results = _teacherCourseRecords
                .Join(_courses, tcr => new {Rubric = tcr.COURSE_RUBRIC, Number = tcr.COURSE_NUMBER},
                    c => new {Rubric = c.RUBRIC, Number = c.NUMBER},
                    (tcr, c) => new
                    {
                        Teaching = tcr,
                        Course = c
                    })
                .Join(_department, tcrC => new {DepartmentCode = tcrC.Course.DEPARTMENT1.CODE},
                    d => new {DepartmentCode = d.CODE}, (tcrC, d) => new
                    {
                        tcrC.Teaching.TEACHER_ID,
                        d.NAME,
                        d.CODE,
                        d.EXCLUDED,
                        d.COLLEGE,
                        tcrC.Teaching.ACADEMIC_YEAR,
                        tcrC.Teaching.ACADEMIC_MONTH
                    })
                .Where(tcrd =>
                    currentAcademicYear.Contains(tcrd.ACADEMIC_MONTH+"-"+tcrd.ACADEMIC_YEAR) &&
                    (tcrd.EXCLUDED == null || tcrd.EXCLUDED != "Y") &&
                    !CodeExclusions.Contains(tcrd.COLLEGE)).ToList()
                .GroupBy(tcrd => tcrd.NAME).Select(grp => new DepartmentVM
                {
                    NAME = grp.Key,
                    DEPARTMENT_CODE = grp.FirstOrDefault().CODE,
                    COLLEGE = grp.FirstOrDefault().COLLEGE,
                    NUMBER_OF_TEACHERS = grp.Select(tcrd => tcrd.TEACHER_ID).Distinct().Count()
                });
                            
            
                        //Filtering
                        if (firstLetter != "All" && !string.IsNullOrEmpty(firstLetter))
                        {
                            results = results.Where(lod=> lod.NAME.StartsWith(firstLetter));
                        }
            
                        if (!string.IsNullOrEmpty(searchTerm))
                        {
                            results = results.Where(d => d.NAME.Contains(searchTerm));
                        }
            
                        results = sortColumnOrder(sort, results);
            
                        return results.ToList();

        }

        /// <summary>
        /// Verifies if the department code is within a list of departments
        /// </summary>
        /// <param name="departmentCode"></param>
        /// <returns>
        /// True if the departmentCode is within the department list
        /// False otherwise
        /// </returns>
        public bool IsDepartmentValid(string departmentCode)
        {
            return
                GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                    .Any(d => d.DEPARTMENT_CODE == departmentCode);
        }

        /// <summary>
        /// Gets a list of departments to be used in a select option box
        /// </summary>
        /// <returns>
        ///     A list of selectlist items
        /// </returns>
        public List<SelectListItem> GetListOfDepartmentSelectListItems()
        {
            return GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                    .Select(d => new SelectListItem()
                    {
                        Text = d.NAME,
                        Value = d.DEPARTMENT_CODE
                    }).ToList();
        }

        public DepartmentVM GetDepartmentByDepartmentCode(string departmentCode)
        {
            return GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                .First(f => f.DEPARTMENT_CODE == departmentCode);
        }

        /// <summary>
        /// Takes in a string that describes what sort order is desired
        /// <example>
        ///     NAME_asc says that we want to sort the name property in a class in ascending order.
        /// </example>
        /// The method then takes that sort order and applies it to the iqueryable that is passed in
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="listOfDepartmentsQuery"></param>
        /// <returns>
        /// An Iqueryable of class T that is sorted in either descending or ascending order.
        /// </returns>
        private static IEnumerable<DepartmentVM> sortColumnOrder(string sort, IEnumerable<DepartmentVM> listOfDepartmentsQuery)
        {
            if (sort.Contains(AscendingPostfix))
            {
                var sortColumn = sort.Replace(AscendingPostfix, "");
                switch (sortColumn)
                {

                    case DepartmentCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderBy(d => d.NAME);
                        break;
                    case NumberOfTeachersCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderBy(d => d.NUMBER_OF_TEACHERS);
                        break;
                    default:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderBy(d => d.NAME);
                        break;
                }
            }
            else if (sort.Contains(DescendingPostfix))
            {
                var sortColumn = sort.Replace(DescendingPostfix, "");
                switch (sortColumn)
                {
                    case DepartmentCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderByDescending(d => d.NAME);
                        break;
                    case NumberOfTeachersCol:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderByDescending(d => d.NUMBER_OF_TEACHERS);
                        break;
                    default:
                        listOfDepartmentsQuery = listOfDepartmentsQuery.OrderByDescending(d => d.NAME);
                        break;
                }
            }

            return listOfDepartmentsQuery;
        }

       

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}