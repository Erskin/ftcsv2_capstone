﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FTCS.Services.Interfaces;
using static FTCS.Services.Semester;
using static System.Globalization.CultureInfo;

namespace FTCS.Services.Implementation
{
    public class AcademicYear
    {
        private const int MinimumYear = 2011;
       

        public static Semester GetCurrentAcademicSemester()
        {
            var currentDate = DateTime.Today;
            return new Semester(currentDate.Month, currentDate.Year);
        }

        public static IEnumerable<Semester> GetAcademicSemestersForYear(int year)
        {
            var nextYear = year + 1;
            if (year >= MinimumYear)
            {
                return new List<Semester>
                {
                    new Semester((int) SemesterMonth.Fall, year),
                    new Semester((int) SemesterMonth.Spring, nextYear),
                    new Semester((int) SemesterMonth.Summer, nextYear)
                };
            }
            return GetCurrentAcademicYear();
        }

        public static IEnumerable<Semester> GetCurrentAcademicYear()
        {
            var currentSemester = GetCurrentAcademicSemester();
            var previousYear = currentSemester.Year - 1;
            List<Semester> academicYear;
            if (currentSemester.Month == (int) SemesterMonth.Fall)
            {
                academicYear = new List<Semester>(GetAcademicSemestersForYear(currentSemester.Year));
            }
            else
            {
                academicYear = new List<Semester>(GetAcademicSemestersForYear(previousYear));
            }
            return academicYear;
        }

        public static string GetCurrentAcademicYearString()
        {
            var academicYear = GetCurrentAcademicYear();
            return AcademicYearString(academicYear);
        }

        private static string AcademicYearString(IEnumerable<Semester> academicYear)
        {
            var fall = academicYear.First();
            var summer = academicYear.Last();
            return $"{fall} - {summer}";
        }

        public static string ConvertMonthNumberToName(int monthNumber)
        {
            return CurrentCulture.DateTimeFormat.GetMonthName(monthNumber);
        }

        public static List<SelectListItem> GetSemesterYearOptions(int year, bool fullDescriptionText)
        {
            var yearRange = (year - MinimumYear) + 1;
            if (yearRange < 1)
            {
                var currentSemesterYear = GetCurrentAcademicYear().First().Year;
                yearRange = (currentSemesterYear - MinimumYear) + 1;
            }
            var years = Enumerable.Range(MinimumYear, yearRange);
            var options = years.Select(x => new SelectListItem()
            {
                Text = fullDescriptionText ? GetAcademicYearStringForYear(x):$"{x}",
                Value = $"{x}"
            }).OrderByDescending(s => s.Value);
            return options.ToList();
        }


        public static string GetAcademicYearStringForYear(int year)
        {
            var academicYear = GetAcademicSemestersForYear(year);
            return AcademicYearString(academicYear);
        }
    }
}