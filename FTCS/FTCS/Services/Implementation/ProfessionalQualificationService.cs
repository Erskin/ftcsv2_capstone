﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FTCS.Models;
using FTCS.Services.Exceptions;
using FTCS.ViewModels;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Reports;

namespace FTCS.Services.Implementation
{
    public class ProfessionalQualificationService : IProfessionalQualificationService
    {
        private readonly FTCSEntities _context;
        private readonly IQueryable<CERTIFICATION> _certification;
        private readonly IQueryable<DOCUMENTED_EXCELLENCE_IN_TEACHING> _documentExcellenceInTeaching;
        private readonly IQueryable<INTERDISCIPLINARY_TRAINING> _interdisciplinaryTraining;
        private readonly IQueryable<LICENSE> _license;
        private readonly IQueryable<NARRATIVE> _narrative;
        private readonly IQueryable<OTHER_ACHIEVEMENTS> _otherAchivements;
        private readonly IQueryable<OTHER_DEMONSTRATED_COMPETENCIES> _otherDemonstratedCompetencies;
        private readonly IQueryable<PRESENTED_PAPER> _presentPaper;
        private readonly IQueryable<PUBLICATION_CITATION> _publicationCitation;
        private readonly IQueryable<RELATED_WORK_EXPERIENCE> _relatedWorkExperience;
        private readonly IQueryable<TOPIC_INSTRUCTOR> _topicInstructor;
        private bool _disposed;

        //Constructors
        public ProfessionalQualificationService(FTCSEntities db)
        {
            _context = db;
            _certification = _context.CERTIFICATIONs;
            _documentExcellenceInTeaching = _context.DOCUMENTED_EXCELLENCE_IN_TEACHING;
            _interdisciplinaryTraining = _context.INTERDISCIPLINARY_TRAINING;
            _license = _context.LICENSEs;
            _narrative = _context.NARRATIVEs;
            _otherAchivements = _context.OTHER_ACHIEVEMENTS;
            _otherDemonstratedCompetencies = _context.OTHER_DEMONSTRATED_COMPETENCIES;
            _presentPaper = _context.PRESENTED_PAPER;
            _publicationCitation = _context.PUBLICATION_CITATION;
            _relatedWorkExperience = _context.RELATED_WORK_EXPERIENCE;
            _topicInstructor = _context.TOPIC_INSTRUCTOR;
        }
        public ProfessionalQualificationService()
        {
            _context = new FTCSEntities();
            _certification = _context.CERTIFICATIONs;
            _documentExcellenceInTeaching = _context.DOCUMENTED_EXCELLENCE_IN_TEACHING;
            _interdisciplinaryTraining = _context.INTERDISCIPLINARY_TRAINING;
            _license = _context.LICENSEs;
            _narrative = _context.NARRATIVEs;
            _otherAchivements = _context.OTHER_ACHIEVEMENTS;
            _otherDemonstratedCompetencies = _context.OTHER_DEMONSTRATED_COMPETENCIES;
            _presentPaper = _context.PRESENTED_PAPER;
            _publicationCitation = _context.PUBLICATION_CITATION;
            _relatedWorkExperience = _context.RELATED_WORK_EXPERIENCE;
            _topicInstructor = _context.TOPIC_INSTRUCTOR;
        }            

        //Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        /// <summary>
        /// Adds new certifications for faculty
        /// </summary>
        /// <param name="teacherId">Unique ID of Faculty, curently Enumber</param>
        /// <param name="name">Certification Name</param>
        /// <param name="organization">Organization giving out certification</param>
        /// <param name="awardMonth">month certification was awarded</param>
        /// <param name="awardyear">year certification was awarded</param>
        /// <param name="expireMonth">month certification expires</param>
        /// <param name="expireYear">year certification expires</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddCertificationsofFaculty(string teacherId, string name, string organization, int awardMonth, int awardyear, int expireMonth, int expireYear)
        {
            var addCertification = new CERTIFICATION()
            {
                TEACHER_ID = teacherId,
                NAME = name,
                ORGANIZATION = organization,
                AWARD_MONTH = awardMonth,
                AWARD_YEAR = awardyear,
                EXPIRE_MONTH = expireMonth,
                EXPIRE_YEAR = expireYear
            };
            _context.CERTIFICATIONs.Add(addCertification);
            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() { IsSuccess = true, Response = "Certification successfully added" };
            }
            return new DbOperationVM() { IsSuccess = false, Response = "An unexpected error has occurred" };
        }

        /// <summary>
        /// Adds new license for faculty
        /// </summary>
        /// <param name="teacherId">Unique Id for faculty, currently Enumber</param>
        /// <param name="name">Name of license</param>
        /// <param name="awardBy">Organization giving out license</param>
        /// <param name="awardMonth">month license was awarded</param>
        /// <param name="awardyear">year license was awarded</param>
        /// <param name="expireMonth">Month license expires</param>
        /// <param name="expireYear">Year License expires</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddLicenseofFaculty(string teacherId, string name, string awardBy, int awardMonth, int awardyear, int expireMonth, int expireYear)
        {
            var addLicense = new LICENSE()
            {
                TEACHER_ID = teacherId,
                NAME = name,
                AWARDED_BY = awardBy,
                AWARD_MONTH = awardMonth,
                AWARD_YEAR = awardyear,
                EXPIRE_MONTH = expireMonth,
                EXPIRE_YEAR = expireYear
            };
            _context.LICENSEs.Add(addLicense);
            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() { IsSuccess = true, Response = "License successfully added" };
            }
            return new DbOperationVM() { IsSuccess = false, Response = "An unexpected error has occurred" };
        }

        /// <summary>
        /// Adds new presented paper for faculty
        /// </summary>
        /// <param name="teacherId">Unique ID for faculty, currently Enumber</param>
        /// <param name="title">Title of paper presented</param>
        /// <param name="conference">Conference paper was presented at</param>
        /// <param name="location">Location of conference</param>
        /// <param name="presentedMonth">Month paper was presented</param>
        /// <param name="presentedYear">year paper was presented</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddPresentedPapersofFaculty(string teacherId, string title, string conference, string location, int presentedMonth, int presentedYear)
        {
            var AddPresentdPaper = new PRESENTED_PAPER()
            {
                TEACHER_ID = teacherId,
                TITLE = title,
                CONFERENCE = conference,
                LOCATION = location,
                PRESENTED_MONTH = presentedMonth,
                PRESENTED_YEAR = presentedYear              
            };
            _context.PRESENTED_PAPER.Add(AddPresentdPaper);
            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() { IsSuccess = true, Response = "Presented paper successfully added" };
            }
            return new DbOperationVM() { IsSuccess = false, Response = "An unexpected error has occurred" };
        }

        /// <summary>
        /// Adds new publication citiation for faculty
        /// </summary>
        /// <param name="teacherId">Unique ID for faculty, currently enumber</param>
        /// <param name="publicationCitation">The publication citation</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddPublicationCitationsofFaculty(string teacherId, string publicationCitation)
        {
            var AddPublicationCitation = new PUBLICATION_CITATION()
            {
                TEACHER_ID = teacherId,                
                PUBLICATION_CITATION1 = publicationCitation
            };
            _context.PUBLICATION_CITATION.Add(AddPublicationCitation);
            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() { IsSuccess = true, Response = "Publication citation successfully added" };
            }
            return new DbOperationVM() { IsSuccess = false, Response = "An unexpected error has occurred" };
        }

        /// <summary>
        /// Adds new documented excellence in teaching for faculty
        /// </summary>
        /// <param name="teacherId">Unique ID for faculty, currently Enumber</param>
        /// <param name="documentedExcellenceInTeaching">The documented excellence in teaching</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddDocumentedExcellenceinTeachingOfFaculty(string teacherId, string documentedExcellenceInTeaching)
        {
            var documentedExcellence = _documentExcellenceInTeaching.FirstOrDefault(gt => gt.TEACHER_ID == teacherId);
            try
            {
                if (documentedExcellence == null)
                {
                    var documentedExcellences = new DOCUMENTED_EXCELLENCE_IN_TEACHING()
                    {
                        TEACHER_ID = teacherId,
                        DOCUMENTED_EXCELLENCE_IN_TEACHING1 = documentedExcellenceInTeaching
                    };
                    _context.DOCUMENTED_EXCELLENCE_IN_TEACHING.Add(documentedExcellences);
                }
                else
                {
                    documentedExcellence.TEACHER_ID = teacherId;
                    documentedExcellence.DOCUMENTED_EXCELLENCE_IN_TEACHING1 = documentedExcellenceInTeaching;
                }
                _context.SaveChanges();
                return new DbOperationVM() { IsSuccess = true, Response = "Documented Excellence in Teaching of Faculty successfully Added" };
            }
            catch (Exception)
            {
                return new DbOperationVM(false);
            }
        }

        /// <summary>
        /// Adds new interdisciplinary training for faculty
        /// </summary>
        /// <param name="teacherId">Unique id for faculty, currently enumber</param>
        /// <param name="interdisciplinaryTraining">The interdisciplinary training the faculty recieved</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddInterdisciplinaryTrainingOfFaculty(string teacherId, string interdisciplinaryTraining)
        {
            var interdisciplinary = _interdisciplinaryTraining.FirstOrDefault(gt => gt.TEACHER_ID == teacherId);
            try
            {
                if (interdisciplinary == null)
                {
                    var interdisciplinarys = new INTERDISCIPLINARY_TRAINING()
                    {
                        TEACHER_ID = teacherId,
                        INTERDISCIPLINARY_TRAINING1 = interdisciplinaryTraining
                    };
                    _context.INTERDISCIPLINARY_TRAINING.Add(interdisciplinarys);
                }
                else
                {
                    interdisciplinary.TEACHER_ID = teacherId;
                    interdisciplinary.INTERDISCIPLINARY_TRAINING1 = interdisciplinaryTraining;
                }
                _context.SaveChanges();
                return new DbOperationVM() { IsSuccess = true, Response = "Interdisciplinary Training of Faculty successfully Added" };
            }
            catch (Exception)
            {
                return new DbOperationVM(false);
            }
        }

        /// <summary>
        /// Adds other, misc., achievements of faculty
        /// </summary>
        /// <param name="teacherId">Unique ID for faculty, currently Enumber</param>
        /// <param name="OtherAchievement">The other, misc., achievement of the faculty</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddOtherAchievementOfFaculty(string teacherId, string OtherAchievement)
        {
            var otherAchievementsofFaculty = _otherAchivements.FirstOrDefault(gt => gt.TEACHER_ID == teacherId);
            try
            {
                if (otherAchievementsofFaculty == null)
                {
                    var otherAchievements = new OTHER_ACHIEVEMENTS()
                    {
                        TEACHER_ID = teacherId,
                        OTHER_ACHIEVEMENTS1 = OtherAchievement
                    };
                    _context.OTHER_ACHIEVEMENTS.Add(otherAchievements);
                }
                else
                {
                    otherAchievementsofFaculty.TEACHER_ID = teacherId;
                    otherAchievementsofFaculty.OTHER_ACHIEVEMENTS1 = OtherAchievement;
                }
                _context.SaveChanges();
                return new DbOperationVM() { IsSuccess = true, Response = "Other Achievements of Faculty successfully Added" };
            }
            catch (Exception)
            {
                return new DbOperationVM(false);
            }
        }

        /// <summary>
        /// Adds other demonstated competencies of faculty
        /// </summary>
        /// <param name="teacherId">unique id of faculty, currently enumber</param>
        /// <param name="otherDemonstratedCompetency">The other demonstrated competency of the faculty </param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddOtherDemonstratedCompetenciesOfFaculty(string teacherId, string otherDemonstratedCompetency)
        {
            var otherDemonstratedCompotenciesofFaculty = _otherDemonstratedCompetencies.FirstOrDefault(gt => gt.TEACHER_ID == teacherId);
            try
            {
                if (otherDemonstratedCompotenciesofFaculty == null)
                {
                    var otherDemonstratedCompotencies = new OTHER_DEMONSTRATED_COMPETENCIES()
                    {
                        TEACHER_ID = teacherId,
                        OTHER_DEMONSTRATED_COMPETENCIES1 = otherDemonstratedCompetency
                    };
                    _context.OTHER_DEMONSTRATED_COMPETENCIES.Add(otherDemonstratedCompotencies);
                }
                else
                {
                    otherDemonstratedCompotenciesofFaculty.TEACHER_ID = teacherId;
                    otherDemonstratedCompotenciesofFaculty.OTHER_DEMONSTRATED_COMPETENCIES1 = otherDemonstratedCompetency;
                }
                _context.SaveChanges();
                return new DbOperationVM() { IsSuccess = true, Response = "Other demonstrated competencies of Faculty successfully Added" };
            }
            catch (Exception)
            {
                return new DbOperationVM(false);
            }
        }

        /// <summary>
        /// Adds new faculty narrative
        /// </summary>
        /// <param name="teacherId">unique id of faculty, currently enumber</param>
        /// <param name="narrative">the narrative to add of the faculty</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddNarrativeOfFaculty(string teacherId, string narrative)
        {
            var narrativeofFaculty = _narrative.FirstOrDefault(gt => gt.TEACHER_ID == teacherId);
            try
            {
                if (narrativeofFaculty == null)
                {
                    var narratives = new NARRATIVE()
                    {
                        TEACHER_ID = teacherId,
                        NARRATIVE1 = narrative
                    };
                    _context.NARRATIVEs.Add(narratives);
                }
                else
                {
                    narrativeofFaculty.TEACHER_ID = teacherId;
                    narrativeofFaculty.NARRATIVE1 = narrative;
                }
                _context.SaveChanges();
                return new DbOperationVM() { IsSuccess = true, Response = "Narrative of Faculty successfully Added" };
            }
            catch (Exception)
            {
                return new DbOperationVM(false);
            }
        }

        /// <summary>
        /// Gets all narratives for faculty member
        /// </summary>
        /// <param name="teacherId">Unique ID for faculty, currently Enumber</param>
        /// <returns>List of Narratives</returns>
        public IEnumerable<NARRATIVE> GetNarrative(string teacherId)
        {
            var results = _narrative
               .Where(gt => gt.TEACHER_ID == teacherId)
               .Select(gt => new NARRATIVE
               {
                   TEACHER_ID = gt.TEACHER_ID,
                   NARRATIVE1 = gt.NARRATIVE1
               });
            return results.ToList();
        }

        /// <summary>
        /// Adds new related work experience for faculty
        /// </summary>
        /// <param name="teacherId">Unique ID for faculty, Currently Enumber</param>
        /// <param name="place">Name of employer where work experience was gathered</param>
        /// <param name="position">Position at previous employer</param>
        /// <param name="responsibilities">Responsibilities at previous employer</param>
        /// <param name="startMonth">Month Started</param>
        /// <param name="startYear">Year Started</param>
        /// <param name="endMonth">Month Ended</param>
        /// <param name="endYear">Year Ended</param>
        /// <returns>Result of database operation, success or error</returns>
        public DbOperationVM AddRelatedWorkExperiencesofFaculty(string teacherId, string place, string position, string responsibilities, int startMonth, int startYear, int endMonth, int endYear)
        {
            var AddRelatedWorkExperience = new RELATED_WORK_EXPERIENCE()
            {
                TEACHER_ID = teacherId,
                PLACE = place,
                POSITION = position,
                RESPONSIBILITIES = responsibilities,
                START_MONTH = startMonth,
                START_YEAR = startYear,
                END_MONTH = endMonth,
                END_YEAR = endYear
            };
            _context.RELATED_WORK_EXPERIENCE.Add(AddRelatedWorkExperience);
            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() { IsSuccess = true, Response = "Related Work Experience successfully added" };
            }
            return new DbOperationVM() { IsSuccess = false, Response = "An unexpected error has occurred" };
        }
    }
}

