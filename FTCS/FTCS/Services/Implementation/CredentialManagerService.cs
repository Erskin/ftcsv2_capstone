﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using FTCS.Models;
using FTCS.Services.Interfaces;
using FTCS.ViewModels;
using FTCS.ViewModels.Task;
using WebGrease.Css.Extensions;

namespace FTCS.Services.Implementation
{
    public class CredentialManagerService : ICredentialManagerService
    {
        private const string Undefined = "UND";
        private const string UnassignedLevelType = "0";
        private FTCSEntities _context;
        private readonly IQueryable<FACULTY_PROGRAM_COORDINATION> _facultyProgramCoordinations;
        private readonly IQueryable<FACULTY_CONCENTRATION_COORDINATION> _facultyConcentrationCoordinations;
        private readonly IQueryable<PROGRAM_LEVEL_TYPE> _programLevelTypes;
        private readonly IQueryable<PROGRAM> _programs;
        private readonly IFacultyService _facultyService;
        private readonly IDepartmentService _departmentService;

        private bool _disposed = false;


        public CredentialManagerService()
        {
            _context = new FTCSEntities();
            _facultyProgramCoordinations = _context.FACULTY_PROGRAM_COORDINATION;
            _facultyConcentrationCoordinations = _context.FACULTY_CONCENTRATION_COORDINATION;
            _programLevelTypes = _context.PROGRAM_LEVEL_TYPE;
            _programs = _context.PROGRAMs;
            _facultyService = new FacultyService(_context);
            _departmentService = new DepartmentService(_context);
        }

        public CredentialManagerService(FTCSEntities db)
        {
            _context = db;
            _facultyProgramCoordinations = db.FACULTY_PROGRAM_COORDINATION;
            _facultyConcentrationCoordinations = _context.FACULTY_CONCENTRATION_COORDINATION;
            _programs = _context.PROGRAMs;
            _programLevelTypes = _context.PROGRAM_LEVEL_TYPE;
            _facultyService = new FacultyService(db);
            _departmentService = new DepartmentService(db);
        }

        public CredentialManagerService(FTCSEntities db, IFacultyService facultyService,
            IDepartmentService departmentService)
        {
            _context = db;
            _facultyProgramCoordinations = db.FACULTY_PROGRAM_COORDINATION;
            _facultyConcentrationCoordinations = _context.FACULTY_CONCENTRATION_COORDINATION;
            _programs = _context.PROGRAMs;
            _programLevelTypes = _context.PROGRAM_LEVEL_TYPE;
            _facultyService = facultyService;
            _departmentService = departmentService;
        }


        public IEnumerable<ProgramsVM> GetProgramsOfStudyForDepartment(string department)
        {
            var results = _facultyProgramCoordinations
                .Where(fpc =>
                        fpc.PROGRAM.DEPARTMENT == department
                        && (fpc.END_ACADEMIC_MONTH == 0 && fpc.END_ACADEMIC_YEAR == 0)
                )
                .Select(fpc => new ProgramsVM
                {
                    ProgramCode = fpc.PROGRAM_MAJOR_CODE,
                    DepartmentCode = fpc.PROGRAM.DEPARTMENT,
                    Program = fpc.PROGRAM.MAJOR_NAME,
                    Level = _programLevelTypes.FirstOrDefault(pl => pl.ID == fpc.PROGRAM.LEVEL.Value).LEVEL ?? "Unknown",
                    Coordinator =
                        fpc.TEACHER.LAST_NAME + ", " + fpc.TEACHER.FIRST_NAME + " " + fpc.TEACHER.MIDDLE_INITIAL,
                    CoordinatorId = fpc.FACULTY_ID,
                    NumberOfConcentrations = fpc.PROGRAM.CONCENTRATIONs.Count()
                });
            return results.ToList();
        }


        public List<SelectListItem> GetProgramOptions()
        {
            var options = _programs.Where(p => p.DEPARTMENT == Undefined).Select(p => new SelectListItem()
            {
                Value = p.MAJOR_CODE,
                Text = p.MAJOR_NAME
            }).ToList();
            options.Insert(0, new SelectListItem() {Text = "-- Choose a Program --", Value = ""});
            return options;
        }

        public List<SelectListItem> GetLevelOptions()
        {
            var options = _programLevelTypes.Select(pl => new SelectListItem()
            {
                Text = pl.LEVEL,
                Value = pl.ID.ToString()
            }).ToList();
            options.Insert(0, new SelectListItem() {Text = "-- Choose a Level --", Value = ""});
            return options;
        }

        public List<SelectListItem> GetCertificationOptions()
        {
            var defaultValue = 2;
            var listOfQuals = new List<Qualification>()
            {
                new Qualification() {Id = (int) Qualification.QualificationType.Academic},
                new Qualification() {Id = (int) Qualification.QualificationType.Professional},
                new Qualification() {Id = (int) Qualification.QualificationType.Unspecified},
                new Qualification() {Id = (int) Qualification.QualificationType.GraduateAssistant},
            };
            return listOfQuals.Select(q => new SelectListItem
            {
                Text = q.Description,
                Value = (q.Id ?? defaultValue).ToString()
            }).ToList();
        }

        public List<SelectListItem> GetCoordinatorOptions(string department, string college)
        {
            if (string.IsNullOrEmpty(college))
            {
                var departmentVm = _departmentService.GetDepartmentByDepartmentCode(department);
                college = departmentVm.COLLEGE;
            }
            var listOfFaculty = _facultyService.GetListOfFaculty(string.Empty, string.Empty, department: string.Empty,
                    sort: FacultyService.DefaultSortOrder)
                .Where(f => f.College == college && f.EmploymentStatus == _context.TEACHER_TYPE.FirstOrDefault(tt => tt.ID == 2)?.NAME)
                .Select(f => new SelectListItem(){Text = f.NAME, Value = f.BANNER_ID})
                .Distinct().ToList();
            listOfFaculty.Insert(0, new SelectListItem() {Text = "-- Choose a Coordinator --", Value = ""});
            return listOfFaculty;
        }

        /// <summary>
        /// Add New Program of Study
        /// This method gathers details of the faculty member and the department of said faculty member
        /// It first checks to see if the faculty member already is the coordinator of the program. If so,
        /// the method returns a DbOperationVM with the message that the faculty member is already the coordinator
        /// If not, we then create a new program coordination object. For each concentration we also assign the faculty
        /// member to be the coordinator for each concentration and we end date the previous faculty member's concentration
        /// coordination record. We then add new concentration coordination records for the new faculty member. We also update
        /// the program record to show that level, department and college the program is being managed by.
        /// </summary>
        /// <param name="programCode"></param>
        /// <param name="levelId"></param>
        /// <param name="coordinatorId"></param>
        /// <returns>
        /// DBOperationVM which contains a bool that shows if the DbOperation was successful
        /// and an associated response message
        /// </returns>
        public DbOperationVM AddNewProgramOfStudy(string programCode, string levelId, string coordinatorId,
            string department)
        {
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();
            var facultyDetails = _facultyService.GetFacultyById(coordinatorId);
            var departmentCode = department ?? facultyDetails?.DepartmentCode;
            var departmentDetails = _departmentService.GetDepartmentByDepartmentCode(departmentCode);
            try
            {
                
                AddFacultyConcentrationCoordinations(programCode, coordinatorId, currentSemester);
                AddProgramQualifications(facultyDetails?.BANNER_ID, programCode);
                UpdateFacultyProgramCoordination(programCode, coordinatorId);
                UpdateProgramDetails(programCode, levelId, departmentDetails.DEPARTMENT_CODE,
                    departmentDetails.COLLEGE);
                _context.SaveChanges();
                return new DbOperationVM(true);
            }
            catch (DbUpdateException)
            {
                return new DbOperationVM(false);
            }
        }


        private void UpdateFacultyProgramCoordination(string programCode, string coordinatorId)
        {
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();
            var existingProgramCoordination = FindExistingProgramCoordination(programCode, coordinatorId,
                currentSemester.Month, currentSemester.Year);
            if (existingProgramCoordination != null)
            {
                existingProgramCoordination.END_ACADEMIC_MONTH = 0;
                existingProgramCoordination.END_ACADEMIC_YEAR = 0;
            }
            else
            {
                existingProgramCoordination = CreateNewFacultyProgramCoordination(programCode, coordinatorId,
                    currentSemester);
                _context.FACULTY_PROGRAM_COORDINATION.Add(existingProgramCoordination);
            }
        }

        public FACULTY_PROGRAM_COORDINATION FindExistingProgramCoordination(string programCode, string coordinatorId,
            int month, int year)
        {
            return _context.FACULTY_PROGRAM_COORDINATION.FirstOrDefault(f =>
                f.PROGRAM_MAJOR_CODE == programCode
                && f.FACULTY_ID == coordinatorId && f.START_ACADEMIC_MONTH == month && f.START_ACADEMIC_YEAR == year);
        }

        public void RemoveProgramQualifications(string programCode)
        {
            var program = _programs.First(p => p.MAJOR_CODE == programCode);
            _context.FACULTY_PROGRAM_QUALIFICATION.RemoveRange(program.FACULTY_PROGRAM_QUALIFICATION);
        }

        public void AddProgramQualifications(string bannerId, string programCode)
        {
            var academicSemester = AcademicYear.GetCurrentAcademicSemester();
            var program = _programs.First(p => p.MAJOR_CODE == programCode);
            var existingFacultyProgramQual =
                _context.FACULTY_PROGRAM_QUALIFICATION.Where(
                    fpq =>
                        fpq.FACULTY_ID == bannerId && fpq.PROGRAM_MAJOR_CODE == programCode &&
                        fpq.ACADEMIC_MONTH == academicSemester.Month && fpq.ACADEMIC_YEAR == academicSemester.Year);

            if (existingFacultyProgramQual.Any())
            {
                existingFacultyProgramQual.ForEach((fpq) => { fpq.HOW_QUALIFIED = (int) QualificationType.Unspecified; });
            }
            else
            {
                var facultyProgramQualification = new FACULTY_PROGRAM_QUALIFICATION()
                {
                    FACULTY_ID = bannerId,
                    ACADEMIC_MONTH = academicSemester.Month,
                    ACADEMIC_YEAR = academicSemester.Year,
                    HOW_QUALIFIED = (int) QualificationType.Unspecified,
                    PROGRAM_MAJOR_CODE = program.MAJOR_CODE
                };
                _context.FACULTY_PROGRAM_QUALIFICATION.Add(facultyProgramQualification);
            }
        }

        private void UpdateProgramDetails(string programCode, string levelId, string departmentCode,
            string departmentCollegeCode)
        {
            var program = _programs.First(p => p.MAJOR_CODE == programCode);
            program.DEPARTMENT = departmentCode;
            program.COLLEGE = departmentCollegeCode;
            program.LEVEL = Convert.ToInt32(levelId);
        }

        private void EndDateFacultyConcentrationCoordinations(string programCode)
        {
            
        }

        public void RemoveProgramCoordinations(string programCode)
        {
            var coordinations = _programs.First(p => p.MAJOR_CODE == programCode)
                .FACULTY_PROGRAM_COORDINATION
                .Where(fpc => fpc.END_ACADEMIC_MONTH == 0 && fpc.END_ACADEMIC_YEAR == 0).ToList();
            ;
            _context.FACULTY_PROGRAM_COORDINATION.RemoveRange(coordinations);
        }

        public void AddFacultyConcentrationCoordinations(string programCode, string coordinatorId,
            Semester currentSemester)
        {
            var concentrations =
                _context.CONCENTRATIONs.Where(c => c.PROGRAM == programCode).Select(c => c.CODE).ToList();
            foreach (var concentration in concentrations)
            {
                var existingCoordinationForSemester = _context.FACULTY_CONCENTRATION_COORDINATION
                    .FirstOrDefault(fcc => fcc.FACULTY_ID == coordinatorId
                                && fcc.CONCENTRATION_CODE == concentration && fcc.PROGRAM_CODE == programCode
                                && fcc.START_ACADEMIC_MONTH == currentSemester.Month &&
                                fcc.START_ACADEMIC_YEAR == currentSemester.Year);

                if (existingCoordinationForSemester == null)
                {
                    _context.FACULTY_CONCENTRATION_COORDINATION.Add(new FACULTY_CONCENTRATION_COORDINATION()
                    {
                        FACULTY_ID = coordinatorId,
                        CONCENTRATION_CODE = concentration,
                        START_ACADEMIC_MONTH = currentSemester.Month,
                        START_ACADEMIC_YEAR = currentSemester.Year,
                        PROGRAM_CODE = programCode,
                        END_ACADEMIC_MONTH = 0,
                        END_ACADEMIC_YEAR = 0
                    });
                }
                else
                {
                    existingCoordinationForSemester.END_ACADEMIC_MONTH = 0;
                    existingCoordinationForSemester.END_ACADEMIC_YEAR = 0;
                }
            }
        }

        public void RemoveConcentrationCoordinations(string programCode)
        {
            var programs = _programs.First(p => p.MAJOR_CODE == programCode);
            programs.CONCENTRATIONs.ForEach(
                x => { _context.FACULTY_CONCENTRATION_COORDINATION.RemoveRange(x.FACULTY_CONCENTRATION_COORDINATION); });
        }

        public void EndCurrentFacultyConcentrations(string programCode)
        {
            var academicSemester = AcademicYear.GetCurrentAcademicSemester();
            _programs.First(p => p.MAJOR_CODE == programCode)?.CONCENTRATIONs.ForEach(x =>
            {
                x.FACULTY_CONCENTRATION_COORDINATION.ForEach(y =>
                {
                    y.END_ACADEMIC_MONTH = academicSemester.Month;
                    y.END_ACADEMIC_YEAR = academicSemester.Year;
                });
            });
        }

        private FACULTY_PROGRAM_COORDINATION CreateNewFacultyProgramCoordination(string programCode,
            string coordinatorId,
            Semester currentSemester)
        {
            var programCoordination = new FACULTY_PROGRAM_COORDINATION()
            {
                END_ACADEMIC_MONTH = 0,
                END_ACADEMIC_YEAR = 0,
                FACULTY_ID = coordinatorId,
                PROGRAM_MAJOR_CODE = programCode,
                START_ACADEMIC_MONTH = currentSemester.Month,
                START_ACADEMIC_YEAR = currentSemester.Year,
            };
            return programCoordination;
        }

        public DbOperationVM DeleteProgramOfStudy(string programCode, string coordinatorId)
        {
            EndCurrentFacultyConcentrations(programCode);
            RemoveProgramCoordinations(programCode);
            UpdateProgramDetails(programCode, UnassignedLevelType, Undefined, Undefined);
            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() {IsSuccess = true, Response = "Program of Study successfully removed"};
            }
            return new DbOperationVM() {IsSuccess = false, Response = "An unexpected error has occured"};
        }

        public DbOperationVM ChangeCoordinatorForProgramOfStudy(string programCode, string newCoordinatorId)
        {
          
            
                var currentSemester = AcademicYear.GetCurrentAcademicSemester();
                try
                {
                    _facultyProgramCoordinations.Where(fpc => fpc.PROGRAM_MAJOR_CODE == programCode)
                        .ForEach(coordination
                            =>
                            {
                                coordination.END_ACADEMIC_MONTH = currentSemester.Month;
                                coordination.END_ACADEMIC_YEAR = currentSemester.Year;
                            });
                    var existingProgramCoordination = FindExistingProgramCoordination(programCode, newCoordinatorId,
                        currentSemester.Month, currentSemester.Year);
                    if (existingProgramCoordination != null)
                    {
                        existingProgramCoordination.END_ACADEMIC_MONTH = 0;
                        existingProgramCoordination.END_ACADEMIC_YEAR = 0;
                    }
                    else
                    {
                        existingProgramCoordination = CreateNewFacultyProgramCoordination(programCode, newCoordinatorId,
                            currentSemester);
                        _context.FACULTY_PROGRAM_COORDINATION.Add(existingProgramCoordination);
                    }
                    _context.SaveChanges();
                    return new DbOperationVM(true);
                }
                catch (DbUpdateException)
                {
                    return new DbOperationVM(false);
                }
            }
        

        public DbOperationVM ChangeCoordinatorForConcentration(string concentrationCode, string programCode,
            string newCoordinatorId)
        {
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();

            var program = _context.PROGRAMs.FirstOrDefault(p => p.MAJOR_CODE == programCode);
            var concentration = program?.CONCENTRATIONs.FirstOrDefault(c => c.CODE == concentrationCode);
            var faculty = _context.FACULTies.First(f => f.BANNER_ID == newCoordinatorId);
            var currentCoordination = concentration?.FACULTY_CONCENTRATION_COORDINATION.FirstOrDefault(fcc =>
                fcc.CONCENTRATION_CODE == concentrationCode && fcc.PROGRAM_CODE == programCode &&
                fcc.END_ACADEMIC_MONTH == 0
                && fcc.END_ACADEMIC_YEAR == 0);

            var existingCoordinationForNewFaculty = concentration?.FACULTY_CONCENTRATION_COORDINATION.FirstOrDefault(fcc =>
                fcc.FACULTY_ID == newCoordinatorId && fcc.CONCENTRATION_CODE == concentrationCode && fcc.PROGRAM_CODE == programCode &&
                fcc.START_ACADEMIC_MONTH == currentSemester.Month
                && fcc.START_ACADEMIC_YEAR == currentSemester.Year);
            

            if (currentCoordination != null)
            {
                currentCoordination.END_ACADEMIC_MONTH = currentSemester.Month;
                currentCoordination.END_ACADEMIC_YEAR = currentSemester.Year;
            }

            if (existingCoordinationForNewFaculty != null)
            {
                existingCoordinationForNewFaculty.END_ACADEMIC_MONTH = 0;
                existingCoordinationForNewFaculty.END_ACADEMIC_YEAR = 0;
            }
            else
            {
                var newFacultyConcentrationCoordination = new FACULTY_CONCENTRATION_COORDINATION()
                {
                    FACULTY_ID = newCoordinatorId,
                    CONCENTRATION_CODE = concentrationCode,
                    START_ACADEMIC_MONTH = currentSemester.Month,
                    START_ACADEMIC_YEAR = currentSemester.Year,
                    PROGRAM_CODE = programCode,
                    END_ACADEMIC_MONTH = 0,
                    END_ACADEMIC_YEAR = 0
                };
                _context.FACULTY_CONCENTRATION_COORDINATION.Add(newFacultyConcentrationCoordination);
            }
            try
            {
                _context.SaveChanges();
                return new DbOperationVM
                {
                    IsSuccess = true,
                    Response = $"A new coordinator for {concentrationCode} was saved successfully"
                };
            }
            catch (DbUpdateException)
            {
                return new DbOperationVM {
                    IsSuccess = false,
                    Response = $"There was a problem making {faculty.TEACHER.LAST_NAME}, {faculty.TEACHER.FIRST_NAME} {faculty.TEACHER.MIDDLE_INITIAL} the coordinator for {concentrationCode}"};
            }
            
            
        }

        public DbOperationVM AddCourseCertification(string teacherId, string courseRubric, string courseNumber,
           int academicMonth, int academicYear, int qualification)
        {
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();
            if (!Enum.IsDefined(typeof(Qualification.QualificationType), qualification))
                return new DbOperationVM() {IsSuccess = false, Response = "The qualification specified is incorrect"};
            var teacherCourseQualification = _context.TEACHER_COURSE_QUALIFICATION
                .FirstOrDefault(tcq => tcq.TEACHER_ID == teacherId && tcq.COURSE_RUBRIC == courseRubric &&
                                      tcq.COURSE_NUMBER == courseNumber);
            try
            {
                if (teacherCourseQualification == null)
                {
                    teacherCourseQualification = new TEACHER_COURSE_QUALIFICATION();
                    teacherCourseQualification.TEACHER_ID = teacherId;
                    teacherCourseQualification.COURSE_RUBRIC = courseRubric;
                    teacherCourseQualification.COURSE_NUMBER = courseNumber;
                    teacherCourseQualification.ACADEMIC_MONTH = currentSemester.Month;
                    teacherCourseQualification.ACADEMIC_YEAR = currentSemester.Year;
                    teacherCourseQualification.HOW_QUALIFIED = qualification;
                    _context.TEACHER_COURSE_QUALIFICATION.Add(teacherCourseQualification);
                }
                else
                {
                    teacherCourseQualification.HOW_QUALIFIED = qualification;
                }
                _context.SaveChanges();
                return new DbOperationVM() { IsSuccess = true, Response = "Course certification successfully changed" };
            }
            catch (DbEntityValidationException)
            {
                return new DbOperationVM() {IsSuccess = false, Response = "An Database validation has occurred"};
            }
        }

        public IEnumerable<ConcentrationVM> GetProgramConcentrations(string programCode)
        {
            var results =
                _context.FACULTY_CONCENTRATION_COORDINATION.Where(
                        fcc =>
                            fcc.END_ACADEMIC_MONTH == 0 && fcc.END_ACADEMIC_YEAR == 0 &&
                            fcc.PROGRAM_CODE == programCode)
                    .Select(fcc => new ConcentrationVM()
                    {
                        Coordinator =
                            fcc.TEACHER.LAST_NAME + ", " + fcc.TEACHER.FIRST_NAME + " " + fcc.TEACHER.MIDDLE_INITIAL,
                        CoordinatorId = fcc.TEACHER.BANNER_ID,
                        ProgramCode = fcc.PROGRAM_CODE,
                        ConcentrationCode = fcc.CONCENTRATION_CODE,
                        Program = fcc.CONCENTRATION.CODE + " " + fcc.CONCENTRATION.NAME
                    }).OrderBy(p => p.Program)
                    .ToList();
            return results.OrderBy(p => p.Program);
        }

        public ConcentrationVM GetProgramConcentration(string programCode, string concentrationCode)
        {
            var results =
                _facultyConcentrationCoordinations.Where(
                        fcc =>
                            fcc.END_ACADEMIC_MONTH == 0 && fcc.END_ACADEMIC_YEAR == 0 && fcc.PROGRAM_CODE == programCode &&
                            fcc.CONCENTRATION_CODE == concentrationCode)
                    .Select(fcc => new ConcentrationVM()
                    {
                        Coordinator =
                            fcc.TEACHER.LAST_NAME + ", " + fcc.TEACHER.FIRST_NAME + " " + fcc.TEACHER.MIDDLE_INITIAL,
                        CoordinatorId = fcc.TEACHER.BANNER_ID,
                        ProgramCode = fcc.PROGRAM_CODE,
                        ConcentrationCode = fcc.CONCENTRATION_CODE,
                        Program = fcc.CONCENTRATION.CODE + " " + fcc.CONCENTRATION.NAME
                    });
            return results.Single();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                    _departmentService.Dispose();
                    _facultyService.Dispose();
                }
            }
            this._disposed = true;
        }

        public IEnumerable<ProgramsVM> GetProgramsOfStudyForCollege(string college)
        {
            var listOfPrograms = new List<ProgramsVM>();
            var listOfDepartments = _departmentService
                .GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                .Where(d => d.COLLEGE == college);

            listOfDepartments.ForEach(
                d => { listOfPrograms.AddRange(GetProgramsOfStudyForDepartment(d.DEPARTMENT_CODE)); });
            return listOfPrograms.OrderBy(p => p.Program);
        }

        public ProgramsVM GetProgramOfStudy(string programCode, string departmentCode)
        {
            if (string.IsNullOrEmpty(programCode) || string.IsNullOrEmpty(departmentCode)) return new ProgramsVM();
            var programs = GetProgramsOfStudyForDepartment(departmentCode);
            return programs
                .FirstOrDefault(p => p.ProgramCode == programCode);
        }
    }
}