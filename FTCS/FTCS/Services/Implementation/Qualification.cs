﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FTCS.Services.Implementation
{
    public class Qualification
    {
        public enum QualificationType
        {
            [Display(Name = "Academic")] Academic,
            [Display(Name = "Professional")] Professional,
            [Display(Name = "Unspecified")] Unspecified,
            [Display(Name = "Graduate Assistant")] GraduateAssistant
        }

        public int? Id
        {
            get { return _id;}
            set
            {
                _id = (value ?? 2);
                Value = _id.ToString();
                var type = (QualificationType)_id ;
                switch (type)
                {
                    case QualificationType.Academic:
                        Description = "Academic";
                        break;
                    case QualificationType.Professional:
                        Description = "Professional";
                        break;
                    case QualificationType.Unspecified:
                        Description = "Unspecified";
                        break;
                    case QualificationType.GraduateAssistant:
                        Description = "Graduate Assistant";
                        break;
                    default:
                        Description = "Unspecified";
                        break;
                }
            }
        }
        public string Description { get; set; }
        public string Value { get; set; }

        private int _id;
    }  
}