﻿using FTCS.Models;
using System.Collections.Generic;
using System.Linq;
using FTCS.Interfaces;
using FTCS.Services.Implementation;
using FTCS.ViewModels;
using FTCS.ViewModels.Reports;
using System;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.Course;
using FTCS.ViewModels.FacultyProfile;
using WebGrease.Css.Extensions;

namespace FTCS.Services
{
    public class ReportService : IReportService
    {
        private bool disposed = false;
        private readonly FTCSEntities _context;
        private readonly IQueryable<TEACHER> _teachers;
        private readonly IQueryable<DEPARTMENT> _department;
        private readonly IQueryable<ACADEMIC_DEGREE> _academicDegrees;
        private readonly IQueryable<NARRATIVE> _narratives;
        private RELATED_WORK_EXPERIENCE _defaultWorkExperience;
        private List<int> _facultyTypes = new List<int>() {0, 1, 2};
        private readonly IQueryable<TEACHER_COURSE_RECORD> _teacherCourseRecords;
        private readonly IQueryable<TEACHER_COURSE_QUALIFICATION> _teacherCourseQualification;
        private readonly IQueryable<COURSE> _course;
        private readonly IDepartmentService _departmentService;

        public ReportService(FTCSEntities db)
        {
            _context = db;
            _teachers = db.TEACHERs;
            _teacherCourseRecords = db.TEACHER_COURSE_RECORD;
            _academicDegrees = db.ACADEMIC_DEGREE;
            _narratives = db.NARRATIVEs;
            _department = db.DEPARTMENTs;
            _course = db.COURSEs;
            _teacherCourseQualification = db.TEACHER_COURSE_QUALIFICATION;
            _departmentService = new DepartmentService(db);
        }
        public ReportService(FTCSEntities db, IDepartmentService departmentService)
        {
            _context = db;
            _teachers = db.TEACHERs;
            _teacherCourseRecords = db.TEACHER_COURSE_RECORD;
            _academicDegrees = db.ACADEMIC_DEGREE;
            _narratives = db.NARRATIVEs;
            _department = db.DEPARTMENTs;
            _course = db.COURSEs;
            _teacherCourseQualification = db.TEACHER_COURSE_QUALIFICATION;
            _departmentService = departmentService;
        }

        

        public ReportService()
        {
            _context = new FTCSEntities();
            _teachers = _context.TEACHERs;
            _teacherCourseRecords = _context.TEACHER_COURSE_RECORD;
            _academicDegrees = _context.ACADEMIC_DEGREE;
            _narratives = _context.NARRATIVEs;
            _department = _context.DEPARTMENTs;
            _course = _context.COURSEs;
            _teacherCourseQualification = _context.TEACHER_COURSE_QUALIFICATION;
            _departmentService = new DepartmentService();
            _defaultWorkExperience = new RELATED_WORK_EXPERIENCE {POSITION = "None Required"};
        }

        /// <summary>
        /// Gets a list of all departments
        /// </summary>
        /// <returns>List of DepartmentVMs corresponding to all departments</returns>
        public IEnumerable<DepartmentVM> GetDepartmentList()
        {
            var departmentList = _department.Select(d => new DepartmentVM
            {
                NAME = d.NAME,
                DEPARTMENT_CODE = d.CODE
            });
            return departmentList.ToList();
        }
        /// <summary>
        /// Gets faculty roster by year, type of faculty, and department faculty
        /// </summary>
        /// <param name="year">The year of the roster you'd like to display</param>
        /// <param name="facultyType">The type of faculty, integer greater than 0 less than 4</param>
        /// <param name="departments">The department to display faculty for</param>
        /// <returns>List of FacultyRosterVMs corresponding to the paramaters</returns>
        public IEnumerable<FacultyRosterVM> GetFacultyRoster(int year, int? facultyType, string departments)
        {
            var academicYear = AcademicYear.GetAcademicSemestersForYear(year);
            var listOfMonths = academicYear.Select(ay => ay.Month).ToList();
            var listOfYears = academicYear.Select(ay => ay.Year).ToList();
            var fallMonth = listOfMonths[0];
            var springMonth = listOfMonths[1];
            var summerMonth = listOfMonths[2];
            var fallYear = listOfYears[0];
            var springYear = listOfYears[1];
            var summerYear = listOfYears[2];
            var result = _teachers.GroupJoin(
                _teacherCourseRecords.Where(
                    tcr => (tcr.ACADEMIC_MONTH == fallMonth && tcr.ACADEMIC_YEAR == fallYear) ||
                           (tcr.ACADEMIC_MONTH == springMonth && tcr.ACADEMIC_YEAR == springYear) ||
                           (tcr.ACADEMIC_MONTH == summerMonth && tcr.ACADEMIC_YEAR == summerYear)),
                t => new {ID = t.BANNER_ID},
                tcr => new {ID = tcr.TEACHER_ID},
                (t, tcr) => new {Teacher = t, CourseRecords = tcr}).Where(ttcr => ttcr.CourseRecords.Any());

            if (!string.IsNullOrEmpty(departments))
            {
                result = result.Where(ttcr => ttcr.CourseRecords.Any(cr => cr.COURSE.DEPARTMENT == departments));
            }

            if (facultyType.HasValue && facultyType.Value != 4)
            {
                result = result.Where(ttcr => ttcr.Teacher.TEACHER_TYPE == facultyType);
            }
            else
            {
                result = result.Where(ttcr => _facultyTypes.Contains(ttcr.Teacher.TEACHER_TYPE.Value));
            }

            return result.OrderBy(t => t.Teacher.LAST_NAME).Select(t => new FacultyRosterVM
            {
                Name = t.Teacher.LAST_NAME + ", " + t.Teacher.FIRST_NAME,
                TeacherType = new TeacherType {Id = t.Teacher.TEACHER_TYPE},
                Department = t.CourseRecords.FirstOrDefault().COURSE.DEPARTMENT1.NAME,
                AcademicDegrees =
                    t.Teacher.ACADEMIC_DEGREE.Select(ad => ad.NAME + " (" + ad.DISCIPLINE + ") " + ad.INSTITUTION),
                Narrative = t.Teacher.NARRATIVE.NARRATIVE1,
                CourseRecords =
                    t.CourseRecords.GroupBy(tcr => new {tcr.ACADEMIC_MONTH, tcr.ACADEMIC_YEAR})
                        .Select(group => new CourseRecordsBySemesterVM
                        {
                            Name = new Semester {Month = group.Key.ACADEMIC_MONTH, Year = group.Key.ACADEMIC_YEAR},
                            Records =
                                group.Select(tcr => tcr.COURSE.RUBRIC + " " + tcr.COURSE.NUMBER + " " + tcr.COURSE.NAME)
                        }).OrderBy(cr => cr.Name.Year).ThenBy(cr => cr.Name.Month),
                SummaryVm =
                    new ExperienceSummaryVM
                    {
                        Narrative = t.Teacher.NARRATIVE.NARRATIVE1,
                        WorkExperience = t.Teacher.RELATED_WORK_EXPERIENCE.Select(rwe => rwe.RESPONSIBILITIES)
                    }
            }).ToList();
        }
        /// <summary>
        /// Creates the faculty credentials report
        /// </summary>
        /// <param name="department">The department to generate the report for</param>
        /// <param name="year">The year to generate the report for</param>
        /// <returns>List of FacultyCredentialsReportVMs corresponding to the parameters</returns>
        public IEnumerable<FacultyCredentialsReportVM> GetFacultyCredentialsReport(string department,
            int year)
        {
            var academicYear = AcademicYear.GetAcademicSemestersForYear(year).ToList();
            var fall = academicYear[0];
            var spring = academicYear[1];
            var summer = academicYear[2];
            var credentials = GetTeacherCourseRecordsWithQualifications()
                .Where(c => c.DepartmentCode == department 
                && ((c.AcademicMonth == fall.Month && c.Year == fall.Year) ||
                   (c.AcademicMonth == spring.Month && c.Year == spring.Year) ||
                   (c.AcademicMonth == summer.Month && c.Year == summer.Year))
                )
                .ToList();
            var facultyCredentialReports = credentials.Select(c => 
            new FacultyCredentialsReportVM()
            {
                TeacherId = c.TeacherId,
                FacultyName = $"{c.TeacherLastName}, {c.TeacherFirstName} {c.TeacherMiddleName} {(new TeacherType() {Id = c.FacultyType}).ShortDescription }",
                Semester = (new Semester(c.AcademicMonth,c.Year)).ToString(),
                Course = $"{c.CourseRubric}-{c.CourseNumber}-{c.CourseSection} {c.CourseName}",
                CourseDescription = c.CourseDescription,
                CourseNumber = c.CourseNumber,
                CourseRubric = c.CourseRubric,
                Credential = new Qualification { Id = c.HowQualified}
            });
            return facultyCredentialReports.OrderBy(c => c.FacultyName);
        }
        /// <summary>
        /// Gets teacher course records with teacher qualification
        /// </summary>
        /// <returns>List of Credentials by teacher for all departments</returns>
        public IQueryable<Credentials> GetTeacherCourseRecordsWithQualifications()
        {
            var departments =
                _departmentService.GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                    .Select(d => d.DEPARTMENT_CODE)
                    .AsQueryable();
            var results = from tcr in _teacherCourseRecords
                join tcq in _teacherCourseQualification
                on 
                new { TeacherId = tcr.TEACHER_ID, Rubric = tcr.COURSE_RUBRIC, Number = tcr.COURSE_NUMBER} 
                equals
                new { TeacherId = tcq.TEACHER_ID, Rubric = tcq.COURSE_RUBRIC, Number = tcq.COURSE_NUMBER}
                          where departments.Contains(tcr.COURSE.DEPARTMENT)
                
                          select new Credentials()
                          {
                              TeacherId = tcr.TEACHER_ID,
                              TeacherFirstName = tcr.TEACHER.FIRST_NAME,
                              TeacherLastName = tcr.TEACHER.LAST_NAME,
                              TeacherMiddleName = tcr.TEACHER.MIDDLE_INITIAL,
                              FacultyType = tcr.TEACHER.TEACHER_TYPE ?? 3,
                              Department = tcr.COURSE.DEPARTMENT1.NAME,
                              DepartmentCode = tcr.COURSE.DEPARTMENT1.CODE,
                              CourseNumber = tcr.COURSE_NUMBER,
                              CourseRubric = tcr.COURSE_RUBRIC,
                              CourseName = tcr.COURSE.NAME,
                              CourseDescription = tcr.COURSE.DESCRIPTION,
                              CourseSection = tcr.COURSE_SECTION,
                              AcademicMonth = tcr.ACADEMIC_MONTH,
                              Year = tcr.ACADEMIC_YEAR,
                              HowQualified = tcq.HOW_QUALIFIED
                          }
                          ;
            return results;
        }
        /// <summary>
        /// Gets faculty credentials report by year and college
        /// </summary>
        /// <param name="college">college to generate report for</param>
        /// <param name="year">the year to generate report for</param>
        /// <returns>list of FacultyCredentialsReportVMs corresponding to parameters</returns>
        public IEnumerable<FacultyCredentialsReportVM> GetFacultyCredentialReportForCollege(string college, int year)
        {
            var reportVms = new List<FacultyCredentialsReportVM>();
            var listOfDepartments = _departmentService
                .GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                .Where(d => d.COLLEGE == college);
            listOfDepartments.ForEach(d =>
            {
                reportVms.AddRange(GetFacultyCredentialsReport(d.NAME, year));
            });
            return reportVms;
        }

        /// <summary>
        /// Get all faculty that have unspecified qualifications in a department and year
        /// </summary>
        /// <param name="department">Department to search for unspecified faculty in</param>
        /// <param name="year">Year to search for unspecified faculty</param>
        /// <returns>List of FacultyCredentialsReportVMs for department and year</returns>
        public IEnumerable<CourseCredentialVM> GetUnspecifiedFacultyCredentials(string department, int year)
        {
            var unspecifiedCredentials =
                GetFacultyCredentialsReport(department, year)
                    .Where(f => f.Credential.Id == (int) Qualification.QualificationType.Unspecified)
                    .Select(c => new CourseCredentialVM()
                    {
                        TeacherId = c.TeacherId,
                        FacultyName = c.FacultyName,
                        Name = c.Course,
                        Department = department
                    });
            return unspecifiedCredentials;
        }
        /// <summary>
        /// Gets the academic semesters for populating menu
        /// </summary>
        /// <returns>List of academic semesters</returns>
        public List<string> GetSemestersForMenu()
        {
            var semester = _teacherCourseQualification.Select(
                tcq => new
                {
                    Year = tcq.ACADEMIC_YEAR
                }).Distinct().OrderByDescending(s => s.Year);

            List<string> semestersForMenu = new List<string>();
            foreach (var item in semester)
            {
                string s = item.Year.ToString();
                int n = Convert.ToInt32(s) + 1;
                semestersForMenu.Add("Fall " + s + " - Summer " + n);
            }

            return semestersForMenu;
        }
        /// <summary>
        /// Gets a list of all departments for populating menue
        /// </summary>
        /// <returns>List of all departments</returns>
        public List<string> GetDepartmentsForMenu()
        {
            var department = from tcq in _teacherCourseQualification
                from c in _course.Where(x => (x.NUMBER == tcq.COURSE_NUMBER) && (x.RUBRIC == tcq.COURSE_RUBRIC))
                from d in _department.Where(x => x.CODE == c.DEPARTMENT)
                select new
                {
                    Department = d.NAME
                };
            department = department.Distinct().OrderBy(d => d.Department);

            List<string> departmentsForMenu = new List<string>();
            foreach (var item in department)
            {
                string s = item.Department;
                departmentsForMenu.Add(s);
            }
            return departmentsForMenu;
        }
        /// <summary>
        /// Get list of faculty in department with incomplete qualifications
        /// </summary>
        /// <param name="departmentCode">the department to search in</param>
        /// <returns>List of FacultyRosterVMs</returns>
        public List<FacultyRosterVM> GetIncompleteProfessionalQualification(string departmentCode)
        {
            var firstAcademicSemester = AcademicYear.GetCurrentAcademicYear().First();
            var faculty =
                GetFacultyRoster(firstAcademicSemester.Year, 4, departmentCode)
                    .Where(
                        fr =>
                            fr.TeacherType.ShortDescription != "(GA)" && !fr.AcademicDegrees.Any() &&
                            (fr.SummaryVm.Narrative == null || !fr.SummaryVm.WorkExperience.Any())).ToList();
            return faculty;
        }
        /// <summary>
        /// Generates a summary, totals, for the faculty credentials report
        /// </summary>
        /// <returns>ChartVM for totals and unspecified departments</returns>
        public ChartVM GetFacultyCredentialsSummary()
        {
            var currentAcademicYear = AcademicYear.GetCurrentAcademicYear().ToList();
            var fall = currentAcademicYear[0];
            var spring = currentAcademicYear[1];
            var summer = currentAcademicYear[2];
            
            var allCredentials = GetTeacherCourseRecordsWithQualifications().ToList();
            var currentYearCredentials = allCredentials.Where(c =>
                ((c.AcademicMonth == fall.Month && c.Year == fall.Year) ||
                 (c.AcademicMonth == spring.Month && c.Year == spring.Year) ||
                 (c.AcademicMonth == summer.Month && c.Year == summer.Year)));
            var yearCredentials = currentYearCredentials as IList<Credentials> ?? currentYearCredentials.ToList();
           var unspecifiedDepartments = yearCredentials.GroupBy(c => c.Department)
                .Select(c => new {Department = c.Key, Unspecified = c.Count(cr => cr.HowQualified == 2)})
                .OrderBy(d => d.Department);
            return new ChartVM()
            {
                UnspecifiedDepartments = unspecifiedDepartments,
                Totals = new CSQPRTotalVM()
                {
                    TotalAcademicNumber = yearCredentials.Count(c => c.HowQualified == 0),
                    TotalProfessionallyQualified = yearCredentials.Count(c => c.HowQualified == 1),
                    TotalUnspecified = yearCredentials.Count(c => c.HowQualified == 2),
                    TotalGraduateAssistants = yearCredentials.Count(c => c.HowQualified == 3)
                }
            };

        }
        /// <summary>
        /// Get list of faculty with incomplete professional qualifications by college
        /// </summary>
        /// <param name="college">college to search for faculty within</param>
        /// <returns>list of FacultyRosterVMs of faculty with incomplete rofessional qualifications</returns>
        public List<FacultyRosterVM> GetIncompleteProfessionalQualificationsForCollege(string college)
        {
            var listOfFacultyRosterVM = new List<FacultyRosterVM>();
            var listOfDepartments = _departmentService
                .GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                .Where(d => d.COLLEGE == college);
            listOfDepartments.ForEach(d =>
            {
                listOfFacultyRosterVM.AddRange(GetIncompleteProfessionalQualification(d.DEPARTMENT_CODE));
            });
            return listOfFacultyRosterVM;

        }
        /// <summary>
        /// Gets the course section qualification percentage report for the given year, generates undergrad, graduate, and total
        /// </summary>
        /// <param name="year">year to generate report for</param>
        /// <returns>CourseSectionQualificationsPercentageReportVM holding undergrad, graduate, and total</returns>
        public CourseSectionQualificationsPercentageReportVM GetCourseSectionQualificationPercentageReport(int year)
        {
            var academicYear = AcademicYear.GetAcademicSemestersForYear(year).ToArray();
            var departmentCodes =
                _departmentService.GetListOfDepartments(string.Empty, string.Empty, string.Empty)
                    .Select(d => d.DEPARTMENT_CODE);
            var fall = academicYear[0];
            var spring = academicYear[1];
            var summer = academicYear[2];

            var graduateCoursesThisYear = GetTeacherCourseRecordsWithQualifications().Where(c =>
            (c.AcademicMonth == fall.Month && c.Year == fall.Year ||
            c.AcademicMonth == spring.Month && c.Year == spring.Year ||
            c.AcademicMonth == summer.Month && c.Year == summer.Year) &&
            string.Compare(c.CourseNumber, "5000", StringComparison.Ordinal) >= 0);

            var allGraduateCourseRecordsForThisYear = _teacherCourseRecords.Where(tcr =>
                    (tcr.ACADEMIC_MONTH == fall.Month && tcr.ACADEMIC_YEAR == fall.Year ||
                        tcr.ACADEMIC_MONTH == spring.Month && tcr.ACADEMIC_YEAR == spring.Year ||
                        tcr.ACADEMIC_MONTH == summer.Month && tcr.ACADEMIC_YEAR == summer.Year)

            );
            var graduateTeacherCourseRecordsForThisYear = allGraduateCourseRecordsForThisYear.Where(tcr =>
                    string.Compare(tcr.COURSE_NUMBER, "5000", StringComparison.Ordinal) >= 0
            );

            var totalGraduateSectionsPerDepartment = graduateCoursesThisYear.Where(c => departmentCodes.Contains(c.DepartmentCode)).GroupBy(c => c.Department)
                .Select(grp => new
                {
                    Department = grp.Key,
                    AllSections = allGraduateCourseRecordsForThisYear.Distinct().Count(tcr => tcr.COURSE.DEPARTMENT1.NAME == grp.Key),
                    TotalSections = graduateTeacherCourseRecordsForThisYear.Distinct().Count(tcr => tcr.COURSE.DEPARTMENT1.NAME == grp.Key),
                    AcademicallyQualified = grp.Count(tcr => tcr.HowQualified == 0),
                    ProfessionallyQualified = grp.Count(tcr => tcr.HowQualified == 1),
                    GAQualified = grp.Count(tcr => tcr.HowQualified == 3),
                    MissingTranscipts = _academicDegrees.Where(a =>
                    grp.Select(tcr => tcr.TeacherId).Distinct().Contains(a.TEACHER_ID) &&
                    a.HAS_TRANSCRIPT == 0).Select(a => a.TEACHER.FIRST_NAME + " " + a.TEACHER.LAST_NAME + " " + a.INITIALS + " " + a.NAME)
                });

            var totalGraduateCourseByDepartment = totalGraduateSectionsPerDepartment.Select(c => new CSQPRGraduateVM()
            {
                DepartmentName = c.Department,
                AllSection = c.AllSections,
                TotalCourse = c.TotalSections,
                AcademicCourse = c.AcademicallyQualified,
                ProfessionalCourse = c.ProfessionallyQualified,
                GaCourse = c.GAQualified,
                TranscriptMissingList = c.MissingTranscipts,
                TotalCoursePercent = (int)Math.Round(((c.TotalSections / (float)c.AllSections) * 100)),
                AcademicCoursePercent = (int)Math.Round(((c.AcademicallyQualified / (float)c.TotalSections) * 100)),
                ProfessionalCoursePercent = (int)Math.Round(((c.ProfessionallyQualified / (float)c.TotalSections) * 100)),
                GaCourseCoursePercent = (int)Math.Round(((c.GAQualified / (float)c.TotalSections) * 100))
            }).OrderBy(c => c.DepartmentName).ToList();

            //Begin Undergraduate Section
            var undergraduateCoursesThisYear = GetTeacherCourseRecordsWithQualifications().Where(uc =>
            (uc.AcademicMonth == fall.Month && uc.Year == fall.Year ||
            uc.AcademicMonth == spring.Month && uc.Year == spring.Year ||
            uc.AcademicMonth == summer.Month && uc.Year == summer.Year) &&
            string.Compare(uc.CourseNumber, "5000", StringComparison.Ordinal) < 0);

            var allUndergraduateCourseRecordsForThisYear = _teacherCourseRecords.Where(utcr =>
                    (utcr.ACADEMIC_MONTH == fall.Month && utcr.ACADEMIC_YEAR == fall.Year ||
                        utcr.ACADEMIC_MONTH == spring.Month && utcr.ACADEMIC_YEAR == spring.Year ||
                        utcr.ACADEMIC_MONTH == summer.Month && utcr.ACADEMIC_YEAR == summer.Year)

            );

            var undergraduateTeacherCourseRecordsForThisYear = allUndergraduateCourseRecordsForThisYear.Where(utcr =>
                    string.Compare(utcr.COURSE_NUMBER, "5000", StringComparison.Ordinal) < 0
            );

            var totalUndergraduateSectionsPerDepartment = undergraduateCoursesThisYear.Where(c => departmentCodes.Contains(c.DepartmentCode)).GroupBy(uc => uc.Department)
                .Select(ugrp => new
                {
                    Department = ugrp.Key,
                    AllSections = allUndergraduateCourseRecordsForThisYear.Distinct().Count(ttcr => ttcr.COURSE.DEPARTMENT1.NAME == ugrp.Key),
                    TotalSections = undergraduateTeacherCourseRecordsForThisYear.Distinct().Count(ttcr => ttcr.COURSE.DEPARTMENT1.NAME == ugrp.Key),
                    AcademicallyQualified = ugrp.Count(ttcr => ttcr.HowQualified == 0),
                    ProfessionallyQualified = ugrp.Count(ttcr => ttcr.HowQualified == 1),
                    GAQualified = ugrp.Count(ttcr => ttcr.HowQualified == 3),
                    MissingTranscipts = _academicDegrees.Where(ua =>
                    ugrp.Select(ttcr => ttcr.TeacherId).Distinct().Contains(ua.TEACHER_ID) &&
                    ua.HAS_TRANSCRIPT == 0).Select(aa => aa.TEACHER.FIRST_NAME + " " + aa.TEACHER.LAST_NAME + " " + aa.INITIALS + " " + aa.NAME)
                });

            var totalUndergraduateCourseByDepartment = totalUndergraduateSectionsPerDepartment.Select(uc => new CSQPRUndergraduateVM()
            {
                DepartmentName = uc.Department,
                AllSection = uc.AllSections,
                TotalCourse = uc.TotalSections,
                AcademicCourse = uc.AcademicallyQualified,
                ProfessionalCourse = uc.ProfessionallyQualified,
                GaCourse = uc.GAQualified,
                TranscriptMissingList = uc.MissingTranscipts,
                TotalCoursePercent = (int)Math.Round(((uc.TotalSections / (float)uc.AllSections) * 100)),
                AcademicCoursePercent = (int)Math.Round(((uc.AcademicallyQualified / (float)uc.TotalSections) * 100)),
                ProfessionalCoursePercent = (int)Math.Round(((uc.ProfessionallyQualified / (float)uc.TotalSections) * 100)),
                GaCourseCoursePercent = (int)Math.Round(((uc.GAQualified / (float)uc.TotalSections) * 100))
            }).OrderBy(uc => uc.DepartmentName).ToList();

            //calculate totals for undergrad, grad, and all
            var totalCourse = new CSQPRTotalVM()
            {
                TotalCourseNumber = totalGraduateCourseByDepartment.Sum(to => to.TotalCourse) + totalUndergraduateCourseByDepartment.Sum(uto => uto.TotalCourse),
                TotalAcademicNumber = totalGraduateCourseByDepartment.Sum(to => to.AcademicCourse) + totalUndergraduateCourseByDepartment.Sum(uto => uto.AcademicCourse),
                TotalProfessionallyQualified = totalGraduateCourseByDepartment.Sum(to => to.ProfessionalCourse) + totalUndergraduateCourseByDepartment.Sum(uto => uto.ProfessionalCourse),
                TotalGraduateAssistants = totalGraduateCourseByDepartment.Sum(to => to.GaCourse) + totalUndergraduateCourseByDepartment.Sum(uto => uto.GaCourse),
                TotalMissingTranscripts = totalGraduateCourseByDepartment.Sum(to => to.TranscriptMissingList.Count()) + totalUndergraduateCourseByDepartment.Sum(uto => uto.TranscriptMissingList.Count())
            };

            totalCourse.TotalAcademicPercent = (int)Math.Round((totalCourse.TotalAcademicNumber / (float)totalCourse.TotalCourseNumber) * 100);
            totalCourse.TotalProfessionallyQualifiedPercent = (int)Math.Round((totalCourse.TotalProfessionallyQualified / (float)totalCourse.TotalCourseNumber) * 100);
            totalCourse.TotalGraduateAssistantsPercent = (int)Math.Round((totalCourse.TotalGraduateAssistants / (float)totalCourse.TotalCourseNumber) * 100);

            var TotalNumberOfCourses = totalCourse.TotalCourseNumber;

            var totalUndergraduateCourse = new CSQPRTotalVM()
            {
                TotalCourseNumber = totalUndergraduateCourseByDepartment.Sum(uto => uto.TotalCourse),
                TotalCoursePercent = (int)Math.Round((totalUndergraduateCourseByDepartment.Sum(uto => uto.TotalCourse) / (float)TotalNumberOfCourses) * 100),
                TotalAcademicNumber = totalUndergraduateCourseByDepartment.Sum(uto => uto.AcademicCourse),
                TotalAcademicPercent = (int)Math.Round(((totalUndergraduateCourseByDepartment.Sum(uto => uto.AcademicCourse) / (float)totalUndergraduateCourseByDepartment.Sum(uto => uto.TotalCourse) * 100))),
                TotalProfessionallyQualified = totalUndergraduateCourseByDepartment.Sum(uto => uto.ProfessionalCourse),
                TotalProfessionallyQualifiedPercent = (int)Math.Round(((totalUndergraduateCourseByDepartment.Sum(uto => uto.ProfessionalCourse) / (float)totalUndergraduateCourseByDepartment.Sum(uto => uto.TotalCourse) * 100))),
                TotalGraduateAssistants = totalUndergraduateCourseByDepartment.Sum(uto => uto.GaCourse),
                TotalGraduateAssistantsPercent = (int)Math.Round(((totalUndergraduateCourseByDepartment.Sum(uto => uto.GaCourse) / (float)totalUndergraduateCourseByDepartment.Sum(uto => uto.TotalCourse) * 100))),
                TotalMissingTranscripts = totalUndergraduateCourseByDepartment.Sum(uto => uto.TranscriptMissingList.Count())
                };

            var totalGraduateCourse = new CSQPRTotalVM()
            {
                TotalCourseNumber = totalGraduateCourseByDepartment.Sum(to => to.TotalCourse),
                TotalCoursePercent = (int)Math.Round((totalGraduateCourseByDepartment.Sum(to => to.TotalCourse) / (float)TotalNumberOfCourses)* 100),
                TotalAcademicNumber = totalGraduateCourseByDepartment.Sum(to => to.AcademicCourse),
                TotalAcademicPercent = (int)Math.Round(((totalGraduateCourseByDepartment.Sum(to => to.AcademicCourse) / (float)totalGraduateCourseByDepartment.Sum(to => to.TotalCourse) * 100))),
                TotalProfessionallyQualified = totalGraduateCourseByDepartment.Sum(to => to.ProfessionalCourse),
                TotalProfessionallyQualifiedPercent = (int)Math.Round(((totalGraduateCourseByDepartment.Sum(to => to.ProfessionalCourse) / (float)totalGraduateCourseByDepartment.Sum(to => to.TotalCourse) * 100))),
                TotalGraduateAssistants = totalGraduateCourseByDepartment.Sum(to => to.GaCourse),
                TotalGraduateAssistantsPercent = (int)Math.Round(((totalGraduateCourseByDepartment.Sum(to => to.GaCourse) / (float)totalGraduateCourseByDepartment.Sum(to => to.TotalCourse) * 100))),
                TotalMissingTranscripts = totalGraduateCourseByDepartment.Sum(to => to.TranscriptMissingList.Count())
            };

            return new CourseSectionQualificationsPercentageReportVM()
            {
                GraduateReport = totalGraduateCourseByDepartment,
                UndergraduateReport = totalUndergraduateCourseByDepartment,
                UndergraduateTotal = totalUndergraduateCourse,
                GraduateTotal = totalGraduateCourse,
                CSQPRTotal = totalCourse
            };
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }

    public class Credentials
    {
        public Credentials()
        {
        }

        public TEACHER_COURSE_RECORD TeacherCourseRecords { get; internal set; }
        public TEACHER_COURSE_QUALIFICATION TeacherCourseQuals { get; set; }
        public string Department { get; internal set; }
        public string DepartmentCode { get; set; }
        public string CourseNumber { get; set; }
        public string CourseRubric { get; set; }
        public string CourseSection { get; set; }
        public int? HowQualified { get; set; }
        public string TeacherId { get; set; }
        public int AcademicMonth { get; set; }
        public int Year { get; internal set; }
        public int FacultyType { get; set; }
        public string TeacherFirstName { get; set; }
        public string TeacherLastName { get; set; }
        public string TeacherMiddleName { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
    }
}