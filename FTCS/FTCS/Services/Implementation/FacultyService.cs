﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FTCS.Models;
using FTCS.Services.Exceptions;
using FTCS.ViewModels;
using FTCS.Services.Interfaces;
using FTCS.ViewModels.FacultyProfile;
using FTCS.ViewModels.Reports;

namespace FTCS.Services.Implementation
{
    public class FacultyService : IFacultyService
    {
        private const string _AscendingPostfix = "_asc";
        private const string _DescendingPostfix = "_desc";
        private const string _HomeDepartmentColumnName = "HOME_DEPARTMENT";
        private const string _NameColumnName = "NAME";
        private const string _LastSemesterTaughtName = "LastSemesterTaught";
        private readonly IQueryable<TEACHER> _teachers;
        private readonly IQueryable<DEPARTMENT> _departments;
        private readonly IQueryable<TEACHER_TYPE> _facultyTypes;
        private readonly IQueryable<TEACHER_COURSE_RECORD> _teacherCourseRecords;
        private readonly IQueryable<COURSE> _courses;
        private readonly FTCSEntities _context;
        private readonly IQueryable<FACULTY_PROGRAM_COORDINATION> _programCoordination;
        private readonly IQueryable<FACULTY_CONCENTRATION_COORDINATION> _concentrationCoordination;
        private readonly IDepartmentService _departmentService;
        private readonly IQueryable<GRADUATE_TRAINING> _graduateTraining;
        private bool _disposed;

        public static string DefaultSortOrder = _NameColumnName + _AscendingPostfix;


        //Constructors

        //Generic Constructor
        //Creates the FTCS DB _context and the Generic Repo for Teacher
        public FacultyService(FTCSEntities db)
        {
            _context = db;
            _teachers = _context.TEACHERs;
            _departments = _context.DEPARTMENTs;
            _facultyTypes = _context.TEACHER_TYPE;
            _teacherCourseRecords = _context.TEACHER_COURSE_RECORD;
            _courses = _context.COURSEs;
            _programCoordination = _context.FACULTY_PROGRAM_COORDINATION;
            _concentrationCoordination = _context.FACULTY_CONCENTRATION_COORDINATION;
            _graduateTraining = _context.GRADUATE_TRAINING;
            _departmentService = new DepartmentService(_context);


        }
        public FacultyService(FTCSEntities db, IDepartmentService departmentService)
        {
            _context = db;
            _teachers = _context.TEACHERs;
            _departments = _context.DEPARTMENTs;
            _facultyTypes = _context.TEACHER_TYPE;
            _teacherCourseRecords = _context.TEACHER_COURSE_RECORD;
            _courses = _context.COURSEs;
            _programCoordination = _context.FACULTY_PROGRAM_COORDINATION;
            _concentrationCoordination = _context.FACULTY_CONCENTRATION_COORDINATION;
            _departmentService = departmentService;
            _graduateTraining = _context.GRADUATE_TRAINING;
        }

        public FacultyService()
        {
            _context = new FTCSEntities();
            _facultyTypes = _context.TEACHER_TYPE;
            _teachers = _context.TEACHERs;
            _teacherCourseRecords = _context.TEACHER_COURSE_RECORD;
            _courses = _context.COURSEs;
            _departments = _context.DEPARTMENTs;
            _programCoordination = _context.FACULTY_PROGRAM_COORDINATION;
            _concentrationCoordination = _context.FACULTY_CONCENTRATION_COORDINATION;
            _departmentService = new DepartmentService(_context);
            _graduateTraining = _context.GRADUATE_TRAINING;

        }

        //Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                    _departmentService.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Get a list of FacultyVM objects that are all distinct faculty members
        /// </summary>
        /// <param name="letter">First letter of faculty last name</param>
        /// <param name="search">string to try to find in faculty name, first or last</param>
        /// <param name="sort">column name to order results by</param>
        /// <param name="departmentCode">department faculty are in</param>
        /// <returns>A list of FaculyVMs</returns>
        public IEnumerable<FacultyVM> GetListOfFaculty(string letter, string search, string sort, string departmentCode)
        {
            var currentAcademicYear = AcademicYear.GetCurrentAcademicYear().Select(ay => ay.Month + "-" + ay.Year);
            // get the teacher and department repositories
            var facultyWithDepartment = _teacherCourseRecords.Join(_courses,
                    tcr => new {RUBRIC = tcr.COURSE_RUBRIC, NUMBER = tcr.COURSE_NUMBER},
                    c => new {RUBRIC = c.RUBRIC, NUMBER = c.NUMBER},
                    (tcr, c) => new
                    {
                        TeacherCourseRecord = tcr,
                        Course = c
                    })
                .Join(_departments, tcrd => new {DepartmentCode = tcrd.Course.DEPARTMENT},
                    d => new {DepartmentCode = d.CODE}, (tcrd, d) => new
                    {
                        BANNER_ID = tcrd.TeacherCourseRecord.TEACHER.BANNER_ID,
                        FIRSTNAME = tcrd.TeacherCourseRecord.TEACHER.FIRST_NAME,
                        LASTNAME = tcrd.TeacherCourseRecord.TEACHER.LAST_NAME,
                        NAME =
                        tcrd.TeacherCourseRecord.TEACHER.LAST_NAME + ", " + tcrd.TeacherCourseRecord.TEACHER.FIRST_NAME +
                        " " + tcrd.TeacherCourseRecord.TEACHER.MIDDLE_INITIAL,
                        DEPARTMENT = d.NAME,
                        DEPARTMENT_CODE = d.CODE,
                        COLLEGE = d.COLLEGE,
                        EMAIL = tcrd.TeacherCourseRecord.TEACHER.EMAIL_ADDRESS,
                        TEACHER_TYPE = tcrd.TeacherCourseRecord.TEACHER.TEACHER_TYPE1.NAME,
                        TITLE = tcrd.TeacherCourseRecord.TEACHER.TITLE,
                        ACADEMIC_YEAR = tcrd.TeacherCourseRecord.ACADEMIC_YEAR,
                        ACADEMIC_MONTH = tcrd.TeacherCourseRecord.ACADEMIC_MONTH
                    }).Where(tcrd =>
                    currentAcademicYear.Contains(tcrd.ACADEMIC_MONTH + "-" + tcrd.ACADEMIC_YEAR)
                    && !DepartmentService.CodeExclusions.Contains(tcrd.COLLEGE))
                .GroupBy(f => new {f.BANNER_ID, f.NAME, f.DEPARTMENT_CODE, f.DEPARTMENT,f.COLLEGE,f.TEACHER_TYPE})
                .Select(group => group.Key);

            var facultyMembers = facultyWithDepartment;

            if (letter != null && letter != "All" && letter != "")
            {
                facultyMembers = facultyMembers.Where(fr => fr.NAME.StartsWith(letter.ToUpper()));
            }
            if (!string.IsNullOrEmpty(search))
            {
                facultyMembers = facultyMembers.Where(fr => fr.NAME.Contains(search));
            }

            if (!string.IsNullOrEmpty(departmentCode))
            {
                facultyMembers = facultyMembers.Where(f => f.DEPARTMENT_CODE == departmentCode);
            }
            if (sort.Contains(_AscendingPostfix))
            {
                var sortColumn = sort.Replace(_AscendingPostfix, "");
                switch (sortColumn)
                {
                    case _HomeDepartmentColumnName:
                        facultyMembers = facultyMembers.OrderBy(f => f.DEPARTMENT);
                        break;
                    case _NameColumnName:
                        facultyMembers = facultyMembers.OrderBy(f => f.NAME);
                        break;
                    default:
                        facultyMembers = facultyMembers.OrderBy(f => f.NAME);
                        break;
                }
            }
            else if (sort.Contains(_DescendingPostfix))
            {
                var sortColumn = sort.Replace(_DescendingPostfix, "");
                switch (sortColumn)
                {
                    case _HomeDepartmentColumnName:
                        facultyMembers = facultyMembers.OrderByDescending(f => f.DEPARTMENT);
                        break;
                    case _NameColumnName:
                        facultyMembers = facultyMembers.OrderByDescending(f => f.NAME);
                        break;
                    default:
                        facultyMembers = facultyMembers.OrderByDescending(f => f.NAME);
                        break;
                }
            }

            var facultyResults = facultyMembers.Select(lof => new FacultyVM()
            {
                BANNER_ID = lof.BANNER_ID,
                //FIRST_NAME = lof.FIRSTNAME,
                //LAST_NAME = lof.LASTNAME,
                NAME = lof.NAME,
                //Title = lof.TITLE,
                HOME_DEPARTMENT = lof.DEPARTMENT,
                DepartmentCode = lof.DEPARTMENT_CODE,
                College = lof.COLLEGE,
                //Email = lof.EMAIL.ToLower(),
                EmploymentStatus = lof.TEACHER_TYPE
            });

            return facultyResults.ToList();
        }

        /// <summary>
        /// Gets the type of faculty i.e. GA, Part-time, Full-Time, Other
        /// </summary>
        /// <returns>A list of the various types of faculty</returns>
        public IEnumerable<FacultyTypeVM> GetFacultyTypes()
        {
            var result =
                _facultyTypes.Where(ft => ft.ID != 3).Select(ft => new {Id = ft.ID, TypeName = ft.NAME}).ToList();
            var facultyTypeVMs = result.Select(ft => new FacultyTypeVM(ft.Id, ft.TypeName)).ToList();
            facultyTypeVMs.Add(new FacultyTypeVM(4, "All"));
            return facultyTypeVMs;
        }

        public List<SelectListItem> GetListOfFacultySelectListItems(string sortOrder, string departmentCode)
        {
            return
                GetListOfFaculty(string.Empty, string.Empty, sortOrder, departmentCode).Select(f => new SelectListItem()
                    {Text = f.NAME, Value = f.BANNER_ID}).ToList();
        }

        /// <summary>
        /// Gets the previous and next faculty member in alphabetical order for use in site navigation
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently ENumber</param>
        /// <param name="department">Department currently being viewed</param>
        /// <returns>List of FacultyVMs comprised of previous, current, and next faculty</returns>
        public List<FacultyVM> GetPreviousAndNextFaculty(string id, string department)
        {
            FacultyVM currentFaculty = null;
            FacultyVM previousFaculty = null;
            FacultyVM nextFaculty = null;

            var facultyList =
                GetListOfFaculty(string.Empty, string.Empty, String.Empty, department).OrderBy(f => f.NAME).ToList();

            var currentFacultyNum = GetIndexOfFacultyInList(id, facultyList);


            if (currentFacultyNum == -1 || currentFacultyNum == 0)
            {
                currentFacultyNum = 0;
                currentFaculty = facultyList[currentFacultyNum];
                nextFaculty = facultyList[currentFacultyNum + 1];
            }
            else if (currentFacultyNum == facultyList.Count - 1)
            {
                currentFaculty = facultyList[currentFacultyNum];
                previousFaculty = facultyList[currentFacultyNum - 1];
            }
            else
            {
                previousFaculty = facultyList[currentFacultyNum - 1];
                currentFaculty = facultyList[currentFacultyNum];
                nextFaculty = facultyList[currentFacultyNum + 1];
            }
            var titleInfo = _teachers.First(f => f.BANNER_ID == id);
            currentFaculty.Title = titleInfo.TITLE;
            currentFaculty.Email = titleInfo.EMAIL_ADDRESS.ToLower();

            return new List<FacultyVM>() {previousFaculty, currentFaculty, nextFaculty};
        }

        /// <summary>
        /// Checks to see if the given faculty is a Coordinator
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently ENumber</param>
        /// <returns>bool</returns>
        public bool IsFacultyCoordinator(string id)
        {
            return _programCoordination.Any(pc => pc.FACULTY_ID == id) ||
                   _concentrationCoordination.Any(cc => cc.FACULTY_ID == id);
        }

        /// <summary>
        /// Checks to see if the given faculty has professional qualifications
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently ENumber</param>
        /// <returns>bool</returns>
        public bool HasProfessionalQualifications(string id)
        {
            return
                _teachers.Any(t => t.BANNER_ID == id && (t.NARRATIVE.NARRATIVE1 != null || t.RELATED_WORK_EXPERIENCE.Any()));
        }

        /// <summary>
        /// Does the faculty ID exist in the system
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently ENumber</param>
        /// <returns>bool</returns>
        public bool IsFacultyIdValid(string id)
        {
            return _teachers.Any(t => t.BANNER_ID == id);
        }

        /// <summary>
        /// Finds the index for a faculty in a list of faculty members
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently ENumber</param>
        /// <param name="list">List of FacultyVMs</param>
        /// <returns>int index position of faculty</returns>
        public int GetIndexOfFacultyInList(string id, IEnumerable<FacultyVM> list)
        {
            return list.ToList().FindIndex(x => x.BANNER_ID == id);
        }

        /// <summary>
        /// Gets faculty member from ID 
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently ENumber</param>
        /// <returns>FacultyVM of faculty whose ID was passed in</returns>
        public FacultyVM GetFacultyById(string id)
        {
            var teacher = GetListOfFaculty(string.Empty,string.Empty,string.Empty,string.Empty)
                .DefaultIfEmpty(new FacultyVM())
                .FirstOrDefault(f => f.BANNER_ID == id);
            return teacher;
        }

        /// <summary>
        /// Gets department of faculty member from faculty ID
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently enumber</param>
        /// <returns>String of department code corresponding to faculty member searched</returns>
        public string GetDepartmentOfFacultyById(string id)
        {
            var faculty = GetFacultyById(id);
            return faculty?.DepartmentCode;
        }


        public FacultyVM FirstFacultyOnInvalidParameters(string id, string department)
        {
            //returns null if the faculty is in the department specified
            var facultyDepartment = GetDepartmentOfFacultyById(id);
            if (id != null && facultyDepartment == department) return null;
            var firstFaculty =
                GetListOfFaculty(string.Empty, string.Empty, _NameColumnName + _AscendingPostfix, department)
                    .First();
            var faculty = new FacultyVM() { BANNER_ID = firstFaculty.BANNER_ID, DepartmentCode = firstFaculty.DepartmentCode };
            return faculty;
        }

        /// <summary>
        /// Gets the academic qualifications of a faculty member
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently enumber</param>
        /// <param name="department">department faculty belongs to</param>
        /// <returns>AcademicQualificationVM of given faculty</returns>
        public AcademicQualificationsVM GetAcademicOfFaculty(string id, string department)
        {
            var teachers = _teachers;
            var teacherAcademic = teachers.Where(t => t.BANNER_ID == id).ToList();
            var academicResults = teacherAcademic.Select(lof => new AcademicQualificationsVM(
                id,
                department,
                lof.FACULTY,
                lof.EMPLOYMENT_TYPE,
                lof.CATEGORY,
                lof.YEARS_TEACHING,
                lof.ACADEMIC_DEGREE.OrderByDescending(a => a.LEVEL).Select(b => new DegreeRecordVM
                {
                    Level = b.LEVEL,
                    Name = b.NAME,
                    Initial = b.INITIALS,
                    Discipline = b.DISCIPLINE,
                    Institution = b.INSTITUTION,
                    Transcript = b.HAS_TRANSCRIPT == 1 ? "Yes" : "No"
                }),
                lof.GRADUATE_TRAINING.Select(d => new GraduateHourVM
                {
                    GraduateDispline = d.DISCIPLINE,
                    GraduateHour = d.SEMESTER_HOURS,
                    TeacherId = d.TEACHER_ID
                })));
            return academicResults.FirstOrDefault();
        }

        /// <summary>
        /// Gets the academic qualifications of a faculty member without needing department
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently enumber</param>
        /// <returns>AcademicQualificationVM of given faculty</returns>
        public AcademicQualificationsVM GetAcademicOfFaculty(string id)
        {
            return GetAcademicOfFaculty(id, "");
        }

        /// <summary>
        /// Get's the graduate hours and discipline of a faculty
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently Enumber</param>
        /// <returns>GraduateHourVM of faculty</returns>
        public IEnumerable<GraduateHourVM> GetFacultyGraduateHours(string id)
        {
            var results = _teachers.First(t => t.BANNER_ID == id).GRADUATE_TRAINING.Select(d => new GraduateHourVM
            {
                GraduateDispline = d.DISCIPLINE,
                GraduateHour = d.SEMESTER_HOURS,
                TeacherId = d.TEACHER_ID
            });
            return results;

        }

        /// <summary>
        /// Gets professional qualifications of a given faculty
        /// </summary>
        /// <param name="id">Unique ID of Faculty, Currently Enumber</param>
        /// <param name="department">Department of faculty</param>
        /// <returns>ProfessionalQualificationsVM for given faculty</returns>
        public ProfessionalQualificationsVM GetProfessionalOfFaculty(string id, string department)
        {
            var teacher = _teachers.Where(t => t.BANNER_ID == id). DefaultIfEmpty().FirstOrDefault();
            var qualifications = new ProfessionalQualificationsVM
            {
                Id = id,
                Department = department,
                Name = teacher.LAST_NAME + ", " + teacher.FIRST_NAME + " "+ teacher.MIDDLE_INITIAL,
                Narrative = teacher.NARRATIVE?.NARRATIVE1,
                DocumentedExcellenceInTeaching = teacher.DOCUMENTED_EXCELLENCE_IN_TEACHING?.DOCUMENTED_EXCELLENCE_IN_TEACHING1,
                PublicationCitation = teacher.PUBLICATION_CITATION?.PUBLICATION_CITATION1,
                OtherAchievements = teacher.OTHER_ACHIEVEMENTS?.OTHER_ACHIEVEMENTS1,
                OtherDemonstratedCompetencies = teacher.OTHER_DEMONSTRATED_COMPETENCIES?.OTHER_DEMONSTRATED_COMPETENCIES1
            };
            qualifications.AddRelatedWorkExperience(teacher.RELATED_WORK_EXPERIENCE);
            qualifications.AddLicenses(teacher.LICENSEs);
            qualifications.AddAwards(teacher.AWARDs);
            qualifications.AddCertifications(teacher.CERTIFICATIONs);
            qualifications.AddPresentedPapers(teacher.PRESENTED_PAPER);
            qualifications.AddCertificates(teacher.CERTIFICATEs);
            return qualifications;
        }

       
        /// <summary>
        /// Gets list of faculty that have taught, or are teaching, the given course
        /// </summary>
        /// <param name="rubric">Course Rubric</param>
        /// <param name="number">Course Number</param>
        /// <param name="sort">Column to sort results by</param>
        /// <returns>A List of FacultyCourseVM of faculty teaching the course</returns>
        public IEnumerable<FacultyCourseVM> GetListOfFacultyTeachingCourse(string rubric, string number, string sort)
        {
            var result = _teachers.Where(t =>
                    t.TEACHER_COURSE_RECORD.Any(tcr =>
                        tcr.COURSE_NUMBER == number &&
                        tcr.COURSE_RUBRIC == rubric))
                .SelectMany(
                    t => t.TEACHER_COURSE_RECORD.OrderByDescending(tcr => tcr.ACADEMIC_YEAR).ThenByDescending(tcr => tcr.ACADEMIC_MONTH).GroupBy(tcr => tcr.TEACHER_ID).Select(grp => grp.FirstOrDefault()),
                    (teacher, record) =>
                        new FacultyCourseVM()
                        {
                            BANNER_ID = teacher.BANNER_ID,
                            NAME = teacher.LAST_NAME + ", " + teacher.FIRST_NAME + " " + teacher.MIDDLE_INITIAL,
                            HOME_DEPARTMENT = record.COURSE.DEPARTMENT1.NAME,
                            DepartmentCode = record.COURSE.DEPARTMENT1.CODE,
                            LastSemesterTaught = new Semester() { Month = record.ACADEMIC_MONTH, Year = record.ACADEMIC_YEAR }
                        });

            if (sort.Contains(_AscendingPostfix))
            {
                var sortColumn = sort.Replace(_AscendingPostfix, "");
                switch (sortColumn)
                {
                    case _HomeDepartmentColumnName:
                        result = result.OrderBy(f => f.HOME_DEPARTMENT);
                        break;
                    case _NameColumnName:
                        result = result.OrderBy(f => f.NAME);
                        break;
                    case _LastSemesterTaughtName:
                        result = result.OrderBy(f => f.LastSemesterTaught.Year).ThenBy(f => f.LastSemesterTaught.Month);
                        break;
                    default:
                        result = result.OrderBy(f => f.NAME);
                        break;
                }
            }
            else if (sort.Contains(_DescendingPostfix))
            {
                var sortColumn = sort.Replace(_DescendingPostfix, "");
                switch (sortColumn)
                {
                    case _HomeDepartmentColumnName:
                        result = result.OrderByDescending(f => f.HOME_DEPARTMENT);
                        break;
                    case _NameColumnName:
                        result = result.OrderByDescending(f => f.NAME);
                        break;
                    case _LastSemesterTaughtName:
                        result = result.OrderByDescending(f => f.LastSemesterTaught.Year).ThenByDescending(f => f.LastSemesterTaught.Month);
                        break;
                    default:
                        result = result.OrderByDescending(f => f.NAME);
                        break;
                }
            }

            return result.ToList();
        }

        /// <summary>
        /// Gets the coordination information of a faculty, qualifications, degrees, etc.
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently Enumber</param>
        /// <returns>List of CoordinationInfoVMs for faculty</returns>
        public IEnumerable<CoordinationInfoVM> GetCoordinationOfFaculty(string id)
        {
            var programCoordination = _programCoordination.Where(t => t.FACULTY_ID == id).ToList();
            var concentrationCoordination = _concentrationCoordination.Where(t => t.FACULTY_ID == id).ToList();
            var programCoordinationResults = programCoordination.Select(lof => new CoordinationInfoVM(
                lof.TEACHER.TEACHER_COURSE_QUALIFICATION.Select(qr => qr.QUALIFICATION_TYPE.NAME).FirstOrDefault(),
                lof.START_ACADEMIC_MONTH,
                lof.START_ACADEMIC_YEAR,
                lof.END_ACADEMIC_MONTH,
                lof.END_ACADEMIC_YEAR,
                lof.PROGRAM.DEGREE_NAME.StartsWith("B") ? "Baccalaureate Degree Program" : "Masters Degree Program",
                lof.PROGRAM.DEGREE_NAME,
                lof.PROGRAM.MAJOR_NAME
            ));
            var concentrationCoordinationResults = concentrationCoordination.Select(lof => new CoordinationInfoVM(
                lof.TEACHER.TEACHER_COURSE_QUALIFICATION.Select(qr => qr.QUALIFICATION_TYPE.NAME).FirstOrDefault(),
                lof.START_ACADEMIC_MONTH,
                lof.START_ACADEMIC_YEAR,
                lof.END_ACADEMIC_MONTH,
                lof.END_ACADEMIC_YEAR,
                lof.CONCENTRATION.NAME,
                "N/A",
                "-CONCENTRATION-"
            ));
            var coordination = programCoordinationResults.Concat(concentrationCoordinationResults);
            return coordination.ToList();
        }

        /// <summary>
        /// Removes graduate hours for faculty
        /// </summary>
        /// <param name="teacherId">Unique id of Faculty, Currently Enumber</param>
        /// <param name="displine">Faculty discipline</param>
        /// <returns>Result of the database operation, success or error message</returns>
        public DbOperationVM DeleteGraduateHourOfFaculty(string teacherId, string displine)
        {
            var graduateHourToRemove = _graduateTraining.Where( gt => gt.TEACHER_ID == teacherId && gt.DISCIPLINE == displine).ToList();
            _context.GRADUATE_TRAINING.RemoveRange(graduateHourToRemove);

            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() { IsSuccess = true, Response = "Graduate Hour of Faculty successfully removed" };
            }
            return new DbOperationVM() { IsSuccess = false, Response = "An unexpected error has occured" };
        }

        /// <summary>
        /// Adds graduate hours for faculty
        /// </summary>
        /// <param name="teacherId">Unique id of Faculty, Currently Enumber</param>
        /// <param name="displine">Faculty Discipline</param>
        /// <param name="hours">Number of hours to add</param>
        /// <returns>Result of the database operation, success or error message</returns>
        public DbOperationVM AddGraduateHourOfFaculty(string teacherId, string displine, int hours)
        {
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();
            var addGraduateHour = new GRADUATE_TRAINING()
            {
                TEACHER_ID = teacherId,
                DISCIPLINE = displine,
                SEMESTER_HOURS = hours,
                YEAR = currentSemester.Year,
                MONTH = currentSemester.Month
            };            
            _context.GRADUATE_TRAINING.Add(addGraduateHour);
            if (_context.SaveChanges() > 0)
            {
                return new DbOperationVM() { IsSuccess = true, Response = "Graduate Hour of Faculty successfully added" };
            }
            return new DbOperationVM() { IsSuccess = false, Response = "An unexpected error has occured" };
        }

        /// <summary>
        /// Updates the Graduate hours for faculty member
        /// </summary>
        /// <param name="teacherId">Unique ID of faculty, currently Enumber</param>
        /// <param name="displine">Faculty Discipline</param>
        /// <param name="hours">Updated value for graduate hours</param>
        /// <returns>result of the database operation, success or error message</returns>
        public DbOperationVM UpdateGraduteHourOfFaculty(string teacherId, string displine, int hours)
        {
            var currentSemester = AcademicYear.GetCurrentAcademicSemester();
            var graduateHourUpdate = _graduateTraining.FirstOrDefault(gt => gt.TEACHER_ID == teacherId && gt.DISCIPLINE == displine);
            try
            {
                if (graduateHourUpdate == null)
                {
                    var graduateTraining = new GRADUATE_TRAINING()
                    {
                        DISCIPLINE = displine,
                        TEACHER_ID = teacherId,
                        SEMESTER_HOURS = hours,
                        MONTH = currentSemester.Month,
                        YEAR = currentSemester.Year
                    };
                    _context.GRADUATE_TRAINING.Add(graduateTraining);
                }
                else
                {
                    graduateHourUpdate.DISCIPLINE = displine;
                    graduateHourUpdate.SEMESTER_HOURS = hours;
                }
                _context.SaveChanges();
                    return new DbOperationVM() { IsSuccess = true, Response = "Graduate Hour of Faculty successfully updated" };
                
                
            }
            catch (Exception)
            {
                return new DbOperationVM(false);
            }
           
        }

        /// <summary>
        /// Gets the graduate hours for a faculty member
        /// </summary>
        /// <param name="teacherId">Unique Id of faculty, currently enumber</param>
        /// <param name="displine">faculty discipline</param>
        /// <returns>list of GraduateHourVMs for faculty</returns>
        public IEnumerable<GraduateHourVM>  GetGraduateHour(string teacherId, string displine)
        {
            var results = _graduateTraining 
               .Where(gt => gt.TEACHER_ID == teacherId && gt.DISCIPLINE == displine)               
               .Select(gt => new GraduateHourVM
               {
                   TeacherId = gt.TEACHER_ID,
                   GraduateDispline = gt.DISCIPLINE,
                   GraduateHour = gt.SEMESTER_HOURS
               });
            return results.ToList();
        }
        
        /// <summary>
        /// Gets the first first graduate Hour for a faculty member
        /// </summary>
        /// <param name="id">Unique Id of faculty, currently enumber</param>
        /// <param name="discipline">faculty discipline</param>
        /// <returns>GraduateHourVM for faculty</returns>
        public GraduateHourVM GetSingleGraduateHour(string id, string discipline)
        {
            return GetGraduateHour(id, discipline).First();
        }       


        public IEnumerable<GraduateHourVM> GetGraduateHourList (string id, string discipline, int hours)
        {
            IEnumerable<GraduateHourVM> graduateHourList = GetGraduateHour(id, discipline);
            GraduateHourVM graduateHours = new GraduateHourVM
            {
                GraduateDispline = discipline,
                GraduateHour = hours
            };
            graduateHourList.ToList().Add(graduateHours);
            graduateHourList.OrderBy(m => m.GraduateDispline);

            return graduateHourList;
        }

    }
}