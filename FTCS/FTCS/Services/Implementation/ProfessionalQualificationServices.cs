﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FTCS.Models;
using FTCS.ViewModels;
using FTCS.Services.Interfaces;
using FTCS.Services.Repositories;

namespace FTCS.Services.Implementation
{
    public class ProfessionalQualificationServices : IProfessionalQualificationService
    {
        private readonly IGenericRepository<TEACHER> _teacherRepository;
        private readonly FTCSEntities context;

        private bool disposed = false;

        //Constructors

        //Generic Constructor
        //Creates the FTCS DB context and the Generic Repo for Teacher

        public ProfessionalQualificationServices(FTCSEntities db,
                                IGenericRepository<TEACHER> teacher
                                )
        {
            context = db;
            _teacherRepository = teacher;
        }
        public ProfessionalQualificationServices()
        {
            context = new FTCSEntities();
            _teacherRepository = new GenericRepository<TEACHER>(context);

        }

        //Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        //Faculty Services

        //Get a list of FacultyVM objects that are all distinct faculty members
       

        public IEnumerable<ProfessionalQualificationVM> GetListOfProfessionalQualifications(string id)
        {
            var teachers = _teacherRepository.Get(null).Cast<TEACHER>().Where(t => t.BANNER_ID == id);
            var results = teachers.Select(t => new ProfessionalQualificationVM
            {
                Narrative = t.NARRATIVE.NARRATIVE1,
                RelatedWorkExperience = t.RELATED_WORK_EXPERIENCE,
                License = t.LICENSEs,
                Certificate = t.CERTIFICATEs,
                Certification = t.CERTIFICATIONs,
                DocumentedExcellenceInTeaching = t.DOCUMENTED_EXCELLENCE_IN_TEACHING.DOCUMENTED_EXCELLENCE_IN_TEACHING1,
                PresentedPaper = t.PRESENTED_PAPER,
                PublicationCitation = t.PUBLICATION_CITATION.PUBLICATION_CITATION1,
                OtherAchievements = t.OTHER_ACHIEVEMENTS.OTHER_ACHIEVEMENTS1,
                Award = t.AWARDs,
                OtherDemonstratedCompetencies = t.OTHER_DEMONSTRATED_COMPETENCIES.OTHER_DEMONSTRATED_COMPETENCIES1
            });
            return results.ToList();
        }

    }
}