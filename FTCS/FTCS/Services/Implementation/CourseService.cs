﻿using FTCS.Models;
using FTCS.Services.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using FTCS.ViewModels.Course;

namespace FTCS.Services.Implementation
{

    public class CourseService : ICourseService
    {
        const string AscendingPostfix = "_asc";
        const string DescendingPostfix = "_desc";
        const string CourseCode = "CourseCode";
        const string Name = "Name";
        const string Department = "Department";
        private readonly IQueryable<DEPARTMENT> _department;
        private readonly IQueryable<COURSE> _course;
        private readonly IQueryable<TEACHER_COURSE_RECORD> _courseRecord;
        private readonly IQueryable<QUALIFICATION_TYPE> _qualificationType;
        private readonly IQueryable<TEACHER_COURSE_QUALIFICATION> _teacherCourseQualification;
        private readonly FTCSEntities _context;

        private bool disposed = false;

        public CourseService(FTCSEntities db)
        {
            _context = db;
            _course = db.COURSEs;
            _department = db.DEPARTMENTs;
            _courseRecord = db.TEACHER_COURSE_RECORD;
            _qualificationType = db.QUALIFICATION_TYPE;
            _teacherCourseQualification = db.TEACHER_COURSE_QUALIFICATION;
        }

        public CourseService()
        {
            _context = new FTCSEntities();
            _department = _context.DEPARTMENTs;
            _course = _context.COURSEs;            
            _courseRecord = _context.TEACHER_COURSE_RECORD;
            _qualificationType = _context.QUALIFICATION_TYPE;
            _teacherCourseQualification = _context.TEACHER_COURSE_QUALIFICATION;
        }

        /// <summary>
        /// Gets a list of all courses ever taught
        /// </summary>
        /// <param name="filter">First letter a course starts with</param>
        /// <param name="sort">Which column to sort on</param>
        /// <param name="search">Text a course must contain</param>
        /// <returns>List of CourseVMs satisfying the parameter conditions</returns>
        public IEnumerable<CourseVM> GetListOfCourses(string filter, string sort, string search)
        {


            var distinctCoursesWithDepartment = _course.Join(
                _department,
                c => new { Department = c.DEPARTMENT },
                d => new { Department = d.CODE },
                (Course, department) => new
                {
                    RUBRIC = Course.RUBRIC,
                    NUMBER = Course.NUMBER,
                    DEPARTMENT = department.NAME,
                    NAME = Course.NAME
                });

            var courseResults = distinctCoursesWithDepartment.Select(loc => new CourseVM
            {
                Name = loc.NAME,
                Department = loc.DEPARTMENT,
                CourseCode = loc.RUBRIC + " " + loc.NUMBER,
                Rubric = loc.RUBRIC,
                Number = loc.NUMBER

            });

            if (filter != "All" && filter != null)
            {
                courseResults = courseResults.Where(cr => cr.Name.StartsWith(filter));
            }
            if (search != null && search != "")
            {
                courseResults = courseResults.Where(cr => cr.Name.Contains(search) || cr.CourseCode.Contains(search));
            }

            if (sort.Contains(AscendingPostfix))
            {
                var sortColumn = sort.Replace(AscendingPostfix, "");
                switch (sortColumn)
                {

                    case Department:
                        courseResults = courseResults.OrderBy(d => d.Department);
                        break;
                    case CourseCode:
                        courseResults = courseResults.OrderBy(d => d.CourseCode);
                        break;
                    default:
                        courseResults = courseResults.OrderBy(d => d.Name);
                        break;
                }
            }
            else if (sort.Contains(DescendingPostfix))
            {
                var sortColumn = sort.Replace(DescendingPostfix, "");
                switch (sortColumn)
                {
                    case Department:
                        courseResults = courseResults.OrderByDescending(d => d.Department);
                        break;
                    case CourseCode:
                        courseResults = courseResults.OrderByDescending(d => d.CourseCode);
                        break;
                    default:
                        courseResults = courseResults.OrderByDescending(d => d.Name);
                        break;
                }
            }
            return courseResults.ToList();
        }

        /// <summary>
        /// Gets the course history ordering by year and month
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently Enumber</param>
        /// <param name="courseRubric">The rubric for the course</param>
        /// <param name="courseNumber">The number for the course</param>
        /// <param name="year">The year to show results for, or all</param>
        /// <returns>List of CourseCredentialsVM satisfying the ID, CourseRubric, CourseNumber, and Year</returns>
        public List<CourseCredentialVM> GetCourseHistory(string id, string courseRubric, string courseNumber, string year)
        {

            IEnumerable<CourseCredentialVM> courseResults =
                _context.TEACHER_COURSE_RECORD
                    .Join(_context.TEACHER_COURSE_QUALIFICATION,
                        record =>
                            new
                            {
                                TeacherId = record.TEACHER_ID,
                                Rubric = record.COURSE_RUBRIC,
                                Number = record.COURSE_NUMBER
                            },
                        qualification =>
                            new
                            {
                                TeacherId = qualification.TEACHER_ID,
                                Rubric = qualification.COURSE_RUBRIC,
                                Number = qualification.COURSE_NUMBER
                            },
                        (record, qualification) =>
                            new CourseCredentialVM()
                            {
                                TeacherId = record.TEACHER_ID,
                                Rubric = record.COURSE_RUBRIC,
                                Number = record.COURSE_NUMBER,
                                AcademicYear = record.ACADEMIC_YEAR,
                                SemesterTaught = new Semester() {Month = record.ACADEMIC_MONTH, Year = record.ACADEMIC_YEAR},
                                CourseCode = record.COURSE_RUBRIC + " " + record.COURSE_NUMBER,
                                Name = record.COURSE.NAME,
                                Description = record.COURSE.DESCRIPTION,
                                Credential = new Qualification() { Id = qualification.HOW_QUALIFIED}
                            }).Where(cc => cc.TeacherId == id && cc.Rubric == courseRubric && cc.Number == courseNumber)
                            .OrderByDescending(cc => cc.AcademicYear)
                            .ThenBy(cc => cc.SemesterTaught.Month);
            if (year == "Current")
            {
                var academicYear = AcademicYear.GetCurrentAcademicYear();
                var listOfMonths = academicYear.Select(ay => ay.Month).ToList();
                var listOfYears = academicYear.Select(ay => ay.Year).ToList();
                var fallMonth = listOfMonths[0];
                var springMonth = listOfMonths[1];
                var summerMonth = listOfMonths[2];
                var fallYear = listOfYears[0];
                var springYear = listOfYears[1];
                var summerYear = listOfYears[2];
                courseResults = courseResults.Where(c =>
                (c.SemesterTaught.Month == fallMonth && c.SemesterTaught.Year == fallYear) ||
                (c.SemesterTaught.Month == springMonth && c.SemesterTaught.Year == springYear) ||
                (c.SemesterTaught.Month == summerMonth && c.SemesterTaught.Year == summerYear)).ToList();
            }
            return courseResults.ToList();
        }

        public IQueryable<CourseCredentialVM> GetAllCourseCredentials()
        {
            var courseResults =
                _context.TEACHER_COURSE_RECORD
                    .Join(_context.TEACHER_COURSE_QUALIFICATION,
                        record =>
                            new
                            {
                                TeacherId = record.TEACHER_ID,
                                Rubric = record.COURSE_RUBRIC,
                                Number = record.COURSE_NUMBER
                            },
                        qualification =>
                            new
                            {
                                TeacherId = qualification.TEACHER_ID,
                                Rubric = qualification.COURSE_RUBRIC,
                                Number = qualification.COURSE_NUMBER
                            },
                        (record, qualification) =>
                            new CourseCredentialVM()
                            {
                                TeacherId = record.TEACHER_ID,
                                Rubric = record.COURSE_RUBRIC,
                                Number = record.COURSE_NUMBER,
                                AcademicYear = record.ACADEMIC_YEAR,
                                SemesterTaught =
                                    new Semester() {Month = record.ACADEMIC_MONTH, Year = record.ACADEMIC_YEAR},
                                CourseCode = record.COURSE_RUBRIC + " " + record.COURSE_NUMBER,
                                Name = record.COURSE.NAME,
                                Description = record.COURSE.DESCRIPTION,
                                Credential = new Qualification() {Id = qualification.HOW_QUALIFIED}
                            });
            return courseResults;
        }

        /// <summary>
        /// Gets list of courses taught by a teacher
        /// </summary>
        /// <param name="id">Unique ID of faculty, currently Enumber</param>
        /// <param name="year">The year to show results for, or all</param>
        /// <returns>List of CourseVMs with all courses taught by a teacher in a given year</returns>
        public List<CourseCredentialVM> GetListOfCoursesByTeacher(string id, string year)
        {
            if (year == "Current")
            {
                var listOfSemesters = AcademicYear.GetCurrentAcademicYear().Select(ay => ay.Month + " " + ay.Year);
                var courseResults =
                    GetAllCourseCredentials()
                        .Where(
                            cc =>
                                cc.TeacherId == id &&
                                listOfSemesters.Contains(cc.SemesterTaught.Month + " " + cc.SemesterTaught.Year))
                        .OrderByDescending(cc => cc.SemesterTaught.Year)
                        .ThenByDescending(cc => cc.SemesterTaught.Month);
                return courseResults.ToList();
            }
            else
            {
                var courseResults = GetAllCourseCredentials()
                    .Where(
                        cc =>
                                cc.TeacherId == id)
                    .OrderByDescending(cc => cc.SemesterTaught.Year)
                    .ThenByDescending(cc => cc.SemesterTaught.Month);
                return courseResults.ToList();
            }
        }
        /// <summary>
        /// Gets the academic semester from integer month
        /// </summary>
        /// <param name="month">What month, 1-12</param>
        /// <returns>string containing the academic semester or "" if month less than 1 or greater than 12</returns>
        public string GetSemester(int month)
        {
            switch (month)
            { 
                case 1:
                    return "Spring";
                case 5:
                    return "Summer";
                case 8:
                    return "Fall";
                default:
                    return "";
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

       /// <summary>
       /// Gets a courses name from rubric and number
       /// </summary>
       /// <param name="rubric">the rubric corresponding to the course</param>
       /// <param name="number">the number corresponding to the course</param>
       /// <returns>string containing the course name</returns>
        public string GetCourseName(string rubric, string number)
        {
            var courseName = _course.Where(c => c.NUMBER == number && c.RUBRIC == rubric).Select(c => c.NAME).ToList();
            
            return courseName[0];
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
