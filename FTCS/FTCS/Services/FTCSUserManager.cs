﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using FTCS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace FTCS.Services
{
    public class FTCSUserManager:UserManager<FTCSUser, string>
    {
        public static string CollegeClaim = "College";
        public static string DepartmentClaim = "Department";
        public FTCSUserManager(IUserStore<FTCSUser, string> store) : base(store)
        {
        }

       

        public static FTCSUserManager Create(IdentityFactoryOptions<FTCSUserManager> options, IOwinContext context)
        {
            var manager = new FTCSUserManager(new FTCSUserStore());
            return manager;
        }

        public override  Task<ClaimsIdentity> CreateIdentityAsync(FTCSUser user, string authenticationType)
        {
            IList<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, user.UserName));
            claims.Add(new Claim(ClaimTypes.NameIdentifier,user.Id));
            claims.Add(new Claim(ClaimTypes.Email, user.Email));
            if (user.Department != null)
            {
                claims.Add(new Claim("Department", user.Department));
            }
            if (user.College != null)
            {
                claims.Add(new Claim("College", user.College));
            }
            
            claims.Add(new Claim(ClaimTypes.Role, user.Roles[0]));
            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            return Task.Factory.StartNew(() => identity);
        }

     
    }

   
}