﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FTCS.Services
{
    public class FTCSUser:IUser<string>
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> Member { get; set; }
        public IList<string> Roles { get; set; }
        public string Department { get; set; }
        public string College { get; set; }
    }
}