﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.Services
{
    public class TeacherType
    {
        public int? Id
        {
            set
            {
                var id = value ?? -1;
                switch (id)
                {
                    case 0:
                        Description = "(GA)";
                        ShortDescription = "(GA)";
                        break;
                    case 1:
                        Description = "(Part-time)";
                        ShortDescription = "(P)";
                        break;
                    case 2:
                        Description = "(Full-time)";
                        ShortDescription = "(F)";
                        break;
                    case 3:
                        Description = "(Other)";
                        ShortDescription = "(O)";
                        break;
                    default:
                        Description = "";
                        break;
                }
            }
        }


        public string Description { get; internal set; }
        public string ShortDescription { get; internal set; }

       
        
    }
}