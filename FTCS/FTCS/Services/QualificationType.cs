﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FTCS.Services
{
    public enum QualificationType
    {
        Academic,
        Professional,
        Unspecified,
        GA
    }
}