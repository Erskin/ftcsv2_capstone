﻿$(function () {
    var searchBoxForm = $("form[data-ftcs-filter-bar-ajax='true']");
    var filterQueries = function () {
        var $form = $(this);

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data:$form.serialize()
        };

        $.ajax(options).done(function (data) {
            var $target = $($form.attr("data-ftcs-target"));
            $target.replaceWith(data);
        });
        return false;
    };

    var alphaFilterButtonClicked = function () {
        var $alphaButton = $(this);
        $("input[data-alpha-filter-ajax='true']").removeClass("active");
        $alphaButton.addClass("active");
        searchBoxForm.find("input[name=firstLetter]").val($alphaButton.val())
        searchBoxForm.submit();
    };

    var navigatePage = function () {
        var paginationButton = $(this);
        var options = {
            url:paginationButton.attr("href"),
            data: searchBoxForm.serialize(),
            type:"get"
        };
        
        $.ajax(options).done(function (data) {
            var $target = $(searchBoxForm.attr("data-ftcs-target"));
            $target.replaceWith(data);
        });
        return false;
    }


    searchBoxForm.submit(filterQueries);
    $("input[data-alpha-filter-ajax='true']").on('click', alphaFilterButtonClicked)
    $(".body-content").on('click','.pagination a', navigatePage)
});