﻿//Success message for updating a certification on incomplete certificaitons
function ajaxSuccessIC() {
    $("#ajax-success-ic").fadeIn(50).fadeOut(50).fadeIn(50).delay(3000).fadeOut();
}
//Success message for changeing a concentration coordinator of a program of study
function ajaxSuccessCON() {
    $("#ajax-success-con").fadeIn(50).fadeOut(50).fadeIn(50).delay(3000).fadeOut();
}
//Success message for changing a program of study coordinator for a program of study
function ajaxSuccessPOS() {
    $("#ajax-success-pos").fadeIn(50).fadeOut(50).fadeIn(50).delay(3000).fadeOut();
}
//Success message for deleting a program of study
function ajaxSuccessDelete() {
    $("#ajax-success-delete").fadeIn(50).fadeOut(50).fadeIn(50).delay(3000).fadeOut();
}

function ajaxSuccess(xhr, status) {
    var ajaxSuccessMessage = $("#ajax-success");
    ajaxSuccessMessage.text(xhr.responseText);
    ajaxSuccessMessage.fadeIn(50).fadeOut(50).fadeIn(50).delay(3000).fadeOut();
}

function ajaxError(xhr, status, error) {
    var ajaxErrorMessage = $("#ajax-error");
    ajaxErrorMessage.text(xhr.responseText);
    ajaxErrorMessage.fadeIn(50).fadeOut(50).fadeIn(50).delay(3000).fadeOut();
}

function loadAllAjaxFunctions() {
    loadIncomplete();
}