﻿$(function () {
    var currentViewInput = "#CurrentView,#currentView";
    var previousFacultySelector = ".prev-faculty";
    var nextFacultySelector = ".next-faculty";
    function editNavigationHref(value) {
        var prevHref = '';
        var prev = $(previousFacultySelector);
        if (prev.length) {
            prevHref = prev.attr('href').split('?');
        }
        var nextHref = '';
        var next = $(nextFacultySelector);
        if (next.length) {
            nextHref = $(nextFacultySelector).attr('href').split('?');
        }
        var param = "?CurrentView=" + value;
        $(previousFacultySelector).attr('href', prevHref[0] + param);
        $(nextFacultySelector).attr('href', nextHref[0] + param);
    }


    $("#academic-qualifications-button").click(function () {
        var view = 'Academic';
        $(currentViewInput).val(view);
        editNavigationHref(view);
    });

    $("#profile-qualifications-button").click(function () {
        var view = 'Professional';
        $(currentViewInput).val(view);
        editNavigationHref(view);
    });

    $("#courses-taught-button").click(function () {
        var view = 'CurrentCourses';
        $(currentViewInput).val(view);
        editNavigationHref(view);
    });

    $("#current-courses-button").click(function () {
        var view = 'CurrentCourses';
        $(currentViewInput).val(view);
        editNavigationHref(view);
    });

    $("#all-courses-button").click(function () {
        var view = 'AllCourses';
        $(currentViewInput).val(view);
        editNavigationHref(view);
    });

    $("#coordinator-information-button").click(function () {
        var view = 'Coordinator';
        $(currentViewInput).val(view);
        editNavigationHref(view);
    });

    $(document).ready(function () {
        var action = $('#faculty-profile-form').attr('action', '/Faculty/FacultyProfile');

    });
});