﻿var acc = document.getElementsByClassName("menu-button");

for (var i = 0; i < acc.length; i++) {
    acc[i].onclick = function () {

        var buttons = document.getElementsByClassName("menu-button");
        var panels = document.getElementsByClassName("panel");
        var trianglesOut = document.getElementsByClassName("triangle-outer");
        var trianglesIn = document.getElementsByClassName("triangle-inner");

        for (var n = 0; n < buttons.length; n++) {
            if (!this.compareDocumentPosition(buttons[n])) {
                buttons[n].classList.add("active");
                trianglesIn[n].style.display = "block";
                trianglesOut[n].style.display = "block";
                panels[n].style.display = "block";

            } else {
                buttons[n].classList.remove("active");
                trianglesOut[n].style.display = "none";
                trianglesIn[n].style.display = "none";
                panels[n].style.display = "none";
            }
        }
    }
}