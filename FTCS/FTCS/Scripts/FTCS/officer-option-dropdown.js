﻿function officerMenuOpenClose() {
    document.getElementById("officer-dropdown-content").classList.add("show");
    document.getElementsByClassName("officer-option-button")[0].classList.add("officer-option-button-active");
}
window.onclick = function (event) {
    if (!event.target.matches(".officer-option-button")) {
        document.getElementById("officer-dropdown-content").classList.remove("show");
        document.getElementsByClassName("officer-option-button")[0].classList.remove("officer-option-button-active");
    }
}