﻿google.charts.load("current", { packages: ["corechart", 'table'] });
google.charts.setOnLoadCallback(drawFacultyCredentails);

function drawFacultyCredentails() {

    var url = "reports/credentials";

    $.getJSON(url, function (json) {

        var data = google.visualization.arrayToDataTable([
        ['', ''],
        ['Academic', json.Totals.TotalAcademicNumber],
        ['Professional', json.Totals.TotalProfessionallyQualified],
        ['GA',json.Totals.TotalGraduateAssistants],
        ['Unspecified', json.Totals.TotalUnspecified]]);

        var options = {
            title: 'Faculty Credentials',
            titleTextStyle: { fontSize: 20, bold: false },
            legend: { position: 'bottom', textStyle: { fontSize: 16 } },
            pieHole: 0.5,
            slices: [{ color: '#4EDB93', textStyle: { color: 'white' } }, { color: '#39A06B', textStyle: { color: 'white' } }, { color: '#5F5AA2' , textStyle:{color:'white'}}, { color: '#F55541', textStyle: { color: 'white' } }]
        };

        var chart = new google.visualization.PieChart(document.getElementById('faculty-credentails-chart'));
        chart.draw(data, options);
    });
}
