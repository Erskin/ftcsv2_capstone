﻿function loadIncomplete(){
    $("form.auto-submit").find("select").change(function() {
        var form = $(this).parent("form").get(0);
        if (!form) {
            form = $(this).parents("form").get(0);
        }
        $(form).submit();
    });
}

$(document).ready(function() {
    loadIncomplete();
})