﻿var acc = document.getElementsByClassName("program-of-study-button");
for (var i = 0; i < acc.length; i++) {
    acc[i].onclick = function () {
        var parent = this.parentNode;
        var panel = parent.nextElementSibling;
        var chevron = parent.getElementsByClassName("chevron");
        var badge = parent.getElementsByClassName("badge");
        if (parent.classList.contains("closed")) {
            this.classList.add("open");
            this.classList.remove("closed");
            parent.classList.add("open");
            parent.classList.remove("closed");
            panel.classList.add("open");
            panel.classList.remove("closed");
            chevron[0].style.display = "none";
            chevron[1].style.display = "inline-block";
            badge[0].style.display = "none";
        } else {
            this.classList.add("closed");
            this.classList.remove("open");
            parent.classList.add("closed");
            parent.classList.remove("open");
            panel.classList.add("closed");
            panel.classList.remove("open");
            chevron[0].style.display = "inline-block";
            chevron[1].style.display = "none";
            badge[0].style.display = "inline-block";
        }
    }
}
function setupAddProgramSection() {
    (function () {
        var level = '#LevelId';
        var program = '#ProgramCode';
        var coordinator = '#CoordinatorId';
        var addProgramButton = '#report-filter-submit';
        function testAllValuesForSelects() {
            if ($(level).val() && $(program).val() && $(coordinator).val()) {
                $(addProgramButton).removeAttr('disabled');
            } else {
                $(addProgramButton).attr('disabled', 'disabled');
            }
        }

        $(level).change(function () {
            testAllValuesForSelects();
        });

        $(program).change(function () {
            testAllValuesForSelects();
        });

        $(coordinator).change(function () {
            testAllValuesForSelects();
        });

    })();
}

