﻿function openIncomplete(element) {
    var row = element.nextElementSibling;
    var chevron = element.childNodes[1].childNodes[1];
    if (row.classList.contains("hidden")) {
        row.classList.remove("hidden");
        row.classList.add("visible");
        chevron.classList.remove("fa-chevron-right");
        chevron.classList.add("fa-chevron-down");
    } else {
        row.classList.remove("visible");
        row.classList.add("hidden");
        chevron.classList.add("fa-chevron-right");
        chevron.classList.remove("fa-chevron-down");
    }
}
