﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FTCS.Services;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace FTCS
{
	public partial class StartUp
	{
	    public void ConfigureAuth(IAppBuilder app)
	    {
           

            // Add our custom managers
            app.CreatePerOwinContext<FTCSUserManager>(FTCSUserManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                LogoutPath = new PathString("/Account/Logout"),
                ExpireTimeSpan = TimeSpan.FromMinutes(30),
            
            });
            
            
            
            app.CreatePerOwinContext<LoginManager>(LoginManager.Create);
	        
	    }
	}
}