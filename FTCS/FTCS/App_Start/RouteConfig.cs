﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FTCS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "Academic",
                url: "Faculty/FacultyProfile/{department}/{id}",
                defaults: new { controller = "Faculty", action = "FacultyProfile", id = UrlParameter.Optional}
                );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );

            routes.MapRoute(
               name: "Undergraduate",
               url: "Reports/CourseSectionQualificationsPercentageReport/Undergraduate",
               defaults: new { controller = "Reports", action = "Undergraduate" }
               );

            routes.MapRoute(
               name: "Graduate",
               url: "Reports/CourseSectionQualificationsPercentageReport/Graduate",
               defaults: new { controller = "Reports", action = "Graduate" }
               );

        }
    }
}