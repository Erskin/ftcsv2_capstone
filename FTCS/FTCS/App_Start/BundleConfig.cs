﻿using System.Web;
using System.Web.Optimization;

namespace FTCS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                         "~/Scripts/jquery.unobtrusive*"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/ftcs-scripts").IncludeDirectory("~/Scripts/FTCS", "*.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/fonts.css",
                      "~/Content/bootstrap.css",
                      "~/Content/GuidelinesForFacultyCredentials.css",
                      "~/Content/PagedList.css",
                      "~/Content/ftcs-table.css",
                      "~/Content/ftcs-pagination.css",
                      "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/bundles/ftcs-scripts").Include(
                      "~/Scripts/FTCS/ajax-success-message.js",
                      "~/Scripts/FTCS/navbar.js",
                      "~/Scripts/FTCS/facultyprofile.js",
                      "~/Scripts/FTCS/report-filter-bar.js",                      
                      "~/Scripts/FTCS/program-of-study.js",
                      "~/Scripts/FTCS/auto-submit.js"                   
                      ));
        }
    }
}
