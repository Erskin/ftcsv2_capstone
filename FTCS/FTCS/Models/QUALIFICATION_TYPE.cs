//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FTCS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class QUALIFICATION_TYPE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QUALIFICATION_TYPE()
        {
            this.FACULTY_CONCENTRATION_QUALIFICATION = new HashSet<FACULTY_CONCENTRATION_QUALIFICATION>();
            this.FACULTY_PROGRAM_QUALIFICATION = new HashSet<FACULTY_PROGRAM_QUALIFICATION>();
            this.TEACHER_COURSE_QUALIFICATION = new HashSet<TEACHER_COURSE_QUALIFICATION>();
        }
    
        public int ID { get; set; }
        public string NAME { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FACULTY_CONCENTRATION_QUALIFICATION> FACULTY_CONCENTRATION_QUALIFICATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FACULTY_PROGRAM_QUALIFICATION> FACULTY_PROGRAM_QUALIFICATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TEACHER_COURSE_QUALIFICATION> TEACHER_COURSE_QUALIFICATION { get; set; }
    }
}
