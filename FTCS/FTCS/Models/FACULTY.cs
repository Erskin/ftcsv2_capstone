//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FTCS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FACULTY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FACULTY()
        {
            this.FACULTY_CONCENTRATION_QUALIFICATION = new HashSet<FACULTY_CONCENTRATION_QUALIFICATION>();
            this.FACULTY_PROGRAM_QUALIFICATION = new HashSet<FACULTY_PROGRAM_QUALIFICATION>();
            this.GRADUATE_ASSOCIATE = new HashSet<GRADUATE_ASSOCIATE>();
        }
    
        public string BANNER_ID { get; set; }
        public int CURRENT_RANK { get; set; }
        public int CURRENT_RANK_MONTH { get; set; }
        public int CURRENT_RANK_YEAR { get; set; }
        public Nullable<int> NEXT_RANK { get; set; }
        public Nullable<int> NEXT_RANK_MONTH { get; set; }
        public Nullable<int> NEXT_RANK_YEAR { get; set; }
        public string TENURE_STATUS { get; set; }
        public string GRADUATE_FACULTY_STATUS { get; set; }
    
        public virtual TEACHER TEACHER { get; set; }
        public virtual RANK_TYPE RANK_TYPE { get; set; }
        public virtual GRADUATE_FACULTY_STATUS_TYPE GRADUATE_FACULTY_STATUS_TYPE { get; set; }
        public virtual RANK_TYPE RANK_TYPE1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FACULTY_CONCENTRATION_QUALIFICATION> FACULTY_CONCENTRATION_QUALIFICATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FACULTY_PROGRAM_QUALIFICATION> FACULTY_PROGRAM_QUALIFICATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GRADUATE_ASSOCIATE> GRADUATE_ASSOCIATE { get; set; }
    }
}
