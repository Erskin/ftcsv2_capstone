﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class ViewAllDepartmentPage
    {
        private const string Url = "http://localhost:8080/Department";

        public static string StartsWithMessage
        {
            get { return Chrome.GetFilterStartsWithMessage(); }
        }

        public static IEnumerable<string> ListOfDepartmentName { get { return DepartmentChrome.ListOfDepartmentName; } }

        public static void Quit()
        {
            DepartmentChrome.Quit();
        }

        public static void Initialize()
        {
            DepartmentChrome.Create();
        }

        public static string GetPageTitle()
        {
            return Chrome.pageTitle;
        }

        public static void GoToPage()
        {
            DepartmentChrome.GoTo(Url);
        }

        public static void ClickLetter(string letter)
        {
            DepartmentChrome.ClickLetter(letter);
        }

        public static void ClickAccountingLink()
        {
            Chrome.ClickAccountingLink();
        }

        public static void EnterSearchText(string search)
        {
            DepartmentChrome.FilterSearchBox = search;
        }

        public static string GetFirstDepartmentName()
        {
            return DepartmentChrome.GetFirstDepartmentName();
        }

        public static void ClickSearchButton()
        {
            DepartmentChrome.ClickFilterSearchButton();
        }

        public static void WaitForPageLoad()
        {
            DepartmentChrome.WaitForPageToLoad();
        }

        public static bool IconForDepartmentHeader()
        {
           return DepartmentChrome.GetDefaultDepartmentHeaderIconIsDisplayed();
        }

        public static bool IconForDepartmentHeaderDescending()
        {
            return DepartmentChrome.GetDefaultDepartmentHeaderDescendingIconIsDisplayed();
        }

        public static void ClickTableHeader(int v)
        {
            DepartmentChrome.ClickColumnHeader(v).Click();
        }
        public static IEnumerable<int> GetColumnData(int columnNumber)
        {
            return DepartmentChrome.ColumnData(columnNumber);
        }

        public static void ClickDepartmentHeader()
        {
            DepartmentChrome.ClickDepartmentHeader();
        }
    }
}
