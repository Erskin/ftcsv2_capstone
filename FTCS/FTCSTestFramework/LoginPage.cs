﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public class LoginPage
    {
        private const string Url = "http://localhost:8080/Account/Login";

        public static void Initialize()
        {
            LoginChrome.Create();
        }

        public static void Wait()
        {
            LoginChrome.WaitForPageToLoad();
        }

        public static void GoToPage()
        {
            LoginChrome.GoTo(Url);
        }

        public static void EnterUsername(string username)
        {
            LoginChrome.EnterUsername(username);
        }

        public static void Quit()
        {
            LoginChrome.Quit();
        }

        public static void EnterPassword(string password)
        {
            LoginChrome.EnterPassword(password);
        }

        public static void ClickLogin()
        {
            LoginChrome.ClickLogin();
        }

        public static string GetErrorMessage()
        {
            return LoginChrome.GetErrorMessage();
        }

        public static string GetAccountName()
        {
            return LoginChrome.GetAccountName();
        }

        public static string GetUserError()
        {
            return LoginChrome.GetUserError();
        }

        public static string GetPasswordError()
        {
            return LoginChrome.GetPasswordError();
        }
    }
}
