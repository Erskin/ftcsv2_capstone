﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class ViewCourseSectionQualificationPercentageReport
    {
        private const String ReportUrl = "http://localhost:8080/Reports/CourseSectionQualificationPercentageReport";
        private const String HomeUrl = "http://localhost:8080/";

        public static void Quit()
        {
            CourseSectionQualificationPercentageReportChrome.Quit();
        }
        public static void Initialize()
        {
            CourseSectionQualificationPercentageReportChrome.Create();
        }
        public static void Wait()
        {
            CourseSectionQualificationPercentageReportChrome.WaitForPageToLoad();
        }
        public static void GoToPage(string url)
        {
            if (url == "home")
            {
                CourseSectionQualificationPercentageReportChrome.GoTo(HomeUrl);
            }
            else
            {
                CourseSectionQualificationPercentageReportChrome.GoTo(ReportUrl);
            }
        }
        public static void HoverOnTheReports()
        {
            CourseSectionQualificationPercentageReportChrome.HoverOnTheReports();
        }
        public static void ClickCourseSectionQualificationPercentageReportButton()
        {
            CourseSectionQualificationPercentageReportChrome.ClickCourseSectionQualificationPercentageReportButton();
        }
        public static void ClickGraduateTab()
        {
            CourseSectionQualificationPercentageReportChrome.ClickGraduateTab();
        }
        public static void ClickStartYearButton()
        {
            CourseSectionQualificationPercentageReportChrome.ClickStartYearButton();
        }
        public static void Click2015Button()
        {
            CourseSectionQualificationPercentageReportChrome.Click2015Button();
        }
        public static void HoverOverAcademicYearDropdown()
        {
            CourseSectionQualificationPercentageReportChrome.HoverOverAcademicYearDropdown();
        }
        public static string CourseSectionQualificationPercentageReportTitle
        {
            get { return CourseSectionQualificationPercentageReportChrome.CourseSectionQualificationPercentageReportTitle; }
        }
        public static string GraduateDepartment
        {
            get
            {
                return CourseSectionQualificationPercentageReportChrome.GraduateDepartment;
            }
        }
        public static string GraduateTotalCourseSection
        {
            get
            {
                return CourseSectionQualificationPercentageReportChrome.GraduateTotalCourseSection;
            }
        }
        public static string GraduateTotalCourseSectionsPercentage
        {
            get
            {
                return CourseSectionQualificationPercentageReportChrome.GraduateTotalCourseSectionsPercentage;
            }
        }
        public static string GraduateNumberAcademicallyQualified
        {
            get
            {
                return CourseSectionQualificationPercentageReportChrome.GraduateNumberAcademiccallyQualified;
            }
        }
        public static string GraduateNumberAcademicallyQualifiedPercentage
        {
            get
            {
                return CourseSectionQualificationPercentageReportChrome.GraduateNumberAcademicallyQualifiedPercentage;
            }
        }
        public static string GraduateNumberProfessionallyQualified
        {
            get
            {
                return CourseSectionQualificationPercentageReportChrome.GraduateNumberProfessionallyQualified;
            }
        }
        public static string GraduateNumberProfessionallyQualifiedPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.GraduateNumberProfessionallyQualifiedPercentage; }
        }
        public static string GraduateNumberGraduateAssistants
        {
            get { return CourseSectionQualificationPercentageReportChrome.GraduateNumberGraduateAssistants; }
        }
        public static string GraduateNumberGraduateAssistantsPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.GraduateNumberGraduateAssistantsPercentage; }
        }
        public static string GraduateMissingTranscripts
        {
            get { return CourseSectionQualificationPercentageReportChrome.GraduateMissingTranscripts; }
        }
        public static string GraduateMissingTranscriptsPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.GraduateMissingTranscriptsPercentage; }
        }
        public static string UndergraduateDepartment
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateDepartment; }
        }
        public static string UndergraduateTotalCourseSection
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateTotalCourseSection; }
        }
        public static string UndergraduateTotalCourseSectionPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateTotalCourseSectionPercentage; }
        }
        public static string UndergraduateNumberAcademicallyQualified
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateNumberAcademicallyQualified; }
        }
        public static string UndergraduateNumberAcademicallyQualifiedPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateNumberAcademicallyQualifiedPercentage; }
        }
        public static string UndergraduateNumberProfessionallyQualified
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateNumberProfessionallyQualified; }
        }
        public static string UndergraduateNumberProfessionallyQualifiedPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateNumberProfessionallyQualifiedPercentage; }
        }
        public static string UndergraduateNumberGraduateAssistants
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateNumberGraduateAssistants; }
        }
        public static string UndergraduateNumberGraduateAssistantsPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateNumberGraduateAssistantsPercentage; }
        }
        public static string UndergraduateMissingTranscripts
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateMissingTranscripts; }
        }
        public static string UndergraduateMissingTranscriptsPercentage
        {
            get { return CourseSectionQualificationPercentageReportChrome.UndergraduateMissingTranscriptsPercentage; }
        }
        public static string StartYear
        {
            get { return CourseSectionQualificationPercentageReportChrome.StartYear; }
        }
    }
}