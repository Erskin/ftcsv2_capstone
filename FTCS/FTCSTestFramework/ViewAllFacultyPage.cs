﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class ViewAllFacultyPage
    {
        private const string Url = "http://localhost:8080/Faculty";

        public static void Quit()
        {
            ViewAllFacultyChrome.Quit();
        }

        public static IEnumerable<string> DepartmentColumnValues
        {
            get
            {
                return 
                    ViewAllFacultyChrome.GetDepartmentNamesOnFacultyPage();
            }
        }

        public static void ClickFaculty(string faculty)
        {
            ViewAllFacultyChrome.ClickFaculty(faculty); ;
        }

        public static IEnumerable<string> ListOfFacultyName { get { return ViewAllFacultyChrome.ListOfFacultyName; } }

        public static IEnumerable<string> ListOfFacultyDepartmentName { get { return ViewAllFacultyChrome.ListOfFacultyDepartmentName; } }

        public static string GetPageTitle()
        {
            return ViewAllFacultyChrome.pageTitle;
        }

        public static void Initialize()
        {
            ViewAllFacultyChrome.Create();
        }

        public static void GoToPage()
        {
            ViewAllFacultyChrome.GoTo(Url);
        }

        public static void EnterSearchBoxFaculty(string search)
        {
            ViewAllFacultyChrome.FilterSearchBox = search;
        }

        public static void ClickLetterFaculty(string letter)
        {
            ViewAllFacultyChrome.ClickLetter(letter);
        }

        public static void ClickSearchButtonFaculty()
        {
            ViewAllFacultyChrome.ClickFilterSearchButton();
        }

        public static void WaitForPageToLoad()
        {
            ViewAllFacultyChrome.WaitForPageToLoad();
        }

        public static object GetFirstFacultyName()
        {
            return ViewAllFacultyChrome.GetFirstFacultyName();
        }

        public static object GetFacultyNamesOnFacultyPage()
        {
            return ViewAllFacultyChrome.GetFacultyNamesOnFacultyPage();
        }

        public static void ClickTableHeader(int v)
        {
            ViewAllFacultyChrome.ClickColumnHeader(v).Click();
        }

        public static string GetFacultyByName(string faculty)
        {
            return ViewAllFacultyChrome.GetFacultyByName(faculty);
        }

        }
}
