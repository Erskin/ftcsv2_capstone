﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class ViewAllCoursePage
    {
        private const string Url = "http://localhost:8080/Course";

        public static void Quit()
        {
            ViewAllCourseChrome.Quit();
        }

        public static void Wait()
        {
            ViewAllCourseChrome.WaitForPageToLoad();
        }

        public static void Initialize()
        {
            ViewAllCourseChrome.Create();
        }

        public static void GoToPage()
        {
            ViewAllCourseChrome.GoTo(Url);
        }

        public static void ClickLetterCourse(string letter)
        {
            ViewAllCourseChrome.ClickLetterCourse(letter);
        }

        public static void EnterSearchBoxCourse(string search)
        {
            ViewAllCourseChrome.ClickEnterSearchBox = search;
        }

        public static void ClickSearchButtonCourse()
        {
            ViewAllCourseChrome.ClickCourseSearchButton();
        }

       
        public static string GetCourseByName(string course)
        {
           return ViewAllCourseChrome.GetCourseByName(course);
        }

        public static void ClickCourseNameHeader()
        {
            ViewAllCourseChrome.ClickCourseNameHeader();
        }
    }
}
