﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class FacultyProfilePage
    { 
        private const string LakenGraceUrl = "http://localhost:8080/Faculty/FacultyProfile/ARTA/E00021575";

        public static void Quit()
        {
            FacultyProfileChrome.FPQuit();
        }

        public static void Wait()
        {
            FacultyProfileChrome.WaitForPageToLoad();
        }

        private const string GregoryLoveUrl = "http://localhost:8080/Faculty/FacultyProfile/Qualifications/Academic/CHEM/E00022810";
        private const string BarrettURL = "http://localhost:8080/Faculty/FacultyProfile/Qualifications/Academic/CSCI/E00007222";

        public static string FacultyName(string facultyName)
        {
            return FacultyProfileChrome.FacultyName(facultyName);
        }

        public static void Initialize()
        {
            FacultyProfileChrome.Create();
        }
        public static void FacultyProfileQuit()
        {
            FacultyProfileChrome.FPQuit();
        }
        public static void GoToPage(string urlDirector)
        {
            if(urlDirector == "Bridges, Laken Grace")
            {
                FacultyProfileChrome.GoTo(LakenGraceUrl);
            } else if(urlDirector == "Barrett, Martin L.")
            {
                FacultyProfileChrome.GoTo(BarrettURL);
            } else
            {
                FacultyProfileChrome.GoTo(GregoryLoveUrl);
            }
        }
        public static class ProfessionalQualifications {
            public static void ClickProfessionalQualificationsOption(string professionalQualifications)
            {
                FacultyProfileChrome.ProfessionalQualificationsChrome.ClickProfessionalQualifications(professionalQualifications);
            }

            public static string Narrative
            {
                get { return FacultyProfileChrome.ProfessionalQualificationsChrome.Narrative; }
            }

            public static string RelatedWorkExperienceTitle
            {
                get { return FacultyProfileChrome.ProfessionalQualificationsChrome.RelatedWorkExperienceTitle; }
            }

            public static string RelatedWorkExperiencePlace
            {
                get { return FacultyProfileChrome.ProfessionalQualificationsChrome.RelatedWorkExperiencePlace; }
            }

            public static string RelatedWorkExperiencePosition
            {
                get { return FacultyProfileChrome.ProfessionalQualificationsChrome.RelatedWorkExperiencePosition; }
            }

            public static string RelatedWorkExperienceResponsibilities
            {
                get { return FacultyProfileChrome.ProfessionalQualificationsChrome.RelatedWorkExperienceResponsibilities; }
            }

            public static string RelatedWorkExperienceStartDate
            {
                get { return FacultyProfileChrome.ProfessionalQualificationsChrome.RelatedWorkExperienceStartDate; }
            }

            public static string RelatedWorkExperienceEndDate
            {
                get { return FacultyProfileChrome.ProfessionalQualificationsChrome.RelatedWorkExperienceEndDate; }
            }

            public static object Description { get; set; }
            public static object RelatedWorkProfessionalExperience { get; set; }
        }

        public static class CoursesTaught
        {
            public static void ClickCoursesTaughtOption(string coursesTaughtOption)
            {
                FacultyProfileChrome.CoursesTaughtChrome.ClickCoursesTaughtOption(coursesTaughtOption);
            }
            public static void ClickAllCoursesTaughtButton(string allCoursesTaughtButton)
            {
                FacultyProfileChrome.CoursesTaughtChrome.ClickAllCoursesTaughtButton(allCoursesTaughtButton);
            }
            public static string CoursesTaughtTableTitle
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.CoursesTaughtTableTitle; }
            }
            public static string CourseID
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.CourseID; }
            }
            public static string CourseTitle
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.CourseTitle; }
            }
            public static string CourseDescription
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.CourseDescription; }
            }
            public static string CourseSemester
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.CourseSemester; }
            }
            public static string CourseCertification
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.CourseCertification; }
            }
            public static string AllCoursesTaughtButtonText
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.AllCoursesTaughtButtonText; }
            }
            public static string AllCoursesTaughtCourseID
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.AllCoursesTaughtCourseID; }
            }
            public static string AllCoursesTaughtCourseTitle
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.AllCoursesTaughtCourseTitle; }
            }
            public static string AllCoursesTaughtCourseTerm
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.AllCoursesTaughtCourseTerm; }
            }
            public static string AllCoursesTaughtCourseCertification
            {
                get { return FacultyProfileChrome.CoursesTaughtChrome.AllCoursesTaughtCourseCertification; }
            }

            public static object ClickCourseHistoryLink { get; set; }
        }

        public static class AcademicQualifications
        {
            public static void ClickAcademicQualifications(string AcademicQualificationsOption)
            {
                FacultyProfileChrome.AcademicQualification.ClickAcademicQualificationOption(AcademicQualificationsOption);
            }
            public static string Degree
            {
                get { return FacultyProfileChrome.AcademicQualification.Degree; }
            }

            public static string DegreeInitial
            {
                get { return FacultyProfileChrome.AcademicQualification.DegreeInitial; }
            }

            public static string Discipline
            {
                get { return FacultyProfileChrome.AcademicQualification.Discipline; }
            }

            public static string Institution
            {
                get { return FacultyProfileChrome.AcademicQualification.Institution; }
            }

            public static string Transcript
            {
                get { return FacultyProfileChrome.AcademicQualification.Transcript; }
            }

            public static string FacultyName
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyName; }
            }

            public static string FacultyTitle
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyTitle; }
            }

            public static string Email
            {
                get { return FacultyProfileChrome.AcademicQualification.Email; }
            }

            public static string FacultyRank
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyRank; }
            }

            public static string FacultyDateOfRank
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyDateOfRank; }
            }

            public static string FacultyEligibleRankDate
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyEligibleRankDate; }
            }

            public static string FacultyGraduateStatus
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyGraduateStatus; }
            }

            public static string FacultyEmploymentStatus
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyEmploymentStatus; }
            }

            public static string FacultyEmploymentCategory
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyEmploymentCategory; }
            }

            public static string Years
            {
                get { return FacultyProfileChrome.AcademicQualification.Years; }
            }

            public static string FacultyTenureStatus
            {
                get { return FacultyProfileChrome.AcademicQualification.FacultyTenureStatus; }
            }
        }

        public static class CoordinatorInformation
        {
            public static void ClickCoordinatorInformationOption(string coordinatorInformationOption)
            {
                FacultyProfileChrome.CoordinatorInformationChrome.ClickCoordinatorInformationOption(coordinatorInformationOption);
            }
            public static string DegreeName
            {
                get { return FacultyProfileChrome.CoordinatorInformationChrome.DegreeName; }
            }
            public static string Level
            {
                get { return FacultyProfileChrome.CoordinatorInformationChrome.Level; }
            }
            public static string Degree
            {
                get { return FacultyProfileChrome.CoordinatorInformationChrome.Degree; }
            }
            public static string HowQualified
            {
                get { return FacultyProfileChrome.CoordinatorInformationChrome.HowQualified; }
            }
            public static string StartDate
            {
                get { return FacultyProfileChrome.CoordinatorInformationChrome.StartDate; }
            }
            public static string EndDate
            {
                get { return FacultyProfileChrome.CoordinatorInformationChrome.EndDate; }
            }
        }
    }
}
