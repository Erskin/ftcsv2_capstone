﻿using ChromeWebDriver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class HomePage
    {
        private const string Url = "http://localhost:8080/";

        public static void Initialize()
        {
            Chrome.Create();
        }

        public static void Wait()
        {
            Chrome.WaitForPageToLoad();
        }

        public static void Quit()
        {
            Chrome.Quit();
        }
        public static IList<string> BrowserTabs
            
        {
            get
            {
                return Chrome.BrowserTabs().ToList();
            }
        }

        public static void HoverOnReports()
        {
            Chrome.HoverOnReports();
        }

        public static string GetPageTitle
        {
            get { return Chrome.pageTitle; }
        }

        public static void GoToPage()
        {
            Chrome.GoTo(Url);
        }

        public static string GetAccountName()
        {
            return Chrome.GetAccountName();
        }

        public static class Home
        {
            public static string Text
            {
                get { return Chrome.Text; }
            }

            public static string ETSUMission
            {
                get { return Chrome.ETSUMission; }
            }

            public static string TeachingCredentialStandard
            {
                get { return Chrome.TeachingCredentialStandard; }
            }

            public static string GraduateFacultyAppointmentGuidelines
            {
                get { return Chrome.GraduateFacultyAppointmentGuidelines; }
            }

            public static string SACSGuidelines
            {
                get { return Chrome.SACSGuidelines; }
            }

            public static string ChairInstructions
            {
                get { return Chrome.ChairInstructions; }
            }

            public static void SwitchToLastTab()
            {
                Chrome.SwitchToLastTab();
            }

            public static class ClickLink
            {
                public static void ClickLinkToAllDepartmentPage()
                {
                    Chrome.ClickLinkToAllDepartmentPage();
                }

                public static void ClickLinkToAllCoursePage()
                {
                    Chrome.ClickLinkToAllCoursePage();
                }

                public static void ClinkLinkToAllFacultyPage()
                {
                    Chrome.ClickLinkToAllFacultyPage();
                }

                public static void ClickLinkToCourseSectionQualificationPercentageReport()
                {
                    Chrome.ClickLinkToCourseSectionQualificationPercentageReport();
                }

                public static void ClickLinkToFacultyCredentialReport()
                {
                    Chrome.ClickLinkToFacultyCredentialReport();
                }

                public static void ClickLinkToFacultyRosterReport()
                {
                    Chrome.ClickLinkToFacultyRosterReport();
                }

                public static void ClickLinkToGraduateAssistantRosterReport()
                {
                    Chrome.ClickLinkToGraduateAssistantRosterReport();
                }

                public static void ClickETSUMissionButton()
                {
                    Chrome.ClickETSUMissionButton();
                }

                public static void ClickTeachingCredentialStandardsButton()
                {
                    Chrome.ClickTeachingCredentialStandardsButton();
                }

                public static void ClickSACSGuidelinesButton()
                {
                    Chrome.ClickSACSGuidelinesButton();
                }

                public static void ClickChairInstructionsButton()
                {
                    Chrome.ClickChairInstructionsButton();
                }

                public static void ClickGraduateFacultyAppointmentGuidelinesButton()
                {
                    Chrome.ClickGraduateFacultyAppointmentGuidelinesButton();
                }

                public static void ClickSearchFacultyNavButton()
                {
                    Chrome.ClickSearchFacultyNavButton();
                }

                public static void HoverOnSearchBy()
                {
                    Chrome.HoverOnSearchBy();
                }
            }
        }
        public static class NavigationBar
        {
            public static void ClickHome()
            {
                Chrome.NavigationBar.ClickHome();
            }
            public static void HoverOnSearchForFaculty()
            {
                Chrome.NavigationBar.HoverOnSearchForFaculty();
            }
            public static void ClickCourseSectionQualificationsPercentageReport()
            {
                Chrome.NavigationBar.ClickCourseSectionQualificationsPercentageReport();
            }
            public static void ClickFacultyCredentialsReport()
            {
                Chrome.NavigationBar.ClickFacultyCredentialsReport();
            }
            public static void ClickFacultyRosterReport()
            {
                Chrome.NavigationBar.ClickFacultyRosterReport();
            }
            public static void ClickGraduateAssistantRosterReport()
            {
                Chrome.NavigationBar.ClickGraduateAssistantRosterReport();
            }
            public static void ClickContactUs()
            {
                Chrome.NavigationBar.ClickContactUs();
            }
            public static void ClickByDepartment()
            {
                Chrome.NavigationBar.ClickByDepartment();
            }
            public static void ClickByName()
            {
                Chrome.NavigationBar.ClickByName();
            }
            public static void ClickByCourse()
            {
                Chrome.NavigationBar.ClickByCourse();
            }
            public static string ReportTitle(string reportTitle)
            {
                if(reportTitle == "Course Section Qualifications Percentage Report")
                {
                    return Chrome.NavigationBar.CourseSectionQualificationsPercentageReportTitle;
                } else if(reportTitle == "Faculty Credentials Report")
                {
                    return Chrome.NavigationBar.FacultyCredentialsReportTitle;
                } else if(reportTitle == "Faculty Roster Report")
                {
                    return Chrome.NavigationBar.FacultyRosterReportTitle;
                }else
                {
                    return Chrome.NavigationBar.GraduateAssistantRosterReportTitle;
                }
            }
            public static string SearchTitle(string search)
            {
                if(search == "By Department")
                {
                    return Chrome.NavigationBar.ByDepartmentTitle;
                } else if(search == "By Course")
                {
                    return Chrome.NavigationBar.ByCourseTitle;
                }
                else
                {
                    return Chrome.NavigationBar.ByNameTitle;
                }
            }
            public static string Home
            {
                get { return Chrome.NavigationBar.Home; }
            }
            public static string ContactUsText
            {
                get { return Chrome.NavigationBar.ContactUsText; }
            }
        }
    }
}
