﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChromeWebDriver;

namespace FTCSTestFramework
{
    public static class ViewFacultyCredentialsReportPage
    {
        private const String Url = "http://localhost:8080/Reports/FacultyCredentialsReport";
       // private const String HomeUrl = "http://localhost:44300/";

        public static void Quit()
        {
            FacultyCredentialsReportChrome.Quit();
        }

        public static void Initialize()
        {
            FacultyCredentialsReportChrome.Create();
        }
        public static void GoToPage()
        {
            FacultyCredentialsReportChrome.GoTo(Url);
        }
        public static void Wait()
        {
            FacultyCredentialsReportChrome.WaitForPageToLoad();
        }
        public static string GetBackgroundColor()
        {
            var result = FacultyCredentialsReportChrome.GetBackgroundColor();
            return result;
        }
        public static void WaitForPageToLoad()
        {
            FacultyCredentialsReportChrome.WaitForPageToLoad();
        }

        public static void SelectADepartment(string department)
        {
            FacultyCredentialsReportChrome.ClickDepartMenu();
            FacultyCredentialsReportChrome.SelectDepartment(department);
            FacultyCredentialsReportChrome.WaitForPageToLoad();
        }
        public static string FacultyCredentialsPDF
        {
            get { return FacultyCredentialsReportChrome.FacultyCredentialsPDF; }
        }

        public static void ClickSubmit()
        {
            FacultyCredentialsReportChrome.ClickSubmitButton();
        }

        public static void SelectASemester(string semester)
        {
            FacultyCredentialsReportChrome.ClickSemester();
            FacultyCredentialsReportChrome.SelectSemester(semester);
            FacultyCredentialsReportChrome.WaitForPageToLoad();
        }

        public static void ClickPDFButton()
        {
            FacultyCredentialsReportChrome.ClickPDFButton();
        }

        public static string FindTeacher(string teacher, string course)
        {
            var result = FacultyCredentialsReportChrome.FindTeacher(teacher, course);
            return result;
        }
    }
}
