﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class ViewGradAssistantPage
    {
        private const String ReportUrl = "http://localhost:8080/Reports/GradAssistantRoster";
        private const String HomeUrl = "http://localhost:8080/";

        public static void Initialize()
        {
            GraduateAssistantRosterReportChrome.Create();
        }
        public static void Quit()
        {
            GraduateAssistantRosterReportChrome.Quit();
        }
        public static void Wait()
        {
            GraduateAssistantRosterReportChrome.WaitForPageToLoad();
        }

        public static void GoToPage(string url)
        {
            if (url == "home")
            {
                GraduateAssistantRosterReportChrome.GoTo(HomeUrl);
            }
            else
            {
                GraduateAssistantRosterReportChrome.GoTo(ReportUrl);
            }
        }
        public static void ClickGraduateAssistantRosterReportButton()
        {
            GraduateAssistantRosterReportChrome.ClickGraduateAssistantRosterReport();
        }

        public static void HoverOnTheReports()
        {
            GraduateAssistantRosterReportChrome.HoverOnTheReports();
        }      

        public static string GraduateAssistantRosterReportTitle
        {
            get { return GraduateAssistantRosterReportChrome.GraduateAssistantRosterReportTitle; }
        }

        public static void clickSubmit()
        {
            GraduateAssistantRosterReportChrome.clickSubmit();
        }

        public static string ViewFaculty(string faculty)
        {
            var result=GraduateAssistantRosterReportChrome.ViewFaculty(faculty);
            return result;
        }

        public static string ViewDepartment(string department)
        {
            var result = GraduateAssistantRosterReportChrome.ViewDepartment(department);
            return result;
        }

        public static void SelectYear(string year)
        {
            GraduateAssistantRosterReportChrome.ClickYearMenu();
            GraduateAssistantRosterReportChrome.SelectYear(year);
            GraduateAssistantRosterReportChrome.WaitForPageToLoad();
        }
        public static string ViewCourse(string course)
        {
            var result = GraduateAssistantRosterReportChrome.ViewCourse(course);
            return result;
        }

        public static string ViewDegree(string degree)
        {
            var result = GraduateAssistantRosterReportChrome.ViewDegree(degree);
            return result;
        }

        public static string ViewQualification(string qualification)
        {
            var result = GraduateAssistantRosterReportChrome.ViewQualification(qualification);
            return result;
        }
    }
}
