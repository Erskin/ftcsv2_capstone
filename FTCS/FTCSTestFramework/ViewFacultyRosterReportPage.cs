﻿using ChromeWebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTCSTestFramework
{
    public static class ViewFacultyRosterReportPage
    {
        private const String Url = "http://localhost:8080/Reports/FacultyRoster";        

        public static void Quit()
        {
            FacultyRosterReportChrome.Quit();
        }
        public static void Wait()
        {
            FacultyRosterReportChrome.WaitForPageToLoad();
        }
        public static void Initialize()
        {
            FacultyRosterReportChrome.Create();
        }
        public static void GoToPage()
        {
            FacultyRosterReportChrome.GoTo(Url);
        }        

        public static void ClickFacultyRosterLink()
        {
            FacultyRosterReportChrome.ClickFacultyRosterLink();
        }

        public static object FindTeacherQualification(string teacher, string qualification)
        {
            var result = FacultyRosterReportChrome.FindTeacherQualification(teacher, qualification);
            return result;
        }

        public static void SelectAYear(string year)
        {
            FacultyRosterReportChrome.ClickYearMenu();
            FacultyRosterReportChrome.SelectYear(year);
            FacultyRosterReportChrome.WaitForPageToLoad();
        }

        public static void ClickPDFButton()
        {
            FacultyRosterReportChrome.ClickPDFButton();
        }

        public static object FindTeacherCourse(string teacher, string course)
        {
            var result = FacultyRosterReportChrome.FindTeacherCourse(teacher, course);
            return result;
        }
        public static object FindTeacherDegree(string teacher, string degree)
        {
            var result = FacultyRosterReportChrome.FindTeacherDegree(teacher, degree);
            return result;
        }
        public static void ClickSubmit()
        {
            FacultyRosterReportChrome.ClickSubmitButton();
        }

        public static void SelectADepartment(string department)
        {
            FacultyRosterReportChrome.ClickDepartmentMenu();
            FacultyRosterReportChrome.SelectDepartment(department);
            FacultyRosterReportChrome.WaitForPageToLoad();
        }

        public static void SelectAType(string type)
        {
            FacultyRosterReportChrome.ClickFacultyTypeMenu();
            FacultyRosterReportChrome.SelectFacultyType(type);
            FacultyRosterReportChrome.WaitForPageToLoad();
        }

        public static bool FacultyRoster()
        {
            throw new NotImplementedException();
        }

        
    }
}
